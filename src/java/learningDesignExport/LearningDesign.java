/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package learningDesignExport;

import java.io.FileOutputStream;
import java.util.Vector;
import org.xml.sax.*;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.xml.sax.helpers.AttributesImpl;

/**
 *
 * @author Administrator
 */
public class LearningDesign {
    private String title;
    private Vector components;
    private Vector<Role> roles;
    private ContentHandler hd;
    private FileOutputStream fos;

    public LearningDesign(String filename,String title)throws Exception{
        try{
            roles = new Vector();
            this.title = title;
            fos = new FileOutputStream(filename);
            OutputFormat of = new OutputFormat("XML","ISO-8859-1",true);
            of.setIndent(1);
            of.setIndenting(true);
            //of.setDoctype(null,"users.dtd");
            XMLSerializer serializer = new XMLSerializer(fos,of);
            this.hd = serializer.asContentHandler();


        }
        catch (Exception ex) {
            throw ex;
            //Logger.getLogger(LearningDesign.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addRole(Role r){
        roles.add(r);
    }

    public void generateLD()throws Exception{
        try{
            hd.startDocument();
// Processing instruction sample.
//hd.processingInstruction("xml-stylesheet","type=\"text/xsl\" href=\"users.xsl\"");
// USER attributes.
            AttributesImpl atts = new AttributesImpl();
// USERS tag.
            hd.startElement("","","learning-design",atts);
// USER tags.
            
            for (int i=0;i<roles.size();i++){
                atts.clear();
                atts.addAttribute("","","identifier","CDATA",roles.get(i).getName());
                hd.startElement("","",roles.get(i).getType(),atts);
                hd.endElement("","",roles.get(i).getType());
            }
            hd.endElement("","","learning-design");
            hd.endDocument();
            fos.close();

        }
        catch(Exception e){
            throw e;
        }

    }
    
   
    /*public static void main(String arg[]){
        try{
            LearningDesign a = new LearningDesign("C:/skata.txt");
            a.generateLD("malakia", null);
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }*/
}
