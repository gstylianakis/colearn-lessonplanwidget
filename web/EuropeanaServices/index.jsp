<%--
    Document   : index
    Created on : 10 Μαρ 2011, 5:25:20 μμ
    Author     : manolis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="css/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen"/>
        <script type="text/javascript" src="js/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.tabs.js"></script>
        <script type="text/javascript"  src="js/ui/external/jquery.bgiframe-2.1.2.js"></script>
        <script type="text/javascript"  src="js/ui/jquery.ui.core.js"></script>
        <script type="text/javascript"  src="js/ui/jquery.ui.widget.js"></script>
        <script type="text/javascript"  src="js/ui/jquery.ui.mouse.js"></script>
        <script type="text/javascript"  src="js/ui/jquery.ui.button.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.draggable.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.position.js"></script>
        <script type="text/javascript"  src="js/ui/jquery.ui.resizable.js"></script>
        <script type="text/javascript"  src="js/ui/jquery.ui.dialog.js"></script>
        <script type="text/javascript"  src="js/ui/jquery.effects.core.js"></script>
        <script type="text/javascript" src="js/ui/jquery.paginate.js"></script>
        <style>
            body { font-size: 62.5%; }
            label, input { float:left;}
            input.text { margin-bottom:12px; width:95%; padding: .4em; }
            fieldset { padding:0; border:0; margin-top:25px; }
            h1 { font-size: 1.2em; margin: .6em 0; }
            div#sresults {  margin: 20px 0; }
            div#sresults table { margin: 1em 0; border-collapse: collapse; width: 100%; }
            div#sresults table td, div#sresults table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }

            div#sresultsinner{ margin: 1em 0; border-collapse: collapse; width: 100%; }
            div#sresultsinner td, div#sresultsinner table th { border: 1px solid #eee; padding: .6em 100px; text-align: left; }

            .ui-dialog .ui-state-error { padding: .3em; }
            .validateTips { border: 1px solid transparent; padding: 0.3em; }
            .resultsDiv{ width:96%; background-color:#0CF; margin-top:10px; margin-left:1%;}
            .resultsDiv:hover{cursor:pointer; width:96%; background-color:#F7F7F7; margin-top:10px; margin-left:1%;border: 2px dotted #CD0A0A}

            .resultsTable{ margin-bottom:4px; margin-top:4px;}
            .hideElem{ display: none;}
            .resultsTableT{ border-bottom: 1px dashed #696464;
                            border-left: 1px dashed #696464;}
            .resultsTableDsc{ border-bottom: 1px dashed #696464;
                              border-left: 1px dashed #696464;}
            .resultsTable img{ max-width: 90px;max-height: 90px; }
            .results{font-size: 9px;}


            .demo{
                width:500px;
                padding:10px;
                margin:10px auto;
                border: 1px solid #fff;
                background-color:#f7f7f7;
            }
            #loading
            {
                margin: auto;
                position: absolute;
                /*                background-color: pink;*/
                top: 50%;
                left: 50%;
            }


        </style>
        <script>
            function setPaging(){
                var res=document.getElementById("totalPages");
                var mpage=document.getElementById("pageNum");
                if(!res || res==null){
                    return;
                }
                if(!mpage || res==null){
                    mpage=1;
                }else{
                    mpage=document.getElementById("pageNum");
                }
                var ctr=res.innerHTML;
                if(ctr>0)
                    $("#resultsPaging,#resultsPagingFooter").paginate({
                        count 		: ctr,
                        start 		: mpage.innerHTML,
                        display     : 15,
                        border					: true,
                        border_color			: '#fff',
                        text_color  			: '#fff',
                        background_color    	: 'black',
                        border_hover_color		: '#ccc',
                        text_hover_color  		: '#000',
                        background_hover_color	: '#fff',
                        images: false,
                        mouse: 'press',
                        onChange  : function(page){
                            
                            getData(page,document.getElementById("searchTerm").innerHTML);
                            $('#dialog-form').scrollTop(0);
                            //                            $('#search').focus();
                            //                            $('#results').focus();
                        }
                });

            }

            function divclick(d){
                if(!d){return};
                var data={
                    image:$(d).find('table').find('td')[0].innerHTML
                    ,title:$(d).find('table').find('td')[1].innerHTML
                    ,dsc:$(d).find('table').find('td')[2].innerHTML
                    ,link:$(d).find('table').find('td')[3].innerHTML
                }
                alert(data);
                //                $(d).copy();
            }
            $(function() {
                

                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    height: 500,
                    width: 700,
                    modal: true,
                    buttons: {
                        Cancel: function() {
                            $( this ).dialog( "close" );
                    
                        }
                    }

                });

                $( "#create-user" )
                .button()
                .click(function() {
                    $( "#dialog-form" ).dialog( "open" );
                    //                    setPaging();
                });
                $( "#search" )
                .button()
                .click(function(e) {
                   
                    getData(-1, $('#terms').val());
                    //                    setPaging();
                });
                

            });


            function Display_Load()
            {
                $("#loading").fadeIn(900,0);
                $("#loading").html("<img src='bigLoader.gif'/>");
            }
            //Hide Loading Image
            function Hide_Load()
            {
                $("#loading").fadeOut('slow'); setPaging();
            }
            function Hide_Load_Fast()
            {
                $("#loading").fadeOut(100); setPaging();
            }

            function checkTermV(term){if(term.replace( /\s/g ,"").length<=2){return false;}return true;}
            var errorMsg="<div class='ui-state-error ui-corner-all' style='padding: 0pt 0.7em; margin:5px 0;'><p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span> Unable to fulfill your request due to a malformed query syntax.                            <strong>Please</strong> try another search.                             </p></div>"
            function getData(page,term){
                Display_Load();
                if(!checkTermV(term)){
                    $("#results").html(errorMsg);
                    Hide_Load_Fast();
                    return;}
                
                $("#results").load("search.jsp?page="+encodeURIComponent(page)+"&term="+encodeURIComponent(term),function() {
                    Hide_Load();
                });
              

                
            }
            
        </script>



    </head>
    <body>

        <div class="demo">

            <div id="dialog-form" title="Europeana Search">
                <div>

                    <form action="javascript:return false;">
                        <label for="terms"> Keywords :</label>
                        <input class="text ui-widget-content ui-corner-all"  type="text" name="terms" id="terms" style="width:250px;">
                        <button id="search">Search</button>
                    </form>
                </div><div id="loading" ></div>
                <div id="results" class="ui-widget">
                </div>
            </div>
            <button id="create-user">Search Europeana</button>
        </div>
    </body>
</html> 