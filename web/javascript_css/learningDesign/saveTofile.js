/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var saveToFile = {
    getFilePathFromDialog:function () {
        document.getElementById('fileBrowser').click();
        document.getElementById('filePath').value = document.getElementById('fileBrowser').value;
    },
    /*publishDesign:function(learnerArray,staffArray,allPhases){
        try{
            if(confirm("You are going to publish Learning design")==false){
                alert('action aborted');
                return;
            }
            var xmldoc = learningDesign.generateLD(learnerArray,staffArray,allPhases);
            var resources = Resources.init(0,xmldoc);
            var manifest = xmldoc.getElementsByTagName("manifest")[0];
            manifest.appendChild(resources);            
            Resources.productResourcesFromObjects(allPhases,0);            
            document.getElementById("design").value = ((new XMLSerializer()).serializeToString(xmldoc));
            document.getElementById("teams").value = generateTeamsDoc();
            $("#uol").val(ManifestID);
            $("#title").val($('#lesson_plan_title').val());
            
            window.open('','window','width=400,height=200');
            document.upform.submit();
        }
        catch(e){
            throw e;
        }
    },*/
    saveDesign: function(learnerArray, staffArray, allPhases,option,ods_groups){
        try{
           /* var response = prompt("Most Browsers dont allow folder chooser dialog boxes.\nPlease type the path of the folder in which the design will be saved");
            
            if(response == null || response==""){
                alert('action aborted');
                return;
            }*/

            if(option=='save'){
                if(confirm("Save Collaboration Script Project")==false){
                    //alert('action aborted');
                    return;
                }
            }
            else if(option=='publish'){
                if(confirm("Deploy Collaboration Script Project")==false){
                    //alert('action aborted');
                    return;
                }
            }
            else if(option=="openProject"){
                if(confirm("Save current Collaboration Script Project")==false)
                    return;
            }
            else if(option=='clone'){
                if(confirm("Clone Collaboration Script Project")==false){
                    //alert('action aborted');
                    return;
                }
            }
            Resources.init(1,null);
            Resources.productResourcesFromObjects(allPhases,1);            
            $("#resourcesXML").val(((new XMLSerializer()).serializeToString(Resources.resourcesXmlTmp)));
 
            var xmldoc = learningDesign.generateLD(learnerArray,staffArray,allPhases,"learningDesign");
            var tmpmanifestEl = xmldoc.getElementsByTagName("manifest")[0];

            var resources = Resources.init(0,xmldoc);
            var manifest = xmldoc.getElementsByTagName("manifest")[0];
            manifest.appendChild(resources);
            Resources.productResourcesFromObjects(allPhases,0);
            
            //var graphics = graphicDetails.generateGraphicDetailsDoc(allPhases);
            $("#manifest").val(((new XMLSerializer()).serializeToString(xmldoc)));
            $("#manifestId").val(tmpmanifestEl.getAttribute("identifier"));
            if(option=='clone'){
                var Manifest_id = "manifest-"+uniqid();
                $("#manifestId").val(Manifest_id);
                tmpmanifestEl.setAttribute("identifier",Manifest_id);
            }

            var graphics = learningDesign.generateLD(learnerArray,staffArray,allPhases,"graphicalRepresentation");
            resources = Resources.init(0,graphics);
            manifest = graphics.getElementsByTagName("manifest")[0];
            manifest.appendChild(resources);
            Resources.productResourcesFromObjects(allPhases,0);

//alert(((new XMLSerializer()).serializeToString(graphics)))
            $("#graphics").val(((new XMLSerializer()).serializeToString(graphics)));
            $("#manifest_subject").val($("#lesson_subject").val());
            $("#manifest_title").val($("#lesson_plan_title").val());
            $("#username").val(user);
            if(ODSGroupID === -1)
                $("#ODSGroupId").val(ODSGroupID);
            else{
                var ods_group_ids="";
                var ods_group_ids_num=0;
                var ods_group_ids_num_count = $("#odsGroup_dialog").find("select").find("option").length;
                $("#odsGroup_dialog").find("select").find("option").each(function(){
                    if($(this).attr("disabled")){
                        if(ods_group_ids_num>0 && ods_group_ids_num < ods_group_ids_num_count)
                            ods_group_ids += ",";
                        console.log("this val "+$(this).val());
                        ods_group_ids = ods_group_ids + $(this).val();
                        ods_group_ids_num ++;
                    }
                });
                
                //for(var l=0;l<ods_group_ids.lenght;l++)                    
                if(ods_group_ids!=="")
                    $("#ODSGroupId").val(ods_group_ids);            
                else{
                    $("#ODSGroupId").val(ODSGroupID);            
                    console.log("einai to ods group id "+ODSGroupID);
                }
            
            }
            if(option==='save')
                $("#operation_option").val("save")                            
            
            else if(option==='publish'){
                $("#operation_option").val("publish");
                $("#teams").val(generateTeamsDoc());

                $("#uol").val(ManifestID);
                $("#title").val($('#lesson_plan_title').val());

            }
            window.open('','window','width=400,height=200');
            
            document.saveDesign.submit();
            

        }
        catch(e){
            console.log("error on saving .. "+e)
            alert(e);
            throw e;
        }
    },
    deleteProject:function(manifest_id){
        if(confirm('Are you sure you want to delete Project?')==false){
            return;
        }
        $("#delete_projectId").val(manifest_id);
        $("#user_encoded").val(base64Encode(user));
        document.deleteDesign.submit();
    }
    

    
    /*,
    saveDialogOpen: function(thisDiv){
    try{
	var div = document.createElement("div");
        div.id = "fileSelection";
        thisDiv.parentNode.insertBefore(div,thisDiv)//appendChild(div);
        div.className = "SelectionDropDown";
        var p = document.createElement("p");
        p.textContent = "Most Browsers dont allow folder chooser dialog boxes.\nPlease type the folder in which the design will be saved";
        div.appendChild(p);
        var input = document.createElement("input");
        input.type = "text";
        div.appendChild(input);
        alert(div)
        document.onclick = function(event){
            if(event.target.id!=thisDiv.id){
                saveToFile.saveDialogClose(div);

            }
        }        
    }
    catch(e){
        alert(e);
    }

    },
// close showed layer
    saveDialogClose: function(ddmenuitem){
	if(ddmenuitem){
            var parent = ddmenuitem.parentNode;
            parent.removeChild(ddmenuitem);
        }
    }*/

}