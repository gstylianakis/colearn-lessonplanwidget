/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GeneralUsage;
import java.net.URLDecoder;

/**
 *
 * @author Administrator
 */
public class URL_parsing {
    public String utf_decode(String utftext)throws Exception{
        try{
            /*
            int i = 0;
            int c, c3, c2 = 0;
            String decoded_string = "";
            while ( i < utftext.length() ) {
                c = (int)utftext.charAt(i);

		if (c < 128) {
                    decoded_string += String.valueOf(c);
                    i++;
		}
		else if((c > 191) && (c < 224)) {
                    c2 = (int)utftext.charAt(i+1);
                    decoded_string += String.valueOf(((c & 31) << 6) | (c2 & 63));
                    i += 2;
		}
		else {
                    c2 = (int)utftext.charAt(i+1);
                    c3 = (int)utftext.charAt(i+2);
                    decoded_string += String.valueOf(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
		}
            }
*/
            URLDecoder decoder = new URLDecoder();
            return decoder.decode(utftext,"utf-8");
        }
        catch(Exception e){
            throw e;
        }
    }
}
