<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:useBean id="save" scope="page" class="GeneralUsage.Design"/>
<%    
    try{
        String manifest_id = request.getParameter("delete_projectId"); 
        String user_encoded = request.getParameter("user_encoded");
        save.deleteManifest(manifest_id); 
            
        String redirectURL = "wizard.jsp?user="+user_encoded+"&projectId=new";
        response.sendRedirect(redirectURL);
    }
    catch(Exception e){
           System.out.println("error while deleting "+e);        
    }
%>