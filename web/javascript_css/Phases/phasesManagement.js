/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function PhasesArray(){
        try{

            this.PhasesArray = new Array();
            this.addPhase = function(phase){               
                this.PhasesArray.push(phase);
            }
            this.getPhases = function(){
                return this.PhasesArray;
            }
        }
        catch(e){
            console.log("PhaseArray "+e);
        }
    }
function initializePhasesArray(){
    try{
        return new PhasesArray();
    }
    catch(e){
        console.log("initPhaseArray"+e)
    }
}
function Phase(name,role,condition,conditionView,conditionType,time,notifications,phaseFeedBackItems,phaseHourDuration){
    try{
        this.start = null;
        this.endPoint = [];
        //this.endPoint = null;
        this.LearningActivitiesArray = initializeActivities();
        this.ActivitiesStructuresArray = activityStructuresArray.initStructureArray();
        this.joinGateways = gateway.initJoinGatewayList();
        this.inclusiveOrGateways = gateway.initInclusiveOrList();
        this.propertyList = [];

        this.getType = function(){
            return "phase";
        }

        this.name = name;
        //this.id = name;
        this.id = uniqid();
        
        this.conditionType = conditionType;
        this.EndCondition = null;
        this.notification = [];
        for(var i=0;i<notifications.length;i++){            
            this.notification.push(notifications[i]);
            
        }
        this.Feedback = [];
        for(i=0;i<phaseFeedBackItems.length;i++)            
            this.Feedback.push(phaseFeedBackItems[i]);

        
        if(this.conditionType=='Condition')
            this.EndCondition = new completeExpressionCondition(role,condition,conditionView);

        else if(this.conditionType=='TimePeriod')
            this.EndCondition = [time,phaseHourDuration];

        else if(this.conditionType=='CompleteActivities'){
            this.EndCondition=new Array();
            for(i=0;i<completeActivities.getRolesActivitiesPairs().length;i++)
                this.EndCondition.push(completeActivities.getRolesActivitiesPairs()[i].slice(0));
        }
        this.getEndCondition = function(){
                return this.EndCondition;
        }

        this.getConditionType = function(){
            return this.conditionType;
        }
        this.setConditionType = function(conditionType){
            this.conditionType = conditionType;
        }
        this.setEndCondition = function(role,condition,conditionView,time,phaseHourDuration){

            if(this.conditionType=='Condition')
                this.EndCondition = new completeExpressionCondition(role,condition,conditionView);

            else if(this.conditionType=='TimePeriod'){
                this.EndCondition = [time,phaseHourDuration];
            }

            else if(this.conditionType=='CompleteActivities'){
                this.EndCondition = new Array();
                for(var i=0;i<completeActivities.getRolesActivitiesPairs().length;i++)
                    this.EndCondition.push(completeActivities.getRolesActivitiesPairs()[i].slice(0));
            }
        }
        this.getLearningActivitiesArray = function(){
            return this.LearningActivitiesArray;
        }
        this.setLearningActivitiesArray = function(learningActivitiesArray){
            this.LearningActivitiesArray = learningActivitiesArray;            
        }

        this.getActivitiesStructuresArray = function(){
            return this.ActivitiesStructuresArray;
        }
         this.getJoinGateways= function(){
            return this.joinGateways;
        }
        this.getInclusiveOrGateways = function(){
            return this.inclusiveOrGateways;
        }
         this.setJoinGateways= function(gatewayList){
            this.joinGateways = gatewayList;
        }
        this.setInclusiveOrGateways = function(gatewayList){
            this.inclusiveOrGateways = gatewayList;
        }
        //this.setActivitiesStructuresArray = function(){

        //}

        this.getName = function(){
            return this.name;
        }
        this.setName = function(name){
            this.name = name;
        }
        this.getId = function(){
            return this.id;
        }
        this.setNotification = function(notifications){
            this.notification = [];
             for(var i=0;i<notifications.length;i++)
                this.notification.push(notifications[i]);

        }
        this.getNotification = function(){
            return this.notification;
        }

        this.setFeedback = function(feedbackItems){
            this.Feedback = [];
             for(var i=0;i<feedbackItems.length;i++)
                this.Feedback.push(feedbackItems[i]);

        }
        this.getFeedback = function(){
            return this.Feedback;
        }

        this.setStartPoint = function(obj){
            this.start = obj;
        }
        this.getStartPoint = function(){
            return this.start;
        }
        this.addEndPoint = function(newEndPoint){
            this.endPoint.push(newEndPoint)
        }
        this.setEndPoint = function(obj){
            this.endPoint = obj;
        }
        this.getEndPoint = function(){
            return this.endPoint;
        }
        this.addProperty = function(property){
            this.propertyList.push(property);
        }
        this.setPropertyList = function(propertyList){
            this.propertyList = propertyList;
        }
        this.getPropertyList = function(){
            return this.propertyList;
        }
    }
    catch(e){
        console.log("Phase "+e);
    }

}
function createPhase(name,phasesArray,notificationArray,phaseFeedBackItem,option){
    try{
        
        if(name==""){
            alert("Please define a name");            
            return;
        }
        var tmpPhase = null;        
        tmpPhase = allPhases.getPhases().containsName(name);
        if(tmpPhase!=null){
            alert("This name already used for other act.\nPlease give different name");
            return;
        }

        var role = document.getElementById('RoleConditionForPhaseCompletion').value;
        var condition = document.getElementById('ConditionForPhaseCompletion').value;
        var conditionView = document.getElementById('conditionView');
        var type;

        if(document.getElementById("NoCondition").checked)
            type = "NoCondition";

        else if(document.getElementById("CompleteActivities").checked)
            type = "CompleteActivities";
        else if (document.getElementById("TimePeriod").checked)
            type = "TimePeriod";
        else if (document.getElementById("Condition").checked)
            type = "Condition";

         //var notification = createNotification();
         var newPhase = new Phase(name,role,condition,conditionView,type,$("#PhaseDays").val(),notificationArray,phaseFeedBackItem,$("#phaseHours").val());
         phasesArray.addPhase(newPhase);
         //create list that contains property you want to change when act completes
         var property = null;

         newPhase.setPropertyList([]);
         $("#changeProperties").find("select").each(function(){
             
              if($(this).val()=="")
                  return true;//continue
                  
              
              property =  new properties.property($(this).val(),"",$(this).parent().parent().children()[1].childNodes[0].value);
              var tmpProp = properties.propertiesList.containsName($(this).val())
              property.setId(tmpProp.getId());
              newPhase.addProperty(property);
         })

         addPhaseToTree(newPhase,phasesArray,option);
         document.getElementById("phasesNum").value = parseInt(document.getElementById("phasesNum").value) + 1;
         document.getElementById("phaseTab").style.display="block";
         
         document.getElementById("phaseTab").name = name;

         var tabLi = document.createElement("li");
         tabLi.name = name;
         tabLi.id = newPhase.getId()+"Tab";
         removeFromTab();
        
         var tabAnchor = document.createElement("a");
         var span = $("<span style='white-space:nowrap'></span>").appendTo(tabAnchor)

         tabAnchor.onclick = function () {
             $("#Phases_Rows").find("span").each(function(){
                 $(this).css("color","#0464BB")
             })
             browsePhase(newPhase,phasesArray)
         };
               
         if(name.length>6){
             span.html(name.substring(0,6)+"..");
         }
         else
             span.html(name);
         
         tabAnchor.title = name;
         tabLi.appendChild(tabAnchor);
         
         if($("#phaseTab").find("ul").children().length!=0)
            tabLi.className="selected";

         $("#phaseTab").find("li").each(function(){
             $(this).removeAttr("class");
         });
          
         $("#phaseTab").find("ul").append(tabLi);
         addImagesInTab(newPhase,tabLi)
         if($("#phaseTab").find("li").length>=5){
             var width = $("#phaseTab").find("ul").width();             
             $("#phaseTab").find("ul").width(width+89);


         }

        if($("#phaseTab").find("li").length==5){
            $("#rightSide").css("display","block");
            $("#leftSide").css("display","block");
           
            $('#leftSide').click(function(e){
                var offset = $("#phaseTab").scrollLeft()//$("#phaseTab").find(">ul").offset().left
                var tabsLen = $("#phaseTab").find("li").length;
                if(((tabsLen-4)*89)>offset){//vrsikw to offset pou xreiazetai to teleutaio tab kai an exw idi kinithei pros ta ekei min kounisw allo
                    $('#phaseTab').scrollLeft(offset+89);
                    
                }
                e.stopPropagation();
                
            });
            $('#rightSide').click(function(e){
                var offset = $("#phaseTab").scrollLeft()
              
                if(offset>0){
                   $('#phaseTab').scrollLeft(offset-89);
                    
                }
              e.stopPropagation();
        });
       }
        
    }
    catch(e){
        console.log("createPhase "+e);
    }
}
function modifyPhase(name,time,notificationArray,phaseFeedBackItem,phaseHourDuration){
    try{
       
        if (!confirm("You are going to modify act")==true)
            return;

        var prevName = document.getElementById("prevPhaseName").value;
        if(prevName!=name){
            var exists = allPhases.getPhases().containsName(name);
            if(exists!=null){
                alert("This name already used for other activity.\nPlease give different name");
                return;
            }
        }
        var role = document.getElementById('RoleConditionForPhaseCompletion').value;
        var condition = document.getElementById('ConditionForPhaseCompletion').value;
        var conditionView = document.getElementById('conditionView');
        
         
        var phase = allPhases.getPhase(document.getElementById("prevPhaseName").value);
        if(phase==-1){
            alert("unexpected error");
            return;
        }
     
        var type;
        if(document.getElementById("NoCondition").checked)
            type = "NoCondition";

        else if(document.getElementById("CompleteActivities").checked)
            type = "CompleteActivities";

        else if (document.getElementById("TimePeriod").checked)
            type = "TimePeriod";
        else if (document.getElementById("Condition").checked)
            type = "Condition";

        phase.setConditionType(type);
        phase.setEndCondition(role,condition,conditionView,time,phaseHourDuration);
        phase.setName(name);
        phase.setNotification(notificationArray);

        var property = null;
        phase.setPropertyList([]);
         $("#changeProperties").find("select").each(function(){
              if($(this).val()=="")
                  return true;//continue
              
              property =  new properties.property($(this).val(),"",$(this).parent().parent().children()[1].childNodes[0].value);
              var tmpProp = properties.propertiesList.containsName($(this).val())
              property.setId(tmpProp.getId());
              phase.addProperty(property);
         })

        phase.setFeedback(phaseFeedBackItem);
        if(document.getElementById("phaseTab").name==prevName)
            document.getElementById("phaseTab").name = name;

        var newName="";
        if(name.length>6){
             newName = name.substring(0,6);
             newName += "..";
         }
         else
             newName = name;
        $("#phaseTab").find("#"+phase.getId()+"Tab").find('span').html(newName);
        $("#phaseTab").find("#"+phase.getId()+"Tab").find('span').attr("title",name);
        //document.getElementById(phase.getId()+"AnchorInPhaseTree").innerHTML = name;
        //document.getElementById(phase.getId()+"AnchorInPhaseTree").title = name;        


        $("#"+phase.getId()+"AnchorInPhaseTree").text(newName);
        $("#"+phase.getId()+"AnchorInPhaseTree").attr("title",name);

        $("#"+phase.getId()+"_PhaseTree"+"_Edit").colorbox.close();
    }
    catch(e){
        console.log("modifyPhase "+e);
    }
}
function addPhaseToTree(phase,phasesArray,option){
     try{
         var rowId = phase.getId()+"_PhaseTree";
         var newRow = $("<li></li>");
         var clickable = $("<div class='hitarea'></div>")
         
         clickable.appendTo(newRow)
         newRow.attr("id",rowId);
         var span = $("<span></span>");
         span.attr("title","click to get to specific phase")
         span.attr("id",phase.getId()+"AnchorInPhaseTree");
         span.attr('class','folder');
         span.mouseover(function(){this.style.cursor='pointer'})
         var name = phase.getName();
         var spanName="";
         if(name.length>12){
                spanName = name.substring(0,12);
                spanName +=".."
            }
         else
            spanName = name;

         clickable.click(function(){
            //newRow.find(">ul").toggle()
            newRow
            .swapClass( "collapsable", "expandable" )
            .swapClass( "lastCollapsable", "lastExpandable" )
             $(this)
                    .replaceClass( "collapsable-hitarea", "expandable-hitarea" )
		    .replaceClass( "lastcollapsable-hitarea", "lastexpandable-hitarea" )
             newRow.find( ">ul" ).toggle();
         })
         /*span.click(function(){
             newRow.find(">ul").toggle()
             
             newRow.find(">.hitarea")
                    .replaceClass( "collapsable-hitarea", "expandable-hitarea" )
		    .replaceClass( "lastcollapsable-hitarea", "lastexpandable-hitarea" )
             browsePhase(phase,phasesArray)
         });*/
         span.text(spanName);

         span.attr("title",name);
         newRow.append(span);        
         newRow.appendTo("#Phases_Rows");
         $("#Phases_Rows").treeview({
            add: newRow
            }
         );
         $("#Phases_Rows").find("span").each(function(){
            $(this).css("color","#0464BB")
         })
         span.css("color","red")

         span.click(function(){
             $("#Phases_Rows").find("span").each(function(){
                 $(this).css("color","#0464BB")
             })
             $(this).css("color","red")
             browsePhase(phase,phasesArray);
         })

         //auto einia i ipodomi na dextei ta apo-katw activities pou 'kremontai'
         var activities = $("<ul></ul>").appendTo(newRow);
         activities.attr("id",newRow.attr("id")+"_ActivitiesTree");
         
         /*
         var newRow = document.createElement('li');
         newRow.id = phase.getId()+"_PhaseTree";
         newRow.style.clear = "both";
         newRow.style.height = "auto";
     
         var anchorDiv = document.createElement('div');
         anchorDiv.className = "informationPanelInnerAnchors";         
         newRow.appendChild(anchorDiv);

         var anchor = document.createElement('a');
         anchor.style.border = "0px";
         anchor.id = phase.getId()+"AnchorInPhaseTree";
         anchor.style.textDecoration = "none";                  
         anchor.style.height = "16px";
         anchor.style.cssFloat="left";
         anchor.href="#";
         anchor.style.width = "auto";
         var name = phase.getName();
         
         if(name.length>12){
                anchor.innerHTML = name.substring(0,12);
                anchor.innerHTML +=".."
            }
         else
            anchor.innerHTML = name;
         anchor.title = name;

         anchor.onclick = function () {browsePhase(phase,phasesArray)};
         
         var expandPhase=document.createElement("IMG");
         expandPhase.setAttribute("src","javascript_css/project_team_form/images/expand_1.png");
         expandPhase.id = newRow.id+"_img";
         expandPhase.style.width = "9px";
         expandPhase.style.cssFloat="left";
         expandPhase.style.height = "11px";
         expandPhase.style.marginTop = "2px";
         expandPhase.align = "middle";
         expandPhase.onclick = function(){ShowHide(newRow.id+"_ActivitiesTree",'toggle');changeIcon(this,newRow.id+"_ActivitiesTree",'toggle');};
         anchorDiv.appendChild(expandPhase);
         anchorDiv.appendChild(anchor);
         
         var imgDiv = document.createElement('div');
         imgDiv.style.cssFloat = "right";
         imgDiv.style.marginRight = "1px";
         imgDiv.style.width = "35px";
         imgDiv.style.height = "16px";

         var editImg=document.createElement("IMG");
         editImg.setAttribute("src","javascript_css/project_team_form/images/edit.gif");
         editImg.style.border="0px";
         editImg.style.cssFloat="left";         
         editImg.id = newRow.id+"_Edit";
         editImg.onclick = function(){loadPhaseProperties(phase)};
         editImg.onmouseover = function(){this.style.cursor='pointer'};
         editImg.title = "edit act";
         editImg.align = "top";
         imgDiv.appendChild(editImg)
         var deleteImg = document.createElement("IMG");
         deleteImg.setAttribute("src","javascript_css/project_team_form/images/delete.png");
         deleteImg.style.border="0px";
         deleteImg.style.cssFloat="left";
         deleteImg.style.marginLeft = "3px";
         deleteImg.onmouseover = function(){this.style.cursor='pointer'};
         deleteImg.align = "top";
         deleteImg.onclick = function(){deletePhase(phase)};
         deleteImg.title = "delete act";
         imgDiv.appendChild(deleteImg);         
         newRow.appendChild(imgDiv);         
         var activities = document.createElement("ul");
         activities.id = newRow.id+"_ActivitiesTree";
         activities.style.clear = "both";         
         newRow.appendChild(activities);
         document.getElementById("Phases_Rows").appendChild(newRow);*/
         if(option==0)
            $(".PhaseOpt").colorbox.close();

         /*$("#"+rowId+"_Edit").colorbox(
                    {width:"68%", height:"65%", inline:true, href:"#PhaseDefinition",
                     onLoad:function(){
                        completeActivities = initCompleteActivitiesCondition();
                        document.getElementById('prevPhaseName').value = phase.getName();
                        document.getElementById('PhaseOpenMode').value = 'modify'
                        deleteCondition("conditionView");                        
                        loadPhaseToEdit(phase);                                        
                        clearNotificationViewer('phase');
                        showNotifications(phase.getNotification(),"Phase");
                        clearItemViewer("PhaseDescription");
                        phaseFeedBackItem = [];
                       
                        document.getElementById("PhaseDescriptionTitle").value = "";document.getElementById("createNewPhaseDescription").checked = true;
                        document.getElementById("createNewPhaseDescriptionContent").value = "Enter your description here";
                        document.getElementById("URLPhaseDescriptionContent").value = "http://"
                        showCompletionPrereq('createNewPhaseDescription','PhaseDescription');
                        showFeedback(phase,'PhaseDescription','');

                        notificationArray = [];
                        notificationArray = phase.getNotification();
                        document.getElementById("notificationSubject").value = "";
                        document.getElementById("notificationContent").value = "";
                        clearRecipients("phase");
                    }
         });*/
         
     }
     catch(e){
         console.log("addPhaseTotree "+e);
     }
    //Phases_Rows
}
function browsePhase(phase,phasesArray){
    try{

        removeFromTab();
        document.getElementById("phaseTab").name = phase.getName();
        //disapear other phases, specifically disappear learning activities and connections
        clearPhaseView(phase,phasesArray);
        var tabs = $("#phaseTab").find("ul").children();
        if(tabs.length>1)
            $("#phaseTab").find("li").each(function(){
                 if($(this).attr("id")!=phase.getId()+"Tab")
                    $(this).removeAttr("class");
                 else{
                     $(this).attr("class","selected");
                     var index = allPhases.getPhases().indexOfStruct(phase.getId())
                     
                     var Offset = parseInt($("#phaseTab").scrollLeft())//parseInt(($("#phaseTab").find(">ul").offset().left)/89)                     
                     var quadrat = Offset/89;                    
                     //var showsSpacemin = quadrat
                     //var showsSpacemax = quadrat+3
                     var showsSpacemin = quadrat;
                     var showsSpacemax = quadrat+3
                     //console.log(" min "+showsSpacemin+" max"+showsSpacemax+" kai index "+index)

                     if(index < showsSpacemin){
                         $("#phaseTab").scrollLeft(Offset+((showsSpacemin-index)*(-89)));
                         Offset = parseInt($("#phaseTab").scrollLeft())                         
                         quadrat = Offset/89;
                         showsSpacemin = quadrat
                         showsSpacemax = quadrat+3                          
                     }
                     else if(index > showsSpacemax){

                        var move = Offset + ((index-showsSpacemax)*89)//etsi kanw to index max
                        
                        $("#phaseTab").scrollLeft(move);//to max gia ginei to index arra tha to kounisw kata index -4 kai tha ginei min =index-4 kai max =index
                      
                        Offset = parseInt($("#phaseTab").scrollLeft())
                        quadrat = Offset/89;
                        showsSpacemin = quadrat
                        showsSpacemax = quadrat+3
                     }
                      //else if(index>showsSpacemax)
                        //  $("#phaseTab").find(">ul").offset({left:(356-index*85)});
                    
                 }
             });
             $("#"+phase.getId()+"AnchorInPhaseTree").css("color","red");
        
    }
    catch(e){
        console.log("browsePhase "+e);
    }
}
function loadPhaseProperties(phase){
    try{
        //clear initial values
        document.getElementById("ActivitiesCompletionPrereq").innerHTML="";
        document.getElementById("PhaseTitle").value = phase.getName();    
        var activityList = phase.getLearningActivitiesArray();
        var activities = activityList.getActivities();
        var activityLi;
        for(var i=0;i<activities.length;i++){            
            activityLi = document.createElement("div");
            activityLi.innerHTML = activities[i].getName();
            document.getElementById("ActivitiesCompletionPrereq").appendChild(activityLi);
        }
        clearPropertyRow("changeProperties");
        var propertyList = phase.getPropertyList()
        console.log(propertyList.length)
        for(i=0;i<propertyList.length;i++){                       
            if(i==0){
                $("#PhaseSelectProperty").find("option").each(function(){                   
                    if($(this).text()==propertyList[i].getName())
                        $(this).attr("selected",true);
                    else
                        $(this).removeAttr("selected");
                })
                $("#PhaseProperty1").val(propertyList[i].getValue())
            }
            else    
                addPropertyChangeRow(propertyList[i].getValue(), propertyList[i].getName(),"changeProperties")
        }
    }
    catch(e){
        console.log("PhaseProperties "+e);
    }
}
function clearAllViews(phasesArray, index){
    try{
        var phases = phasesArray.getPhases();
        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById("vectorGraphicsDoc");
        var textPath = null;

        var PhaseActivitiesArray;
        var activitiesList,structureArrays,joinGateways,inclusiveOr;
        var connectionsList;
        var activity,structure;
        for(var i=index;i<phases.length;i++){
            PhaseActivitiesArray = phases[i].getLearningActivitiesArray();
            activitiesList = PhaseActivitiesArray.getActivities();
            connectionsList = PhaseActivitiesArray.getConnectionsArray();
            structureArrays = phases[i].getActivitiesStructuresArray();
            joinGateways = phases[i].getJoinGateways();
            inclusiveOr = phases[i].getInclusiveOrGateways();
            if(phases[i].getStartPoint()!=null)
                phases[i].getStartPoint().style.display="none";

            if(phases[i].getEndPoint().length!=0)
                for(var k=0;k<phases[i].getEndPoint().length;k++)
                    phases[i].getEndPoint()[k].style.display="none";

            for(var j=0;j<activitiesList.length;j++){
                activity = activitiesList[j];
                activity.get_Shape().style.display = "none";
                //if(connectionsList[j]!=null)//to elenxo giati o arithmos twn connection einai to poli isos me to miso tou arithmou twn learning activities
                  //  connectionsList[j].style.display = "none";
            }
            //apo dw kai katw
            for(j=0;j<structureArrays.length;j++){
                structure = structureArrays[j];
                structure.getShape().style.display = "none";
            }
            for(j=0;j<joinGateways.length;j++){                                
                joinGateways[j].getShape().style.display = "none";
            }
            for(j=0;j<inclusiveOr.length;j++){
                inclusiveOr[j].getShape().style.display = "none";
            }

            for(j=0;j<connectionsList.length;j++){
                connectionsList[j].style.display = "none";
                if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta eksafanizei
                    textPath = root.getElementById("textRefered_"+connectionsList[j].getAttribute("id"));
                    if(textPath!=null)
                        textPath.style.display = "none";
                }
            }
        }
        if(index==1){
            var tabs = $("#phaseTab").find("ul").children();
            if(tabs.length>1){
                tabs[0].className="selected";
                tabs[(tabs.length-1)].className = "";
            }
        }

    }
    catch(e){
        console.log("createAllViews "+e);
    }
}

function clearPhaseView(phase,phasesArray){
    try{
        var phases = phasesArray.getPhases();
        var PhaseActivitiesArray;
        var activitiesList,structureArrays,joinGateways,inclusiveOr;
        var connectionsList;
        var activity,structure;
        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById("vectorGraphicsDoc");
        var textPath = null;
        for(var i=0;i<phases.length;i++){
           if(phases[i].getId()!=phase.getId()){
                PhaseActivitiesArray = phases[i].getLearningActivitiesArray();
                activitiesList = PhaseActivitiesArray.getActivities();
                connectionsList = PhaseActivitiesArray.getConnectionsArray();
                structureArrays = phases[i].getActivitiesStructuresArray();
                joinGateways = phases[i].getJoinGateways();
                inclusiveOr = phases[i].getInclusiveOrGateways();
                if(phases[i].getStartPoint()!=null)
                    phases[i].getStartPoint().style.display = "none";

                if(phases[i].getEndPoint()!=null)
                     for(var k=0;k<phases[i].getEndPoint().length;k++)
                         phases[i].getEndPoint()[k].style.display = "none"

                for(var j=0;j<activitiesList.length;j++){
                    activity = activitiesList[j];
                    activity.get_Shape().style.display = "none";
                    //if(connectionsList[j]!=null){//to elenxo giati o arithmos twn connection einai to poli isos me to miso tou arithmou twn learning activities
                      //  connectionsList[j].style.display = "none";
                        
                    //}
                    //apo dw kai katw                    
                }
                for(j=0;j<structureArrays.length;j++){
                        structure = structureArrays[j];
                        structure.getShape().style.display = "none";
                 }
                 for(j=0;j<connectionsList.length;j++){
                    connectionsList[j].style.display = "none";
                    if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta eksafanizei
                        textPath = root.getElementById("textRefered_"+connectionsList[j].getAttribute("id"));
                        if(textPath!=null)
                            textPath.style.display = "none";
                    }
                 }
                 for(j=0;j<joinGateways.length;j++){                                      
                    joinGateways[j].getShape().style.display = "none";
                 }
                 for(j=0;j<inclusiveOr.length;j++){
                    inclusiveOr[j].getShape().style.display = "none";
                 }

            }
            else{
                PhaseActivitiesArray = phases[i].getLearningActivitiesArray();
                activitiesList = PhaseActivitiesArray.getActivities();
                connectionsList = PhaseActivitiesArray.getConnectionsArray();
                structureArrays = phases[i].getActivitiesStructuresArray();
                joinGateways = phases[i].getJoinGateways();
                inclusiveOr = phases[i].getInclusiveOrGateways();
                if(phases[i].getStartPoint()!=null)
                    phases[i].getStartPoint().style.display = "block";

                if(phases[i].getEndPoint()!=null)
                    for(k=0;k<phases[i].getEndPoint().length;k++)
                         phases[i].getEndPoint()[k].style.display = "block"

                for(j=0;j<activitiesList.length;j++){
                    activity = activitiesList[j];
                    activity.get_Shape().style.display = "block";

                    //if(connectionsList[j]!=null){//to elenxo giati o arithmos twn connection einai to poli isos me to miso tou arithmou twn learning activities
                      //connectionsList[j].style.display = "block";
                    
                    //}
                }
                for(j=0;j<structureArrays.length;j++){
                        structure = structureArrays[j];
                        structure.getShape().style.display = "block";
                 }
                 for(j=0;j<connectionsList.length;j++){
                        connectionsList[j].style.display = "block";
                        if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta eksafanizei
                            textPath = root.getElementById("textRefered_"+connectionsList[j].getAttribute("id"));
                            if(textPath!=null)
                                textPath.style.display = "block";
                        }
                 }
                 for(j=0;j<joinGateways.length;j++){
                    joinGateways[j].getShape().style.display = "block";
                 }
                  for(j=0;j<inclusiveOr.length;j++){
                    inclusiveOr[j].getShape().style.display = "block";
                 }
            }
        }
    }
    catch(e){
        console.log("createPhaseView "+e);
    }
}
    function addLearningActivityToPhaseTree(LearningActivity/*codeAllagi Name*/,PhaseName){
        try{
            
            var newRow = $("<li></li>");            
            newRow.attr("id",LearningActivity.getId()+ "inActivitiesTree");
            var span = $("<span></span>");
            span.attr('class','file');
            //span.mouseover(function(){this.style.cursor='pointer'})
            span.attr("id",LearningActivity.getId()+ "inActivitiesTreeAnchor");
            var spanName = "";
            if(LearningActivity.getName().length>13){
                spanName = LearningActivity.getName().substring(0,13);
                spanName +=".."
            }
            else
                spanName = LearningActivity.getName().substring(0,13);

            
            span.text(spanName);
            span.attr("title",LearningActivity.getName());
            newRow.append(span);
            newRow.appendTo($("#"+PhaseName+"_ActivitiesTree"));
            $("#Phases_Rows").treeview({
                add: newRow
            });
         /*var newRow = document.createElement('li');
         newRow.id = LearningActivity.getId()+ "inActivitiesTree";//codeAllagi LearningActivityName + "inActivitiesTree";
         newRow.style.marginLeft = "3px";
         newRow.style.clear = "both";
         newRow.style.marginBottom="10px";
         
         var anchorDiv = document.createElement('div');
         anchorDiv.className = "informationPanelInnerAnchors";                           
         newRow.appendChild(anchorDiv);

         var anchor = document.createElement('a');
         anchor.style.border = "0px";
         anchor.id = LearningActivity.getId()+ "inActivitiesTreeAnchor";         
         anchor.style.textDecoration = "none";
         anchor.style.width = "auto";
         anchor.style.height = "16px";         
         anchor.style.cssFloat="left";
         
         if(LearningActivity.getName().length>13){
             anchor.innerHTML = LearningActivity.getName().substring(0,13);
             anchor.innerHTML += "..";
         }
         else
            anchor.innerHTML = LearningActivity.getName();//codeAllagi LearningActivityName;
        anchor.title =  LearningActivity.getName();
        anchorDiv.appendChild(anchor);

        var imgDiv = document.createElement('div');
        imgDiv.style.cssFloat = "right";
        imgDiv.style.marginRight = "1px";
        imgDiv.style.width = "35px";
        imgDiv.style.height = "16px";        
        newRow.appendChild(imgDiv);

        var editImg=document.createElement("IMG");
        editImg.onclick = function () {BrowseActivity(LearningActivity);};
        editImg.setAttribute("src","javascript_css/project_team_form/images/edit.gif");
        editImg.style.border="0px";
        editImg.style.cssFloat="left";
        editImg.align = "top";
        editImg.title = "edit activity";
        editImg.name = LearningActivity.getId();
        editImg.onmouseover = function(){this.style.cursor='pointer'};
        imgDiv.appendChild(editImg);

        var deleteImg = document.createElement("IMG");
        deleteImg.setAttribute("src","javascript_css/project_team_form/images/delete.png");
        deleteImg.style.border="0px";
        deleteImg.style.cssFloat="left";
        deleteImg.style.marginLeft = "3px";
        deleteImg.onmouseover = function(){this.style.cursor='pointer'};
        deleteImg.align = "top";
        deleteImg.title = "delete activity";
        deleteImg.onclick = function(){deleteActivity(LearningActivity,0);};
        imgDiv.appendChild(deleteImg);
        if($("#"+PhaseName+"_ActivitiesTree")!=null)
            $("#"+PhaseName+"_ActivitiesTree").append(newRow);*/
         
         
    }
    catch(e){
        console.log("addLearningActivityToPhaseTree "+e);
    }

}

function loadPhaseToEdit(phase){
    try{
        var type = phase.getConditionType();        
        showCompletionPrereq(type,"");
        //document.getElementById("prevPhaseName").value = phase.getName();
        if(type=="Condition"){
            document.getElementById("NoCondition").checked = false;
            document.getElementById("CompleteActivities").checked = false;
            document.getElementById("TimePeriod").checked = false;
            
            document.getElementById("Condition").checked = true;
            var expression = phase.getEndCondition();
            document.getElementById("RoleConditionForPhaseCompletion").value = expression.getExpressionRole();
            document.getElementById("ConditionForPhaseCompletion").value = expression.getInitCondition();
        
            if(expression.getConditionView().childNodes.length>=1){
                document.getElementById("ConditionForPhaseCompletion").disabled = true;
                document.getElementById("addCondition").disabled = true;
            }
            
            document.getElementById('conditionView').appendChild(expression.getConditionView());
        }
        else if(type=="TimePeriod"){
            document.getElementById("NoCondition").checked = false;
            document.getElementById("CompleteActivities").checked = false;
            document.getElementById("TimePeriod").checked = true;
            //document.getElementById("Condition").checked = false;

            $("#PhaseDays").val(phase.getEndCondition()[0]);
            $("#phaseHours").val(phase.getEndCondition()[1]);

        }
        else if(type=="CompleteActivities"){
            document.getElementById("NoCondition").checked = false;
            document.getElementById("CompleteActivities").checked = true;
            document.getElementById("TimePeriod").checked = false;
            //document.getElementById("Condition").checked = false;

            for(var i=0;i<phase.getEndCondition().length;i++){
                document.getElementById(phase.getEndCondition()[i][0]+(phase.getEndCondition()[i][1]).getId()+"_checkbox").checked = true;//codeAllagi document.getElementById(phase.getEndCondition()[i][0]+phase.getEndCondition()[i][1]+"_checkbox").checked = true;
                completeActivities.addRoleActivityPair(phase.getEndCondition()[i][0],phase.getEndCondition()[i][1]);
            }
        }
        else{
            document.getElementById("NoCondition").checked = true;
            document.getElementById("CompleteActivities").checked = false;
            document.getElementById("TimePeriod").checked = false;
            //document.getElementById("Condition").checked = false;
        }

        
        //document.getElementById('conditionView').appendChild();
    }
    catch(e){
        console.log("loadPhaseToedit "+e);
    }
}




function showAllActivities(){
    try{
       
        var allActivities = new Array();
        var PhaseActivities = new Array();
        
        for(var i=0;i<allPhases.getPhases().length;i++){            
            PhaseActivities  = allPhases.getPhases()[i].getLearningActivitiesArray().getActivities();           
            for(var j=0;j<PhaseActivities.length;j++)
                allActivities.push(PhaseActivities[j]);                
        }
        return allActivities;
    }
    catch(e){
        console.log("showAllActivities "+e);
    }
}

PhasesArray.prototype.getPhase = function(value){
    var ctr = -1;
    

    for (var i=0; i < this.getPhases().length; i++) {
    
    if (this.getPhases()[i].getName() == value) {
            return this.getPhases()[i];
        }
    }

    return ctr;
};

function getActivityPhase(activityId){
     try{
        var PhaseActivities = new Array();

        for(var i=0;i<allPhases.getPhases().length;i++){
            PhaseActivities  = allPhases.getPhases()[i].getLearningActivitiesArray().getActivities();
            for(var j=0;j<PhaseActivities.length;j++)
                if(PhaseActivities[j].getId()==activityId)
                    return allPhases.getPhases()[i];
        }
        return null;
    }
    catch(e){
        console.log("getActivityPhase "+e);
    }
}
function getPhaseOfStructure(structureId){
     try{
        var PhaseStructures = new Array();
        for(var i=0;i<allPhases.getPhases().length;i++){
            PhaseStructures  = allPhases.getPhases()[i].getActivitiesStructuresArray();
            for(var j=0;j<PhaseStructures.length;j++)
                if(PhaseStructures[j].getId()==structureId)
                    return allPhases.getPhases()[i];
        }
        return null;
    }
    catch(e){
        console.log("getPhaseOfStructure "+e);
    }
}

function removeFromTab(){
    try{
         $("#phaseTab").find("#activityStructureView").remove();

         if($("#phaseTab").find("ul").children().length<5){
             $("#leftSide").css("display","none")
             $("#rightSide").css("display","none")
             $("#phaseTab").scrollLeft(-89);
         }
    }
    catch(e){
        alert("error while trying to remove from tab "+e);
    }
}
function removeSpecificTab(id){
    $("#phaseTab").find("#"+id+'Tab').remove();
    
    if($("#phaseTab").find("ul").children().length<5){
        $("#leftSide").css("display","none")
        $("#rightSide").css("display","none")
    }
}
function deletePhase(phase){
    try{
        if (!confirm("You are going to delete act")==true)
            return;
        //remove Activities
        var activities = phase.getLearningActivitiesArray().getActivities();
        //var num = activities.length;
        
        while(activities.length>0)            
            deleteActivity(activities[0],1);
        
        //remove structures
        var structures = phase.getActivitiesStructuresArray();
        while(structures.length>0){
                //num += structures[i].getLearningActivitiesArray().getActivities().length;
                dele.deleteStructure(structures[0]);
        }
        //remove start point
        if(phase.getStartPoint()!=null)
            deleteStartPoint(phase.getStartPoint().getAttribute("id"),phase,0);

        if(phase.getEndPoint()!=null)
             for(var k=0;k<phase.getEndPoint().length;k++)
                deleteEndPoint(phase.getEndPoint()[k].getAttribute("id"),phase,0);
                
        
        deleteAllGateways(phase);
        //remove from phases
        var index = allPhases.getPhases().indexOfStruct(phase.getId());
        if(index==null)
            throw "error while deleting act";
        allPhases.getPhases().splice(index,1);
        //remove all connections

        //remove from phase view
        console.log("to phase einai "+phase.getId());
        var del = document.getElementById(phase.getId()+"_PhaseTree");
        del.parentNode.removeChild(del);

        removeSpecificTab(phase.getId());
        $("#phasesNum").val($("#phasesNum").val()-1);

        if(allPhases.getPhases().length!=0)//otan diagrapseis ena phase tote pigaine kai kane current to prwto stin lista
            browsePhase(allPhases.getPhases()[0],allPhases)

        select();
    }
    catch(e){
        alert("error while deleting act "+e);
    }
}

    function addImagesInTab(newPhase,tabLi){
        var imgDiv = $("<div></div>");
        imgDiv.css({"float":"left"})
        imgDiv.appendTo(tabLi);
        var editDiv = $("<div></div>");
        imgDiv.append(editDiv)
        var editImg = $("<img></img>");
        editImg.css({"clear":"both"})
        editImg.attr("id",newPhase.getId()+"_PhaseTree"+"_Edit");
        editImg.colorbox(
                    {width:"70%", height:"68%", inline:true, href:"#PhaseDefinition",
                     
                     onLoad:function(){
                        loadPhaseProperties(newPhase)
                        completeActivities = initCompleteActivitiesCondition();
                        document.getElementById('prevPhaseName').value = newPhase.getName();
                        document.getElementById('PhaseOpenMode').value = 'modify'
                        deleteCondition("conditionView");
                        loadPhaseToEdit(newPhase);
                        clearNotificationViewer('phase');
                        showNotifications(newPhase.getNotification(),"Phase");
                        clearItemViewer("PhaseDescription");
                        phaseFeedBackItem = [];

                        document.getElementById("PhaseDescriptionTitle").value = "";document.getElementById("createNewPhaseDescription").checked = true;
                        document.getElementById("createNewPhaseDescriptionContent").value = "Enter your description here";
                        document.getElementById("URLPhaseDescriptionContent").value = "http://"
                        showCompletionPrereq('createNewPhaseDescription','PhaseDescription');
                        showFeedback(newPhase,'PhaseDescription','');

                        notificationArray = [];
                        notificationArray = newPhase.getNotification();
                        document.getElementById("notificationSubject").value = "";
                        document.getElementById("notificationContent").value = "";
                        clearRecipients("phase");
                    }
         });
         editImg.mouseover(function(){this.style.cursor='pointer'});
         editImg.attr("title" ,"edit "+newPhase.getName()+" properties");
         editImg.attr("src","javascript_css/images/Edit_icon.png");
         imgDiv.append(editImg)

        var deleteImg = $("<img></img>");
        deleteImg.attr("src","javascript_css/images/icon_delete.gif");
        deleteImg.css({"clear":"both"})
        deleteImg.mouseover(function(){this.style.cursor='pointer'});
        deleteImg.click(function(){deletePhase(newPhase)});
        deleteImg.attr("title","delete "+newPhase.getName());
        editDiv.append(deleteImg)

        
    }