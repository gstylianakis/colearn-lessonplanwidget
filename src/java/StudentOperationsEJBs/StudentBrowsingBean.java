/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package StudentOperationsEJBs;

/**
 *
 * @author Administrator
 */



import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.ejb.Stateless;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;



@Stateless
public class StudentBrowsingBean implements StudentBrowsing{

    public Vector student_searching(String criteria)throws Exception{
        Vector students = null;
        DB_connection connect = new DB_connection();
        Statement stmt = null;  
        ResultSet rs = null;
        try{
            students = new Vector();
            stmt = connect.getConnection().createStatement();
            rs = stmt.executeQuery(criteria);
            Vector tmp = new Vector();
            while (rs.next()) {
                tmp.add(rs.getString(1));
                tmp.add(rs.getString(2));
                tmp.add(rs.getString(3));
                students.add(tmp);
                tmp = new Vector();
            }            
            return students;
        }
        catch(Exception e){
            System.out.println("eskase sto student_browsing");
            throw e;
        } finally {
            rs.close();
            stmt.close();
            connect.close();
        }
    }
    
    public Vector student_searching()throws Exception{
        Vector students = null;
        DB_connection connect = new DB_connection();
        Statement stmt = null;
        
        try{
            students = new Vector();
            stmt = connect.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM students");
            Vector tmp = new Vector();
            while (rs.next()) {
                tmp.add(rs.getString(1));
                tmp.add(rs.getString(2));
                tmp.add(rs.getString(3));
                students.add(tmp);
                tmp = new Vector();
            }
            return students;
        }
        catch(Exception e){
            throw e;
        } finally{
            stmt.close();
            connect.close();            
        }
    }

    public boolean student_exist(String name)throws Exception{
        DB_connection connect = new DB_connection();
        Statement stmt = null;        
        try{
            boolean exists = false;
            stmt = connect.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT Name FROM students where students.Name='"+name+"'");           
            while (rs.next()) {
                exists = true;
                break;
            }
            return exists;
        }
        catch(Exception e){
            throw e;
        } finally {
            stmt.close();
            connect.close();
        }
    }

    public boolean usernamePsw_exist(String name, String psw)throws Exception{
        DB_connection connect = new DB_connection();
        Statement stmt = null;
        
        try{
            boolean exists = false;
            stmt = connect.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT name FROM ld_author where ld_author.name='"+name+"' or ld_author.password='"+psw+"'");

            while (rs.next()) {
                exists = true;
                break;
            }
            return exists;
        }
        catch(Exception e){
            throw e;
        } finally{
            stmt.close();
            connect.close();
        }
    }
    public String enroll_student(String name, String lastName, String username, String psw, String email, String gender) throws Exception{
        DB_connection connect = new DB_connection();
        Statement stmt = null;        
        try{
            if(usernamePsw_exist(username,psw))
                return "username/password allready exist";
            else{
                stmt = connect.getConnection().createStatement();
                String insertion = "INSERT INTO ld_author VALUES ('"+username+"','"+psw+"','"+email+"','"+name+"','"+lastName+"','"+gender+"')";

                stmt.executeUpdate(insertion);
                
            }
            return "registration completed succesfully";
        }
        catch(Exception e){
            throw e;
        } finally{
            stmt.close();
            connect.close();            
        }

    }
    
    public String getStudentODSGroup(String ods_groupId) throws Exception{
        //ArrayList team_students = new ArrayList();
        DB_connection con = new DB_connection();
        PreparedStatement pstmt = null;    
        ResultSet rs;  
        JSONObject team_studs = new JSONObject();
        try{
            LinkedList students = new LinkedList();
            con.getConnection().setAutoCommit(false);            
            pstmt = con.getConnection().prepareStatement("select t.Name, students.Name,students.FirstName from students inner join ("+
                                                                  "select team.Name,studentsinteam.student from team inner join studentsinteam on team.Name = studentsinteam.Group_participation "+
                                                                              "where team.ODS_Node_ID = ?) as t on students.Name = t.student");
            pstmt.setString(1,ods_groupId);                        
            rs = pstmt.executeQuery();
            int count = 0;
            while (rs.next()) {
                Map stud = new LinkedHashMap();
                if(count == 0){
                    team_studs.put("teamName", rs.getString(1));                    
                }
                count++;
                //JSONObject team_json = new JSONObject();
                //team.put("teamName", rs.getString(1));
                stud.put("username", rs.getString(2));
                stud.put("userFirstName", rs.getString(3));                
                students.add(stud);                
            }
            team_studs.put("students",students);
            con.getConnection().commit();
            
            return team_studs.toJSONString();
        }catch(Exception e){
            throw new Exception("error while loading students from ODS group");
        } finally{
            pstmt.close();
            con.close();            
        }                
    }
    
    public String getStudentParticipationGroups(String username) throws Exception{
        //ArrayList team_students = new ArrayList();
        DB_connection con = new DB_connection();
        PreparedStatement pstmt = null;    
        ResultSet rs;  
        JSONObject team_studs = new JSONObject();
        try{
            LinkedList groups = new LinkedList();
            con.getConnection().setAutoCommit(false);            
            pstmt = con.getConnection().prepareStatement("select ODS_NODE_Title, ODS_Node_ID from team as t inner join studentsinteam as s on t.Name = s.Group_participation where s.student = (select Name from students where ODSUsername = ?)");
            pstmt.setString(1,username);                        
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
                Map stud = new LinkedHashMap();   
                if(rs.getString(1)!=null && !rs.getString(1).isEmpty()){
                    stud.put("groupName", rs.getString(1));
                    stud.put("groupId", rs.getString(2));
                    groups.add(stud);                
                }
            }
            team_studs.put("groups",groups);
            con.getConnection().commit(); 

            return team_studs.toJSONString();
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("error while loading students from ODS group");
        } finally{
            pstmt.close();
            con.close();            
        }                
    }

}
