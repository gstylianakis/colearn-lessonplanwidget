/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ODSPortalIntegration;

import StudentOperationsEJBs.DB_connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import org.json.simple.JSONValue;

/**
 * REST Web Service
 *
 * @author stylianos
 */
@Path("GetRunIDFromManifestID/{manifestId}/{group_name}")
public class GetRunIDFromManifestIDResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GetRunIDFromManifestIDResource
     */
    public GetRunIDFromManifestIDResource() {
    }

    /**
     * Retrieves representation of an instance of ODSPortalIntegration.GetRunIDFromManifestIDResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getRunId(@PathParam("manifestId") String manifest_id, @PathParam("group_name") String group_name) throws Exception{
        //TODO return proper representation object
        System.out.println("einai "+manifest_id+" group_name "+group_name); 
        DB_connection con = new DB_connection();
        PreparedStatement stmt = null;  
        ResultSet rs = null;   
      
        try{      
            Map ld = new LinkedHashMap();
            stmt = con.getConnection().prepareStatement("SELECT run_id FROM manifestfile_run_id where manifest_name = ? and group_name=?");            
            stmt.setString(1,manifest_id);
            stmt.setString(2,group_name);
            rs = stmt.executeQuery();
                                  
            while (rs.next()) {                
                ld.put("run_id",rs.getString(1));
                return JSONValue.toJSONString(ld);                                
            }            
            return JSONValue.toJSONString(ld.put("run_id","null"));
        } catch(Exception e){
            e.printStackTrace();
            Logger.getLogger(GetRunIDFromManifestIDResource.class.getName()).log(Level.SEVERE, null, e);
            throw new Exception("Error while retrieving run id");
        }
        finally{
            rs.close();
            stmt.close();
            con.close();
        }
    }

    /**
     * PUT method for updating or creating an instance of GetRunIDFromManifestIDResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
}
