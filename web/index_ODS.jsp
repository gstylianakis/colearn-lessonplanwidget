<%-- 
    Document   : index
    Created on : 22 Απρ 2011, 3:20:54 μμ
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
    session.setAttribute("username",null);
        
    //session.setAttribute("enroll","false");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--jquery-->
        <link href="javascript_css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="javascript_css/jquery-1.4.1.min.js"></script>
        <script src="javascript_css/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="javascript_css/ui/ui.core.js"></script>
        <script type="text/javascript" src="javascript_css/ui/ui.draggable.js"></script>

        <script type="text/javascript">            
            $().ready(function() {
                function gup( name ){
                    try{
                        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
                        var regexS = "[\\?&]"+name+"=([^&#]*)";
                        var regex = new RegExp( regexS );
                        var results = regex.exec( window.location.href );
                        if( results == null )
                            return -1;
                        else
                            return results[1];
                    }
                    catch(e){
                        throw "error while reading username "+e
                    }
                }
                var user = -1;
                user = gup('user'); 
                
                
                    $('#accept_access_dialog').dialog({
                        autoOpen: true,
                        draggable: false,
                        height:270,
                        width:350,
                        modal: true,
                        title: 'Welcome to CS authoring tool',
                        open: function() { jQuery('.ui-dialog-titlebar-close').hide(); },
                        buttons: {
                            
                            "Accept": function () {
                                $("#loginloader").show();

                                $.ajax({
                                    type: "POST",
                                    url: "userValidation.jsp?password="+user+"&username="+user,
                          // Send the login info to this page
                                    statusCode: {

                                        404: function() {
                                            $('#loginloader').hide();
                                            alert('page not found');
                                        }
                                    },
                                    success: function(msg){
                                        $('#loginloader').hide();

                                        if(msg.indexOf('error')==-1){ // LOGIN OK?                                            
                                            $('#accept_access_dialog').dialog("close");                                            
                                            initBase64()
                                            user = base64Encode(user);     
                                            if(gup('project_id')!=-1)
                                                window.location = "wizard.jsp?user="+user+"&ODSGroupId="+gup('ODSGroupId')+"&projectId="+gup('project_id')+"&deployed="+gup("deployed");
                                            else
                                                window.location = "wizard.jsp?user="+user+"&projectId=new&ODSGroupId="+gup('ODSGroupId');
                                        }
                                        else{
                                            alert("The username or password you entered is incorrect");
                                            $('#psw').val("");
                                            $('#psw').focus();

                                        }

                                    },
                                    error: function(){
                                        $("#loginloader").hide();
                                        alert("error while requesting login")

                                    }

                                });
                             }
                             
                        }
                    });
            })
           function initBase64() {
                enc64List = new Array();
                dec64List = new Array();
                var i;
                for (i = 0; i < 26; i++) {
                    enc64List[enc64List.length] = String.fromCharCode(65 + i);
                }
                for (i = 0; i < 26; i++) {
                    enc64List[enc64List.length] = String.fromCharCode(97 + i);
                }
                for (i = 0; i < 10; i++) {
                    enc64List[enc64List.length] = String.fromCharCode(48 + i);
                }
                enc64List[enc64List.length] = "+";
                enc64List[enc64List.length] = "/";
                for (i = 0; i < 128; i++) {
                    dec64List[dec64List.length] = -1;
                }
                for (i = 0; i < 64; i++) {
                    dec64List[enc64List[i].charCodeAt(0)] = i;
                }
        }

        function base64Encode(str) {
            var c, d, e, end = 0;
            var u, v, w, x;
            var ptr = -1;
            var input = str.split("");
            var output = "";
            while(end == 0) {
                c = (typeof input[++ptr] != "undefined") ? input[ptr].charCodeAt(0) :
                    ((end = 1) ? 0 : 0);
                d = (typeof input[++ptr] != "undefined") ? input[ptr].charCodeAt(0) :
                    ((end += 1) ? 0 : 0);
                e = (typeof input[++ptr] != "undefined") ? input[ptr].charCodeAt(0) :
                    ((end += 1) ? 0 : 0);
                u = enc64List[c >> 2];
                v = enc64List[(0x00000003 & c) << 4 | d >> 4];
                w = enc64List[(0x0000000F & d) << 2 | e >> 6];
                x = enc64List[e & 0x0000003F];

                // handle padding to even out unevenly divisible string lengths
                if (end >= 1) {x = "=";}
                if (end == 2) {w = "=";}

                if (end < 3) {output += u + v + w + x;}
            }
            // format for 76-character line lengths per RFC
            var formattedOutput = "";
            var lineLength = 76;
            while (output.length > lineLength) {
              formattedOutput += output.substring(0, lineLength) + "\n";
              output = output.substring(lineLength);
            }
            formattedOutput += output;
            return formattedOutput;
        }

        </script>
    <!---->
        <title>login page</title>
    </head>
    <body>
         
            <div id='accept_access_dialog' class='hidden'>
                <p>Do you accept application to access your data?</p>                                    
                <ul style="float: left;list-style:none;margin-top:15px">
                    <li style="display: none" id="loginloader"><img src="javascript_css/project_team_form/images/loading.gif" /></li>
                </ul>
            </div>
    </body>
</html>
