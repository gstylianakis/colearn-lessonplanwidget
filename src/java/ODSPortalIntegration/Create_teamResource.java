/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ODSPortalIntegration;

import StudentOperationsEJBs.DB_connection;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;

/**
 * REST Web Service
 *
 * @author stylianos
 */
@Path("create_team")
public class Create_teamResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Create_teamResource
     */
    public Create_teamResource() {
    }

    /**
     * Retrieves representation of an instance of ODSPortalIntegration.Create_teamResource
     * @return an instance of java.lang.String
     */
    /*@GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }*/

    /**
     * PUT method for updating or creating an instance of Create_teamResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @POST
    @Consumes("application/json")
    public void teamData(String team_content)throws Exception {
        
        DB_connection con = new DB_connection();
        PreparedStatement pstmt = null;        
        try{ 
                  
            String group_title = "", group_id = "";
            JSONParser parser = new JSONParser();
            ContainerFactory containerFactory = new ContainerFactory(){
                public List creatArrayContainer() {
                  return new LinkedList();
                }
                public Map createObjectContainer() {
                  return new LinkedHashMap();
                }                        
            };                
            Map json = (Map)parser.parse(team_content, containerFactory);
            Iterator iter = json.entrySet().iterator();
            while(iter.hasNext()){
              Map.Entry entry = (Map.Entry)iter.next();              
              if(entry.getKey().toString().equals("group_id")){
                  group_id = entry.getValue().toString();
              } else{
                  group_title = entry.getValue().toString();
              }
            }                        
            con.getConnection().setAutoCommit(false);            
            pstmt = con.getConnection().prepareStatement("INSERT INTO team (Name, ODS_Node_ID, ODS_Node_Title) VALUES (?,?,?)");
            pstmt.setString(1, group_title+"_"+group_id);
            pstmt.setString(2,group_id);
            pstmt.setString(3,group_title);
            pstmt.executeUpdate();                        
            con.getConnection().commit();
            

        }catch(SQLIntegrityConstraintViolationException e){
            //throw new Exception("group already exists");            
            e.printStackTrace();
        }finally {
            pstmt.close();
            con.getConnection().close();                      
        }
    }    
}
