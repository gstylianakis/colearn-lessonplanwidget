/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package integrationServicesClientJavaBean;

import IntegrationServices.Adapter;
import IntegrationServices.CopperCoreAdapter;
import IntegrationServices.Dispatcher;
import org.apache.log4j.PropertyConfigurator;
import java.util.*;
import java.io.*;
import org.coppercore.clients.Clicc;
import org.coppercore.common.Message;
import org.coppercore.common.MessageList;
import org.coppercore.common.Util;
import org.coppercore.delegate.LDCourseManagerDelegate;
import org.coppercore.delegate.LDEngineDelegate;
import org.coppercore.dto.ValidationResult;

/**
 *
 * @author Administrator
 */
public class integrationClient {
    private Dispatcher dispatcher;
    private Properties configFile;
    private String schemaLocation;
    private String contextUriOffset;
    private String webRoot;


    public integrationClient() throws IOException {
        dispatcher = new Dispatcher();
        configFile  = new Properties();
        configFile.load( new FileInputStream("C:/coppercore.properties"));
        schemaLocation = Util.convertPathToPlatform(configFile.getProperty("schemalocation"));
        contextUriOffset =  Util.removeTrailingSlash(configFile.getProperty("contenturioffset"));
        webRoot = Util.convertPathToPlatform(configFile.getProperty("webroot"));

        
    }

    public Dispatcher create(){
        return this.dispatcher;
    }

    public Adapter getLDAdapter()throws Exception{
        try{
            return this.dispatcher.getLDAdapter();
        }
        catch(Exception e){
            throw new Exception("can't get Adapter\n"+e.toString());

        }
    }
    /*public LDCourseManagerDelegate publish()throws Exception{
        MessageList s ;
        LDCourseManagerDelegate _flddelegate = null;
        try{
           _flddelegate = new LDCourseManagerDelegate();

            //CopperCoreAdapter t = (CopperCoreAdapter)this.dispatcher.getLDAdapter();
            s  = _flddelegate.publishUol("C:/test.zip",schemaLocation,null,null).getMessages();

        }
        catch(Exception e){
            throw e;
        }
        return _flddelegate;//return s;

    }*/
    public MessageList validatePackage()throws Exception{
        MessageList s ;
        //LDEngineDelegate a = new LDEngineDelegate(webRoot, i)
        LDCourseManagerDelegate _flddelegate = null;
        try{
           _flddelegate = new LDCourseManagerDelegate();
            //CopperCoreAdapter t = (CopperCoreAdapter)this.dispatcher.getLDAdapter();
            s  = _flddelegate.validate("C:/prolixUOL.zip",null).getMessages();

        }
        catch(Exception e){
            throw e;
        }
        return s;//return s;

    }
    public String getCourseManager() throws Exception{
         LDCourseManagerDelegate _flddelegate = null;
        String a;
         try{
            _flddelegate = new LDCourseManagerDelegate();
            a = _flddelegate.getVersion();
        }
        catch(Exception e){
            throw e;
        }
        return a;
    }
    public Message[] publish()throws Exception{
        Message[] s ;
        LDCourseManagerDelegate _flddelegate = null;
        try{


            CopperCoreAdapter t = (CopperCoreAdapter)this.dispatcher.getLDAdapter();
            s  = t.publishUOL("C:/prolixUOL.zip",schemaLocation,contextUriOffset,webRoot).getMessageArray();

        }
        catch(Exception e){
            throw e;
        }
        return s;//return s;

    }
}
