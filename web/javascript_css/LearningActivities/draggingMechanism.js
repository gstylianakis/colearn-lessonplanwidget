/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    var to_connect;
    var can_connect = false;
    var to_delete;
    var toBeConnectedNodes = [];

    var connectedFlag = 0;
    var moused = false;
    var mouseX = 0;
    var mouseY = 0;
    
    var SVGDocument = null;
    var SVGRoot = null;
    var TrueCoords = null;
    var GrabPoint = null;
    var DragTarget = null;

    var toolTip = null;
    var SVGViewBox = null;
    var tipBox = null;
    var tipText = null;
    var tipTitle = null;
    var tipDesc = null;
    var lastElement = null;
    var titleText = '';
    var titleDesc = '';
    var edit_image = '';

    function Init(evt){
        
        SVGDocument = document.getElementById('svgPanel').contentDocument//evt.target.ownerDocument;
        //var svgDoc = document.getElementById('svgPanel').contentDocument;

        SVGRoot = SVGDocument.documentElement;
        TrueCoords = SVGRoot.createSVGPoint();
        GrabPoint = SVGRoot.createSVGPoint();

        toolTip = SVGDocument.getElementById('ToolTip');
        tipBox = SVGDocument.getElementById('tipbox');
        tipText = SVGDocument.getElementById('tipText');
        tipTitle = SVGDocument.getElementById('tipTitle');
        tipDesc = SVGDocument.getElementById('tipDesc');
        edit_image = SVGDocument.getElementById('edit_image');

    }
    function Grab(evt){
         // find out which element we moused down on                       
        var target = (evt.currentTarget) ? evt.currentTarget : evt.srcElement;                       
        if(to_delete==true){
            if(target.getAttribute("ObjType")=="activity"){
                var activityObj = (getCurrentActivityList()).getActivities().containsId(evt.currentTarget.getAttribute("id"));
                if(activityObj==null){
                    alert("error while trying to delete activity");
                    evt.stopPropagation();
                    return;
                }
                deleteActivity(activityObj,0)
                evt.stopPropagation();
                return;
            }
            else if(target.getAttribute("ObjType")=="activity-structure"){
                var curr_phase = allPhases.getPhase(document.getElementById('phaseTab').name);
                
                var structureObj = curr_phase.getActivitiesStructuresArray().containsId(evt.currentTarget.getAttribute("id"));
                
                if(structureObj==null){
                    alert("error while trying to delete activity structure");
                    evt.stopPropagation();
                    return;
                }
                dele.deleteAllStructure(structureObj);
                evt.stopPropagation();
                return;
            }
            else if(target.getAttribute("ObjType")=="inclusiveOrPoint" || evt.currentTarget.getAttribute("ObjType")=="syncPoint"){
                deleteGateway(evt.currentTarget);
                evt.stopPropagation();
                return;
            }
            else if(target.getAttribute("ObjType")=="startPoint" ){
                 deleteStartPoint(evt.currentTarget.getAttribute("id"),allPhases.getPhase(document.getElementById('phaseTab').name),1)
                 evt.stopPropagation();
                 return;
            }
            else if(target.getAttribute("ObjType")=="endPoint" ){
                 deleteEndPoint(evt.currentTarget.getAttribute("id"),allPhases.getPhase(document.getElementById('phaseTab').name),1)
                 evt.stopPropagation();
                 return;
            }
           
        }
        try{
            if(DragTarget==null){
                var targetElement = target;
                can_connect = false;

                if(to_connect){
                    var svgdoc = document.getElementById('svgPanel').contentDocument;
                    var drawline = svgdoc.getElementById("drawline");
                    drawline.removeAttribute("visibility");
                    DrawLine_mouseDown(evt); 
                    onMouseClick(evt);
                    return;
                }

                DragTarget = targetElement;      
                
                // move this element to the "top" of the display, so it is (almost)
                //    always over other elements (exception: in this case, elements that are
                DragTarget.parentNode.appendChild( DragTarget );
                // turn off all pointer events to the dragged element, this does 2 things:
                //    1) allows us to drag text elements without selecting the text
                //    2) allows us to find out where the dragged element is dropped (see Drop)
                DragTarget.setAttributeNS(null,'pointer-events', 'none');

                // we need to find the current position and translation of the grabbed element,
                //    so that we only apply the differential between the current location
                //    and the new location
                var transMatrix = DragTarget.getCTM();
                GrabPoint.x = TrueCoords.x - Number(transMatrix.e);
                GrabPoint.y = TrueCoords.y - Number(transMatrix.f);


            }
        }catch(e){
            
            console.log("error in Grab function "+e);
        }
    }
    function Drag(evt){   
        //console.log(drawlined)                  
         GetTrueCoords(evt);
         var svgDoc = document.getElementById('svgPanel').contentDocument;
         var root = svgDoc.getElementsByTagNameNS(svgns,"svg")[0];
   
         if(!to_connect && !to_delete){
            if (DragTarget){
                if(evt.type == "touchmove" && DragTarget.getAttribute("mousemoveControl")=="false")
                    return;
                var newX = TrueCoords.x - GrabPoint.x;
                var newY = TrueCoords.y - GrabPoint.y;  

                // account for the offset between the element's origin and the
                //    exact place we grabbed it... this way, the drag will look more natural
                
                if(DragTarget.getAttribute("ObjType")=="activity"||DragTarget.getAttribute("ObjType")=="activity-structure"){
                    if(newX<=0||newX>=600)
                    //vrika sta akra apo 0 ews 600
                        return;
                    if(newY<=0||newY>=342)
                        //vrika sto panw katw apo
                        return;
                }
                else{//an einai kapoio apo ta alla sxhmata
                    if(newY<=0||newY>=353)
                        //vrika sto panw katw apo
                        return;
                    if(newX<=0||newX>=660)
                    //vrika sta akra apo 0 ews 600
                        return;
                }
                
                // apply a new tranform translation to the dragged element, to display
                //    it in its new location
            
                DragTarget.setAttribute('transform', 'translate(' + newX + ',' + newY + ')');
                DragTarget.setAttribute('dragX', newX );
                DragTarget.setAttribute('dragY', newY );
                var phase = allPhases.getPhase(document.getElementById('phaseTab').name)
                var activities = getCurrentActivityList();
                var structures = getCurrentStructures();
                var connections = activities.getConnectionsArray();
                var shortSyncPointsList = phase.getJoinGateways();
                var shortOrPointsList = phase.getInclusiveOrGateways();
                var textOfOrCondition=null;
  
                var tmpConnections = activities.getConnectionsArray().slice();
                for(var i=0;i<tmpConnections.length;i++){
                    var con = tmpConnections[i];                    
                    //kounas to 'from' mias sindesis kai psaxenis gia to 'to' stoixeio wste na enimerwseis tin sindesi
                    if(con.getAttribute("from")==DragTarget.getAttribute("id")){// || con.getAttribute("to")==e.currentTarget.getAttribute("id")){                    
                        var objectToConn = null;
                        textOfOrCondition=null;//tha einai to text panw sto connection
                        if(DragTarget.getAttribute("ObjType")=="inclusiveOrPoint"){
                            if(navigator.userAgent.indexOf("Chrome")!=-1)
                                textOfOrCondition =  root.getElementById("textRefered_"+con.getAttribute("id"));
                        }

                        if(con.getAttribute("toType")=="activity"){
                            for(var j=0;j<activities.getActivities().length;j++){
                                var activity = activities.getActivities()[j];
                                if(activity.getId()==con.getAttribute("to")){
                                    objectToConn = activity.get_Shape();
                                    break;
                                }
                            }
                        }
                        else if(con.getAttribute("toType")=="syncPoint"){
                            objectToConn = null;
                            for(j=0;j<shortSyncPointsList.length;j++){
                                var syncPoint = shortSyncPointsList[j];
                                if(syncPoint.getShape().getAttribute("id")==con.getAttribute("to")){
                                    objectToConn = syncPoint.getShape();
                                    break;
                                }
                            }
                        }
                        else if(con.getAttribute("toType")=="inclusiveOrPoint"){
                            objectToConn = null;
                            for(j=0;j<shortOrPointsList.length;j++){
                                var orPoint = shortOrPointsList[j];
                                if(orPoint.getShape().getAttribute("id")==con.getAttribute("to")){
                                    objectToConn = orPoint.getShape();                                    
                                    break;
                                }
                            }
                        }
                        else if(con.getAttribute("toType")=="activity-structure"){
                            objectToConn = null;
                            for(j=0;j<structures.length;j++){
                                var structure = structures[j];
                                if(structure.getId("id")==con.getAttribute("to")){
                                    objectToConn = structure.getShape();
                                    break;
                                }
                            }
                        }
                        else{//psaxnw to end point
                          objectToConn = phase.getEndPoint().containsAttributeId(con.getAttribute("to"));
                        }
                  
                        if(objectToConn!=null){
                         
                            removeConnection(con,connections);
                            connections.push(connection(DragTarget,objectToConn));
                            if(textOfOrCondition!=null){
                                textOfOrCondition.getElementsByTagName("textPath")[0].setAttribute("href","#"+con.getAttribute("id"))

                            }

                        }
                    }
                    //kounas to 'to' mias sindesis kai psaxenis gia to 'from' stoixeio wste na enimerwseis tin sindesi
                    else if(con.getAttribute("to")==DragTarget.getAttribute("id")){
                        textOfOrCondition = null;//einai to text panw sto connection
                        var objectFromConn = null;
                        if(con.getAttribute("fromType")=="activity"){
                            for(j=0;j<activities.getActivities().length;j++){
                                activity = activities.getActivities()[j];
                                if(activity.getId()==con.getAttribute("from")){
                                    objectFromConn = activity.get_Shape();
                                    break;
                                }
                            }
                        }
                        else if(con.getAttribute("fromType")=="syncPoint"){
                            syncPoint = null;
                            objectFromConn = null;
                            for(j=0;j<shortSyncPointsList.length;j++){
                                syncPoint = shortSyncPointsList[j];
                                if(syncPoint.getShape().getAttribute("id")==con.getAttribute("from")){
                                    objectFromConn = syncPoint.getShape();
                                    break;
                                }
                            }
                        }
                        else if(con.getAttribute("fromType")=="inclusiveOrPoint"){
                            orPoint = null;
                            objectFromConn = null;
                            for(j=0;j<shortOrPointsList.length;j++){
                                orPoint = shortOrPointsList[j];
                                if(orPoint.getShape().getAttribute("id")==con.getAttribute("from")){
                                    objectFromConn = orPoint.getShape();
                                    if(navigator.userAgent.indexOf("Chrome")!=-1)
                                        textOfOrCondition =  root.getElementById("textRefered_"+con.getAttribute("id"));
                                    break;
                                }
                            }
                        }
                        else if(con.getAttribute("fromType")=="activity-structure"){
                            structure = null;
                            objectFromConn = null;
                            for(j=0;j<structures.length;j++){
                                structure = structures[j];
                                if(structure.getId("id")==con.getAttribute("from")){
                                    objectFromConn = structure.getShape();
                                    break;
                                }
                             }
                        }
                        else{
                            objectFromConn = phase.getStartPoint();
                        }
                        if(objectFromConn!=null){
                            removeConnection(con,connections);                            
                            connections.push(connection(objectFromConn,DragTarget));
                            if(textOfOrCondition!=null){
                                textOfOrCondition.getElementsByTagName("textPath")[0].setAttribute("href","#"+con.getAttribute("id"))
                            }
                        }
                    }
                }
               
                /*if(DragTarget.getAttribute("ObjType")!=null){
                    if(DragTarget.getAttribute("ObjType")=="activity"){
                        edit_image.setAttribute("display","block");
                        edit_image.setAttribute("x",0)
                        edit_image.setAttribute("y",0)
                         console.log(newX)
                    }
                }*/
                
            }
        }
    }
    function Drop(evt){
      
         // if we aren't currently dragging an element, don't do anything
        var targetElement = evt.currentTarget;
      
      
        if ( DragTarget ){
            // turn the pointer-events back on, so we can grab this item later
            DragTarget.setAttributeNS(null,'pointer-events', 'all');                  
            DragTarget = null;            
        }
        //console.log(evt.type)
        moused = false;            
        var svgdoc = document.getElementById('svgPanel').contentDocument;
        var drawline = svgdoc.getElementById("drawline");

        if(targetElement.getAttribute("ObjType")=="startPoint"){
            can_connect = false;
            drawline.setAttribute("visibility", "hidden");
        }
        if(evt.type != "touchend"){
            if(can_connect){          
                can_connect = false;
                drawline.setAttribute("visibility", "hidden");
                onMouseClick(evt);//o xristis exei afisei to koumpi tou mouse kai kaleitai i sinartisi pou tha
            }                       //ftiaksei to connection
            else{
                toBeConnectedNodes = [];
                connectedFlag=0;
            }
        }
    }

      
      function GetTrueCoords(evt){
         // find the current zoom level and pan setting, and adjust the reported
         //    mouse position accordingly
         
         var newScale = SVGRoot.currentScale;
         var translation = SVGRoot.currentTranslate;  
          if(evt.type=="touchmove"){
            TrueCoords.x = (evt.touches[0].clientX); //- translation.x)/newScale;
            TrueCoords.y = (evt.touches[0].clientY); //- translation.y)/newScale;
           
         } else   if(evt.type=="mousemove"){
            TrueCoords.x = (evt.clientX - translation.x)/newScale;
            TrueCoords.y = (evt.clientY - translation.y)/newScale;
         }
         
      }


    function HideTooltip( evt )
      {
         if(navigator.userAgent.indexOf("Chrome")!=-1)
             return;
         toolTip.setAttributeNS(null, 'visibility', 'hidden');
      };


      function ShowTooltip( evt )
      {
          //if not moving show edit delete
          
         GetTrueCoords( evt );
         
//o chrome ta deixnei apo monos toy
         if(navigator.userAgent.indexOf("Chrome")!=-1 || firefoxVersion>=4)
             return;         
         
         var tipScale = 1/SVGRoot.currentScale;
         var textWidth = 0;
         var tspanWidth = 0;
         var boxHeight = 20;

         tipBox.setAttributeNS(null, 'transform', 'scale(' + tipScale + ',' + tipScale + ')' );
         tipText.setAttributeNS(null, 'transform', 'scale(' + tipScale + ',' + tipScale + ')' );
         //put on top of other element
         toolTip.parentNode.appendChild( toolTip );
         var titleValue = '';
         var descValue = '';
         var targetElement = evt.target;
         if ( lastElement != targetElement )
         {
            var targetTitle = targetElement.getElementsByTagName('title').item(0);
            if ( targetTitle )
            {
               // if there is a 'title' element, use its contents for the tooltip title
               titleValue = targetTitle.firstChild.nodeValue;
            }

            var targetDesc = targetElement.getElementsByTagName('desc').item(0);
            if ( targetDesc )
            {
               // if there is a 'desc' element, use its contents for the tooltip desc
               descValue = targetDesc.firstChild.nodeValue;

               if ( '' == titleValue )
               {
                  // if there is no 'title' element, use the contents of the 'desc' element for the tooltip title instead
                  titleValue = descValue;
                  descValue = '';
               }
            }

            // if there is still no 'title' element, use the contents of the 'id' attribute for the tooltip title
            if ( '' == titleValue )
            {
                return;
               //titleValue = targetElement.getAttributeNS(null, 'id');
            }

            // selectively assign the tooltip title and desc the proper values,
            //   and hide those which don't have text values
            //
            var titleDisplay = 'none';
            if ( '' != titleValue )
            {
               tipTitle.firstChild.nodeValue = titleValue;
               titleDisplay = 'inline';
            }
            tipTitle.setAttributeNS(null, 'display', titleDisplay );


            var descDisplay = 'none';
            if ( '' != descValue )
            {
               tipDesc.firstChild.nodeValue = descValue;
               descDisplay = 'inline';
            }
            tipDesc.setAttributeNS(null, 'display', descDisplay );
         }

         // if there are tooltip contents to be displayed, adjust the size and position of the box
         if ( '' != titleValue )
         {
            var xPos = TrueCoords.x + (10 * tipScale);
            var yPos = TrueCoords.y + (10 * tipScale);

            //return rectangle around text as SVGRect object
            var outline = tipText.getBBox();
            tipBox.setAttributeNS(null, 'width', Number(outline.width) + 10);
            tipBox.setAttributeNS(null, 'height', Number(outline.height) + 10);

            // update position
            toolTip.setAttributeNS(null, 'transform', 'translate(' + xPos + ',' + yPos + ')');
            toolTip.setAttributeNS(null, 'visibility', 'visible');
         }
      };
    
    Array.prototype.indexOfSVGObj = function(id){
        for (var i=0; i < this.length; i++) {
            if (this[i].getAttribute("id") == id)
                return i;
        }
        return -1;
    }