//var xmlhttp;
var contentID;
var loadingGifID;

function loadContent()
{
    try{
        
        var lessonsParticipated = $("#lessons").val();
        
        var count=0,query=" select Name,LastName,Email from lessonplan_widget.students ";

        if(lessonsParticipated!=null){
            query = "select b.student_id from ("+
                    " select a.student_id, count(student_id)as countStud from"+
                    " (select * from lessonplan_widget.lesson_participation where "
            for(var i=0; i<lessonsParticipated.length;i++){
                if(count>0)
                    query += " or "
                query += "lesson_id = (SELECT Lesson_id FROM lessonplan_widget.lessons where Name='"+lessonsParticipated[i]+"')"

                count++;
            }
            query += ")as a group by a.student_id"
                  + ")as b where countStud>"+(count-1);

            query = "select Name,LastName,Email from (lessonplan_widget.students inner join ("+query+")as c on c.student_id=students.Name)";
        }
        if($('input[name="sex"]:checked').val()!="all")
            query += "where sex='"+$('input[name="sex"]:checked').val()+"'";
        
        
    
    }

    catch(e){
        alert(e)
    }
    var xmlhttp=GetXmlHttpObject();
    $("#browsingloader").show();
    if (xmlhttp==null)
    {
        alert ("Your browser does not support Ajax HTTP");
        return;
    }

    var url="javascript_css/campusOperations/studentsBrowsing.jsp?query="+query;
    //url=url+"?q="+str;

    contentID = "StudentContent";
    loadingGifID = "#browsingloader";
    xmlhttp.onreadystatechange = function(){
         if (xmlhttp.readyState==4 && xmlhttp.status == 200){
            $("#StudentContent").html(xmlhttp.responseText);
            $("#browsingloader").hide();
        }
    }
    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);
}

function getOutput()
{
    if (xmlhttp.readyState==4){
        
        document.getElementById(contentID).innerHTML=xmlhttp.responseText;
        $(loadingGifID).hide();
    }
}

function GetXmlHttpObject()
{
    if (window.XMLHttpRequest)
    {
       return new XMLHttpRequest();
    }
    if (window.ActiveXObject)
    {
      return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}

function loadServices()
{
    var xmlhttp=GetXmlHttpObject();
    $("#servicesloading").show();
    if (xmlhttp==null)
    {
        alert ("Your browser does not support Ajax HTTP");
        return;
    }

    var url="javascript_css/campusOperations/servicesBrowsing.jsp?option=activity";
    //url=url+"?q="+str;
    contentID = "ServicesContent";
    loadingGifID = "#servicesloading";
    xmlhttp.onreadystatechange= function(){
         if (xmlhttp.readyState==4 && xmlhttp.status == 200){
            $("#ServicesContent").html(xmlhttp.responseText);
            $("#servicesloading").hide();
        }
    }
    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);
}

function loadStructureServices()
{
    var xmlhttp=GetXmlHttpObject();
    $("#structureservicesloading").show();
    if (xmlhttp==null)
    {
        alert ("Your browser does not support Ajax HTTP");
        return;
    }

    var url="javascript_css/campusOperations/servicesBrowsing.jsp?option=structure";
    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState==4 && xmlhttp.status == 200){
            $("#structureServicesContent").html(xmlhttp.responseText);
            $("#structureservicesloading").hide();
        }
    }
    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);
}

function searchingCriteria(){
    var display = $("#learnerSearchCriteria").css("display")
    
    if(display=="none")
        ShowHide('learnerSearchCriteria','show')
    else
        ShowHide('learnerSearchCriteria','hide')

}