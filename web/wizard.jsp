<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page  contentType="text/html" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <%
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
        
        if(session.getAttribute("username")==null){
             response.sendRedirect("index.jsp");
        }
    %>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <title>Lesson Plan Wizard Application</title>
    <!--SVG Implementing Library-->
    <script src="javascript_css/svg_src/svg.js" data-path="javascript_css/svg_src/" data-htc-filename="svg-htc.jsp"></script>
    
    <!---->
    <!--jquery-->
        
        <link href="javascript_css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="javascript_css/jquery-1.4.1.min.js"></script>
        <script src="javascript_css/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="javascript_css/ui/ui.core.js"></script>
        <script type="text/javascript" src="javascript_css/ui/ui.draggable.js"></script>
        
        <script src="http://malsup.github.com/jquery.form.js"></script> 
    <!---->
    
    <!--wizards-->
        <!--<link href="javascript_css/style_wizard_vertical.css" rel="stylesheet" type="text/css"/>-->
        <link href="javascript_css/style_wizard.css" rel="stylesheet" type="text/css"/>
        <script src="javascript_css/VerticalWizard.js" type="text/javascript"></script>
    <!---->
    <!--show information on mouseover-->
    <!--<script type='text/javascript' src='javascript_css/jqueryFileTree/show_info_onMouseOver.js'></script>-->
    <!--end of showing information on mouse over scripts-->
    
    <script src="javascript_css/jqueryFileTree/jqueryFileTree.js" type="text/javascript"></script>
    <script src="javascript_css/jqueryFileTree/jqueryTree/jquery.treeview.js" type="text/javascript"></script>
    <link href="javascript_css/jqtransformplugin/jqtransform.css" rel="stylesheet" type="text/css" media="screen" />
    <script type="text/javascript" src="javascript_css/jqtransformplugin/jquery.jqtransform.js"></script>
    <link href="javascript_css/jqueryFileTree/jqueryFileTree.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="javascript_css/jqueryFileTree/jqueryTree/jquery.treeview.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="javascript_css/tree.css" rel="stylesheet" type="text/css" media="screen" />
        
    <script src="javascript_css/campusOperations/campusOperation.js" type="text/javascript"></script>
    <link href="javascript_css/campusOperations/serviceBrowsing.css" rel="stylesheet"></link>
    <script src="javascript_css/campusOperations/serviceManagment.js" type="text/javascript"></script>
    <!--group management-->
    <!--group definition-->
    <link href="javascript_css/project_team_form/definingTeamGroups.css" rel="stylesheet" type="text/css"/>
    <link href="javascript_css/project_team_form/RoleDefinition.css" rel="stylesheet" type="text/css"/>
    <script src="javascript_css/project_team_form/teamCreationOperations.js" type="text/javascript"></script>
    <script src="javascript_css/project_team_form/RoleCreationOperations.js" type="text/javascript"></script>
    <!--end group definition-->
    <!--properties-->
    <link rel="stylesheet" href="javascript_css/propertiesManagement/properties.css" type="text/css" />
    <link rel="stylesheet" href="javascript_css/ResourcesManagement/resources.css" type="text/css" />
    <script src="javascript_css/propertiesManagement/propertiesManagement.js" type="text/javascript"></script>
    <!--group browsing-->
    <link href="javascript_css/project_team_form/teamBrowsing.css" rel="stylesheet" type="text/css"/>
    <link href="javascript_css/project_team_form/creatingTable.css" rel="stylesheet" type="text/css"/>
    <!--end group browsing-->
    <!-- colorbox to apply project team form -->
        <script src="javascript_css/project_team_form/jquery.colorbox.js" type="text/javascript"></script>
        <link href="javascript_css/project_team_form/colorbox.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- end colorbox -->
    <!--validate form-->
    <script src="javascript_css/validate_forms/messages.js" type="text/javascript"></script>
    <script src="javascript_css/validate_forms/validateStudent.js" type="text/javascript"></script>
    <link href="javascript_css/validate_forms/messages.css" rel="stylesheet" type="text/css" media="screen" />
    <!--end validate form-->
    <!--end team group management-->
    <!--Europeana-->
    <script src="EuropeanaServices/js/europeanaSearchServiceJS.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="EuropeanaServices/js/ui/jquery.paginate.js"></script>
    <link href="EuropeanaServices/css/style.css" rel="stylesheet" type="text/css"></link>
    <!--end of europeana scripts-->
    <!--paginate-->
    <script type="text/javascript" src="EuropeanaServices/js/paginate.js"></script>
    <!--styling boxes and radio buttons-->
    <script src="javascript_css/LearningActivities/stylingCheckBoxesRadioButtons.js" type="text/javascript"></script>
    <script src="javascript_css/LearningActivities/operationsOnSVG.js" type="text/javascript"></script>
    <!--end of styling boxes and radio buttons-->
    

    <!--word editor-->
    <link rel="stylesheet" href="javascript_css/Editor/jquery.wysiwyg.css" type="text/css" />
    <script type="text/javascript" src="javascript_css/Editor/jquery.wysiwyg.js"></script>

   
    <!--sychronization-->
    <script type="text/javascript" src="javascript_css/LearningActivities/gateway.js"></script>
    <!--activity Panel & Phases-->
    <link rel="stylesheet" href="javascript_css/LearningActivities/activities.css" type="text/css" />
    <!--annotations-->
    <script src="javascript_css/LearningActivities/annotation.js" type="text/javascript"></script>
    
    <!--end of activity panel-->
    <!--<script type="text/javascript" src="javascript_css/WordEditor/nicEdit.js"></script>-->
    <!--Phases Management-->
    <link rel="stylesheet" href="javascript_css/Phases/phases.css" type="text/css" />
    <script type = "text/javascript" src="javascript_css/Phases/phasesManagement.js"></script>
    <script type = "text/javascript" src="javascript_css/Phases/conditionsView.js"></script>
    <script type = "text/javascript" src="javascript_css/Phases/completeConditions.js"></script>
    <script type = "text/javascript" src="javascript_css/Phases/notifications.js"></script>
    <script type = "text/javascript" src="javascript_css/Phases/Feedback.js"></script>
    <!--end of phases Management-->
    <!--end of word editor-->
   
    <!--vector graphics library-->
    <script type="text/javascript" src="javascript_css/vector graphics library/raphaelVectorGraphicLibrary.js"></script>
    <script type="text/javascript" src="javascript_css/vector graphics library/graffle.js"></script>
    
    <!--end of vector graphic libarary-->
    <!--blockUI-->
    <script type="text/javascript" src="javascript_css/learningDesign/blockUI.js"></script>
    <!--learningDesign export&import-->
    <script type="text/javascript" src="javascript_css/learningDesign/generateLearningDesign.js"></script>
    <script type="text/javascript" src="javascript_css/learningDesign/loadLearningDesign.js" charset="utf-8"></script>
    <script type="text/javascript" src="javascript_css/learningDesign/uploadFile.js"></script>
    <!---->
    <!--Resources-->
    <script type="text/javascript" src="javascript_css/ResourcesManagement/manageResources.js"></script>
    <!--end Resources-->
    <!--environment-->
    <script type="text/javascript" src="javascript_css/LearningActivities/activity_environments.js"></script>
    <!--end of environment-->
    <!--save design-->
    <script type="text/javascript" src="javascript_css/learningDesign/saveTofile.js"></script>
    <script type="text/javascript" src="javascript_css/learningDesign/generateGraphicSchema.js"></script>
    <!--activities operations-->
    <style type="text/css"> #educationalProcessOperationPanel p {cursor:move}</style>
    <script type="text/javascript" src="javascript_css/LearningActivities/ActivitiesOperations.js"></script>
    <script type="text/javascript" src="javascript_css/LearningActivities/draggingMechanism.js"></script>
    <!--end of activities operations-->
     <!--activity structures-->
    <script type="text/javascript" src="javascript_css/LearningActivities/activityStructure.js"></script>
    <!--end of activity structures-->
    <!--tooltip-->
    <script type="text/javascript" src="javascript_css/jqtransformplugin/beutyTips.js"></script>
    <!--xmpp libs & createNode code
    <script type="text/javascript" src="javascript_css/xmppSetUp/strophe/strophe.js"></script>
    <script type="text/javascript" src="javascript_css/xmppSetUp/strophe/flXHR.js"></script>
    <script type="text/javascript" src="javascript_css/xmppSetUp/strophe/strophe.flxhr.js"></script>
    <script type="text/javascript" src="javascript_css/xmppSetUp/xmppSetUp.js"></script>
    -->
    <!--tutorial-->
    <script type="text/javascript" src="javascript_css/tutorial.js"></script>
    <!--multiple selection-->
    <script type="text/javascript" src="javascript_css/jquery.asmselect.js"></script>
    <link rel="stylesheet" type="text/css" href="javascript_css/jquery.asmselect.css" />
    <!--undoManagment-->
    <script type="text/javascript" src="javascript_css/undoManager/undoActionList.js"></script>

    <script type="text/javascript">

        var teams = new Array();
        var learnerArray=[];
        var staffArray=[];
        var roleBeingUse = [];
        var descriptionItems = [];
        var prerequisitesItems = [];
        var learningObjectivesItems=[];
        var activityCompletionFeedback = [];
        var LearningObjectItems = [];
        var structureLearningObjectItems=[];
        var structureInformationItems = [];
        var xPositionOfnewActivity = 0;
        var yPositionOfnewActivity = 0;
        var allPhases;
        var completeActivities = null;
        var notificationArray = [];
        var activityCompletionNotificationArray = [];
        var phaseFeedBackItem = [];
        var serviceArray=[];
        var structureserviceArray=[];
        var firefoxVersion = 0;
        var ManifestID = -1;
        var user = "";
        var author = "";
        var ODSGroupID = -1;
        
        try{
            $().ready(function() {
                try{
                    
                    if(gup("deployed")==1){                        
                        $("input").each(function(){                            
                          if($(this).attr("value")=="Deploy Design")
                              $(this).css("display","none");
                          if($(this).attr("value")=="Save Design")
                              $(this).css("display","none");
                        });                        
                        $("#menu_deploy").css('display','none');
                        $("#save_designSpan").css('display','none');
                    } 
                    tutorialTab.init();
                    menuTab.init();
                    $("#resizeWikiMindMap").click(function(){
                       
                        if($("#OntologySide").width()=="900")
                            $("#OntologySide").animate({width:'560px',height:'500px'},200)
                        else
                            $("#OntologySide").animate({width:'900px',height:'650px'},200)
                    });

                    $("select[multiple]").asmSelect({
                        sortable: true,
                        addItemTarget: 'top'
                    });

                    try{
                        
                        user =  gup( 'user' );
                        if(user==-1){
                            window.location="index.jsp";
                            return;
                        }

                        initBase64();
                        user = base64Decode(user)
                        ManifestID = gup('projectId')
                        if(ManifestID!='new' && ManifestID!=-1){
                            $.blockUI(); 
                            loadLD.loadLearningDesign(ManifestID);
                        }
                        ODSGroupID = gup('ODSGroupId');                        
                        
                        if(ODSGroupID !== -1){                            
                            loadLD.loadODSTeam(ODSGroupID);
                        }
                        
                    }
                    catch(e){
                        console.log(e)
                        window.location="index.jsp";
                            return;
                    }
                    $('#endPointInfo').bt('Determines the end of the learning process. When end point is reached all activities within the process are terminated', {trigger: 'click', positions: 'left'});
                    $('#startPointInfo').bt('Determines the place where a learning process starts', {trigger: 'click', positions: 'left'});
                    $('#orPointInfo').bt('<img src=\"javascript_css/images/Inclusive_OR.png\" width=\"156\" height=\"85\"/><br/>specifies that one or more of the available paths will be taken if the condition that is defined is true. <a href=\"http://en.bpmn-community.org/tutorials/4/\"target=\'new\'>more info</a>', {trigger: 'click', positions: 'right'});
                    $('#notificationInfo').bt('You can define notifications to be sent to users when activity has complete.<br/> Type the subject, the content of the message, choose users that receive the message and press \'add notification\' button', {trigger: 'click', positions: 'right'});
                    $('#activityServicesMaterialInfo').bt('You can select educational tools/services to be used from users <br/>in order to facillitate educational process.<br/> Additionally author defines the educational material of this activity ',{trigger: 'click', positions: 'right'});
                    $('#structureServicesMaterialInfo').bt('You can select educational tools/services to be used from users <br/>in order to facillitate educational process.<br/> Additionally author defines the educational material of this structure ',{trigger: 'click', positions: 'right'});
                    $("#activityServicesInfo").bt('Select educational tools/services and set up configuration.<br/>In order to save configuration you have to press \'save service config\'.',{trigger: 'click', positions: 'right'});
                    $("#structureServicesInfo").bt('Select educational tools/services and set up configuration.<br/>In order to save configuration you have to press \'save service config\'.',{trigger: 'click', positions: 'right'});
                    var synchGatewayInfo = "<img src=\"javascript_css/images/Split.png\" width=\"156\" height=\"85\"/><br/>used when Process Flows can run in parallel"
                    synchGatewayInfo += "<img src=\"javascript_css/images/Merge.png\" width=\"156\" height=\"85\"/><br/>used as a \'join\' when two or more parallel Process Flows need to join back into a single output flow. <a href=\"http://en.bpmn-community.org/tutorials/4/\"target=\'new\'>more info</a>"
                    
                    $('#joinPointInfo').bt(synchGatewayInfo, {trigger: 'click', positions: 'left'});
                    $('#learningActivityInfo').bt('The activity describes what a role is to do and what environment is available to it within the act', {trigger: 'click', positions: 'right'});
                    $('#structureActivityInfo').bt('An activity-structure aggregates a set of related activities into a single structure', {trigger: 'click', positions: 'left'});

                    $('#selectOptionInfo').bt('select this option in order to move the objects placed in the panel', {trigger: 'click', positions: 'left'});
                    $('#connectOptionInfo').bt('select this option and move the mouse cursor -keep the mouse clicked- above the objects you want to connect.<p>By connecting objects you create a flow that  starts at a start event, travels through activities/activity structures, and then ends at an end event.</p>', {trigger: 'click', positions: 'right'});
                    $('#deleteOptionInfo').bt('select this option and click the mouse cursor on the object you want to delete.', {trigger: 'click', positions: 'left'});
                    $('#actInfo').bt('The teaching-learning process is modelled in the method on the notion of a theatrical play. A play has acts, and in each act, activities and activity structures take place.Each activity is carried out by one or more roles. The acts in a play follow each other in a sequence.', {trigger: 'click', positions: 'left'});
                    $('#roleInfo').bt('Define a role that a person gets in the teaching-learning process. Each activity/activity structure is a perfored by one or more roles.', {trigger: 'click', positions: 'left'});
                    $('#groupsInfo').bt('Create groups of students that will participate in the learning design.<br/>Each student of the group is connected to a role.', {trigger: 'click', positions: 'left'});


                    /*$('#login_dialog').dialog({
                        autoOpen: true,
                        draggable: false,
                        modal: true,
                        title: 'login to LD authoring tool',
                        open: function() { jQuery('.ui-dialog-titlebar-close').hide(); },
                        buttons: {
                            "Connect": function () {
                                $("#loginloader").show();

                                $.ajax({
                                    type: "POST",
                                    url: "userValidation.jsp?password="+$('#psw').val()+"&username="+$('#jid').val(),
                          // Send the login info to this page
                                    statusCode: {
                                        
                                        404: function() {
                                            $('#loginloader').hide();
                                            alert('page not found');
                                        }
                                    },
                                    success: function(msg){
                                        $('#loginloader').hide();
                                        
                                        if(msg.indexOf('error')==-1){ // LOGIN OK?
                                            user = $('#jid').val();
                                            $('#login_dialog').dialog("close");
                                        }
                                        else{
                                            alert("The username or password you entered is incorrect");
                                            $('#psw').val("");
                                            $('#psw').focus();
                                            
                                        }

                                    },
                                    error: function(){
                                        $("#loginloader").hide();
                                        alert("error while requesting login")

                                    }
                                   
                                });
                             }
                        }
                    });*/

                    ManifestID = "manifest-"+uniqid();
                    
                    //upload.init();//kanw tin magkia gia to upload files, diladi trexw tin sinartisi pou kanei dinaton to mi reload tis selidas otan kanw upload files
                    /*$('iframe#upload_target').load(function() {
                        try{                                                         
                            if($(this).document().body==null)
                                return;
                            if( $(this).document().body.childNodes[0]==null)
                                return;

                            var Server_response = $(this).document().body.childNodes[0].textContent;                        

                            Server_response = Server_response.substr(0, Server_response.length-2)              
                            var Server_responseStatus = Server_response.substr(0,Server_response.indexOf("-"));
                            var submitedButton = Server_response.substr(Server_response.indexOf("-")+1);

                            if(Server_responseStatus == "saved"){
                                $("#"+submitedButton+"LoadGif").css("display","none");

                                $("#successUpload"+submitedButton).attr("class","success");                            
                                $("#successUpload"+submitedButton).html("file uploaded succesfully (click to 'add item') <span class='exit' onclick=\"$(this).parent().css('display','none')\">exit</span>");
                                $("#successUpload"+submitedButton).css("display","block");
                                $('#ExistingFile'+submitedButton+'Button').attr('name','click')
                            }
                            else if(Server_responseStatus=="error"){
                                 $("#"+submitedButton+"LoadGif").css("display","none");
                                 $("#successUpload"+submitedButton).attr("class","error");
                                 $("#successUpload"+submitedButton).html("error while uploading file<span class='exit' onclick=\"$(this).parent().css('display','none')\">exit</span>");
                                 $("#successUpload"+submitedButton).css("display","block");
                            }
            
                        } catch(e){
                            alert(e);
                        }
                    });*/
                    if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){ //test for Firefox/x.x or Firefox x.x (ignoring remaining digits);
                        firefoxVersion = new Number(RegExp.$1) // capture x.x portion and store as a number
                    }
                    $('#Phases_Rows').treeview();
                    
                    $('#project_team_list').treeview();

                    $('#smartwizard').smartWizard();
                    //$('#EducationalProcessWizard').wizard();
                    $(".project_team_form").colorbox({width:"50%", inline:true, href:"#projet_team_inputForm",
                        onLoad:function(){
                            if(learnerArray.getRoles().length==0 && staffArray.getRoles().length==0){
                                alert('you must first define roles')
                                $(".project_team_form").colorbox.close()
                            }
                            $("#team_prevName").val("");
                        }
                    });
                                       
                    $("#datepicker").datepicker({showOn: 'button', buttonImage: 'javascript_css/images/calendar.gif', buttonImageOnly: true});
                    $(".jqtransform").each(function(){
                    
                        $(this).jqTransform();                        
                        $(this).show();
                    })
                    //$("#SubjectAndTeamDefintionProcessTabs").tabs();
                    initializeGroups(new Array());
                    learnerArray = initializeRoleArrays();
                    staffArray = initializeRoleArrays();
                    //var role =
                    allPhases = initializePhasesArray();
                    completeActivities = initCompleteActivitiesCondition();
                    
                    $("#learnerOpt").colorbox({width:"50%", inline:true, href:"#roleDefinition",
                        onLoad:function(){
                            $("#prevLearnerName").val("");
                            $('#Role_name').val("");
                            $('#Role_name').val("");
                            $('#maxRoleNum').val("");
                            $('#minRoleNum').val("");
                            $('#roleConstraint_yes').attr("checked",true)
                            $('#roleConstraint_no').attr("checked",false)

                        }
                    });
                    $("#PropertiesOpt").colorbox({width:"50%", inline:true, href:"#PropertiesDefinition",
                        onLoad:function(){
                            $("#propertyPrevValue").val("");
                            document.getElementById("PropertyName").value = "";                                                       
                            document.getElementById("PropertyType").value = "String";
                            $("#propertyValue").find("select").removeAttr("class");
                            $("#propertyValue").find("select").css('display','none');
                            $("#PropValue").css('display','block');
                            $("#PropValue").unbind();
                            $("#PropValue").val("");
                            $("#PropValue").addClass("PropertyValue");
                            
                        }
                    });
                    
                    $("#PhaseOpt").colorbox({width:"70%",height:"68%", inline:true, href:"#PhaseDefinition",
                         onLoad:function(){
                                
                                document.getElementById('PhaseOpenMode').value = 'create';
                                document.getElementById('prevPhaseName').value="";
                                document.getElementById("PhaseTitle").value="";
                                document.getElementById("NoCondition").checked = true;//$("#CompleteActivities").attr("checked",true);
                                showCompletionPrereq("NoCondition","");//showCompletionPrereq("CompleteActivities","");
                                completeActivities = initCompleteActivitiesCondition();
                                $("#PhaseDays").val("1");
                                $("#phaseHours").val("1");

                                deleteCondition("conditionView");
                                clearNotificationViewer('phase');
                                notificationArray=[];
                                phaseFeedBackItem = [];
                                document.getElementById("notificationSubject").value = "";
                                document.getElementById("notificationContent").value = "";
                                clearRecipients("phase");

                                document.getElementById("PhaseDescriptionTitle").value = "";document.getElementById("createNewPhaseDescription").checked = true;
                                document.getElementById("createNewPhaseDescriptionContent").value = "Enter your description here";
                                document.getElementById("URLPhaseDescriptionContent").value = "http://"
                                showCompletionPrereq('createNewPhaseDescription','PhaseDescription');
                                clearItemViewer("PhaseDescription");
                                clearPropertyRow("changeProperties");
                            }

                    });

                    $("#learningActivity").colorbox(
                    {
                            width:"78%",height:"78%", inline:true,  href:"#learningActivityForm",
                            onLoad:function(){
                                try{
                                    document.getElementById('submitForm').style.display = 'block';
                                    document.getElementById('editForm').style.display = 'none';
                                    $("#successUploadactivityLearningObjectives").css("display","none");
                                    $("#successUploadDescription").css("display","none");
                                    $("#successUploadLearningObject").css("display","none");
                                    $("#successUploadPrerequisites").css("display","none");
                                    $("#successUploadactivityCompletion").css("display","none");
                                    $("#successUploadPhaseDescription").css("display","none");
                                    $("#successUploadstructureLearningObject").css("display","none");
                                    $("#successUploadstructureInformation").css("display","none");
                                    
                                    $(".ActivityLearningDefinitionTabs").tabs();
                                    $(".completionActivityParamTabs").tabs();
                                    
                                    document.getElementById('prevName').value="";//codeAllagi document.getElementById('activityId').value="";

                                    document.getElementById('activityName').value="";
                                    document.getElementById("activityDescriptionTitle").value = "";
                                    document.getElementById("createNewDescriptionContent").value = "Enter your description here"; 
                                    document.getElementById("URLDescriptionContent").value = "http://";
                                    //document.getElementById("ExistingFileDescriptionContent").value = "";
                                    //document.getElementById("ExistingResourceDescriptionContent").value = "";
                                    document.getElementById("none").checked=true;//document.getElementById("Days").value="";document.getElementById("Days").disabled = 'true';document.getElementById("none_li").style.color="#0464BB";document.getElementById("timer").checked=false;document.getElementById("timer_li").style.color="#b0b0b0";document.getElementById("Hours").value="";document.getElementById("Hours").disabled='true';document.getElementById("otherLearningActivity").checked=false;document.getElementById("otherLearningActivity_li").style.color="#b0b0b0";document.getElementById("feedback").checked=false;document.getElementById("feedbackDescription").disabled=true;document.getElementById("feedbackDescription").style.color="#b0b0b0";document.getElementById("feedbackDescription").value="Enter your description here";document.getElementById('Hours').value='';document.getElementById('Days').value='';
                                    $("#createNewDescription").attr('checked','true');
                                    $("#activityDays").val("1");
                                    $("#activityHours").val("1");

                                    document.getElementById("activityCompletionTimeView").style.display = 'none';
                                    document.getElementById("activityCompletionPropertySetView").style.display = 'none';
                                    showCompletionPrereq("createNewDescription",'Description');
                                    clearItemViewer("Description");
                                    //addPropertiesToWhenActivityComplete();

                                    descriptionItems=[];

                                    document.getElementById("activityPrerequisitesTitle").value = "";document.getElementById("createNewPrerequisites").checked = true;
                                    document.getElementById("createNewPrerequisitesContent").value = "Enter your description here";
                                    document.getElementById("URLPrerequisitesContent").value = "http://";
                                    document.getElementById("ExistingFilePrerequisitesContent").value = "";
                                    //document.getElementById("ExistingResourcePrerequisitesContent").value = "";
                                    clearItemViewer("Prerequisites");
                                    showCompletionPrereq('createNewPrerequisites','Prerequisites');
                                    prerequisitesItems=[];

                                    learningObjectivesItems=[];
                                    document.getElementById("createNewactivityLearningObjectives").checked=true;
                                    document.getElementById("activityLearningObjectivesTitle").value = "";
                                    document.getElementById("createNewactivityLearningObjectivesContent").value = "Enter your description here";
                                    document.getElementById("URLactivityLearningObjectivesContent").value = "http://";
                                    //document.getElementById("ExistingFileactivityLearningObjectivesContent").value = "";
                                    //document.getElementById("ExistingResourceactivityLearningObjectivesContent").value = "";
                                    clearItemViewer("activityLearningObjectives");
                                    showCompletionPrereq('createNewactivityLearningObjectives','activityLearningObjectives');

                                    document.getElementById("createNewactivityCompletion").checked=true;
                                    document.getElementById("activityCompletionTitle").value = "";
                                    document.getElementById("createNewactivityCompletionContent").value = "Enter your description here";
                                    document.getElementById("URLactivityCompletionContent").value = "http://";
                                    //document.getElementById("ExistingResourceactivityCompletionContent").value="";
                                    showCompletionPrereq('createNewactivityCompletion','activityCompletion');
                                    clearItemViewer("activityCompletion");
                                    activityCompletionFeedback = [];

                                    activityCompletionNotificationArray = [];
                                    clearNotificationViewer('activity');
                                    document.getElementById("activityCompletionNotificationSubject").value = "";
                                    document.getElementById("activityCompletionNotificationContent").value = "";
                                    clearRecipients("activity");
                                    clearRolesActivityForm();

                                    var curPhase = allPhases.getPhase($("#phaseTab").attr("name"));
                                    if(curPhase.getType()=="activity-structure"){
                                        $(".ActivityLearningDefinitionTabs").tabs("disable",3);
                                        //$(".ActivityLearningDefinitionTabs").tabs("disable",4);
                                    }


                                    document.getElementById("createNewLearningObject").checked=true;

                                    document.getElementById("LearningObjectTitle").value = "";
                                    document.getElementById("createNewLearningObjectContent").value = "Enter your description here";
                                    document.getElementById("URLLearningObjectContent").value = "http://";
                                    document.getElementById("ExistingFileLearningObjectContent").value = "";
                                    //document.getElementById("ExistingResourceLearningObjectContent").value = "";
                                    clearItemViewer("LearningObject");
                                    
                                    showCompletionPrereq('createNewLearningObject','LearningObject');
                                   LearningObjectItems = [];
                                    
                                    $("#LearningObjectTitle").val("");
                                    $(".toolsMaterialsTabs").tabs();
                                    $("#service_setUp").css('display','none');
                                    serviceArray = [];
                                    var tmp_checkboxId = "";
                                    $("#ServicesContent input:checkbox").each(function(){
                                        $(this).attr('checked',false);
                                        tmp_checkboxId = ($(this).attr('id').substring($(this).attr('id').indexOf('service_')+8,$(this).attr('id').length));
                                        showSetUp(false,tmp_checkboxId,'activity');
                                    })
                                    clearPropertyRow("changeActivityCompletionProperties")
                                }
                                catch(e){
                                    alert(e);
                                }
                            }
                    });
                    $('#activityStructure').colorbox({
                        width:"70%",height:"80%", inline:true,  href:"#structureActivityForm",
                        onLoad:function(){
                             try{
                                $("#prevStructureName").val("");
                                $("#structureName").val("");
                                $("#numOfIncludedActivities").val(0);
                                $("#NoOfStructureActivitesTobeExecuted").val(0);
                                 $(".toolsMaterialsTabs").tabs();
                                $("#activityStructureTabs").tabs();
                                var curPhase = allPhases.getPhase($("#phaseTab").attr("name"));
                                if(curPhase.getType()=="activity-structure"){
                                    $("#activityStructureTabs").tabs("disable",1);
                                    $("#activityStructureTabs").tabs("disable",2);
                                }
                                structureInformationItems=[];
                                document.getElementById("createNewstructureInformation").checked=true;
                                document.getElementById("structureInformationTitle").value = "";
                                document.getElementById("createNewstructureInformationContent").value = "Enter your description here";
                                document.getElementById("URLstructureInformationContent").value = "http://";
                                document.getElementById("ExistingFilestructureInformationContent").value = "";
                                //document.getElementById("ExistingResourcestructureInformationContent").value = "";
                                clearItemViewer("structureInformation");
                                showCompletionPrereq('createNewstructureInformation','structureInformation');
                                document.getElementById("createNewstructureLearningObject").checked=true;
                                
                                document.getElementById("createNewstructureLearningObject").checked=true;
                             document.getElementById("structureLearningObjectTitle").value = "";
                             document.getElementById("createNewstructureLearningObjectContent").value = "Enter your description here";
                             document.getElementById("URLstructureLearningObjectContent").value = "http://";
                             document.getElementById("ExistingFilestructureLearningObjectContent").value = "";
                             //document.getElementById("ExistingResourcestructureLearningObjectContent").value = "";
                                clearItemViewer("structureLearningObject");
                                showCompletionPrereq('createNewstructureLearningObject','structureLearningObject');
                                structureLearningObjectItems = [];
                                clearRolesStructureActivity();
                                structureserviceArray=[];
                                var tmp_checkboxId = "";
                                $("#structureServicesContent input:checkbox").each(function(){
                                        $(this).attr('checked',false);
                                        tmp_checkboxId = ($(this).attr('id').substring($(this).attr('id').indexOf('service_')+8,$(this).attr('id').length));
                                        showSetUp(false,tmp_checkboxId,'structure');
                                    })

                            }
                            catch(e){
                                alert(e)
                            }

                        }
                    });

                    $("#ResourcesOpt").colorbox({width:"50%",height:"55%", inline:true, href:"#ResourcesDefinition"});

                    $("#staffOpt").colorbox(
                        {
                            width:"50%",
                            inline:true,
                            href:"#roleDefinition",
                            onLoad: function(){
                                $("#prevStaffName").val("");
                                $('#Role_name').val("");
                                $('#maxRoleNum').val("");
                                $('#minRoleNum').val("");
                                $('#roleConstraint_yes').attr("checked",true)
                                $('#roleConstraint_no').attr("checked",false)

                            }
                        }
                    );

                    $('#model').hover(function() {
                        $(this).animate({"background-color": "#edf3f5"}, 800);
                        },
                        function() {
                            $(this).animate({"background-color": "#fafafa"}, 10);
                        });
                    $('#options').hover(function() {
                        $(this).animate({"background-color": "#edf3f5"}, 800);
                        },
                        function() {
                            $(this).animate({"background-color": "#fafafa"}, 10);
                        });
                        
                    $('#points').hover(function() {
                        $(this).animate({"background-color": "#edf3f5"}, 800);
                        },
                        function() {
                            $(this).animate({"background-color": "#fafafa"}, 10);
                        });

                   
           
                }
                catch(e){
                    alert(e);
                }
                $('#available_teamsNum').val(0);
                //document.getElementById('activitiesNum').value = 0;
                $('#phasesNum').val(0);
                $("#available_properties").val(0);
                
                
                //get ODS user Groups
              
                $( "#odsGroup_dialog" ).dialog({
                    autoOpen: false,       
                    modal: false,
                    draggable: false,
                    buttons: {
                        
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        },
                        "Ok": function() {
                            saveToFile.saveDesign(learnerArray, staffArray, allPhases,$(this).data('operation'));
                            $( this ).dialog( "close" );
                        }
                    }
                });
                $("#filename").click(function(){
                    if(ODSGroupID !== -1){
                        $.ajax({
                            url:"javascript_css/campusOperations/getStudentParticipationGroups.jsp?name="+user,
                            success:function(result){
                               $("#odsGroup_dialog").empty();
                               $("#odsGroup_dialog").append($("<p>You can save your design to specific group</p>"));
                               $("#odsGroup_dialog").append($("<select></select>").attr({"id":"odsGroup_selection","name":"odsGroup_selection","title":"select group","class":"asmselect"}));
                               var ods_groups = $.parseJSON(result).groups;                            
                                for(var i=0;i<ods_groups.length;i++){

                                    $("#odsGroup_selection").append("<option value='"+ods_groups[i].groupId+"'>"+ods_groups[i].groupName+"</option>");
                                }
                                 $("select[name=\"odsGroup_selection\"]").asmSelect({
                                    sortable: true,
                                    addItemTarget: 'top'
                                });
                                $("#odsGroup_dialog" ).data('operation', 'save').dialog("open");                        
                                
                            //
                        }});
                    } else{
                        try{
                            saveToFile.saveDesign(learnerArray, staffArray, allPhases,'save',null);
                        }catch(e){
                            console.log('error on saving file '+e)
                        }
                    }
                    
                });    
               
                $("#clone_designSpan").click(function(){
                    if(ODSGroupID !== -1){
                        $.ajax({
                            url:"javascript_css/campusOperations/getStudentParticipationGroups.jsp?name="+user,
                            success:function(result){                            
                                $("#odsGroup_dialog").empty();
                                $("#odsGroup_dialog").append($("<p>You can save your design to specific group</p>"));
                                $("#odsGroup_dialog").append($("<select></select>").attr({"id":"odsGroup_selection","name":"odsGroup_selection","title":"select group","class":"asmselect"}));
                                var ods_groups = $.parseJSON(result).groups;                            
                                
                                for(var i=0;i<ods_groups.length;i++){
                                    
                                    $("#odsGroup_selection").append("<option value='"+ods_groups[i].groupId+"'>"+ods_groups[i].groupName+"</option>");
                                }
                                 $("select[name=\"odsGroup_selection\"]").asmSelect({
                                    sortable: true,
                                    addItemTarget: 'top'
                                });
                                console.log("einai "+$("#odsGroup_selection").val());
                                $( "#odsGroup_dialog" ).data('operation', 'clone').dialog("open");     

                        }});
                    } else{
                        try{
                            saveToFile.saveDesign(learnerArray, staffArray, allPhases,'clone',null);
                        }catch(e){
                            console.log('error on saving file '+e)
                        }
                    }
                });
                $("#save_designSpan").click(function(){    
                    if(ODSGroupID !== -1){
                        $.ajax({
                            url:"javascript_css/campusOperations/getStudentParticipationGroups.jsp?name="+user,
                            success:function(result){
                               $("#odsGroup_dialog").empty();
                               $("#odsGroup_dialog").append($("<p>You can save your design to specific group</p>"));
                               $("#odsGroup_dialog").append($("<select></select>").attr({"id":"odsGroup_selection","name":"odsGroup_selection","title":"Select group","class":"asmselect"}));
                                var ods_groups = $.parseJSON(result).groups;                            
                                for(var i=0;i<ods_groups.length;i++){                                    
                                    $("#odsGroup_selection").append("<option value='"+ods_groups[i].groupId+"'>"+ods_groups[i].groupName+"</option>");
                                }                            
                                $("select[name=\"odsGroup_selection\"]").asmSelect({
                                    sortable: true,
                                    addItemTarget: 'top'
                                });
                                $("#odsGroup_dialog" ).data('operation', 'save').dialog("open");                            
                                //$('#filename').click();
                        }});
                    } else{
                        try{
                            saveToFile.saveDesign(learnerArray, staffArray, allPhases,'save',null);
                        }catch(e){
                            console.log('error on saving file '+e)
                        }
                    }
                });
            });
        }
        catch(e){
            alert(e);
        }          
        
              function doAjaxPost(id){
              try{
                  $("#"+id+"uploadFile").ajaxSubmit({
                        success: function(data){                             
                                $("#"+id+"LoadGif").css("display","none");                                
                                $("#successUpload"+id).attr("class","success");                            
                                $("#successUpload"+id).html("file uploaded succesfully (click to 'add item') <span class='exit' onclick=\"$(this).parent().css('display','none')\">exit</span>");
                                $("#successUpload"+id).css("display","block");
                                $('#ExistingFile'+id+'Button').attr('name','click')
                        },
                        error: function(jqXHR, textStatus, errorMessage) {
                            $("#"+id+"LoadGif").css("display","none");
                            $("#successUpload"+id).attr("class","error");
                            $("#successUpload"+id).html("error while uploading file<span class='exit' onclick=\"$(this).parent().css('display','none')\">exit</span>");
                            $("#successUpload"+id).css("display","block");
                        },
                        type: "POST",
                        url: $("#"+id+"uploadFile").attr("action"),
                        data: $("#"+id+"uploadFile").serialize(),
                        forceSync: true
               });
               return false;
              }catch(e){alert(e)}
                    return false;
              }          
    function initBase64() {
        enc64List = new Array();
        dec64List = new Array();
        var i;
        for (i = 0; i < 26; i++) {
            enc64List[enc64List.length] = String.fromCharCode(65 + i);
        }
        for (i = 0; i < 26; i++) {
            enc64List[enc64List.length] = String.fromCharCode(97 + i);
        }
        for (i = 0; i < 10; i++) {
            enc64List[enc64List.length] = String.fromCharCode(48 + i);
        }
        enc64List[enc64List.length] = "+";
        enc64List[enc64List.length] = "/";
        for (i = 0; i < 128; i++) {
            dec64List[dec64List.length] = -1;
        }
        for (i = 0; i < 64; i++) {
            dec64List[enc64List[i].charCodeAt(0)] = i;
        }
    }
    function base64Decode(str) {
        var c=0, d=0, e=0, f=0, i=0, n=0;
        var input = str.split("");
        var output = "";
        var ptr = 0;
        do {
            f = input[ptr++].charCodeAt(0);
            i = dec64List[f];
            if ( f >= 0 && f < 128 && i != -1 ) {
                if ( n % 4 == 0 ) {
                    c = i << 2;
                } else if ( n % 4 == 1 ) {
                    c = c | ( i >> 4 );
                    d = ( i & 0x0000000F ) << 4;
                } else if ( n % 4 == 2 ) {
                    d = d | ( i >> 2 );
                    e = ( i & 0x00000003 ) << 6;
                } else {
                    e = e | i;
                }
                n++;
                if ( n % 4 == 0 ) {
                    output += String.fromCharCode(c) +
                              String.fromCharCode(d) +
                              String.fromCharCode(e);
                }
            }
        }
        while (typeof input[ptr] != "undefined");
        output += (n % 4 == 3) ? String.fromCharCode(c) + String.fromCharCode(d) :
                  ((n % 4 == 2) ? String.fromCharCode(c) : "");
        return output;
    }
    function gup( name ){
        try{
            name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
            var regexS = "[\\?&]"+name+"=([^&#]*)";
            var regex = new RegExp( regexS );
            var results = regex.exec( window.location.href );
            if( results == null )
                return -1;
            else
                return results[1];
        }
        catch(e){
            throw "error while reading username "+e
        }
    }
      function enableLearningActivityCreation(option){
                if(option==1){
                    if(allPhases.getPhases().length==0){
                        alert('at least one phase must be created');                         
                        $("#learningActivity").removeClass('cboxElement');
                    }
                    else
                        $("#learningActivity").addClass('cboxElement');
                    
                }
                else{
                   
                    if(allPhases.getPhases().length==0){
                        alert('at least one phase must be created')
                        $("#activityStructure").removeClass('cboxElement');
                    }
                    else
                        $("#activityStructure").addClass('cboxElement');
               }
                   
      }
      function disable_Enable(anchor){
          if(allPhases.getPhase(document.getElementById('phaseTab').name).getType()=='activity-structure'){
              anchor.removeAttribute('href');
              anchor.style.background = "transparent"
          }
          else{
              anchor.setAttribute('href','#')
              anchor.style.backgroundColor = "#ffc77e"
          }          
      }

       function ShowHide(id,animation){
       
            $("#"+id).animate({"height": animation}, { duration: 1000 });
            
       }
       function loadOntology(){
           try{
           var ontologyURI = document.getElementById('course_concept').value;
           if(ontologyURI==""){
               alert('please enter uri');
               return false;
           }
           document.getElementById('OntologySide').style.display = 'block';
           document.getElementById('show_hideOntology').style.display = 'block';
          
           $('#ontologyTree').fileTree({root:ontologyURI, script: 'javascript_css/jqueryFileTree/connectors/jqueryFileTree.jsp'});
           
           // $("#loader").hide();
            return false;
           }
           catch(e){
               alert(e);
           }
       }
       function clear_ontology_entry(){
           try{
               document.getElementById('OntologySide').style.display = 'none';
               document.getElementById('show_hideOntology').style.display = 'none';               
           }
           catch(e){
               alert(e);
           }
       }
       
       function changeColor(){
           var concept = document.getElementById('course_concept');
           if(concept.value=="enter topic to teach"){
               concept.value = "";               
           }           
           concept.style.color='#000';
       }
       function makeButtonSelected(buttonId){
           /*if(buttonId=="LearningActivity"){
               document.getElementById('LearningActivity').className = "LearningActivitySelected";
               document.getElementById('groups').className = "Groups";
               document.getElementById('roles').className = "Roles";
               document.getElementById('phases').className = "phases";
               document.getElementById('resourcesButton').className = "Resources";
               document.getElementById('propertiesButton').className = "properties";
           }*/
           if(buttonId=="roles"){
               document.getElementById('resourcesButton').className = "Resources";
               //document.getElementById('LearningActivity').className = "LearningActivity";
               document.getElementById('groups').className = "Groups";
               document.getElementById('roles').className = "RolesSelected";
               document.getElementById('phases').className = "phases";
               document.getElementById('propertiesButton').className = "properties";
           }
           else if(buttonId=="phases"){
               document.getElementById('resourcesButton').className = "Resources";
               //document.getElementById('LearningActivity').className = "LearningActivity";
               document.getElementById('groups').className = "Groups";
               document.getElementById('roles').className = "Roles";
               document.getElementById('phases').className = "phasesSelected";
               document.getElementById('propertiesButton').className = "properties";
           }
           else if(buttonId=="resourcesButton"){
               document.getElementById('resourcesButton').className = "ResourcesSelected";
               //document.getElementById('LearningActivity').className = "LearningActivity";
               document.getElementById('groups').className = "Groups";
               document.getElementById('roles').className = "Roles";
               document.getElementById('phases').className = "phases";
               document.getElementById('propertiesButton').className = "properties";
           }
           
           else if(buttonId=="groups"){
               document.getElementById('resourcesButton').className = "Resources";
               //document.getElementById('LearningActivity').className = "LearningActivity";
               document.getElementById('groups').className = "GroupsSelected";
               document.getElementById('roles').className = "Roles";
               document.getElementById('phases').className = "phases";
               document.getElementById('propertiesButton').className = "properties";
           }
           else if(buttonId=="propertiesButton"){
               document.getElementById('resourcesButton').className = "Resources";
               //document.getElementById('LearningActivity').className = "LearningActivity";
               document.getElementById('groups').className = "Groups";
               document.getElementById('roles').className = "Roles";
               document.getElementById('phases').className = "phases";
               document.getElementById('propertiesButton').className = "propertiesSelected";
           }

       }
       var checked = 0;
       function load_Services(option){
               try{
                   if(option=="activity")
                        loadServices();
                   else
                        loadStructureServices();
               }
               catch(e){
                   alert(e);
               }
               return false;
       }

       function checkAllServices(option,parentDiv){
           
              
           var ul_parent = document.getElementById(parentDiv+"ServicesContent");
           if(ul_parent==null)
               return;
           var ulNodes = ul_parent.getElementsByTagName("ul");
           var id = "",serviceNameStartIndex="",furtherDescription=""
// Iterate through the ul's
           for( var i = 0; i < ulNodes.length; i++ ){
                  // get the child nodes of the li
               var checkBox_liNode = ulNodes.item(i).firstChild;
               var checkBox = checkBox_liNode.firstChild;
               id = checkBox.getAttribute("id");
               
               if(parentDiv=="structure"){
                   furtherDescription = "structure"
                   serviceNameStartIndex = id.indexOf("service_",0)+17;
               }
               else{
                   furtherDescription = "activity"
                   serviceNameStartIndex = id.indexOf("service_",0)+8;
               }
               document.getElementById(checkBox.getAttribute("id")).checked = option;
           console.log(id.substring(serviceNameStartIndex,id.length))
               showSetUp(option,parentDiv+id.substring(serviceNameStartIndex,id.length),furtherDescription)
           }
           
       }
       function isNumberKey(evt){
         var charCode = (evt.which) ? evt.which : event.keyCode
         
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;        

        return true;
      }
      var un_id = 0;
      function uniqid(){
          var newDate = new Date;
          un_id++;
          return parseFloat(newDate.getTime())+un_id;

      }


    
    function changeIcon(img,id,option){
       try{
          
           var item = document.getElementById(id);
           var height = item.style.height;
           if(option!="show"){
               if(parseInt(height)>=12){
                  
                    img.src = 'javascript_css/project_team_form/images/expand_0.png';
               }
               
               else if(parseInt(height)>0 && parseInt(height)<12){
                    img.src = 'javascript_css/project_team_form/images/expand_1.png';
               }
           }
           else{

               document.getElementById(img).src = 'javascript_css/project_team_form/images/expand_1.png';
               ShowHide(id,'show')
           }
       }
       catch(e){
           alert("error in changing icon "+e);
       }
    }
    function defineRolesSelected(type){
        //var name = "";
        var id = -1;
        var checkBox;
        var rolesSeleted = new Array();
        try{
            for(var i=0;i<staffArray.getRoles().length;i++){
                //name = staffArray.getRoles()[i].getName();
                //checkBox = document.getElementById(name+type);
                id = staffArray.getRoles()[i].getId()
                checkBox = document.getElementById(id+type);
                

                rolesSeleted.push(checkBox);
                
            }
            for(var i=0;i<learnerArray.getRoles().length;i++){
                //name = learnerArray.getRoles()[i].getName();
                //checkBox = document.getElementById(name+type);
                id = learnerArray.getRoles()[i].getId();
                checkBox = document.getElementById(id+type);
                
                //if(checkBox.checked)
                rolesSeleted.push(checkBox);
            }

            return rolesSeleted;
        }
        catch(e){
            alert(e);
        }
    }
    function showCompletionPrereq(id,type){
      try{
           
        if(id=="NoCondition"){
            document.getElementById("ActivitiesCompletionPrereq").style.display = "none";
            document.getElementById("TimePrereq").style.display = "none";
            document.getElementById("ConditionPrereg").style.display = "none";
        }
        else if(id=="CompleteActivities"){            
            /*var currentPhaseId = document.getElementById("PhaseTitle").value;
            var availableActivities = new Array();
            var availableStructures = [];
            var currentPhase = allPhases.getPhase(currentPhaseId);
            if(currentPhase!=-1){
                availableActivities = currentPhase.getLearningActivitiesArray().getActivities();
                availableStructures = currentPhase.getActivitiesStructuresArray();
            }
           
            var activit;
            var label;
            document.getElementById("ActivitiesCompletionPrereq").innerHTML="";
            
            if(availableActivities.length==0 && availableStructures.length==0){
              label = document.createElement("p");
              label.innerHTML = "No activities defined for this phase yet";
              document.getElementById("ActivitiesCompletionPrereq").appendChild(label);
             
            }
            else{                
                var li;
                var list = document.createElement("ul");
                list.className = "Phase_Rows";
                var tmpCount = 0;
                var role;
                var name,id;                
                for(var i=0;i<availableActivities.length;i++){
                    activit = availableActivities[i];
                    name = activit.getName();
                    id = activit.getId();
                    for(var j=0;j<activit.getRolesAssigned().length;j++){
                        if(activit.getRolesAssigned()[j].checked){
                            li = document.createElement("li");
                            li.style.clear = "both";
                            var checkbox = document.createElement("input");                            
                            checkbox.type = "checkbox";
                            role = activit.getRolesAssigned()[j].name;                            
                            checkbox.onclick = new_pair(role,checkbox,activit);//codeAllagi new_pair(role,checkbox,name);
                            checkbox.id = role+id+"_checkbox";//codeAllagi checkbox.id = role+name+"_checkbox";
                      
                            var role_name = staffArray.getRoles().containsId(role);
                            if(role_name==null)
                                role_name = learnerArray.getRoles().containsId(role);
                            if(role_name==null)
                                throw "error while trying to present roles ";

                            label = document.createElement("label");
                            label.innerHTML = role_name.getName()+" completes '"+name+"'";
                            li.appendChild(checkbox);
                            li.appendChild(label);                            
                            list.appendChild(li);
                            tmpCount++;
                        }
                    }
                }
                for(var i=0;i<availableStructures.length;i++){
                    activit = availableStructures[i];
                    name = activit.getName();
                    id = activit.getId();
                    for(var j=0;j<activit.getRolesAssigned().length;j++){
                        if(activit.getRolesAssigned()[j].checked){
                            li = document.createElement("li");
                            li.style.clear = "both";
                            var checkbox = document.createElement("input");
                            checkbox.type = "checkbox";
                            role = activit.getRolesAssigned()[j].name;
                            checkbox.onclick = new_pair(role,checkbox,activit);//codeAllagi new_pair(role,checkbox,name);
                            checkbox.id = role+id+"_checkbox";//codeAllagi checkbox.id = role+name+"_checkbox";

                            var role_name = staffArray.getRoles().containsId(role);
                            if(role_name==null)
                                role_name = learnerArray.getRoles().containsId(role);
                            if(role_name==null)
                                throw "error while trying to present roles ";
                            label = document.createElement("label");
                            label.innerHTML = role_name.getName()+" completes '"+name+"'";
                            li.appendChild(checkbox);
                            li.appendChild(label);
                            list.appendChild(li);
                            tmpCount++;
                        }
                    }
                }
                if(tmpCount==0){
                    li = document.createElement("li");
                    label = document.createElement("label");
                    label.innerHTML = "There are no roles assigned to activity";
                    li.appendChild(label);
                    list.appendChild(li);
                }                

                document.getElementById("ActivitiesCompletionPrereq").appendChild(list);
                for(var i=0;i<completeActivities.getRolesActivitiesPairs().length;i++){
                    document.getElementById(completeActivities.getRolesActivitiesPairs()[i][0]+completeActivities.getRolesActivitiesPairs()[i][1]+"_checkbox").checked = true;
                }

            }*/
            document.getElementById("ActivitiesCompletionPrereq").innerHTML="<p>Completion of this act is defined by end points<br/>you have used in the activity flow</p>";
            document.getElementById("ActivitiesCompletionPrereq").style.display = "block";
            document.getElementById("TimePrereq").style.display = "none";
            document.getElementById("ConditionPrereg").style.display = "none";
        }
        else if(id=="TimePeriod"){
           
            document.getElementById("ActivitiesCompletionPrereq").style.display = "none";
            document.getElementById("TimePrereq").style.display = "block";
            document.getElementById("ConditionPrereg").style.display = "none";
        }
        else if(id=="Condition"){
           
            document.getElementById("ActivitiesCompletionPrereq").style.display = "none";
            document.getElementById("TimePrereq").style.display = "none";
            document.getElementById("ConditionPrereg").style.display = "block";
        }//////////////////////////////
        
        else if(id=="createNew"+type){
          
            //document.getElementById("createNew"+type+"Content").disabled = false;
            //document.getElementById("URL"+type+"Content").disabled = true;
            //document.getElementById("ExistingResource"+type+"Content").disabled = true;
            //document.getElementById("ExistingFile"+type+"Content").disabled = true;
       
            $("#createNew"+type+"Content").css("display","block");
            $("#URL"+type+"Content").css("display","none");
            
            $("#ExistingFile"+type+"Content").css("display","none");
            //$("#File"+type+"Button").css("display","none");
        }

        else if(id=="URL"+type){
            
            //document.getElementById("createNew"+type+"Content").disabled = true;
            //document.getElementById("URL"+type+"Content").disabled = false;
            //document.getElementById("ExistingResource"+type+"Content").disabled = true;
            //document.getElementById("ExistingFile"+type+"Content").disabled = true;
            $("#createNew"+type+"Content").css("display","none");
            $("#URL"+type+"Content").css("display","block");
            $("#ExistingFile"+type+"Content").css("display","none");
            //$("#File"+type+"Button").css("display","none");
        }
        else if(id=="ExistingFile"+type){
            //document.getElementById("createNew"+type+"Content").disabled = true;
            //document.getElementById("URL"+type+"Content").disabled = true;
            //document.getElementById("ExistingResource"+type+"Content").disabled = true;
            //document.getElementById("ExistingFile"+type+"Content").disabled = false;
            $("#createNew"+type+"Content").css("display","none");
            $("#URL"+type+"Content").css("display","none");
            $("#ExistingFile"+type+"Content").css("display","block");
            //$("#File"+type+"Button").css("display","block");
            
        }
        /*else if(id=="ExistingResource"+type){
            document.getElementById("createNew"+type+"Content").disabled = true;
            document.getElementById("URL"+type+"Content").disabled = true;
            document.getElementById("ExistingResource"+type+"Content").disabled = false;
            document.getElementById("ExistingFile"+type+"Content").disabled = true;
        }*/
        else if(id=="NoResource"+type){
            //document.getElementById("createNew"+type+"Content").disabled = true;
            //document.getElementById("URL"+type+"Content").disabled = true;
            //document.getElementById("ExistingResource"+type+"Content").disabled = true;
           // document.getElementById("ExistingFile"+type+"Content").disabled = true;
            $("#createNew"+type+"Content").css("display","none");
            $("#URL"+type+"Content").css("display","none");
            $("#ExistingFile"+type+"Content").css("display","none");
            //$("#File"+type+"Button").css("display","none");
        }

        else if(id=="none"){           
            document.getElementById('activityCompletionTimeView').style.display = "none";
            document.getElementById('activityCompletionPropertySetView').style.display = "none";
        }
        else if(id=="userChoice"){
            
            document.getElementById('activityCompletionTimeView').style.display = "none";
            document.getElementById('activityCompletionPropertySetView').style.display = "none";
        }
        else if(id=="activityCompletionTime"){            
            document.getElementById('activityCompletionTimeView').style.display = 'block';
            document.getElementById('activityCompletionPropertySetView').style.display = "none";
        }
        else if(id=="activityCompletionPropertySet"){
            
            document.getElementById('activityCompletionTimeView').style.display = "none";
            document.getElementById('activityCompletionPropertySetView').style.display = "block";
        }
      }
      catch(e){
          alert(e);
      }
        
    }

    function addItemToItemViewer(item,type,category){
        try{
        
            var itemViewer = document.getElementById(type+"ItemViewer");
            var ItemNum = document.getElementById(type+"noItem");
            //var item1 = document.getElementById(type+"Item1");
            if(ItemNum.value=="1")
                ItemNum.value = parseInt(ItemNum.value)+1;

            var valueForItem1 = item[0];
            var anchor = document.createElement("a");
            anchor.href = "#";
            anchor.style.textDecoration = "none";
            anchor.innerHTML = valueForItem1;
            anchor.onclick = function(){loadFromItemViewer(item,type,category)};
            anchor.title="present "+item[0];

            var anchorResource = document.createElement("a");
            anchorResource.href = "#";
            anchorResource.style.textDecoration = "none";
            anchorResource.innerHTML = item[2];
            if(item[1].indexOf("URL")!=-1){
                anchorResource.onclick = function(){window.open(item[2],'_blank','toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=1,resizable=1');};
                anchorResource.title="browse at "+item[2];
            }
            else if(item[1].indexOf("File")!=-1){
                anchorResource.title="download "+item[2];
                anchorResource.onclick = function(){window.location = "downloadFileFromServer.jsp?manifestId="+ManifestID+"&filename="+type+"-"+item[2]}//,'_blank','toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=1,resizable=1')};
            }


            var deleteItem = document.createElement("a");
            deleteItem.href = "#";
            deleteItem.className = "empty"
            //deleteItem.style.background = "url('javascript_css/images/delete.gif') no-repeat center";
            //deleteItem.style.color = "#0464BB"
            //deleteItem.innerHTML = "delete";
            deleteItem.title="delete resource";
            var value = parseInt(ItemNum.value);
            
                var newItem = document.createElement("div");
                newItem.style.clear = "both";
                
                
                var tmp;

                for(var i=0;i<3;i++){
                    tmp = document.createElement("div");
                    if(i!=2)
                        tmp.className = "td";

                    //tmp.style.height="12px";
                    if(i==0){
                        tmp.appendChild(anchor);
                    }
                    else if(i==1){
                        tmp.appendChild(anchorResource);
                    }
                    else if(i==2){
                        //tmp.className = "empty";
                        
                        tmp.appendChild(deleteItem);
                    }
                    newItem.appendChild(tmp);
                }

                //newItem.style.height = "12px";
                newItem.style.width = "300px";
                newItem.id = type+"Item"+ItemNum.value;
                itemViewer.appendChild(newItem);
                ItemNum.value = parseInt(ItemNum.value)+1;

            //}
            var feedItem = createFeedbackItem(item[0],item[1],item[2],item[3]);
            
            deleteItem.addEventListener("click",
                         function(){                             
                            deleteFromItemViewer(type,value,feedItem.getId());
                         },
                         false);
            if(type=="PhaseDescription")
                phaseFeedBackItem.push(feedItem);
            else if(type=="Description")
                descriptionItems.push(feedItem);
            else if(type=="activityLearningObjectives")
                learningObjectivesItems.push(feedItem);
            else if(type=="Prerequisites")
                prerequisitesItems.push(feedItem);
            else if(type=="activityCompletion")
                activityCompletionFeedback.push(feedItem)
            else if(type=='structureInformation')
                structureInformationItems.push(feedItem);
            else if(type=='LearningObject')
                LearningObjectItems.push(feedItem);
            else if(type=='structureLearningObject')
                structureLearningObjectItems.push(feedItem);


        }
        catch(e){
            alert(e);
        }
    }
    function deleteFromItemViewer(type,ItemNum,itemId){
        try{
            
            var feedbackItemArray = null;
            if(type=="PhaseDescription")
                feedbackItemArray = phaseFeedBackItem;
            else if(type=="Description")
                feedbackItemArray = descriptionItems;
            else if(type=="activityLearningObjectives")
                feedbackItemArray = learningObjectivesItems;
            else if(type=="Prerequisites")
                feedbackItemArray = prerequisitesItems;
            else if(type=="activityCompletion")
                feedbackItemArray = activityCompletionFeedback;            
            else if(type=='structureInformation')
                feedbackItemArray = structureInformationItems;
            else if(type=='LearningObject')
                feedbackItemArray = LearningObjectItems;
            else if(type=='structureLearningObject')
                feedbackItemArray = structureLearningObjectItems;

            var itemViewer = document.getElementById(type+"ItemViewer");
            itemViewer.removeChild(document.getElementById(type+"Item"+ItemNum));

            
            for(var i=0;i<feedbackItemArray.length;i++)
                if(feedbackItemArray[i].getId()==itemId)
                    feedbackItemArray.splice(i,1);
            
        }
        catch(e){
            alert(e)
        }
    }
    function loadFromItemViewer(item,type,category){
        try{
            document.getElementById(category+type+"Title").value = item[0];
            document.getElementById(item[1]).checked = true;                        
     
            if(item[1]!="NoResource"+type)
                document.getElementById(item[1]+"Content").value = item[2];

            if(document.getElementById("createNew"+type).checked)            
             showCompletionPrereq("createNew"+type,type);
            

            else if(document.getElementById("URL"+type).checked){
                
                showCompletionPrereq("URL"+type,type);
            }
            else if(document.getElementById("ExistingFile"+type).checked){
                showCompletionPrereq("ExistingFile"+type,type);
                //showCompletionPrereq("File"+type+"Button",type);
            }
            else if(document.getElementById("ExistingResource"+type).checked){
                showCompletionPrereq("ExistingResource"+type,type);
            }
            else if(document.getElementById("NoResource"+type).checked){
                showCompletionPrereq("NoResource"+type,type);
            }
            /*document.getElementById("createNew"+type+"Content").disabled = false;
            document.getElementById("URL"+type+"Content").disabled = true;
            document.getElementById("ExistingResource"+type+"Content").disabled = true;
            document.getElementById("ExistingFile"+type+"Content").disabled = true;*/
        }
        catch(e){
            alert(e)
        }
    }
    function clearItemViewer(type){
        try{            
            var ItemViewer = document.getElementById(type+"ItemViewer");
            document.getElementById(type+"noItem").value="1";
            var node;
            var  startRemoving = 0;
            //var tmpDescriptionItemViewer = DescriptionItemViewer.cloneNode(true);
            //var childNum = DescriptionItemViewer.childNodes.length;
            
            for(var i=0;i<ItemViewer.childNodes.length;i++){
                node = ItemViewer.childNodes[i];                
                if(startRemoving==1){                    
                    ItemViewer.removeChild(node);
                    i--;
                    continue;
                }
                else{
                    if(node.id!=type+"Item1"){
                        continue;
                    }
                    else{
                        for(var j=0;j<node.childNodes.length;j++){
                            node.childNodes[j].innerHTML = "";
                        }
                        startRemoving = 1;
                    }
                }
            }   
        }
        catch(e){
            alert(e);
        }
    }

    function defineItemsProperties(type,category){
        try{
            
            var item = new Array();
            var itemType;
            var itemValue;
            var itemTitle = $("#"+category+type+"Title").val();
            $("#"+category+type+"Title").val("");

            if(document.getElementById("createNew"+type).checked){
                itemType = "createNew"+type;
                itemValue = $("#createNew"+type+"Content").val();
                $("#createNew"+type+"Content").val("");
            }
            else if(document.getElementById("URL"+type).checked){
                itemType = "URL"+type;
                itemValue = $("#URL"+type+"Content").val();
                $("#URL"+type+"Content").val("http://")
            }
             else if(document.getElementById("ExistingFile"+type).checked){
                itemType = "ExistingFile"+type;
                var itemStr = $("#File"+type+"Button").val();
                if($('#ExistingFile'+type+'Button').attr('name')=="loadingManifest")
                    itemStr = $("#ExistingFile"+type+"Content").val();
                 
                if(itemStr=="" || $('#ExistingFile'+type+'Button').attr('name')=='unclickable'){
                    $("#successUpload"+type).attr("class","info");
                    $("#successUpload"+type).html("no file uploaded<span class='exit' onclick=\"$(this).parent().css('display','none')\">exit</span>");
                    $("#successUpload"+type).css("display","block");
                    return;
                }
                if(itemStr.indexOf("fakepath")!=-1)//afora chrome
                    itemValue = itemStr.substr(itemStr.indexOf("fakepath")+9);
                else
                    itemValue = itemStr;//den vazei fake path k.l.lp o mozilla
                $("#File"+type+"Button").val("");
                $('#ExistingFile'+type+'Button').attr('name','unclickable');
                $('#successUpload'+type).css('display','none');
            }
             /*else if(document.getElementById("ExistingResource"+type).checked){
                itemType = "ExistingResource"+type;
                itemValue = document.getElementById("ExistingResource"+type+"Content").value;
            }*/
            else if(document.getElementById("NoResource"+type).checked){
                itemType = "NoResource"+type;
                itemValue = "";
            }
            
            item.push(itemTitle);
            item.push(itemType);
            item.push(itemValue);
            if(parseInt(document.getElementById(type+"noItem").value)==1)
                item.push((parseInt(document.getElementById(type+"noItem").value))+1);
            else
                item.push(parseInt(document.getElementById(type+"noItem").value));

            addItemToItemViewer(item,type,category);
            //return item;
        }
        catch(e){
            alert(e);
        }
    }

    function defineNotificationProperties(option){
        try{
            //var notification = new Array();
            var notifiedRoles = new Array();
            var tmp;
            var notificationLearnerRoles;
            var notificationStaffRoles;
            var subject;
            var content;
            if(option=="Phase"){
                notificationLearnerRoles = document.getElementById('PhaseSelectLearnerRole');
                notificationStaffRoles = document.getElementById('PhaseSelectStaffRole');                                
                subject = document.getElementById("notificationSubject").value;
                content = document.getElementById("notificationContent").value;
                notificationArray.push(createNotification(option,subject,content,notificationLearnerRoles,notificationStaffRoles));
            }
            else{
                notificationLearnerRoles = document.getElementById('activityCompletionSelectLearnerRole');            
                notificationStaffRoles = document.getElementById('activityCompletionSelectStaffRole');
                subject = document.getElementById("activityCompletionNotificationSubject").value;
                content = document.getElementById("activityCompletionNotificationContent").value;
                activityCompletionNotificationArray.push(createNotification(option,subject,content,notificationLearnerRoles,notificationStaffRoles));
            }
            for(var i=0;i<notificationLearnerRoles.childNodes.length;i++){
                    tmp = notificationLearnerRoles.childNodes[i];
                    if(tmp.nodeName=="LI"){
                        if(tmp.childNodes[0].checked){
                            //notifiedRoles.push(tmp.childNodes[1].innerHTML);
                            //var index = (tmp.childNodes[0].id).indexOf("_");
                            //var id = (tmp.childNodes[0].id).substring(0, index);

                            notifiedRoles.push(tmp.childNodes[0].id);
                        }
                    }
            }
            for(i=0;i<notificationStaffRoles.childNodes.length;i++){
                    tmp = notificationStaffRoles.childNodes[i];
                    if(tmp.nodeName=="LI"){
                        if(tmp.childNodes[0].checked){
                            //notifiedRoles.push(tmp.childNodes[1].innerHTML);
                            //var index = (tmp.childNodes[0].id).indexOf("_");
                            //var id = (tmp.childNodes[0].id).substring(0, index);
                            notifiedRoles.push(tmp.childNodes[0].id);
                        }
                    }
            }
            //notification.push(subject);
            //notification.push(content);
            //notification.push(notifiedRoles);
            addItemToNotificationViewer(subject,content,notifiedRoles,option);
            //return notification;
        }
        catch(e){
            alert(e);
        }
    }
    function addItemToNotificationViewer(subject,content,notifiedRoles,option,notificationId){
        try{
            var notificationNum;
            //var item;
            var viewer;
            if(option=='Phase'){
                notificationNum = document.getElementById("PhaseNotificationNoItem");
                //item = document.getElementById("PhaseNotificationItem1");
                viewer = document.getElementById("PhaseNotificationItemViewer");
            }
            else{
                notificationNum = document.getElementById("activityNotificationNoItem");
                //item = document.getElementById("activityCompletionNotificationItem1");
                viewer = document.getElementById("activityNotificationItemViewer");
            }

            var anchor = document.createElement("a");
            anchor.href = "#";
            anchor.style.textDecoration = "none";
            anchor.innerHTML = subject;
            anchor.title = "click to reveal message info";

            anchor.onclick = function(){loadFromNotificationViewer(subject,content,notifiedRoles,option)};

            //var deleteItem = document.createElement("a");
           // deleteItem.href = "#";
            //deleteItem.style.textDecoration = "none";
            //deleteItem.style.color = "#0464BB"
            //deleteItem.innerHTML = "delete";
            //deleteItem.className="empty"
            //deleteItem.title = "delete notification"
                   
                var newItem = document.createElement("div");
                newItem.style.clear = "both";
                var tmp;
                for(var i=0;i<2;i++){
                    tmp = document.createElement("div");
                    //tmp.style.width = "106px";
                    //tmp.style.height="12px";
                    /*if(i==0){
                        tmp.className = "td";
                        tmp.style.width = "3px";
                        tmp.innerHTML =  parseInt(notificationNum.value);//notificationNum.value;
                    }
                    else{*/
                        
                        if(i==1){
                            //tmp.style.width = "16px";
                            tmp.className = "empty";
                            //tmp.appendChild(deleteItem);
                            tmp.style.cursor = "pointer";
                            var value = (notificationNum.value);
                            tmp.addEventListener("click",
                                function(){
                                    deleteFromNotificationViewer(option,value,notificationId);
                                },false);
                        }
                        else{
                            tmp.className = "td";
                            tmp.appendChild(anchor);
                        }
                    //}

                    newItem.appendChild(tmp);

                }
               
                newItem.id = option+"notificationItem"+notificationNum.value;
                
                viewer.appendChild(newItem);
                notificationNum.value = parseInt(notificationNum.value)+1;



            
            //}
        }
        catch(e){
            alert(e);
        }
    }
    function deleteFromNotificationViewer(option,value,notificationId){
        try{
            var feedbackItemArray = null;
            if(option=="Phase")
                feedbackItemArray = notificationArray;
            else
                feedbackItemArray = activityCompletionNotificationArray;

            var itemViewer = document.getElementById(option+"NotificationItemViewer");
            itemViewer.removeChild(document.getElementById(option+"notificationItem"+value));

            //var index,role_id,role=null;
            for(var i=0;i<feedbackItemArray.length;i++){
                //console.log("kanw delete to "+option+"notificationItem"+value +" "+feedbackItemArray[i].getId())
                if(feedbackItemArray[i].getIdentifier()==notificationId){//if(feedbackItemArray[i].getId()==option+"notificationItem"+value){
                    feedbackItemArray.splice(i,1);

                    /*var checkbox = feedbackItemArray[i].getReceivers();
                    for(var j=0;j<checkbox.length;j++){
                        if(document.getElementById(checkbox).checked){
                            index = (checkbox).indexOf("_");
                            role_id = (checkbox).substring(0, index);                        
                            role = staffArray.getRoles().containsId(role_id);
                            if(role==null)
                                role = learnerArray.getRoles().containsId(role_id);
                            if(role!=null)
                                role.setDeleteFlag(true);
                        }                        
                    }*/
                }
            }

        }
        catch(e){
            alert(e)
        }
    }
    function loadFromNotificationViewer(subject,content,notifiedRoles,option){
        try{
            var notificationLearnerRoles;
            var notificationStaffRoles;
            var tmp;
            if(option == 'Phase'){
                document.getElementById("notificationSubject").value = subject;
                document.getElementById("notificationContent").value = content;
                notificationLearnerRoles = document.getElementById('PhaseSelectLearnerRole');
                notificationStaffRoles = document.getElementById('PhaseSelectStaffRole');
            }
            else{
               document.getElementById("activityCompletionNotificationSubject").value = subject;
               document.getElementById("activityCompletionNotificationContent").value = content;
               notificationLearnerRoles = document.getElementById('activityCompletionSelectLearnerRole');
               notificationStaffRoles = document.getElementById('activityCompletionSelectStaffRole');
            }
            for(var i=0;i<notificationLearnerRoles.childNodes.length;i++){
                tmp = notificationLearnerRoles.childNodes[i];               
                if(tmp.nodeName=="LI"){                     
                    if(tmp.childNodes[1]!=null){
                        if(notifiedRoles.containsIdInArray(tmp.childNodes[0].id)!=-1)//if(notifiedRoles.containsNameInArray(tmp.childNodes[1].innerHTML))
                            tmp.childNodes[0].checked = true;
                        
                        else
                            tmp.childNodes[0].checked = false;
                    }
                }
            }
            for(i=0;i<notificationStaffRoles.childNodes.length;i++){
                tmp = notificationStaffRoles.childNodes[i];                
                if(tmp.nodeName=="LI"){
                    if(tmp.childNodes[1]!=null){                        
                        if(notifiedRoles.containsIdInArray(tmp.childNodes[0].id)!=-1)//if(notifiedRoles.containsNameInArray(tmp.childNodes[1].innerHTML))
                            tmp.childNodes[0].checked = true;
                        else
                            tmp.childNodes[0].checked = false;
                    }
                }
            }
        }
        catch(e){
            alert(e)
        }
    }
    function clearRecipients(option){
        var notificationLearnerRoles = null;
        var notificationStaffRoles = null;
        if(option=="phase"){
            notificationLearnerRoles = document.getElementById('PhaseSelectLearnerRole');
            notificationStaffRoles = document.getElementById('PhaseSelectStaffRole');
        }
        else if(option=="activity"){
            notificationLearnerRoles = document.getElementById('activityCompletionSelectLearnerRole');
            notificationStaffRoles = document.getElementById('activityCompletionSelectStaffRole');
        }
        var tmp;
        for(var i=0;i<notificationLearnerRoles.childNodes.length;i++){
            tmp = notificationLearnerRoles.childNodes[i];                
            if(tmp.nodeName=="LI")
                if(tmp.childNodes[1]!=null)
                    tmp.childNodes[0].checked = false;
        }
        for(i=0;i<notificationStaffRoles.childNodes.length;i++){
            tmp = notificationStaffRoles.childNodes[i];
            if(tmp.nodeName=="LI")
                if(tmp.childNodes[1]!=null)
                    tmp.childNodes[0].checked = false;
        }
    }
    function clearNotificationViewer(option){
        try{
            var ItemViewer;
            var nodeId;
            if(option=='phase'){
                ItemViewer = document.getElementById("PhaseNotificationItemViewer");
                document.getElementById("PhaseNotificationNoItem").value="1";
                nodeId = "PhaseNotificationItem1";
            }
            else{
                ItemViewer = document.getElementById("activityNotificationItemViewer");
                document.getElementById("activityNotificationNoItem").value="1";
                nodeId = "activityCompletionNotificationItem1"
            }

            var node;
            var  startRemoving = 0;
            //var tmpDescriptionItemViewer = DescriptionItemViewer.cloneNode(true);
            //var childNum = DescriptionItemViewer.childNodes.length;
            for(var i=0;i<ItemViewer.childNodes.length;i++){
                node = ItemViewer.childNodes[i];
                if(startRemoving==1){
                    ItemViewer.removeChild(node);
                    i--;
                    continue;
                }
                else{
                    if(node.id!=nodeId){
                        continue;
                    }
                    else{
                        for(var j=0;j<node.childNodes.length;j++){
                            node.childNodes[j].innerHTML = "";
                        }
                        startRemoving = 1;
                    }
                }
            }
        }
        catch(e){
            alert(e);
        }
    }

    
    function showProperties(selection){
        try{

            if((propertiesArray.length)==selection.childNodes.length)
                return;

            for(var i=0;i<propertiesArray.length;i++){
                option = document.createElement("option");
                option.value = propertiesArray[i];
                option.innerHTML = option.value;
                selection.appendChild(option);
            }
        }
        catch(e){
            alert(e);
        }
    }
    function clearPropertyRow(option){
        $("#"+option).find("a").each(function(){
            $(this).parent().parent().remove();
        })
        $("#"+option).find("select").each(function(){
            $(this).children().each(function(index){
                if(index==0)
                    $(this).attr("selected","selected")
                else
                    $(this).removeAttr("selected")
            })

        })
        if(option=="changeProperties")
            $("#PhaseProperty1").val("")
        else
            $("#ActivityProperty1").val("")
    }
    function addPropertyChangeRow(optionValue,optionText,optionDiv){
        var new_row = $("<div style='clear:both'></div>").appendTo($("#"+optionDiv));
        var option = $("<option value="+optionText+"></option>").text(optionText);
        var column1 = $("<div class='td'></div>").append($("<select style='width:115px;height:17px;'></select>").append(option))
        var input = $("<input value='"+optionValue+"' type='text' style='width:115px;height:14px;'></input>");
       
        var column2 = $("<div class='td'></div>").append(input);
        new_row.append(column1);
        new_row.append(column2);

        var anchor = document.createElement("a");
        anchor.href= '#'
        anchor.title="delete row"
        //anchor.style.textDecoration="none"
        //anchor.style.color="#0464BB"
        //anchor.textContent = "delete";
        anchor.className = "empty";
        anchor.onclick = function(){document.getElementById(optionDiv).removeChild(anchor.parentNode.parentNode)};
        var column3 = $("<div></div>").append(anchor);
        new_row.append(column3);
        addPropertiesToWhenComplete(new_row)

    } 
    
    /*function lookup(inputString) {
        if(inputString.length == 0) {
            $('#suggestions').hide();
        } else {
            $.post("terms.jsp", {queryString: ""+inputString+""}, function(data){
                
                if(data.length >2) {
                    $('#suggestions').show();
                    
                    $("#suggestionListpagerId").css("display","block");
                    
                    $('#autoSuggestionsList').children('tbody:first').html(data);
                    $(".currentPag").text(1);
                    $('#autoSuggestionsList').paginateTable({
                        rowsPerPage:10,currentPage:'.currentPag',
                        totalPages:'.totalPags',prevPage:'.prevPag',nextPage:'.nextPag'});

                    $("#autoSuggestionsList td:nth-child(1)").click(function(){
                        fill($(this).html())
                    });
                    


                }
                else{
                    
                    $('#suggestions').hide();
                }
            });
        }
    }
    function fill(thisValue) {

        $('#course_concept').val(thisValue);
        setTimeout($('#suggestions').hide(), 50);
    }
    function addToConceptArea(){
       try{
            if($('#course_concept').val()!=''){
                $('#termsToConceptMapSelection').append("<option value="+$('#course_concept').val()+">"+$('#course_concept').val()+"</option>");

                $('#termsToConceptMapSelection').change();


            }
            return false;
       }
       catch(e){
           alert(e);
       }
    }*/
    


   
    </script>
    
</head>

    <body>
   
    <div id="smartwizard" class="wiz-container">
        <ul id="wizard-anchor">
            <li>
                <a href="#wizard-1" onclick="$('.tutorialSteps li:nth-child(1)').addClass('tutorialStepSelected').siblings('li').removeClass('tutorialStepSelected');$('.tutorialExplanation > li:nth-child(1)').show().siblings().hide();"><h2>Step 1</h2>
                    <small><b>General</b></small>
                </a>
            </li>
            <li>
                <a href="#wizard-2" onclick="if(this.className=='wiz-anc-default')return;$('.tutorialSteps li:nth-child(2)').addClass('tutorialStepSelected').siblings('li').removeClass('tutorialStepSelected');$('.tutorialExplanation > li:nth-child(2)').show().siblings().hide();"><h2>Step 2</h2>
                <small><b>Topic to teach</b></small>
                </a>
            </li>
            <li>
                <a href="#wizard-3" onclick="if(this.className=='wiz-anc-default')return;$('.tutorialSteps li:nth-child(3)').addClass('tutorialStepSelected').siblings('li').removeClass('tutorialStepSelected');$('.tutorialExplanation > li:nth-child(3)').show().siblings().hide();"><h2>Step 3</h2>
                <small><b>Educational Process Design</b></small>
                </a>
            </li>
            
	</ul>
        
    	<div id="wizard-body" class="wiz-body">
           
            <div style="display:none" id="openManifestForm" title="LD Project">
                <p>Create a new Project</p>
                <div style="clear:both;height:25px;padding-bottom:10px">
                    <a title="create new CSCL project" class="nicebutton" href="#" onclick="newProject()"><span>New Project</span></a>
                </div>
                <p>Search Existing Project By:</p>
                <div>
                    <form class="jqtransform" style="display:none" action="javascript:return false;">
                        <div>
                            <div  class="searchrowElem">
                                <label for="name"> Project Id :</label>
                                <input class="text ui-widget-content ui-corner-all"  type="text" name="terms" id="ProjectId" style="width:250px;"></input>
                            </div>
                            <div  class="searchrowElem">
                                <label for="terms"> Lesson Title :</label>
                                <input class="text ui-widget-content ui-corner-all"  type="text" name="terms" id="ProjectTitle" style="width:250px;"/>
                            </div>
                        </div>
                        <div>
                            <div  class="searchrowElem">
                                <label for="terms"> Writer Name :</label>
                                <input class="text ui-widget-content ui-corner-all"  type="text" name="terms" id="WriterName" style="width:250px;"/>
                            </div>
                            <div  class="searchrowElem">
                                <label for="terms"> Lesson Subject :</label>
                                <input class="text ui-widget-content ui-corner-all"  type="text" name="terms" id="LessonSubject" style="width:250px;"/>
                            </div>
                        </div>
                        <div style="clear:both;padding-left:200px">
                            <a title="search exisitng project" class="nicebutton" href="#" onclick="loadLD.getResultData($('#ProjectId').val(),$('#WriterName').val(),$('#ProjectTitle').val(),$('#LessonSubject').val())"><span>Search_Project</span></a>
                        </div>
                        <div id="resultTable" style="clear:both" class="searchrowElem">
                            <p id="resultsNum">Results:</p>
                            <div style="/*height:100px*/">
                                <div>
                                    <span style="color:#0464BB; text-decoration: underline;padding-left:3px">Lesson Title:</span>
                                    <span style="color:#0464BB; text-decoration: underline;padding-left:105px">Lesson Subject:</span>
                                    <span style="color:#0464BB; text-decoration: underline;padding-left:85px">Author:</span>
                                </div>
                                <table id="projectResults">                                                                                                                
                                    <tbody>                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class='pager' id="pagerId" style="padding:10px 10px 0px 100px">
                                <a href='#' alt='Previous' class='prevPage'>Prev</a>
                                <span class='currentPage'></span> of <span class='totalPages'></span>
                                <a href='#' alt='Next' class='nextPage'>Next</a>
                            </div>
                        </div>
                    </form>
               </div>
               <div id="loadingProject" style="margin:40px 0px 0px 270px;clear:both"></div>
               
            </div>
            <div id="wizard-1">
                <p style="margin:1em;">In the section bellow we define general information</p>
                <div class="wiz-content">
                     <form class="jqtransform" style="display:none">
                        <div class="rowElem">
                            <label for="name" >Lesson Plan Name: </label>
                            <input type="text" size="20" id="lesson_plan_title" name="lesson_plan_title"></input>
                        </div>
                        <div class="rowElem">
                            <label for="name" >Lesson Subject: </label>
                            <textarea id="lesson_subject" name="lesson_subject" rows="8" cols="45"></textarea>
                        </div>
                        <div class="rowElem">
                            <label for="name">Date:</label>
                            <input type="text" size="20" id="datepicker"/>
                        </div>                        
                        <div class="rowElem">
                            <label for="name" >Writer Name: </label>
                            <input type="text" size="20" id="course_writer" name="course_writer"/>
                        </div>
                        <div class="rowElem">
                            <label for="name" >Grade Level: </label>
                            <input type="text" size="20" name="course_level"/>
                        </div>
                    </form>
                </div>
                <div class="wiz-nav">
                    <input class="back btn" id="openLD" onclick="loadLD.openManifest();" type="button" value="LD Project" />
                    <input  class="next btn" id="next" type="button" value="Next >" onclick="$('.tutorialSteps li:nth-child(2)').addClass('tutorialStepSelected').siblings('li').removeClass('tutorialStepSelected');$('.tutorialExplanation > li:nth-child(2)').show().siblings().hide();" />
                </div>                
            </div>
            
            <div id="wizard-2">
                <!--<div align="left" id="SubjectAndTeamDefintionProcessTabs">
                    <ul>
                        <span>
                            <li><a href="#subjectDefinition"><small >Problem Definition</small></a></li>
                            <li><a href="#problemCompose"><small >Edit Problem</small></a></li>                            
                        </span>
                    </ul>-->
                    <div style=" color:#0464BB;" id="subjectDefinition">
                        <p style="margin:1em;">In the section bellow we define information about the topic and content of lesson
                            <span id="resizeWikiMindMap" style="float:right;cursor:pointer"title="click to resize wikimindmap">resize <img align="center" src="javascript_css/images/enlarge.gif"/></span>
                        </p>
                         <div id="OntologySide">
                                <!--<div style="float:none;position:absolute;width:310px;right:20px">
                                    <div style="float:right"><a onclick="$('#selectionConceptMap > div:eq(0)').find('ol').animate({'height': 'toggle'}, { duration: 1000 }); return false;" href="#"> show/hide</a></div>
                                    <div id="selectionConceptMap">
                                        <select id="termsToConceptMapSelection" multiple="multiple" class="asmselect" title="Please select a concept map">

                                        </select>
                                    </div>

                                </div>-->

                                <div class="WikipediaCorner" >
                                    <iframe name="WikipediaIframe" id="WikipediaIframe" width="100%" height="99%" src="http://www.wikimindmap.org/"></iframe>
                                </div>
                                <!--<div class="OntologySideTop" ></div>
                                <div class="OntologyContent">
                                    <div style="display:none;margin-top:150px;" align="center" id="dvloader"><img src="javascript_css/images/loadinfo.gif" /></div>
                                    <div id="ontologyTree" class="ontologyTreeSide"></div>
                                </div>
                                <div class="OntologySideBottom" ></div>-->
                            </div>
                        <div class="wiz-content">
                            <form class="jqtransform" style="display:none">
                                <div class="rowElem">
                                    <label for="name">Concept/Topic to teach:</label>
                                    <input type="text" size="20" value="enter topic to teach" onclick="changeColor()" onkeyup="lookup(this.value);"  style="color:gray"  id="course_concept" name="course_concept"/>
                                    <div class="suggestionsBox" id="suggestions" style="display: none;">
                                        <div>
                                            <table class="suggestionList" id="autoSuggestionsList">
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <div style="position:absolute" id="loadingConcepts"><img src="EuropeanaServices/js/bigLoader.gif" /></div>
                                        </div>
                                        <div class='pager' id="suggestionListpagerId" style="width:120px;margin:10px auto">
                                            <a href='#' alt='Previous' class='prevPag'>Prev</a>
                                            <span class='currentPag'></span> of <span class='totalPags'></span>
                                            <a href='#' alt='Next' class='nextPag'>Next</a>
                                        </div>
                                        
                                    </div>
                                    <!--<button class="button_add" style="margin:7px 0px 0px 0px" onclick="return addToConceptArea()"></button>
                                    <span style="top:0px;font-size:9px;">add to concept area</span>-->
                                </div>
                                <div class="rowElem">
                                    <label for="name">Prerequisites: </label>
                                    <textarea  id="CoursePrerequisites" name="course_prerequisites" rows="8" cols="45"></textarea>
                                </div>
                                <div class="rowElem">
                                    <label for="name">General Goals:</label>
                                    <textarea id="General_Goals" name="course_general_goals" rows="8" cols="45"></textarea>
                                </div>                               
                             </form>
                        </div>                        
                    </div>
                <!--
                    <div id="problemCompose">
                       <div  style=" color:#0464BB;">
                           <p style="margin-bottom:10px;">In this step you can compose the problem to be presented</p>
                            <textarea name="wysiwyg" id="wordEditor" rows="5" cols="90"></textarea>
                       </div>
                    </div>
                    
                </div>-->
                <div class="wiz-nav">
                    <input class="back btn" id="back" type="button" value="< Back" onclick="$('.tutorialSteps li:nth-child(1)').addClass('tutorialStepSelected').siblings('li').removeClass('tutorialStepSelected');$('.tutorialExplanation > li:nth-child(1)').show().siblings().hide();"/>
                    <input class="next btn" id="next" type="button" value="Next >"  onclick="$('.tutorialSteps li:nth-child(3)').addClass('tutorialStepSelected').siblings('li').removeClass('tutorialStepSelected');$('.tutorialExplanation > li:nth-child(3)').show().siblings().hide();"/>
                </div>
            </div>
            <div id="wizard-3" >                
                <div style="float:left;margin-top:5px;">
                    <!--<div class="educationalProcessOperationListTop"></div>-->
                    <div class="educationalProcessOperationList">
                        <!--<input  type="button" onclick="load_Services(this)" class="EducationOperationButton"/>-->
                        <a  title="create new act" href="#" id="phases" onclick="makeButtonSelected(this.id);document.getElementById('Phases').style.display='block';document.getElementById('Resources').style.display='none';/*document.getElementById('Activities').style.display='none';*/document.getElementById('GroupsView').style.display='none';document.getElementById('Roles').style.display='none';document.getElementById('PropertiesView').style.display='none'"  class="phasesSelected" ></a>
                        <!--<a  title="Created activities" id="LearningActivity" href="#" class="LearningActivity" onclick="makeButtonSelected(this.id);document.getElementById('Resources').style.display='none';document.getElementById('Roles').style.display='none';document.getElementById('Phases').style.display='none';document.getElementById('Activities').style.display='block';document.getElementById('GroupsView').style.display='none';document.getElementById('PropertiesView').style.display='none'" ></a>-->
                        <a  title="create/edit roles" href="#" id="roles" onclick="makeButtonSelected(this.id);document.getElementById('Resources').style.display='none';document.getElementById('Roles').style.display='block';/*document.getElementById('Activities').style.display='none';*/document.getElementById('Phases').style.display='none';document.getElementById('GroupsView').style.display='none';document.getElementById('PropertiesView').style.display='none'"  class="Roles" ></a>
                        <a  href="#" title="create/edit user groups" id="groups" onclick="makeButtonSelected(this.id);document.getElementById('Resources').style.display='none';document.getElementById('Roles').style.display='none';/*document.getElementById('Activities').style.display='none';*/document.getElementById('Phases').style.display='none';document.getElementById('GroupsView').style.display='block';document.getElementById('PropertiesView').style.display='none'"  class="Groups" ></a>
                        <a  style="display:none" href="#" id="resourcesButton" onclick="makeButtonSelected(this.id);document.getElementById('Resources').style.display='block';document.getElementById('Roles').style.display='none';/*document.getElementById('Activities').style.display='none';*/document.getElementById('Phases').style.display='none';document.getElementById('GroupsView').style.display='none';document.getElementById('PropertiesView').style.display='none'"  class="Resources" ></a>
                        <a  title="Create new property" href="#" id="propertiesButton" onclick="makeButtonSelected(this.id);document.getElementById('Phases').style.display='none';document.getElementById('Resources').style.display='none';/*document.getElementById('Activities').style.display='none';*/document.getElementById('GroupsView').style.display='none';document.getElementById('Roles').style.display='none';document.getElementById('PropertiesView').style.display='block'"  class="properties" ></a>
                    </div>
                    <!--<div class="educationalProcessOperationListBottom"></div>-->
                </div>
               
                <div class="informationPanel">
                    <div class="informationPanelTop"></div>
                    
                    <div class="Phse" id="Phases">
                        <div style="height:20px; margin:0px;font:normal 12px Verdana, Arial, Helvetica, sans-serif; text-align:center; text-decoration: underline; color:#0464BB;">Learning Acts
                            <img id="actInfo" title="info about act" style="cursor:pointer; margin:0px 3px 0px 0px; float:right;height:16px;width:16px" src="javascript_css/images/help.png"/>
                            
                        </div>
                        <div>No:<input value="0" id="phasesNum" type="text" disabled="true" size="1" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB; background:transparent;border:0px;"/></div>
                        <a title="add new act" href="#" id="PhaseOpt" style="border:0px; margin-top:5px;text-decoration:none">
                            <img src="javascript_css/project_team_form/images/buttons3.png" class="add_buttonRole" align="top"/>
                        </a>New Act
                        <ul id="Phases_Rows" class="filetree treeview"></ul>
                    </div>
                    <div class="activitiesPanel" id="Resources">
                        <div style="height:24px; margin:0px;"><img style=" height:24px;width:24px;" align="middle" src="javascript_css/images/System-Package-24x24.png" /><span style="padding:0px; font:normal 12px Verdana, Arial, Helvetica, sans-serif; text-align:justify; text-decoration: underline; color:#0464BB;">Resources</span></div>
                        <div>No:<input value="0" id="ResourcesNum" type="text" disabled="true" size="1" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB; background:transparent;border:0px;"/></div>
                        <a href="#" id="ResourcesOpt" style="border:0px; margin-top:5px;text-decoration:none" >
                                <img src="javascript_css/project_team_form/images/buttons3.png" class="add_buttonRole" align="top"/>
                        </a>Add Resource
                        <ul id="Resources_Rows"></ul>
                    </div>
                    <!--<div  class="activitiesPanel" id="Activities">
                        <div style="margin:0px; font:normal 12px Verdana, Arial, Helvetica, sans-serif; text-align:center; text-decoration: underline; color:#0464BB;">Learning Activities</div>
                        <div>No:<input value="0" id="activitiesNum" type="text" disabled="true" size="1" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB; background:transparent;border:0px;"/></div>
                        <ul id="activities_Rows"></ul>
                    </div>-->
                    
                    <div  class="addRole" id="Roles">
                        <div style="margin:0px; font:normal 12px Verdana, Arial, Helvetica, sans-serif; text-align:center; text-decoration: underline; color:#0464BB;">Roles
                            <img id="roleInfo" style="cursor:pointer; margin:0px 3px 0px 0px; float:right;height:16px;width:16px" src="javascript_css/images/help.png"/>
                        </div>
                        <input type="hidden" id="prevLearnerName" value=""/>                        
                        <div style="height:15px;">
                            <a href="#" id="learnerOpt" style="border:0px; margin-top:5px;text-decoration:none" onclick="document.getElementById('roleCategory').value='learner';document.getElementById('ArrayList').value='learnerArray';document.getElementById('function').value='add_newRow';">
                                <img src="javascript_css/project_team_form/images/buttons3.png" class="add_buttonRole" align="top"/>
                                <!--<input type="button" style="" class="add_buttonRole"  onclick="add_newRow('learner')"></input>-->
                            </a>New Learner
                        </div>
                        <img id="learner_Rows_img" style="padding-left:5px;width:9px;height:11px;margin-top:5px;" onclick="ShowHide('learner_Rows','toggle');changeIcon(this,'learner_Rows','toggle');" src="javascript_css/project_team_form/images/expand_1.png" align="bottom" />Learner


                        <ul id="learner_Rows" class="learner_Rows"></ul>
                        <div style="height:12px;"></div>
                        <input type="hidden" id="prevStaffName" value=""/>
                        <div style="height:15px;">
                            <a href="#" id="staffOpt" style="border:0px; margin-top:5px; text-decoration:none" onclick="document.getElementById('roleCategory').value='staff';document.getElementById('ArrayList').value='staffArray';document.getElementById('function').value='add_newRow';">
                                <img src="javascript_css/project_team_form/images/buttons3.png" class="add_buttonRole" align="top"/>
                                <!--<input type="button" style="margin-top:5px;margin-right:5px;" class="add_buttonRole"  onclick="add_newRow('staff',staffArray)"></input>-->
                            </a>New Staff
                        </div>
                        <img id="staff_Rows_img" style="padding-left:5px;width:9px;height:11px;margin-top:5px;" onclick="ShowHide('staff_Rows','toggle');changeIcon(this,'staff_Rows','toggle');" src="javascript_css/project_team_form/images/expand_1.png" align="bottom" />Staff
                        <ul id="staff_Rows" class="learner_Rows"></ul>
                        
                    </div>
                   
                    <div  class="GroupsRows" id="GroupsView">
                        <div style="margin:0px; font:normal 12px Verdana, Arial, Helvetica, sans-serif; text-align:center; text-decoration: underline; color:#0464BB;">Project teams
                        <img id="groupsInfo" style="cursor:pointer; margin:0px 3px 0px 0px; float:right;height:16px;width:16px" src="javascript_css/images/help.png"/>
                        </div>
                        <div class="project_teams_list">
                            
                            No:<input value="0" id="available_teamsNum" type="text" disabled="true" size="1" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB; background:transparent;border:0px;"/>
                        </div>
                        <input type="button" onclick="clearTeamsForm()" class="project_team_form" id="addGroup" onmouseover="this.style.cursor='pointer'"><span  style="margin-left:3px;">New Team</span></input>
                        
                        <ul class="filetree"  id="project_team_list">
                            
                        </ul>
                        
                    </div>
                    <div  class="propertiesPanel" id="PropertiesView">
                        <div style="margin:0px; font:normal 12px Verdana, Arial, Helvetica, sans-serif; text-align:center; text-decoration: underline; color:#0464BB;" >Properties</div>
                        <div>No:<input value="0" id="available_properties" type="text" disabled="true" size="1" style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB; background:transparent;border:0px;"/></div>                        
                        <a href="#" id="PropertiesOpt" style="border:0px; margin-top:5px;text-decoration:none" >
                                <img src="javascript_css/project_team_form/images/buttons3.png" class="add_buttonRole" align="top"/>
                        </a>Add Property
                        <ul id="properties_Rows"></ul>
                    </div>
                    <div class="informationPanelBottom"></div>
                </div>
                <div class="educationalProcessOperationPanelTop">
                    
                    <div class="educationalProcessOperationInnerPallette" id="options">
                        <div style="float:left">
                            <div style="float:none;height:20px;margin:5px 5px 0px 5px;"><img src="javascript_css/images/select.png" style="width:18px;height:16px;" align="top"/><a id="selectOpt" href="#" style="text-decoration:none;color:#6a6a6a;height:16px;margin-left:3px;" onclick="select()">Select</a></div>
                            <div style="float:none;height:20px; margin:0px 5px 0px 5px;"><img src="javascript_css/images/connection.png" style="width:18px;height:10px;" align="bottom"/><a id="connectOpt" href="#" style="text-decoration:none;color:#6a6a6a;height:16px;margin-left:3px;" onclick="connect(this)">Connect</a></div>
                        </div>
                        <div style="float:left">
                            <div style="cursor:pointer;height:20px;margin:5px 5px 0px 5px;"  title="info about select option" >
                                <img id="selectOptionInfo" style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                            </div>
                            <div style="cursor:pointer;height:20px;margin:0px 5px 0px 5px;" title="info about connect option" >
                                <img id="connectOptionInfo"  style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                            </div>
                        </div>
                        <div style="float:left">
                            <div style="float:none;height:20px;margin:5px 5px 0px 5px;"><img src="javascript_css/images/delete.gif" style="width:16px;height:16px;" align="top"/><a id="deleteSVGElement" href="#" style="text-decoration:none;color:#6a6a6a;" onclick="deleteSVGElement(this)">Delete</a></div>
                            <div style="float:none;height:20px;margin:0px 0px 0px 5px;"><img src="javascript_css/images/undo.png" style="width:16px;height:16px;" align="top"/><a id="undoAction" href="#" style="text-decoration:none;color:#6a6a6a;" onclick="undoActionList.undoAction()">Undo</a></div>
                        </div>
                        <div style="float:left">
                            <div style="cursor:pointer;height:20px;margin:5px 5px 0px 5px;"  title="info about delete option" >
                                <img id="deleteOptionInfo" style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                            </div>
                        </div>
                        <div style="background-color:#eeecdd; clear:both;height:10px;font-weight:bold;font-size:10px"  align="center">Actions</div>
                        
                    </div>
                    <div id="model" class="educationalProcessOperationInnerPallette" style="margin-left:5px">                        
                        <div style="float:left">
                            <div style="float:none;height:20px;margin:5px 5px 0px 5px;"><a title="create activity" href="#" id="learningActivity" style="text-decoration:none;color:#6a6a6a;" onclick="enableLearningActivityCreation(1)">New Learning Activity</a></div>
                            <div style="float:left;height:20px;margin:0px 0px 0px 5px;"><a title="create structure" id="activityStructure" href="#" style="text-decoration:none;color:#6a6a6a;" onclick="enableLearningActivityCreation(2)">New Activity Structure</a></div>
                        </div>
                        <div style="float:left">
                            <div style="cursor:pointer;height:20px;margin:5px 5px 0px 5px;"  title="info about learning activity" >
                                <img id="learningActivityInfo" style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                            </div>
                            <div style="cursor:pointer;height:20px;margin:0px 5px 0px 5px;" title="info about structure activity" >
                                <img id="structureActivityInfo"  style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                            </div>
                        </div>
                        <div style="background-color:#eeecdd; clear:both;height:10px;font-weight:bold;font-size:10px"  align="center">Model</div>
                    </div>
                    <div id="points"  class="educationalProcessOperationInnerPallette"  style="margin-left:5px">
                        <div id="innerPoints"  style="float:none;height:45px;">
                            <div style="float:left;">
                                <div  style="float:none;height:20px;margin:5px 5px 0px 5px; color:#6a6a6a;"><img src="javascript_css/images/inclusiveOr.png" style="width:16px;height:16px;" align="top"><a href="#" title="create inclusive or gateway" style="text-decoration:none;color:#6a6a6a;margin-left:3px;" onclick="if(allPhases.getPhases().length!=0){createInclusiveOrGateway(5,5,0)}else{alert('at least one phase must be created')}">Or Point</a></img></div>
                                <div style="float:none;height:20px;margin:0px 5px 0px 5px; color:#6a6a6a;"><img src="javascript_css/images/synch.png" style="width:16px;height:16px;" align="top"><a href="#" title="create parallel gateway" onclick="if(allPhases.getPhases().length!=0){createSyncElement(5,5,0);}else{alert('at least one phase must be created')}"style="text-decoration:none;color:#6a6a6a;margin-left:3px;">Sync.Point</a></img></div>
                            </div>
                            <div style="float:left">
                                <div style="cursor:pointer;height:20px;margin:5px 5px 0px 5px;"  title="info about inclusive or gateway" >
                                    <img id="orPointInfo" style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                                </div>
                                <div style="cursor:pointer;height:20px;margin:0px 5px 0px 5px;" title="info about parallel gateway" >
                                    <img id="joinPointInfo"  style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                                </div>
                            </div>
                            <div style="float:left;">
                                <div style="float:none;height:20px;margin:5px 5px 0px 5px; color:#6a6a6a;">
                                    <img src="javascript_css/images/control_stop.png" style="width:16px;height:16px;" align="top"><a onmouseout="this.style.background = 'transparent'" onmouseover="disable_Enable(this)" href="#" title="create end event" onclick="if(allPhases.getPhases().length!=0){addEndPoint(-1,document.getElementById('phaseTab').name,0,0)}else{alert('at least one phase must be created')}" style="text-decoration:none;color:#6a6a6a;margin-left:3px;">End Point</a></img>
                                </div>
                                <div style="float:none;height:20px;margin:5px;margin-top:0px; margin-bottom:0px; color:#6a6a6a;"><img src="javascript_css/images/startIcon.png" style="width:16px;height:16px;" align="top"><a href="#" title="create start event" onclick="if(allPhases.getPhases().length!=0){addStartPoint(0,0)}else{alert('at least one phase must be created')}" style="text-decoration:none;color:#6a6a6a;margin-left:3px;">Start Point</a></img></div>
                            </div>
                            <div style="float:left">
                                <div style="cursor:pointer;height:20px;margin:5px 5px 0px 5px;" title="info about end events">
                                    <img id="endPointInfo"  style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                                </div>
                                <div style="cursor:pointer;height:20px;margin:0px 5px 0px 5px;" title="info about start event">
                                    <img id="startPointInfo"   style="height:16px;width:16px" src="javascript_css/images/help.png"/>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both; background-color:#eeecdd;  height:10px; font-weight:bold;font-size:10px;" align="center">Points</div>
                    </div>
                </div>
                <div id="educationalProcessOperationPanel" class="educationalProcessOperationPanelCentral" >
                    <div id="phaseTab">                    
                        <ul>
                        </ul>
                    </div>                   
                    <div id="leftSide" ></div>
                    <div id="rightSide" ></div>
                    <div id="svgContainer" align="center">
                        <!--[if IE]>
                        <object style='width: 700px;height: 400px;' id='svgPanel' src='javascript_css/LearningActivities/ActivitiesOperations_1.svg' classid='image/svg+xml'>
                    <![endif]-->
                    <!--[if !IE]>-->
                     <object style='width: 700px;height: 400px;'id='svgPanel' data='javascript_css/LearningActivities/ActivitiesOperations.svg' type='image/svg+xml' >
            <!--<![endif]-->
                     </object>
                    </div>
                    
                </div>
                <div class="educationalProcessOperationPanelBottom"></div>
               
                <div class="wiz-nav">
                    <input class="back btn" id="back" type="button" value="< Back" onclick="$('.tutorialSteps li:nth-child(2)').addClass('tutorialStepSelected').siblings('li').removeClass('tutorialStepSelected');$('.tutorialExplanation > li:nth-child(2)').show().siblings().hide();Display_Palette('none');"/>
                    <input  type="button" value="Save Design" class="btn" style="float:right" id="filename" />                    
                    <input class="btn" id="next" type="button"  value="Deploy Design" onclick="try{saveToFile.saveDesign(learnerArray, staffArray, allPhases,'publish')}catch(e){console.log(e)}"/>

                    <form method="post" name="saveDesign" target="window" action="http://147.27.41.121:8080/runtimeEngine/saveToFile.jsp">
                        <input type="hidden" name="resourcesXML" id="resourcesXML" value=""/>
                        <input type="hidden" name="manifest" id="manifest" value=""/>
                        <input type="hidden" name="manifestId" id="manifestId" value=""/>
                        <input type="hidden" name="ODSGroupId" id="ODSGroupId" value=""/>
                        <input type="hidden" name="manifest_title" id="manifest_title" value=""/>
                        <input type="hidden" name="manifest_subject" id="manifest_subject" value=""/>
                        <input type="hidden" name="graphics" id="graphics" value=""/>
                        <input type="hidden" name="operation_option" id="operation_option" value=""/>
                        <!--xreiazontai sto publish-->
                        <input type="hidden" name="teams" id="teams" value=""/>
                        <input type="hidden" name="title" id="title" value=""/>
                        <input type="hidden" name="uolFile" id="uol" value=""/>
                        <input type="hidden" name="username" id="username" value=""/>
                    </form>
                    
                     <form method="post" name="deleteDesign" action="deleteProject.jsp">
                        <input type="hidden" name="delete_projectId" id="delete_projectId" value=""/>
                        <input type="hidden" name="user_encoded" id="user_encoded" value=""/>
                    </form>
                    <!--<form method="post" name="upform" target="window" action="http://localhost:8080/runtimeEngine/index.jsp?saveFileDir="<%session.getAttribute("saveDir");%>>
                        <input type="hidden" name="design" id="design" value=""/>
                        <input type="hidden" name="teams" id="teams" value=""/>
                        <input type="hidden" name="title" id="title" value=""/>
                        <input type="hidden" name="uolFile" id="uol" value=""/>
                    </form>-->

                    <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;display: none"></iframe>
                   
                </div>
            </div>            
        </div>
    </div>
    <!--<div class="toolPanel" id="toolPanel">
       <div class="toolPanelTop"><span style="padding:45px;line-height:30px;color:#6d5afa"><b>Palette</b></span</div>
        <div class="toolPanelInner">
            <ul>
                <li><img src="javascript_css/images/select.png" style="width:18px;height:16px;" align="bottom"/><a href="#"><span onclick="select()" style="margin-left:3px;">Select</span></a></li>
                <li><img src="javascript_css/images/connection.png" style="width:18px;height:10px;" align="bottom"/><a href="#"><span style="margin-left:3px;" onclick="connect()">Connection</span></a></li>
                <li><a href="#"><span onclick="deleteActivity()">Delete Activity</span></a></li>
                <li style="padding:0px;margin:0px; background-color:#eeecdd;width:90%"><span style="margin-left:25px;line-height:20px;font-weight:bold">Model</span></li>
                <li><img src="javascript_css/images/learningActivityIcon.png" style="width:12px;height:16px;"align="top"/><a href="#" id="learningActivity" style="text-decoration:none;color:#6a6a6a"><span style="line-height:16px;">Learning Activity</span></a></li>
            </ul>
        </div>
        <div class="toolPanelBottom"></div>
    </div>-->
    <!--this contains the modla window for annotations-->
    <div id="annotation-form" style="" title="Create annotation">	
        <div>
            <label for="name">annotation</label>
        </div>
        <textarea cols="45" rows="10" name="name" id="annotationTextArea" class="text ui-widget-content ui-corner-all"></textarea>
	
    </div>
    <!-- This contains the hidden content for inline calls -->
    <div style="display:none">
        <div class="browsingTeams" id="browsingTeams">
            <ul id="browsing_teamName" style="top:0px; list-style:none; overflow:auto;"> </ul>
        </div>
    </div>
    
    <div style='display:none'>
        <input id="StudentsNum" type="hidden" value="1"/>
        <input id="exists" type="hidden" value="false"/>
        <input id="team_prevName" type="hidden" value=""/>
        <div align="center" id='projet_team_inputForm' >
            <div id="learnerSearchCriteria"style="width:350px;z-index:1;margin:102px 0px 0px 20px" >
                <div style="background: transparent;padding:10px;">
                    <span style="float:right;text-decoration: underline;cursor: pointer" onclick="ShowHide('learnerSearchCriteria','hide')">close</span>
                    <span>Lesson Participated</span>
                    <div style="float:none">
                        <select id="lessons" name="lessons" multiple="multiple" class="asmselect" title="Please select a lesson">
                            <option>Databases</option>
                            <option>Human Computer Interaction</option>
                            <option>Developing Web Applications</option>
                        </select>
                    </div>
                    <span>Gender</span>
                    <div style="float:none; padding-left:65px">
                        <input type="radio" id="everyone" name="sex" value="all" checked style="float:left"> <label for="everyone" style="float:left;padding-left:10px">Everyone</label><br /><br />
                        <input type="radio" id="male" name="sex" value="male" style="float:left" > <label for="male" style="float:left;padding-left:10px">Male</label><br /><br />
                        <input type="radio" id="female" name="sex" value="female" style="float:left" > <label for="female" style="float:left;padding-left:10px">Female</label>
                    </div>
                    <p></p>
                    <div style="padding-left:137px">
                        <a href="#" class="nicebutton" onclick="loadContent();ShowHide('learnerSearchCriteria','hide')"><span>Search</span></a>
                    </div>
                </div>
                    
            </div>            
            <p align="left" style="height:5px;"><strong>This form helps you define/organize project teams.</strong></p>
            <div align="center" class="teamGroup_container">                                
                <div style="width:100%; background:transparent" class="ProjectTeamsDefinitionForm">
                    <ul>
                        <li><a href="#teamDefinitionForm"><small>Define Teams</small></a></li>
                        <li><a href="#studentBrowse"><small>Browse Students</small></a></li>
                        <!--<li><a href="#RoleDefinition"><small>Define Roles</small></a></li>-->
                    </ul>
                    <div style=" height:200px; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none" id="teamDefinitionForm">
                        <div style="height:25px">
                            <input  type="button" class="add_button" style="float:left;cursor:pointer;" title="add new student in team" onclick="add_newStudentRowInForm(0,'','','')"></input>
                            <div style="margin:4px;float:left">New student</div>
                        </div>
                        <form name="TeamForm" onsubmit="return add_newTeam(this,'')">
                            <div class="rowStud" id="projectTeam">
                                <label for="name">Team Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                                <input type="text" size="25" maxlength="25" id="team_name" value="" name="team_name"/>
                                <input type="submit" id="submitTeamHidden" style="display:none"></input>
                            </div>
                            <div style="position:relative; margin:0px 0px 0px 90px; padding-top:7px; float:left" >
                                <a href="#" class="nicebutton" id="submitTeam" onclick="$('#submitTeamHidden').click()"><span>Complete</span></a>
                            </div>
                        </form>                        
                    </div>
                    <!--edw itan to role Definition palia-->
                    <div id="studentBrowse" style="overflow:auto; height:200px;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;">
                        <div style="display:none; position:relative; margin-left:50px; top:50px;" id="browsingloader"><img src="javascript_css/images/loadinfo.gif" /></div>
                        <p align="left">
                            <div style="height:24px"> 
                                <span style="float:left;line-height:24px">Click </span><img style="float:left;margin:0px 2px 0px 2px; cursor: pointer" title="define search criteria" src="javascript_css/images/search.png" onclick="searchingCriteria()"></img>
                                <span style="float:left;line-height: 24px"> to define search criteria</span>

                            </div>
                            <!--<a href="#" class="nicebutton" onclick="loadContent()"><span>Search</span></a>-->
                        </p>
                        
                            <div id="StudentContent">                                       
                            </div>

                    </div>
                  </div>                
            </div>
        </div>
	</div>

        <div style='display:none'>
            <div id='structureActivityForm'>
                <p align="left" style="height:5px;"><strong>This area allows you to edit the parameters for this Activity Structure.</strong></p>
                <p align="left" style="margin-top:10px;">
                    <label>Title</label><input id="structureName" value="" type="input" maxlength="30" size="25"/>
                    <input type="hidden" id="prevStructureName"/>
                </p>
                <div class="teamGroup_container">
                    <div style="width:100%; background:transparent; display:block" id="activityStructureTabs">
                            <ul>
                                <li><a href="#structureDescripiton"><small>Activity Structure Description</small></a></li>
                                <li><a href="#structureEnvironment"><small>Activity Structure Environment</small></a></li>
                                <!--<li><a href="#AssignStructureRoles" id="AssignRolesStructureTab"><small>Assign Roles</small></a></li>-->
                                <li><a href="#structureInformation"><small>Information</small></a></li>
                            </ul>
                            <div style="background:transparent; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none;" id="structureDescripiton">
                                <div style="float:none;height:auto;">
                                    <p style="padding-top:0px">Number of activities to be executed<p><span style="font-size:10px">Student decides how many will be executed if it is not all selected</span></p></p>
                                    <p><input id="AllActivityToExecute" type="checkbox" onclick="if(this.checked){$('#NoOfStructureActivitesTobeExecuted').val($('#numOfIncludedActivities').val())} else $('#NoOfStructureActivitesTobeExecuted').val('0')">Every activity will be executed</input></p>
                                    <p><label style="float:left;padding-top:10px">Number to select: </label></p>                                    
                                    <input type="text" style=" float:left; width:50px;margin:5px 0px 0px 5px" disabled onkeypress="return isNumberKey(event)" value="0" id="NoOfStructureActivitesTobeExecuted"></input>
                                    <input type="hidden" id="numOfIncludedActivities" value="0"/>
                                    <div style="float:left; margin:0px;">
                                        <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/increaseArrow.png')" onclick="if(!$('#AllActivityToExecute').attr('checked')){if(parseInt($('#NoOfStructureActivitesTobeExecuted').val())<$('#numOfIncludedActivities').val())$('#NoOfStructureActivitesTobeExecuted').val(parseInt($('#NoOfStructureActivitesTobeExecuted').val())+1)}"></input></div>
                                        <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/decreaseArrow.png')" onclick="if(!$('#AllActivityToExecute').attr('checked')){if(parseInt($('#NoOfStructureActivitesTobeExecuted').val())>0)$('#NoOfStructureActivitesTobeExecuted').val(parseInt($('#NoOfStructureActivitesTobeExecuted').val())-1)}"></input></div>
                                    </div>
                                </div>
                            </div>
                            <div style="background:transparent; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none;" id="structureEnvironment">
                                <div class="toolsMaterialsTabs" style="background:transparent; width:100%">
                                    <p style="background-color:#ffe0b3;height:31px;padding:0px">
                                            <img src="javascript_css/images/tools.png" align="top" style="margin-right:2px; top:0px;"/><span style="line-height:31px;"><b>Select Tools and Material Associated with this Activity</b></span><span title="info about services and learning objects"><img id="structureServicesMaterialInfo" style="cursor:pointer;height:16px;width:16px;margin:7px 0px 0px 9px;" align="top" src="javascript_css/images/help.png"/></span>
                                    </p>
                                    <ul>
                                        <li><a href="#structureServices"><small>Services</small></a></li>
                                        <li><a href="#structureLearningObjects"><small>Learning Objects</small></a></li>
                                    </ul>
                                    <div id="structureServices" style="background:transparent;">                                        
                                         <p>In this step you set up communication among student to clarify the problem
                                         using each other and the facilitator to discuss and clarify terminology and any open issues.
                                        </p>
                                        <p>Available Services <small><a href="#" onclick="checkAllServices(true,'structure')">check all</a></small>&nbsp;&nbsp;<small><a href="#" onclick="checkAllServices(false,'structure')">uncheck all</a></small><span title="info about tools/services"><img id="structureServicesInfo" style="cursor:pointer;height:16px;width:16px;margin:1px 0px 0px 7px;" align="top" src="javascript_css/images/help.png"/></span></p>
                                        <div style="float:left">
                                            <div  id="structureServicesContent" class="availableServicesBox" >
                                                <img style="display:none; margin:50px auto;" align="center" id="structureservicesloading" src="javascript_css/images/loadinfo.gif" />
                                            </div>

                                            
                                            <div style="float:none;margin-top:5px;margin-left: 47px">
                                                <a href="#" class="nicebutton" onclick="load_Services('structure')" ><span>load services</span></a>
                                            </div>
                                        </div>
                                        <div id="structureservice_setUpContent" style="float:left;margin-left:70px">
                                            <fieldset class="service_fieldset" id="structureservice_setUp" >

                                            </fieldset>
                                        </div>
                                    </div>
                                    <div id="structureLearningObjects" class="activityCompletion" style="overflow: auto;width:830px;">
                                        <div style="float:none;height:auto;">
                                            <div style="height:60px">
                                                <p style="padding-top:0px">Add a new Resource Item</p>
                                                <div style="float:left">
                                                    <label>Title: </label>
                                                    <input type="text" style="width:150px;" value="" id="structureLearningObjectTitle"></input>
                                                </div>
                                                <a href="#" class="nicebutton" onclick="defineItemsProperties('structureLearningObject','');"><span>add item</span></a>
                                            </div>
                                            <div style="float:none;margin-top:4px;width:790px">
                                                Select the type of content
                                                <a href="#" style="float:right;margin-right:6px;text-decoration:none; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('structureLearningObjectItemViewer', 'toggle')">Item Viewer</a>
                                            </div>
                                            <div>
                                                <div class="completeThisPhasePanel">
                                                   <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="structureLearningObject" id="createNewstructureLearningObject" checked/>Text<br />
                                                   <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="structureLearningObject" id="URLstructureLearningObject"/>URL<br />
                                                   <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="structureLearningObject" id="ExistingFilestructureLearningObject"/>Upload File<br />
                                                   <!--<input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="structureLearningObject" id="ExistingResourcestructureLearningObject" />Existing Resource<br />-->
                                                   <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="structureLearningObject" id="NoResourcestructureLearningObject" />No Resource
                                                   <div>
                                                        <a class="nicebutton" href="#" id="searchButtonstructureLearningObject"><span>Search Europeana</span></a>
                                                   </div>
                                                </div>
                                                <div class="completeThisPhasePanel" style="padding:5px;width:330px;height:150px" >
                                                    <div style=" float:none">
                                                        <textarea  id="createNewstructureLearningObjectContent" cols="50" rows="8" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                                        <input type="text" id="URLstructureLearningObjectContent" style="margin-top:5px;display:block; width:250px;"  value="http://" />
                                                        <!--<input type="text" id="ExistingFilestructureLearningObjectContent" style="margin-top:5px;display:block; width:150px;" disabled/>-->
                                                        <div id="ExistingFilestructureLearningObjectContent">
                                                            <form style="width:190px;float:left;margin-top:6px;height:50px"  method="post" name="structureLearningObjectuploadFile" enctype="multipart/form-data" id="structureLearningObjectuploadFile" action="upload.jsp?submitedButton=structureLearningObject" >
                                                                <input id ="structureLearningObjectfilenameToSave" name="filenameToSave" type="hidden" value=""/>
                                                                <input id ="typestructureLearningObjectfilenameToSave" name="typefilenameToSave" type="hidden" value=""/>
                                                                <input size="15px" id="FilestructureLearningObjectButton" onclick="$('#successUploadstructureLearningObject').css('display','none');$('#ExistingFilestructureLearningObjectButton').attr('name','unclickable')" name="FilestructureLearningObjectButton" type="file"  value=""/>
                                                                <input id="structureLearningObjectsubmitFile" style="display:none" type="submit"/>
                                                            </form>
                                                            <a href="#" id="ExistingFilestructureLearningObjectButton" name="unclickable" onclick="upload.toServer('structureLearningObject')" class="nicebutton" style="margin-top:6px"><span>upload</span></a>
                                                            <img id="structureLearningObjectLoadGif" class="uploadGif" src="javascript_css/project_team_form/images/loading.gif" />
                                                            <div id="successUploadstructureLearningObject" class="success"></div>
                                                       </div>
                                                        <!--<input type="text" style="display:block; margin-top:5px; width:150px;" id="ExistingResourcestructureLearningObjectContent" disabled />-->
                                                    </div>
                                                </div>
                                                <div class="completeThisPhasePanel" style="height:250px;padding:5px;overflow:auto;width:310px;" id="structureLearningObjectItemViewer">
                                                    <input id="structureLearningObjectnoItem" type="hidden" value="1"/>
                                                    <div style="clear:both;">
                                                        <div class="th">Item Title</div>
                                                        <div class="th">Resources</div>
                                                        <div></div>
                                                    </div>
                                                    <div id="structureLearningObjectItem1"></div><!-- style="clear:both;"><div class="td"></div><div class="td"></div><div class="empty"></div></div>-->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                           <!--<div style="height:250px" id="AssignStructureRoles" >
                                <p style='font-weight:bold;'>Assign roles to this activity</p>
                                <ul class="existingRoles" id="selectStructureLearnerRole">
                                    <span style='font:normal 11px Verdana, Arial, Helvetica, sans-serif;color:#0464BB;'>Roles associated with learner responsibilities</span>
                                </ul>
                                <ul class="existingRoles" id="selectStructureStaffRole">
                                    <span style='font:normal 11px Verdana, Arial, Helvetica, sans-serif;color:#0464BB;'>Roles associated with staff responsibilities</span>                                    
                                </ul>
                            </div>-->
                            <div style="background:transparent; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none;" id="structureInformation">
                                <div style="float:none;height:auto;">
                                    <div style="height:60px">
                                        <p style="padding-top:0px">Add a new Resource Item</p>
                                        <div style="float:left">
                                            <label>Title: </label>
                                            <input type="text" style="width:150px;" value="" id="structureInformationTitle"></input>
                                        </div>
                                        <a class="nicebutton" href="#" onclick="defineItemsProperties('structureInformation','');"><span>add item</span></a>
                                    </div>
                                <div style="float:none;margin-top:4px;">
                                    Select the type of content
                                    <a href="#" style="float:right;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('structureInformationItemViewer', 'toggle')">Item Viewer</a>
                                </div>
                                <div>
                                    <div id="structureInformationRadioButtons" class="completeThisPhasePanel">
                                       <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="sRadioButtons"  value="structureInformation" id="createNewstructureInformation" checked/>Text<br />
                                       <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="sRadioButtons"  value="structureInformation" id="URLstructureInformation"/>URL<br />
                                       <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="sRadioButtons"  value="structureInformation" id="ExistingFilestructureInformation"/>Upload File<br />
                                       <!--<input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="sRadioButtons"  value="structureInformation" id="ExistingResourcestructureInformation" />Existing Resource<br />-->
                                       <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="sRadioButtons" value="structureInformation" id="NoResourcestructureInformation" />No Resource
                                       <div>
                                            <a class="nicebutton" href="#" id="searchButtonstructureInformation"><span>Search Europeana</span></a>
                                       </div>
                                    </div>
                                    <div class="completeThisPhasePanel" style="padding:5px;width:330px;height:150px" >
                                        <div style=" float:none">
                                            <textarea  id="createNewstructureInformationContent" cols="50" rows="8" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                            <input type="text" id="URLstructureInformationContent" style="margin-top:5px;display:block; width:250px;"  value="http://" />
                                            <!--<input type="text" id="ExistingFilestructureInformationContent" style="margin-top:5px;display:block; width:150px;" disabled/>-->
                                            <div id="ExistingFilestructureInformationContent">
                                                <form style="width:190px;float:left;margin-top:6px;height:50px"  method="post" name="structureInformationuploadFile" onsubmit ="return doAjaxPost('structureInformation');"enctype="multipart/form-data" id="structureInformationuploadFile" action="http://147.27.41.121:8080/runtimeEngine/upload.jsp?submitedButton=structureInformation" >
                                                    <input id ="structureInformationfilenameToSave" name="filenameToSave" type="hidden" value=""/>
                                                    <input id ="typestructureInformationfilenameToSave" name="typefilenameToSave" type="hidden" value=""/>
                                                    <input size="15px" id="FilestructureInformationButton" onclick="$('#successUploadstructureInformation').css('display','none');$('#ExistingFilestructureInformationButton').attr('name','unclickable')" name="FilestructureInformationButton" type="file"  value=""/>
                                                    <input id="structureInformationsubmitFile" style="display:none" type="submit"/>
                                                 </form>
                                                 <a href="#" id="ExistingFilestructureInformationButton" name="unclickable" onclick="upload.toServer('structureInformation')" class="nicebutton" style="margin-top:6px"><span>upload</span></a>
                                                 <img id="structureInformationLoadGif" class="uploadGif" src="javascript_css/project_team_form/images/loading.gif" />
                                                 <div id="successUploadstructureInformation" class="success"></div>
                                            </div>
                                            <!--<input type="text" style="display:block; margin-top:5px; width:150px;" id="ExistingResourcestructureInformationContent" disabled />-->
                                        </div>
                                    </div>
                                    <div class="completeThisPhasePanel" style="height:250px;padding:5px;overflow:auto;width:300px;" id="structureInformationItemViewer">
                                        <input id="structureInformationnoItem" type="hidden" value="1"/>
                                        <div style="clear:both;">
                                            <div class="th">Item Title</div>
                                            <div class="th">Resources</div>
                                            <div></div>
                                        </div>
                                        <div id="structureInformationItem1"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="nicebutton" href="#" style="margin:50px 0px 0px 450px" onclick="var environement = createEnvironment(structureLearningObjectItems,structureserviceArray,'structure',-1); activityStructuresArray.newActivityStructure(document.getElementById('structureName').value,defineRolesSelected('_activityStructure'),structureInformationItems,0,0,0,environement,'')"><span>submit</span></a>
                <!--<input style="margin-top:50px;margin-left:50%;width:50px;height:30px" value="submit" type="button" onclick=" var environement = createEnvironment(structureLearningObjectItems,structureserviceArray,'structure',-1); activityStructuresArray.newActivityStructure(document.getElementById('structureName').value,defineRolesSelected('_activityStructure'),structureInformationItems,0,0,0,environement,'')"/>-->
            </div>
        </div>
        <div style='display:none'>            
            <div align="center" id='learningActivityForm' >
                <p align="left" style="height:5px;"><strong>This area allows you to edit the parameters for this Learning Activity.</strong></p>
                <p align="left" style="margin-top:10px;">
                    <label>Title</label><input id="activityName" type="input" maxlength="30" size="25"/>
                    <input type="hidden" id="prevName"/><!--codeAllagi <input type="hidden" id="activityId"/>-->
                </p>
                
                <div class="teamGroup_container" style="overflow:auto;width:95%">
                    <div style="width:940px; overflow: auto; background:transparent" class="ActivityLearningDefinitionTabs">
                        <ul>
                            <li><a href="#ActivityDescripiton"><small>Activity Description</small></a></li>
                            <li><a href="#LearningObjectives"><small>Learning Objectives</small></a></li>
                            <li><a href="#Prerequisites"><small>Prerequisites</small></a></li>
                            <li><a href="#ToolsMaterials"><small>Environment</small></a></li>
                            <!--<li><a href="#AssignRoles" id='AssignRolesTab'><small>Assign Roles</small></a></li>-->
                            <li><a href="#completionActivityParam"><small>Finishing this activity</small></a></li>
                        </ul>
                        <div style="overflow: auto;width:870px;background:transparent; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none;" id="ActivityDescripiton">
                            <div style="float:none;height:auto;">
                                <div style="height:60px">
                                    <p style="padding-top:0px">Add a new Resource Item</p>
                                    <div style="float:left">
                                        <label>Title: </label>
                                        <input type="text" style="width:150px;" value="" id="activityDescriptionTitle"></input>
                                    </div>
                                    <a class="nicebutton" href="#" onclick="defineItemsProperties('Description','activity');"><span>add item</span></a>
                                    
                                </div>
                                <div style="clear:both;margin-top:4px;">
                                    Select the type of content
                                    <a href="#" style="float:right;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('DescriptionItemViewer', 'toggle')">Item Viewer</a>
                                </div>
                                <div>
                                    <div id="activityDescriptionRadioButtons" class="completeThisPhasePanel">
                                       <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="aRadioButtons"  value="Description" id="createNewDescription" checked/>Text<br />
                                       <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="aRadioButtons"  value="Description" id="URLDescription"/>URL<br />
                                       <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="aRadioButtons"  value="Description" id="ExistingFileDescription"/>Upload File<br />
                                       <!--<input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="aRadioButtons"  value="Description" id="ExistingResourceDescription" />Existing Resource<br />-->
                                       <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="aRadioButtons" value="Description" id="NoResourceDescription" />No Resource
                                       <div>
                                           <div id="dialog-form" title="Europeana Search">
                                                <div>
                                                    <form action="javascript:return false;">
                                                        <label for="terms"> Keywords :</label>
                                                        <input class="text ui-widget-content ui-corner-all"  type="text" name="terms" id="terms" style="width:250px;"/>
                                                        <button id="search">Search</button>
                                                    </form>
                                                </div><div id="loading" ></div>
                                                <div id="results" class="ui-widget">
                                                </div>
                                          </div>
                                           <a class="nicebutton" href="#" id="searchButton"><span>Search Europeana</span></a>
                                      </div>
                                    </div>
                                    <div class="completeThisPhasePanel" style="padding:5px;width:330px;height:150px" >
                                        <div style=" float:none">
                                            <textarea  id="createNewDescriptionContent" cols="50" rows="8" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                            <input type="text" id="URLDescriptionContent" style="margin-top:5px;display:block; width:250px;" value="http://" />
                                            
                                            <!--<input type="text" id="ExistingFileDescriptionContent" style="margin-top:5px;display:block; width:150px;" disabled/>-->
                                            <div id="ExistingFileDescriptionContent">
                                                <form style="width:190px;float:left;margin-top:6px;height:50px"  method="post" onsubmit="return doAjaxPost('Description')" name="DescriptionuploadFile" enctype="multipart/form-data" id="DescriptionuploadFile" action="http://147.27.41.121:8080/runtimeEngine/upload.jsp?submitedButton=Description">
                                                    <input id ="DescriptionfilenameToSave" name="filenameToSave" type="hidden" value=""/>
                                                    <input id ="typeDescriptionfilenameToSave" name="typefilenameToSave" type="hidden" value=""/>
                                                    <input size="15px" id="FileDescriptionButton" onclick="$('#successUploadDescription').css('display','none');$('#ExistingFileDescriptionButton').attr('name','unclickable')" type="file" name="ExistingFileDescriptionButton"  value=""/>
                                                    <input id="DescriptionsubmitFile" style="display:none" type="submit"/>
                                                </form>
                                                <a href="#" id="ExistingFileDescriptionButton" name="unclickable" onclick="upload.toServer('Description')" class="nicebutton" style="margin-top:6px"><span>upload</span></a>
                                                <img id="DescriptionLoadGif" class="uploadGif" src="javascript_css/project_team_form/images/loading.gif" />
                                                <div id="successUploadDescription" class="success"></div>
                                            </div>
                                            <!--<input type="text" style="display:block; margin-top:5px; width:150px;" id="ExistingResourceDescriptionContent" disabled />-->
                                        </div>                                        
                                    </div>
                                    <div class="completeThisPhasePanel" style="height:250px;padding:5px;overflow:auto;width:350px;" id="DescriptionItemViewer">
                                        <input id="DescriptionnoItem" type="hidden" value="1"/>
                                        <div style="clear:both;">
                                            <div class="th">Item Title</div>
                                            <div class="th">Resources</div>
                                            <div></div>
                                        </div>                                        
                                        <div id="DescriptionItem1"></div><!-- style="clear:both;"><div class="td"></div><div class="td"></div><div class="empty"></div></div>-->
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div style=" overflow: auto;width:870px; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none;background:transparent" id="LearningObjectives">
                           <div style="float:none;height:auto;">
                                <div style="height:60px">
                                    <p style="padding-top:0px">Add a new Resource Item</p>
                                    <div style="float:left">
                                        <label>Title: </label>
                                        <input type="text" style="width:150px;" value="" id="activityLearningObjectivesTitle"></input>
                                    </div>
                                    <a href="#" class="nicebutton" onclick="defineItemsProperties('activityLearningObjectives','');"><span>add item</span></a>
                                </div>
                                <div style="clear:both;margin-top:4px;">
                                    Select the type of content
                                    <a href="#" style="float:right;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('activityLearningObjectivesItemViewer', 'toggle')">Item Viewer</a>
                                </div>
                                <div>
                                    <div class="completeThisPhasePanel">
                                       <input type="radio" name="aORadioButtons" onclick="showCompletionPrereq(this.id,this.value);" value="activityLearningObjectives" id="createNewactivityLearningObjectives" checked/>Text<br />
                                       <input type="radio" name="aORadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="activityLearningObjectives" id="URLactivityLearningObjectives"/>URL<br />
                                       <input type="radio" name="aORadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="activityLearningObjectives" id="ExistingFileactivityLearningObjectives"/>Upload File<br />
                                       <!--<input type="radio" name="aORadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="activityLearningObjectives" id="ExistingResourceactivityLearningObjectives" />Existing Resource<br />-->
                                       <input type="radio" name="aORadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="activityLearningObjectives" id="NoResourceactivityLearningObjectives" />No Resource
                                       <div>                                           
                                           <a class="nicebutton" href="#" id="searchButtonactivityLearningObjectives"><span>Search Europeana</span></a>
                                       </div>
                                    </div>
                                    <div  class="completeThisPhasePanel" style="padding:5px;width: 330px;height:150px" >
                                        <div id="activityLearningObjectivesResources" style=" float:none">
                                            <textarea  id="createNewactivityLearningObjectivesContent" cols="50" rows="8" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                            <input type="text" id="URLactivityLearningObjectivesContent" style="margin-top:5px;display:block; width:250px;"  value="http://" />
                                            <!--<input type="text" id="ExistingFileactivityLearningObjectivesContent" style="margin-top:5px;display:block; width:150px;" disabled/>-->
                                            <div id="ExistingFileactivityLearningObjectivesContent">
                                                <form style="width:190px;float:left;margin-top:6px;height:50px" method="post" onsubmit="return doAjaxPost('activityLearningObjectives');" name="uploadFile" enctype="multipart/form-data" id="activityLearningObjectivesuploadFile" action="http://147.27.41.121:8080/runtimeEngine/upload.jsp?submitedButton=activityLearningObjectives" >
                                                    <input id ="activityLearningObjectivesfilenameToSave" name="filenameToSave" type="hidden" value=""/>
                                                    <input id ="typeactivityLearningObjectivesfilenameToSave" name="typefilenameToSave" type="hidden" value=""/>
                                                    <input size="15px" id="FileactivityLearningObjectivesButton" onclick="$('#successUploadactivityLearningObjectives').css('display','none');$('#ExistingFileactivityLearningObjectivesButton').attr('name','unclickable')" name="FileactivityLearningObjectivesButton" type="file"  value=""/>
                                                    <input id="activityLearningObjectivessubmitFile" style="display:none" type="submit"/>                                                                                                    
                                                </form>
                                                <a href="#" id="ExistingFileactivityLearningObjectivesButton" name="unclickable" onclick="upload.toServer('activityLearningObjectives')" class="nicebutton" style="margin-top:6px"><span>upload</span></a>
                                                <img id="activityLearningObjectivesLoadGif" class="uploadGif" src="javascript_css/project_team_form/images/loading.gif" />
                                                <div id="successUploadactivityLearningObjectives" class="success"></div>                                                
                                            </div>
                                            <!--<input type="text" style="display:block; margin-top:5px; width:150px;" id="ExistingResourceactivityLearningObjectivesContent" disabled />-->
                                        </div>
                                    </div>
                                    <div class="completeThisPhasePanel" style="height:250px;padding:5px;overflow:auto;width:350px;" id="activityLearningObjectivesItemViewer">
                                        <input id="activityLearningObjectivesnoItem" type="hidden" value="1"/>
                                        <div style="clear:both;">
                                            <div class="th">Item Title</div>
                                            <div class="th">Resources</div>
                                            <div></div>
                                        </div>
                                        <div id="activityLearningObjectivesItem1"></div><!-- style="clear:both;"><div class="td"></div><div class="td"></div><div></div></div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="overflow: auto;width:870px; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none;background:transparent" id="Prerequisites">
                          <div style="float:none;height:auto;">
                              <div style="height:60px">
                                    <p style="padding-top:0px">Add a new Resource Item</p>
                                    <div style="float:left">
                                        <label>Title: </label>
                                        <input type="text" style="width:150px;" value="" id="activityPrerequisitesTitle"></input>
                                    </div>
                                    <a href="#" class="nicebutton" onclick="defineItemsProperties('Prerequisites','activity');"><span>add item</span></a>
                                </div>
                                <div style="float:none;margin-top:4px;">
                                    Select the type of content
                                    <a href="#" style="float:right;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('PrerequisitesItemViewer', 'toggle')">Item Viewer</a>
                                </div>
                                <div>
                                    <div class="completeThisPhasePanel">
                                       <input type="radio" name="pRadioButtons" onclick="showCompletionPrereq(this.id,this.value);" value="Prerequisites" id="createNewPrerequisites" checked/>Text<br />
                                       <input type="radio" name="pRadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="Prerequisites" id="URLPrerequisites"/>URL<br />
                                       <input type="radio" name="pRadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="Prerequisites" id="ExistingFilePrerequisites"/>Upload File<br />
                                       <!--<input type="radio" name="pRadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="Prerequisites" id="ExistingResourcePrerequisites" />Existing Resource<br />-->
                                       <input type="radio" name="pRadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="Prerequisites" id="NoResourcePrerequisites" />No Resource
                                       <div>                                           
                                           <a class="nicebutton" href="#" id="searchButtonPrerequisites"><span>Search Europeana</span></a>
                                       </div>
                                    </div>
                                    <div class="completeThisPhasePanel" style="padding:5px;width: 330px;height:150px" >
                                        <div style=" float:none">
                                            <textarea  id="createNewPrerequisitesContent" cols="50" rows="8" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                            <input type="text" id="URLPrerequisitesContent" style="margin-top:5px;display:block; width:250px;" value="http://" />
                                            <!--<input type="text" id="ExistingFilePrerequisitesContent" style="margin-top:5px;display:block; width:150px;" disabled/>-->
                                            <div id="ExistingFilePrerequisitesContent">
                                                <form style="width:190px;float:left;margin-top:6px;height:50px"  method="post" name="uploadFile" enctype="multipart/form-data" onsubmit="return doAjaxPost('Prerequisites')" id="PrerequisitesuploadFile" action="http://147.27.41.121:8080/runtimeEngine/upload.jsp?submitedButton=Prerequisites" >
                                                    <input id ="PrerequisitesfilenameToSave" name="filenameToSave" type="hidden" value=""/>
                                                     <input id ="typePrerequisitesfilenameToSave" name="typefilenameToSave" type="hidden" value=""/>
                                                    <input size="15px" id="FilePrerequisitesButton" onclick="$('#successUploadPrerequisites').css('display','none');$('#ExistingFilePrerequisitesButton').attr('name','unclickable')" name="FilePrerequisitesButton" type="file"  value=""/>
                                                    <input id="PrerequisitessubmitFile" style="display:none" type="submit"/>
                                                </form>
                                                <a href="#" id="ExistingFilePrerequisitesButton" name="unclickable" onclick="upload.toServer('Prerequisites')" class="nicebutton" style="margin-top:6px"><span>upload</span></a>
                                                <img id="PrerequisitesLoadGif" class="uploadGif" src="javascript_css/project_team_form/images/loading.gif" />
                                                <div id="successUploadPrerequisites" class="success"></div>

                                            </div>

                                            <!--<input type="text" style="display:block; margin-top:5px; width:150px;" id="ExistingResourcePrerequisitesContent" disabled />-->
                                        </div>
                                    </div>
                                    <div class="completeThisPhasePanel" style="height:250px;padding:5px;overflow:auto;width:350px;" id="PrerequisitesItemViewer">
                                        <input id="PrerequisitesnoItem" type="hidden" value="1"/>
                                        <div style="clear:both;">
                                            <div class="th">Item Title</div>
                                            <div class="th">Resources</div>
                                            <div></div>
                                            
                                        </div>
                                        <div id="PrerequisitesItem1"></div><!-- style="clear:both;"><div class="td"></div><div class="td"></div><div class="empty"></div></div>-->
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div style="font:normal 12px Verdana, Arial, Helvetica, sans-serif; background:transparent; color:#0464BB;float:none;" id="ToolsMaterials">                            
                            <div class="toolsMaterialsTabs" style="background:transparent; width:100%">
                                <p style="background-color:#ffe0b3;height:31px;padding:0px">
                                    <img src="javascript_css/images/tools.png" align="top" style="margin-right:2px; top:0px;"/><span style="line-height:31px;"><b>Select Tools and Material Associated with this Activity</b></span><span title="info about services and learning objects"><img id="activityServicesMaterialInfo" style="cursor:pointer;height:16px;width:16px;margin:7px 0px 0px 9px;" align="top" src="javascript_css/images/help.png"/></span>
                                </p>
                                <ul>
                                    <li><a href="#Services"><small>Services</small></a></li>
                                    <li><a href="#LearningObjects"><small>Learning Objects</small></a></li>                                    
                                </ul>                                
                                <div id="Services" style="background:transparent;">                                    
                                     <p>In this step you set up communication among student to clarify the problem
                                     using each other and the facilitator to discuss and clarify terminology and any open issues.
                                    </p>
                                    <p>Available Services <small><a href="#" onclick="checkAllServices(true,'')">check all</a></small>&nbsp;&nbsp;<small><a href="#" onclick="checkAllServices(false,'')">uncheck all</a></small><span title="info about tools/services"><img id="activityServicesInfo" style="cursor:pointer;height:16px;width:16px;margin:1px 0px 0px 7px;" align="top" src="javascript_css/images/help.png"/></span></p>
                                    <div style="float:left">
                                        <div  id="ServicesContent" class="availableServicesBox" >
                                            <img style="display:none; margin-top:100px;" align="center" id="servicesloading" src="javascript_css/images/loadinfo.gif" />
                                        </div>

                                        <div style="float:none;margin-top:5px;margin-left: 47px">
                                            <a href="#" class="nicebutton" onclick="load_Services('activity')"><span>load services</span></a>
                                        </div>
                                    </div>
                                    <div id="service_setUpContent"style="float:left;margin-left:70px">
                                        <fieldset class="service_fieldset" id="service_setUp" >
                                            
                                        </fieldset>
                                    </div>
                                </div>
                                <div id="LearningObjects" class="activityCompletion" style="overflow: auto;width:870px;">
                                    <div style="float:none;height:auto;">
                                        <div style="height: 60px">
                                            <p style="padding-top:0px">Add a new Resource Item</p>
                                            <div style="float:left">
                                                <label>Title: </label>
                                                <input type="text" style="width:150px;" value="" id="LearningObjectTitle"></input>
                                            </div>
                                            <a href="#" class="nicebutton" onclick="defineItemsProperties('LearningObject','')"><span>add item</span></a>
                                        </div>
                                        <div style="float:none;margin-top:4px;">
                                            Select the type of content
                                            <a href="#" style="float:right;margin-right:6px;text-decoration:none; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('LearningObjectItemViewer', 'toggle')">Item Viewer</a>
                                        </div>
                                        <div>
                                            <div class="completeThisPhasePanel">
                                               <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="LearningObject" id="createNewLearningObject" checked/>Text<br />
                                               <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="LearningObject" id="URLLearningObject"/>URL<br />
                                               <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="LearningObject" id="ExistingFileLearningObject"/>Upload File<br />
                                               <!--<input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="LearningObject" id="ExistingResourceLearningObject" />Existing Resource<br />-->
                                               <input type="radio" onclick="showCompletionPrereq(this.id,this.value);" name="bRadioButtons"  value="LearningObject" id="NoResourceLearningObject" />No Resource
                                               <div>
                                                   <a class="nicebutton" href="#" id="searchButtonLearningObject"><span>Search Europeana</span></a>
                                               </div>
                                            </div>
                                            <div class="completeThisPhasePanel" style="padding:5px;width: 330px;height:150px" >
                                                <div style=" float:none">
                                                    <textarea  id="createNewLearningObjectContent" cols="50" rows="8" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                                    <input type="text" id="URLLearningObjectContent" style="margin-top:5px;display:block; width:250px;" value="http://" />
                                                    <!--<input type="text" id="ExistingFileLearningObjectContent" style="margin-top:5px;display:block; width:150px;" disabled/>-->
                                                    <div id="ExistingFileLearningObjectContent">
                                                        <form style="width:190px;float:left;margin-top:6px;height:50px"  method="post" name="LearningObjectuploadFile" enctype="multipart/form-data" onsubmit="return doAjaxPost('LearningObject');" id="LearningObjectuploadFile" action="http://147.27.41.121:8080/runtimeEngine/upload.jsp?submitedButton=LearningObject" >
                                                            <input id ="LearningObjectfilenameToSave" name="filenameToSave" type="hidden" value=""/>
                                                             <input id ="typeLearningObjectfilenameToSave" name="typefilenameToSave" type="hidden" value=""/>
                                                            <input size="15px" id="FileLearningObjectButton" onclick="$('#successUploadLearningObject').css('display','none');$('#ExistingFileLearningObjectButton').attr('name','unclickable')" name="FileLearningObjectButton" type="file"  value=""/>
                                                            <input id="LearningObjectsubmitFile" style="display:none" type="submit"/>
                                                        </form>
                                                        <a href="#" id="ExistingFileLearningObjectButton" name="unclickable" onclick="upload.toServer('LearningObject')" class="nicebutton" style="margin-top:6px"><span>upload</span></a>
                                                        <img id="LearningObjectLoadGif" class="uploadGif" src="javascript_css/project_team_form/images/loading.gif" />
                                                        <div id="successUploadLearningObject" class="success"></div>
                                                    </div>
                                                    <!--<input type="text" style="display:block; margin-top:5px; width:150px;" id="ExistingResourceLearningObjectContent" disabled />-->
                                                </div>
                                            </div>
                                            <div class="completeThisPhasePanel" style="height:225px;padding:5px;overflow:auto;width:350px;" id="LearningObjectItemViewer">
                                                <input id="LearningObjectnoItem" type="hidden" value="1"/>
                                                <div style="clear:both;">
                                                    <div class="th">Item Title</div>
                                                    <div class="th">Resources</div>
                                                    <div></div>
                                                </div>
                                                <div id="LearningObjectItem1"></div><!-- style="clear:both;"><div class="td"></div><div class="td"></div><div class="empty"></div></div>-->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--<div style="height:250px;background:transparent; " id="AssignRoles" >
                            <p style='font-weight:bold;'>Assign roles to this activity</p>
                            <ul class="existingRoles" id="selectLearnerRole" style="height:200px;overflow:auto">
                                <span style='font:normal 11px Verdana, Arial, Helvetica, sans-serif;color:#0464BB;'>Roles associated with learner responsibilities</span>                                
                            </ul>
                            <ul class="existingRoles" id="selectStaffRole" style="height:200px;overflow:auto">
                                <span style='font:normal 11px Verdana, Arial, Helvetica, sans-serif;color:#0464BB;'>Roles associated with staff responsibilities</span>                                
                            </ul>
                        </div>-->
                        <div id="completionActivityParam" style="background:transparent;">
                            <div class="completionActivityParamTabs" style="background:transparent;width:95%">
                                <ul>
                                    <li><a href="#activityCompletionRules"><small>Completion Rules</small></a></li>
                                    <li><a href="#activityCompletionFeedback"><small>Completion Feedback</small></a></li>
                                </ul>
                                <div class="activityCompletion" id="activityCompletionRules">
                                    <div style="float:none">
                                        <p style="padding-top:0px; font-weight:bold">Complete this Activity</p>
                                        <div class="completeThisPhasePanel">
                                            <input type="radio" name="caRadioButtons" onclick="showCompletionPrereq(this.id,'');" id="none" checked/>No condition<br/>
                                            <input type="radio" name="caRadioButtons" onclick="showCompletionPrereq(this.id,'');" id="userChoice" checked/>User choice<br/>
                                            <input type="radio" name="caRadioButtons" onclick="showCompletionPrereq(this.id,'');" id="activityCompletionTime"/>After a period of time<br/>
                                            <!--<input type="radio" name="caRadioButtons" onclick="showCompletionPrereq(this.id,'');" id="activityCompletionPropertySet"/>When a Property is set<br/>-->
                                        </div>

                                        <div class="completeThisPhasePanel" id="activityCompletionTimeView" style="padding:5px; display:none" >
                                            <label style="float:left;padding:2px">Days</label>
                                            <input type="text" value="1" size="5" id="activityDays" style="float:left;margin-top:4px" maxlength="3" onkeypress="return isNumberKey(event)"></input>
                                            <div style="float:left; margin:0px;">
                                                <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/increaseArrow.png')" onclick="if(document.getElementById('activityDays').value<99)document.getElementById('activityDays').value++"></input></div>
                                                <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/decreaseArrow.png')" onclick="if(document.getElementById('activityDays').value>1)document.getElementById('activityDays').value--"></input></div>
                                            </div>

                                            <label style="float:left;padding-top:2px;margin-left:10px;">Hours</label>
                                            <input type="text" value="1" size="5" id="activityHours" style="float:left;margin-top:4px" maxlength="3" onkeypress="return isNumberKey(event)"></input>
                                            <div style="float:left; margin:0px;">
                                                <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/increaseArrow.png')" onclick="if(document.getElementById('activityHours').value<99)document.getElementById('activityHours').value++"></input></div>
                                                <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/decreaseArrow.png')" onclick="if(document.getElementById('activityHours').value>1)document.getElementById('activityHours').value--"></input></div>
                                            </div>
                                        </div>
                                        <div class="completeThisPhasePanel" id="activityCompletionPropertySetView" style="padding:5px; display:none" >
                                        </div>
                                        <!--<p style="clear:both;padding-top:15px;font-weight:bold">When the activity is completed</p>-->
                                    </div>
                                    <div style="clear:left">                                        
                                        <!--<div style="clear:both">
                                            <p>
                                                <div style="float:left">send Notification                                                
                                                </div>
                                                <a class="nicebutton" href="#" onclick="defineNotificationProperties('activity')"><span>add notification</span></a>
                                                <a href="#" style="float:right;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('activityNotificationItemViewer', 'toggle')">Show/Hide Notifications</a>
                                            </p>
                                            <div class="completeThisPhasePanel" style="margin-top:0px;clear:both">
                                                <label>Subject</label><br/>
                                                <input type="text" id="activityCompletionNotificationSubject"></input><br/>
                                                <label>Content</label><br/>
                                                <textarea id="activityCompletionNotificationContent"></textarea>
                                            </div>
                                            <div class="completeThisPhasePanel" style="margin-top:0px;width:397px;height:150px;padding:5px; white-space: nowrap;">
                                                <ul class="existingRoles" id="activityCompletionSelectLearnerRole"style="width:196px;margin-right:5px;margin-top:1px;height: auto">
                                                    <li>Learner</li>
                                                </ul>
                                                <ul class="existingRoles" id="activityCompletionSelectStaffRole" style="width:196px;margin:1px 0px 0px 0px;height: auto">
                                                    <li>Staff</li>                                                
                                                </ul>
                                            </div>
                                            <div class="completeThisPhasePanel" style="height:150px;padding:5px;overflow:auto;width:190px;margin-top:0px;" id="activityNotificationItemViewer">
                                                <input id="activityNotificationNoItem" type="hidden" value="1"/>
                                                <div style="clear:both;">
                                                    <div class="th">Notification</div>
                                                </div>
                                                <div id="activityCompletionNotificationItem1"></div>
                                            </div>                                            
                                        </div>-->
                                        <div style="width:300px;clear:both">
                                            <p>change properties</p>
                                            <div style="clear:both;" id="changeActivityCompletionProperties">
                                                <input id="ActivityPropertiesnoItem" type="hidden" value="1"/>
                                                <div style="clear:both;">
                                                    <div class="th">Property</div>
                                                    <div class="th">Value</div>
                                                    <div><input onmouseover="this.style.cursor='pointer'" title="add rows" onclick="addPropertyChangeRow('','','changeActivityCompletionProperties')" type="button" class="button_add"></input></div>
                                                </div>
                                                 <div id="ActivityPropertiesItem1" style="clear:both;">
                                                     <div class="td"><select style="width:115px;height:17px;" id="ActivitySelectProperty"><option selected value=""></option></select></div>
                                                     <div class="td"><input id="ActivityProperty1" value="" style="width:115px;height:14px;" type="text"/></div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="activityCompletion" style="overflow: auto;width:880px;" id="activityCompletionFeedback">
                                    <div style="float:none;height:auto;">
                                        <div style="height:60px">
                                            <p style="padding-top:0px">Add a new Resource Item</p>
                                            <div style="float:left">
                                                <label>Title: </label>
                                                <input type="text" style="width:150px;" value="" id="activityCompletionTitle"></input>
                                            </div>
                                            <a href="#" class="nicebutton" onclick="defineItemsProperties('activityCompletion','');"><span>add item</span></a>
                                        </div>
                                        <div style="float:none;margin-top:4px;">
                                            Select the type of content
                                            <a href="#" style="float:right;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('activityCompletionItemViewer', 'toggle')">Item Viewer</a>
                                        </div>
                                        <div>
                                            <div class="completeThisPhasePanel">
                                               <input type="radio" name="aCFRadioButtons" onclick="showCompletionPrereq(this.id,this.value);" value="activityCompletion" id="createNewactivityCompletion" checked/>Text<br />
                                               <input type="radio" name="aCFRadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="activityCompletion" id="URLactivityCompletion"/>URL<br />
                                               <input type="radio" name="aCFRadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="activityCompletion" id="ExistingFileactivityCompletion"/>Upload File<br />
                                               <!--<input type="radio" name="aCFRadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="activityCompletion" id="ExistingResourceactivityCompletion" />Existing Resource<br />-->
                                               <input type="radio" name="aCFRadioButtons" onclick="showCompletionPrereq(this.id,this.value);"  value="activityCompletion" id="NoResourceactivityCompletion" />No Resource
                                               <div>
                                                   <a class="nicebutton" href="#" id="searchButtonactivityCompletion"><span>Search Europeana</span></a>
                                               </div>

                                            </div>
                                            <div class="completeThisPhasePanel" style="padding:5px;width:330px;height:150px" >
                                                <div style=" float:none">
                                                    <textarea  id="createNewactivityCompletionContent" cols="50" rows="8" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                                    <input type="text" id="URLactivityCompletionContent" style="margin-top:5px;display:block; width:250px;" value="http://" />
                                                    <!--<input type="text" id="ExistingFileactivityCompletionContent" style="margin-top:5px;display:block; width:150px;" disabled/>-->
                                                    <div id="ExistingFileactivityCompletionContent">
                                                        <form style="width:190px;float:left;margin-top:6px;height:50px"  method="post" name="activityCompletionuploadFile" onsubmit="return doAjaxPost('activityCompletion');" enctype="multipart/form-data" id="activityCompletionuploadFile" action="http://147.27.41.121:8080/runtimeEngine/upload.jsp?submitedButton=activityCompletion" >
                                                            <input id ="activityCompletionfilenameToSave" name="filenameToSave" type="hidden" value=""/>
                                                             <input id ="typeactivityCompletionfilenameToSave" name="typefilenameToSave" type="hidden" value=""/>
                                                            <input size="15px" id="FileactivityCompletionButton" onclick="$('#successUploadactivityCompletion').css('display','none');$('#ExistingFileactivityCompletionButton').attr('name','unclickable')" name="FileactivityCompletionButton" type="file"  value=""/>
                                                            <input id="activityCompletionsubmitFile" style="display:none" type="submit"/>
                                                        </form>
                                                        <a href="#" id="ExistingFileactivityCompletionButton" name="unclickable" onclick="upload.toServer('activityCompletion')" class="nicebutton" style="margin-top:6px"><span>upload</span></a>
                                                        <img id="activityCompletionLoadGif" class="uploadGif" src="javascript_css/project_team_form/images/loading.gif" />
                                                        <div id="successUploadactivityCompletion" class="success"></div>
                                                    </div>
                                                    <!--<input type="text" style="display:block; margin-top:5px; width:150px;" id="ExistingResourceactivityCompletionContent" disabled />-->
                                                </div>
                                            </div>
                                            <div class="completeThisPhasePanel" style="height:225px;padding:5px;overflow:auto;width:350px;" id="activityCompletionItemViewer">
                                                <input id="activityCompletionnoItem" type="hidden" value="1"/>
                                                <div style="clear:both;">
                                                    <div class="th">Item Title</div>
                                                    <div class="th">Resources</div>
                                                </div>
                                                <div id="activityCompletionItem1"></div><!-- style="clear:both;"><div class="td"></div><div class="td"></div></div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin:30px auto;width:50px;height:30px">
                    <a class="nicebutton" href="#" id="submitForm" onclick="try{var activities = getCurrentActivityList();var rolesSelected = defineRolesSelected('_activity');var environement = createEnvironment(LearningObjectItems,serviceArray,'',-1);addNewActivity(document.getElementById('activityName').value,activities,rolesSelected,descriptionItems,learningObjectivesItems,prerequisitesItems,activityCompletionFeedback,activityCompletionNotificationArray,environement,0,0,0,'');}catch(e){alert('error while creating object '+e)}">
                        <span>confirm</span>
                    </a>
                </div>
                
                <input id="editForm" value="apply changes"  type="button" style="display:none; margin-top:30px;width:90px;height:30px" />
                <!--(name,description,tools_materials,objectives,activities)-->
                
            </div>
            
        </div>

    <div style="display:none">
        <input type="hidden" id="PhaseOpenMode"/>        
        <div style="background:transparent; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none;"  id="PhaseDefinition">
            <div>
                <label>Title: </label>
                <input type="text" style="width:150px;" value="" id="PhaseTitle"></input>
                <input type="hidden" id="prevPhaseName" value=""/>
             </div>
             <div class="teamGroup_container">
                    <div style="width:100%; background:transparent" class="PhaseDefinitionTabs">
                        <ul>
                            <li><a href="#CompletionRule"><small>Completion Rule</small></a></li>                            
                            <li><a href="#SendNotification"><small>Send Notifications</small></a></li>
                            <li><a href="#CompletionFeedback"><small>Completion Feedback</small></a></li>
                        </ul>
                        <div style="width:95%;overflow:auto;float:none; background:transparent; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;margin:0px;height:auto;" id="CompletionRule"><!--style=" font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;float:none"-->
                            <span style="float:none; margin:0px;">Complete this phase</span>
                            <div style="clear:both; margin:0px;">
                                <div class="completeThisPhasePanel">
                                    <input type="radio" name="group1"  value="" id="NoCondition" onclick=" showCompletionPrereq(this.id,this.value);" checked><label for="NoCondition">No condition</label><br/>
                                    <input type="radio" name="group1"  value="" id="CompleteActivities" onclick=" showCompletionPrereq(this.id,this.value);" checked><label for="CompleteActivities" >When Activities have been completed</label><br />
                                    <input type="radio" name="group1"  value="" id="TimePeriod" onclick=" showCompletionPrereq(this.id,this.value);"><label for="TimePeriod">After a period of time</label><br />
                                    <!--<input type="radio" name="group1"  value="" id="Condition" onclick=" showCompletionPrereq(this.id,this.value);"/>When a condition is true<br />-->
                                </div>
                                <div style="display:none; height:60px; float:left"  class="completeThisPhasePanel" id="ActivitiesCompletionPrereq">
                                </div>
                                <div style="display:none" class="completeThisPhasePanel" id="TimePrereq">
                                    <p>Define the number of days available for the completion of this phase</p>
                                    <label style="float:left;padding:2px">Days</label>
                                    <input type="text" value="1" size="5" id="PhaseDays" style="float:left;margin-top:4px" maxlength="3" onkeypress="return isNumberKey(event)"></input>
                                    <div style="float:left; margin:0px;">
                                        <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/increaseArrow.png')" onclick="if(document.getElementById('PhaseDays').value<99)document.getElementById('PhaseDays').value++"></input></div>
                                        <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/decreaseArrow.png')" onclick="if(document.getElementById('PhaseDays').value>1)document.getElementById('PhaseDays').value--"></input></div>
                                    </div>

                                    <label style="float:left;padding-top:2px;margin-left:10px;">Hours</label>
                                    <input type="text" value="1" size="5" id="phaseHours" style="float:left;margin-top:4px" maxlength="3" onkeypress="return isNumberKey(event)"></input>
                                    <div style="float:left; margin:0px;">
                                        <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/increaseArrow.png')" onclick="if(document.getElementById('phaseHours').value<99)document.getElementById('phaseHours').value++"></input></div>
                                        <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/decreaseArrow.png')" onclick="if(document.getElementById('phaseHours').value>1)document.getElementById('phaseHours').value--"></input></div>
                                    </div>
                                </div>
                                 <div style="display:none" class="completeThisPhasePanel" id="ConditionPrereg">
                                     <div style="float:none">
                                         <label>Role</label>
                                         <select title="learner" id="RoleConditionForPhaseCompletion" onclick="showRoles(this)"><option value="learner">Learner</option><option value="staff">Staff</option></select>
                                     </div>
                                     
                                     <div style="float:none">
                                         <label>Condition</label>
                                         <select id="ConditionForPhaseCompletion">
                                             <option value="memberOfRole">A member of a role</option>
                                             <option value="usersInRole">Users in role</option>
                                             <option value="propertyHasNoValue">Property has no value</option>
                                             <option value="timeAnActivityStarted">Time an Activity started</option>
                                             <option value="completed">Completed</option>
                                             <option value="all">All of the following are true</option>
                                             <option value="any">Any of the following are true</option>
                                             <option value="none">None of the following are true</option>
                                             
                                         </select>
                                         <input type="button" value="add condition" id="addCondition" onclick="showAppropriateExpressionForCondition(document.getElementById('ConditionForPhaseCompletion').value,'conditionView')"></input>
                                     </div>
                                    
                                     <div style="margin-top:5px; float:none;" id="conditionView">                                     
                                     </div>
                                   
                                   
                                </div>
                            </div>                            
                            <div style="clear:both;padding-top:25px;">
                                <p style="margin-bottom:10px;">When the phase is completed change properties</p>
                                <div style="clear:both;" id="changeProperties">
                                     <input id="PhasePropertiesnoItem" type="hidden" value="1"/>
                                        <div style="clear:both;">
                                            <div class="th">Property</div>
                                            <div class="th">Value</div>
                                            <div><input onmouseover="this.style.cursor='pointer'" title="add rows" onclick="addPropertyChangeRow('','','changeProperties')" type="button" class="button_add"></input></div>
                                        </div>
                                     <div id="PhasePropertiesItem1" style="clear:both;">
                                         <div class="td"><select style="width:115px;height:17px;" id="PhaseSelectProperty"><option selected value=""></option></select></div>
                                         <div class="td"><input id="PhaseProperty1" value="" style="width:115px;height:14px;" type="text"/></div>
                                     </div>
                                     
                                </div>
                            </div>
                        </div>
                        <div style="width:95%;background:transparent;overflow:auto;clear:both;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB" id="SendNotification">
                            <div style="width:810px">
                                <div style="clear:both;height:30px">
                                       <a  class="nicebutton" href="#" onclick="defineNotificationProperties('Phase')"><span>add notification</span></a>
                                       <a href="#" style="float:right;margin-right:5px;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('PhaseNotificationItemViewer', 'toggle')">Show/Hide Notifications</a>
                                </div>
                                <div class="completeThisPhasePanel" style="float:left">
                                    <label>Subject</label><br/>
                                    <input type="text" id="notificationSubject"></input><br/>
                                    <label>Content</label><br/>
                                    <textarea id="notificationContent"></textarea>
                                </div>
                                <div class="completeThisPhasePanel" style="width:397px;height:150px;padding:5px; white-space: nowrap;">
                                    <ul class="existingRoles" id="PhaseSelectLearnerRole" style="width:196px;margin-right:5px;margin-top:1px;height: auto">
                                        <li>Learner</li>
                                    </ul>
                                    <ul class="existingRoles" id="PhaseSelectStaffRole" style="width:196px;margin:1px 0px 0px 0px;height: auto">
                                        <li>Staff</li>
                                    <!--<li><input type="checkbox" style="float:left;"><p style="float:left;margin-left:2px;">Staff</p> </input></li>-->
                                    </ul>
                                </div>

                               <div class="completeThisPhasePanel" style="width:200px;height:150px;padding:5px;" id="PhaseNotificationItemViewer">
                                    <input id="PhaseNotificationNoItem" type="hidden" value="1"/>
                                    <div style="clear:both;">
                                        <div class="th">Notification</div>
                                    </div>
                                    <div id="PhaseNotificationItem1"></div><!-- style="clear:both;"><div class="td" style="width:3px"></div><div class="td" style="width:85px"></div></div>-->
                               </div>
                                
                            </div>
                        </div>
                        <div style="width:90%;overflow:auto;clear:both;background:transparent; font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" id="CompletionFeedback">
                            <div>
                                <div style="height:40px">
                                    <p style="padding-top:0px">Add a new Resource Item</p>
                                    <div style="float:left">
                                        <label>Title: </label>
                                        <input type="text" style="width:150px;" value="" id="PhaseDescriptionTitle"></input>
                                    </div>
                                    <a href="#" class="nicebutton" onclick="defineItemsProperties('PhaseDescription','');"><span>add item</span></a>
                                </div>
                                <div style="clear:both;margin-top:4px;">
                                    Select the type of content
                                    <a href="#" style="float:right;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('PhaseDescriptionItemViewer', 'toggle')">Item Viewer</a>
                                </div>
                                <div style="width: 830px;">
                                    <div class="completeThisPhasePanel">
                                        <input type="radio" onclick="showCompletionPrereq(this.id,'PhaseDescription');" name="pRadioButtons"  value="Description" id="createNewPhaseDescription" checked><label for="createNewPhaseDescription"> Text</label><br />
                                            <input type="radio" onclick="showCompletionPrereq(this.id,'PhaseDescription');" name="pRadioButtons"  value="Description" id="URLPhaseDescription"><label for="URLPhaseDescription">URL</label><br />
                                       <input type="radio" onclick="showCompletionPrereq(this.id,'PhaseDescription');" name="pRadioButtons"  value="Description" id="ExistingFilePhaseDescription"><label for="ExistingFilePhaseDescription">Existing File</label><br />
                                       <!--<input type="radio" onclick="showCompletionPrereq(this.id,'PhaseDescription');" name="pRadioButtons"  value="Description" id="ExistingResourcePhaseDescription" />Existing Resource<br />-->
                                       <input type="radio" onclick="showCompletionPrereq(this.id,'PhaseDescription');" name="pRadioButtons" value="Description" id="NoResourcePhaseDescription"><label for="NoResourcePhaseDescription">No Resource</label>
                                       <div>
                                            <a class="nicebutton" href="#" id="searchButtonPhaseDescription"><span>Search Europeana</span></a>
                                       </div>

                                    </div>
                                    <div class="completeThisPhasePanel" style="padding:5px;width:340px;height:150px" >
                                        <div style=" float:none">
                                            <textarea  id="createNewPhaseDescriptionContent" cols="50" rows="8" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                            <input type="text" id="URLPhaseDescriptionContent" style="margin-top:5px;display:block; width:250px;"  value="http://" />
                                            <!--<input type="text" id="ExistingFilePhaseDescriptionContent" style="margin-top:5px;display:block; width:150px;" disabled/>-->
                                            <div id="ExistingFilePhaseDescriptionContent">
                                                <form style="width:190px;float:left;margin-top:6px;height:50px"  method="post" name="PhaseDescriptionuploadFile" onsubmit="return doAjaxPost('PhaseDescription')" enctype="multipart/form-data" id="PhaseDescriptionuploadFile" action="http://147.27.41.121:8080/runtimeEngine/upload.jsp?submitedButton=PhaseDescription" >
                                                    <input id ="PhaseDescriptionfilenameToSave" name="filenameToSave" type="hidden" value=""/>
                                                    <input id ="typePhaseDescriptionfilenameToSave" name="typefilenameToSave" type="hidden" value=""/>
                                                    <input size="15px" id="FilePhaseDescriptionButton" onclick="$('#successUploadPhaseDescription').css('display','none');$('#ExistingFilePhaseDescriptionButton').attr('name','unclickable')" name="FilePhaseDescriptionButton" type="file"  value=""/>
                                                    <input id="PhaseDescriptionsubmitFile" style="display:none" type="submit"/>
                                                </form>
                                                <a href="#" id="ExistingFilePhaseDescriptionButton" name="unclickable" onclick="upload.toServer('PhaseDescription')" class="nicebutton" style="margin-top:6px"><span>upload</span></a>
                                                <img id="PhaseDescriptionLoadGif" class="uploadGif" src="javascript_css/project_team_form/images/loading.gif" />
                                                <div id="successUploadPhaseDescription" class="success"></div>
                                            </div>
                                            <!--<input type="text" style="display:block; margin-top:5px; width:150px;" id="ExistingResourcePhaseDescriptionContent" disabled />-->
                                        </div>
                                    </div>                                    
                                    <div class="completeThisPhasePanel" style="height:225px;padding:5px;overflow:auto;width:288px;" id="PhaseDescriptionItemViewer">
                                        <input id="PhaseDescriptionnoItem" type="hidden" value="1"></input>
                                        <div style="clear:both;">
                                            <div class="th">Item Title</div>
                                            <div class="th">Resources</div>                                            
                                        </div>
                                        <div id="PhaseDescriptionItem1"></div><!-- style="clear:both;"><div class="td"></div><div class="td"></div></div>-->
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                    </div>
             </div>
            <div style="margin:30px auto;width:50px;height:30px;">
                <a id="submitPhase" href="#" class="nicebutton"  onclick="if(document.getElementById('PhaseOpenMode').value=='create'){clearAllViews(allPhases,0);createPhase(document.getElementById('PhaseTitle').value,allPhases,notificationArray,phaseFeedBackItem,0); }else{modifyPhase($('#PhaseTitle').val(),$('#PhaseDays').val(),notificationArray,phaseFeedBackItem,$('#phaseHours').val());}">
                    <span>confirm</span>
                </a>
            </div>
        </div>
    </div>
    <!--Div to open and define/assign role to activity -->
    <div style='display:none'>
        <div style="height:300px;width:500px;background:transparent; " id="AssignRoles" >
            <div style="height:250px;" >
                <p style='font-weight:bold;'>Assign roles to this activity</p>
                <p>Choose the roles that will perform the specific activity</p>
                
                    <ul class="existingRoles" id="selectLearnerRole" style="height:200px;overflow:auto">
                        <p style='margin-bottom:2px'>Learner Roles</p>
                    </ul>
                    <ul class="existingRoles" id="selectStaffRole" style="height:200px;overflow:auto;float:right">
                        <p style='margin-bottom:2px'>Staff Roles</p>
                    </ul>
                
            </div>
            <a id="assignRoleButton" name="" class="nicebutton" style="margin-left:190px" href="#" onclick="var rolesSelected = defineRolesSelected('_activity'); assignRole(this.name,rolesSelected,'activity')"><span>Assign Role(s)</span></a>
        </div>
    </div>
    <div style='display:none'>
        <div style="height:300px;width:500px;background:transparent; " id="AssignStructureRoles" >
            <div style="height:250px;" >
                <p style='font-weight:bold;'>Assign roles to this activity</p>
                <p>Choose the roles that will perform the specific activity</p>
                <ul class="existingRoles" id="selectStructureLearnerRole" style="height:200px;overflow:auto">
                    <p style='margin-bottom:2px'>Learner Roles</p>
                </ul>
                <ul class="existingRoles" id="selectStructureStaffRole" style="height:200px;overflow:auto;float:right">
                    <p style='margin-bottom:2px'>Staff Roles</p>
                </ul>
            </div>
            <a id="assignStructureRoleButton" name="" class="nicebutton" style="margin-left:190px" href="#" onclick="var rolesSelected = defineRolesSelected('_activityStructure'); assignRole(this.name,rolesSelected,'structure')"><span>Assign Role(s)</span></a>
        </div>
    </div>
    <!--end of role assign-->
    <!--send notification-->
    <div style='display:none'>
        <div style="width:812px; height:310px;overflow:auto" id="sendNotificationAfterCompletion">
             <p style="clear:both;padding:10px;font-weight:bold">Define Notifications to be sent when the activity is completed
                
             </p>
             <p>                
                <div style="float:left;margin-right:5px;padding:2px">
                    <img id="notificationInfo" style="cursor:pointer;height:16px;width:16px;margin-left:4px;" src="javascript_css/images/help.png"/>
                    send Notification
                </div>
                <a class="nicebutton" href="#" onclick="defineNotificationProperties('activity')"><span>add notification</span></a>
                <a href="#" style="float:right;text-decoration:none;font:normal 12px Verdana, Arial, Helvetica, sans-serif; color:#0464BB;" onclick="ShowHide('activityNotificationItemViewer', 'toggle')">Show/Hide Notifications</a>                
              </p>
              <div class="completeThisPhasePanel" style="height:150px;padding:5px;width:180px;margin-top:8px;clear:both">
                <label>Subject</label><br/>
                <input type="text" id="activityCompletionNotificationSubject"></input><br/>
                <label>Content</label><br/>
                <textarea style="height:84px" id="activityCompletionNotificationContent"></textarea>
              </div>
              <div class="completeThisPhasePanel" style="margin-top:8px;width:397px;height:150px;padding:5px; white-space: nowrap;">
                <ul class="existingRoles" id="activityCompletionSelectLearnerRole"style="width:196px;margin-right:5px;margin-top:1px;height: auto">
                    <li>Learner</li>
                </ul>
                <ul class="existingRoles" id="activityCompletionSelectStaffRole" style="width:196px;margin:1px 0px 0px 0px;height: auto">
                    <li>Staff</li>
                </ul>
             </div>
             <div class="completeThisPhasePanel" style="height:150px;padding:5px;overflow:auto;width:160px;margin-top:8px;" id="activityNotificationItemViewer">
                 <input id="activityNotificationNoItem" type="hidden" value="1"/>
                <div style="clear:both;">
                    <div class="th">Notification</div>
                </div>
                <div id="activityCompletionNotificationItem1"></div>
            </div>
             <div style="float:left;height:20px;margin:45px 0px 0px 380px">
                <a class="nicebutton" onclick="if(confirm('exit notification window'))$.fn.colorbox.close()" href="#"><span>complete</span></a>
             </div>
        </div>
    </div>
    <!--send notification-->
    <div style='display:none'>
        <div id='roleDefinition'>
            <input id="ArrayList" type="hidden" value=""/>
            <input id="roleCategory" type="hidden" value=""/>
            <input id="function" type="hidden" value="add_newRow"/>
            <input id="index" type="hidden" value="-1"/>
            <p>Define Roles for this Unit of Learning</p>
            <div class="RoleDefinition" id="rolesDef">                                
                <ul>
                    <li>
                        <label>Name</label><br/>
                        <input id="Role_name" maxlength="23" size="25"/>
                    </li>                    
                    <li>
                        <label>Person assigned to this role may not be assigned to any other role</label><br/>
                        <input type="checkbox" checked onclick="document.getElementById('roleConstraint_no').checked=false" id="roleConstraint_yes" value="yes">yes</input><input type="checkbox" style="margin-left:3px;"  onclick="document.getElementById('roleConstraint_yes').checked=false" id="roleConstraint_no"  value="no">no</input>
                    </li>
                    
                    <li>
                        <label>Number of person in this role</label><br/>
                        <input onkeypress="return isNumberKey(event)" id="maxRoleNum" value="" size="5" maxlength="3" type="text">Max # </input> <input style="margin-left:3px;" id="minRoleNum" value="" onkeypress="return isNumberKey(event)" type="text" size="5" maxlength="3">Min # </input>
                    </li>                    
                    <li>
                        <a title="submit" style="margin:30px 0px 0px 350px;" class="nicebutton" href="#" onclick="roleOperate(0)"><span>submit</span></a>
                        
                    </li>
                </ul>
            </div>
        </div>         
    </div>


    <div style="display:none">
        <div id="ResourcesDefinition">
            <p>Add a new Resource Item</p>
            <div class="teamGroup_container" style="padding:8px;margin:0px;">            
                <div style="float:none; height:auto;">
                               <div>
                                    <label>Identifier: </label>
                                    <input type="text" style="width:150px;" value="" id="ResourceIDentifier"></input>
                                </div>
                                <p>Select the type of content</p>
                                <div style="float:left; ">

                                    <div class="completeThisPhasePanel">
                                        <input type="radio" checked name="radio" class="styled" id="createNewResource"><label for="createNewResource" >Text</label><br />
                                        <input type="radio" name="radio" class="styled" id="ResourceURL"> <label for="ResourceURL" >URL</label><br />
                                        <input type="radio" name="radio" class="styled" id="ResourceFile"><label for="ResourceFile" >File</label><br />
                                    </div>
                                    <div class="completeThisPhasePanel" style="padding:5px;" >
                                        <div style=" float:none">
                                            <textarea id="ResourceDescription" cols="50" rows="8"onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                            <input type="text" id="ResourceurlDef" style="margin-top:5px;display:block; width:150px;" disabled value="http://" />
                                            <input type="text" id="ResourceFileDef" style="margin-top:5px;display:block; width:150px;" disabled/>
                                        </div>
                                    </div>
                                </div>
                            </div>
             </div>
        </div>
    </div>


     <div style="display:none">
        <div id="PropertiesDefinition">
            <input type="hidden" value="" id="propertyPrevValue"/>
            <p>Define Properties for this Unit of learning</p>
            <div class="Properties_container">
                <ul>                   
                    <li>
                        <label>Name:</label><br/>
                        <input type="text" style="width:150px;" maxlength="25" value="" id="PropertyName"></input>
                    </li>
                    <li>
                        <label>Type</label><br/>
                        <select id="PropertyType" onchange="properties.checkType(this.value)">
                            <option >string</option>
                            <option >boolean</option>
                            <option >integer</option>
                            <option >datetime</option>
                        </select>
                    </li>
                    <li id="propertyValue">
                       <label>Value:</label><br/>
                       <input id="PropValue" type="text" maxlength="25" style="width:150px;" value="" class="PropertyValue"></input>
                       <select style="display:none">
                           <option>True</option>
                           <option>False</option>
                       </select>
                    </li>

                    <li style="width:50%">
                        <input type="button" style="margin-top:40px; width:40px; float:right; height:30px" value="ok" onclick="properties.Operate()"/>
                    </li>
                </ul>
            </div>
        </div>
         <div style="display:none">
             <div id="orCondView" style="float:left">
                 <p>In this form you can specify condition in order to enable corresponding learning path</p>
                 <div id="orConditionDefinition">
                    <p>In the table above you can see the defined properties</p>
                    <div style="clear:both;">
                        <div class="th">Name</div>
                        <div class="th">Type</div>
                        <div class="th">Init Value</div>
                    </div>
                </div>
               <div style="float:right">
                    <input style="width:115px;height:25px" type="button" value="submit expression" onclick="addCondition($('#expressionValue').text());"/>
               </div>
               <p>current expression:</p>
               <label id="prevExpressionValue"></label>

                <div id="typingExpressionArea" style="display:none;">
                    <p>You are typing</p>
                    <label id="expressionValue"></label>                    
                </div>
                 <div id="conditionalStatement">
                    <p>Define condition</p>
                    <select id="conditionType" onchange="properties.CombineExpression('conditionType','conditionalStatement')">
                        <option selected>expression</option>
                        <option>equal</option>
                        <option>not equal</option>
                        <option>less than</option>
                        <option>greater than</option>
                        <option>sum</option>
                        <option>subtract</option>
                        <option>divide</option>
                        <option>multiply</option>
                        <option>and</option>
                        <option>or</option>                        
                        <option>not</option>
                    </select>                    
                </div>                
            </div>
            
        </div>
    </div>

    <div class="menu-panel">
        <a class="menu-tab" id="menu-tab"></a>
        <div>
            <ul class="menuSteps" style="margin-top:5px">
                <li>
                    <span style="color:#8A2BE2" onclick="newProject();">New CS</span>
                </li>
                <li>
                    <span style="color:#8A2BE2" onclick="loadLD.openManifest();">Load CS</span>
                </li>
                <li>
                    <span id ="save_designSpan" style="color:#8A2BE2" >Save CS</span>
                </li>
                <li>
                    <span id="clone_designSpan" style="color:#8A2BE2" >Clone CS</span>
                </li>
                <li>
                    <span style="color:#8A2BE2" id="menu_deploy" onclick="try{saveToFile.saveDesign(learnerArray, staffArray, allPhases,'publish')}catch(e){console.log(e)}">Deploy CS</span>
                </li>
                <li>
                    <span style="color:#8A2BE2" onclick="try{if(user!=author){alert('You are not authorized to delete project')}else{saveToFile.deleteProject(ManifestID)}}catch(e){console.log(e)}">Delete CS</span>
                </li>
                <li>
                    <span style="color:#8A2BE2" onclick="window.location='./index.jsp'">Exit Tool</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="tutorial-panel">
        <a class="tutorial-tab" id="tutorial-tab">Feedback</a>
         <div id="form-wrap">
             <ul class="tutorialSteps">
                <li class="tutorialStepSelected">
                    <span style="color:#8A2BE2">Step1</span>
                </li>
                <li>
                    <span style="color:#8A2BE2">Step2</span>
                </li>
                 <li>
                     <span style="color:#8A2BE2">Step3</span>
                 </li>
             </ul>
             <ul class="tutorialExplanation">
                 <li>
                     The first step of the graphical interface where the user fills<br/>metadata to the corresponding text fields.
                     <br/>More Specific, user defines
                     <ul style="padding:5px;list-style:none">
                         <li>The name of the Lesson Plan that he is going to design</li>
                         <li>The subject of the lesson, in the context of which this Lesson Plan takes place</li>
                         <li>The creation date of the Lesson Plan</li>
                         <li>Finally, he states his name and the level of the students that will participate</li>
                     </ul>

                 </li>
                 <li>
                     The second step of the graphical interface where user gives general information about
                         <ul style="padding:5px;list-style:none">
                             <li>The topic he is planning to teach.</li>
                             <li>The prerequisites students should have in order to participate in the process.</li>
                             <li>The general goal of the teaching process he organizes.</li>
                         </ul>
                         Finally, in order to facillitate the process of defining the abovementioned, user can consult the wikimindMap tool.
                     
                 </li>
                 <li>
                    The third step of the graphical interface where user designs the process of the scenario.
                    User creates phases (corresponds to element "Act") that holds learning activities and activity structures.
                    Each activity holds educational material and/or educational tools.<br/>
                     Each activity is assigned to specific roles. So through the design process specific roles must be created<br/>
                     The resulting educational scenario will be assigned to specific group of students each of which stands for a role
                 </li>

             </ul>
         </div>
    </div>
    
    <div id="odsGroup_dialog" title="ODS Portal Integration">        
    </div>
</body>
</html>
<!--
                    <span style="margin-top:7px; margin-left:50px; float:left">days available for this step</span>
                   <div style="float:left;margin-top:7px;margin-left:4px;" >
                       <input type="text" value="1" id="DiscussionDays" maxlength="2" style="font : normal 12px Verdana, Arial, Helvetica, sans-serif; width:16px;height:14px;" onkeypress="return isNumberKey(event)"/>
                    </div>
                    <div style="float:left;">
                       <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/increaseArrow.png')" onclick="if(document.getElementById('DiscussionDays').value<99)document.getElementById('DiscussionDays').value++"></input></div>
                       <div><input type="button" style="width:15px; height:14px; border:0px; background-image:url('javascript_css/images/decreaseArrow.png')" onclick="if(document.getElementById('DiscussionDays').value>1)document.getElementById('DiscussionDays').value--"></input></div>

                    </div>
                    -->


                    <!--complete learning activity
                     <p>Completion Parameters</p>
                            <p>Who or what determines when the activity is over:</p>
                            <ul id="completionParam">
                                <li><input type="checkbox" checked id="none"  onclick="document.getElementById('none').checked=true;document.getElementById('timer').checked=false;document.getElementById('otherLearningActivity').checked=false; document.getElementById('none_li').style.color='#0464BB';document.getElementById('timer_li').style.color='#b0b0b0';document.getElementById('otherLearningActivity_li').style.color='#b0b0b0';document.getElementById('Hours').disabled=true;document.getElementById('Days').disabled=true;"></input><img src="javascript_css/images/none.png" style="margin-left:2px;width:11px;height:13px" align="top"></img> <span style="margin-left:2px;line-height:13px;">None</span></li>
                                <li><input type="checkbox" id="timer" onclick="document.getElementById('none').checked=false;document.getElementById('timer').checked=true;document.getElementById('otherLearningActivity').checked=false;document.getElementById('none_li').style.color='#b0b0b0';document.getElementById('timer_li').style.color='#0464BB';document.getElementById('otherLearningActivity_li').style.color='#b0b0b0';document.getElementById('Hours').disabled=false;document.getElementById('Days').disabled=false;"></input><img src="javascript_css/images/clock.png" style="margin-left:2px;width:16px;height:16px" align="top"></img><span style="line-height:16px">Finish after time limit</span></li>
                                <li><input type="checkbox" id="otherLearningActivity" onclick="document.getElementById('otherLearningActivity').checked=true;document.getElementById('timer').checked=false;document.getElementById('none').checked=false;document.getElementById('none_li').style.color='#b0b0b0';document.getElementById('timer_li').style.color='#b0b0b0';document.getElementById('otherLearningActivity_li').style.color='#0464BB';document.getElementById('Hours').disabled=true;document.getElementById('Days').disabled=true;"></input><span style="line-height:16px;">Activity</span></li>
                            </ul>
                            <ul style="margin-left:10px;">
                                <li id="none_li"><span style="line-height:16px">No temporal or personal restrictions are associated with this activity</span></li>
                                <li style="color:#b0b0b0" id="timer_li">
                                    <label >Days</label><input type="text" disabled size="5" id="Days" maxlength="3" onkeypress="return isNumberKey(event)"></input>
                                    <label style="margin-left:8px;">Hours</label><input disabled size="5" type="text"  id="Hours" maxlength="3" onkeypress="return isNumberKey(event)"></input>
                                </li>
                                <li id="otherLearningActivity_li" style="color:#b0b0b0"><span style="line-height:16px;">Activity is over when another activity has finished</span></li>
                            </ul>
                            <ul style="float:left;border-top:#b0b0b0 solid 1px;padding-top:10px;margin-top:10px;">
                                <li><span>Feedback description</span></li>
                                <li><input type="checkbox" id="feedback" onclick="if(document.getElementById('feedback').checked){document.getElementById('feedbackDescription').disabled=false;document.getElementById('feedbackDescription').style.color='#000000';} else{document.getElementById('feedbackDescription').disabled=true;document.getElementById('feedbackDescription').style.color='#b0b0b0';}"></input>Show the following feedback when the activity has finished</li>
                                <li>
                                    <textarea style="color:#b0b0b0" disabled id="feedbackDescription" cols="60" rows="5" onclick="if(this.value=='Enter your description here')this.value=''">Enter your description here</textarea>
                                </li>
                            </ul>
                    -->