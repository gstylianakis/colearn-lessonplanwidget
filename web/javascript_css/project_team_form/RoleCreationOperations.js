/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//object pou krataei tis omades....oi omades swzontai se array
  function RolesArray(rolesArray){
        try{
            
            this.learnerRolesArray = rolesArray;
            this.addRole = function(role){
                this.learnerRolesArray.push(role);
            }
            this.getRoles = function(){
                return this.learnerRolesArray;
            }
        }
        catch(e){
            alert(e);
        }
    }

    //object pou einai mia omada..Oi mathites kathe omadas kratountai se array auths ths domis
    function Role(roleName,minRoleNum,maxRoleNum,personExclusiveToOneRole,option){
        this.name = roleName;
        if(option==0)
            this.identifier = uniqid();
        else
            this.identifier = option;

        this.id = this.identifier;
        /*this.to_delete = 0;
        this.setDeleteFlag = function(flag){
            if(flag){
                if(this.to_delete>0)
                    this.to_delete--;
            }
            else
                this.to_delete++;
           alert(this.to_delete)
        }
        this.getDeleteFlag = function(){
            return this.to_delete;
        }*/

        this.getId = function(){
            return this.id;
        }
        this.minNum = minRoleNum;
        this.maxNum = maxRoleNum;
        this.constraint = personExclusiveToOneRole;

        this.getName = function() {
            return this.name;
        }
        this.getMinNum = function() {
            return this.minNum;
        }
        this.getMaxNum = function() {
            return this.maxNum;
        }
         this.getConstraint = function() {
            return this.constraint;
        }
        this.setName = function(new_name){
            this.name = new_name;
        }
        this.setMinNum = function(new_MinNum){
            this.minNum = new_MinNum;
        }
        this.setMaxNum = function(new_MaxNum){
            this.maxNum = new_MaxNum;
        }
        this.setConstraint = function(new_constraint){
            this.constraint = new_constraint;
        }
        this.getIdentifier = function(){
            return this.identifier;
        }
    }

   function initializeRoleArrays(){
       try{
           return new RolesArray(new Array());
       }
       catch(e){
           alert(e);
       }
   }
    

//dimiourgia enos role...
    function createNewRole(roleName,minRoleNum,maxRoleNum,personExclusiveToOneRole,option){
        try{
            return new Role(roleName,minRoleNum,maxRoleNum,personExclusiveToOneRole,option);
            
        }
        catch(e){
            alert(e);
        }
    }
      
    
       

    function add_newRow(character,array,option){
        try{
            
            var newRow = document.createElement('li');

            var roleName = document.getElementById('Role_name').value;
            var minRoleNum = -1,maxRoleNum=-1;
            if(document.getElementById('minRoleNum').value!="")
                minRoleNum = parseInt(document.getElementById('minRoleNum').value);
            if(document.getElementById('maxRoleNum').value!="")
                maxRoleNum = parseInt(document.getElementById('maxRoleNum').value);

            var personExclusiveToOneRole = document.getElementById('roleConstraint_yes').checked
            var newRole = createNewRole(roleName,minRoleNum,maxRoleNum,personExclusiveToOneRole,option);
            var index = 0;
            index = array.getRoles().length;
                        
            array.addRole(newRole);
            var anchorDiv = document.createElement('div');
            anchorDiv.className = "informationPanelInnerAnchors";
            newRow.appendChild(anchorDiv);

            var anchor = document.createElement('a');            
            anchor.href = "#";
            anchor.setAttribute("class", character+"RoleOp"+index);
            anchor.onclick = function () {BrowseRole(index,array,character);};//"BrowseRole(index,character)";
            anchor.style.border = "0px";
            anchor.style.marginTop = "5px";
            anchor.style.marginRight = "5px";
            anchor.style.textDecoration = "none";
            anchor.style.width = "102px";
            anchor.style.height = "16px";
            anchor.style.overflow = "hidden";           
            //anchor.style.display = "inline";
            if(roleName.length>11){
                anchor.innerHTML = roleName.substring(0,11);
                anchor.innerHTML +=".."
            }
            else
                anchor.innerHTML = roleName;
            anchor.title = roleName;

            anchor.id = newRole.getIdentifier();
            anchorDiv.appendChild(anchor);
            
            newRow.id = newRole.getIdentifier()+"RoleTree";

            var imgDiv = document.createElement('div');
            
            imgDiv.style.cssFloat = "right";
            imgDiv.style.display="none"
            imgDiv.style.marginRight = "1px";
            imgDiv.style.marginTop = "5px";
            imgDiv.style.width = "35px";
            imgDiv.style.height = "16px";
            newRow.appendChild(imgDiv);
            newRow.onmouseover = function(){imgDiv.style.display='block'};
            newRow.onmouseout = function(){imgDiv.style.display = "none"};

            
            var editImg=document.createElement("IMG");
            editImg.setAttribute("src","javascript_css/images/Edit_icon.png");
            editImg.style.border="0px";
            editImg.style.cssFloat="left";
            editImg.align = "top";
            editImg.title = "edit role properties";
            editImg.onclick = function () {BrowseRole(index,array,character);};
            editImg.onmouseover = function(){this.style.cursor='pointer'};
            imgDiv.appendChild(editImg);
            //anchor.appendChild(editImg);
            var deleteImg = document.createElement("IMG");
            deleteImg.setAttribute("src","javascript_css/project_team_form/images/delete.png");
            deleteImg.style.border="0px";
            deleteImg.style.cssFloat="left";
            deleteImg.style.marginLeft = "3px";
            deleteImg.onmouseover = function(){this.style.cursor='pointer'};
            deleteImg.align = "top";
            deleteImg.title = "delete role";
            deleteImg.onclick = function(){deleteRole(newRole)};
            imgDiv.appendChild(deleteImg);


            document.getElementById(character+"_Rows").appendChild(newRow);
            //$("."+character+"RoleOp"+index).colorbox({width:"50%", inline:true, href:"#roleDefinition"});
            if(option==0)
                $(".RoleOpt").colorbox.close();
            
            changeIcon(character+'_Rows_img',character+'_Rows','show');

            addRolesToActivityForm(character,newRole);
       }
       catch(e){
           alert(e);
       }
    }
    function BrowseRole(index,array,character){
        try{
            var role = array.getRoles();

            document.getElementById("function").value = "edit";
            if(character=="learner"){
                document.getElementById("ArrayList").value="learnerArray";
                $("#prevLearnerName").val(role[index].getName());
            }
            else{
                 document.getElementById("ArrayList").value="staffArray";
                 $("#prevStaffName").val(role[index].getName());
            }
            document.getElementById("roleCategory").value = character;
            if(index<0)
                throw('please try again something went wrong');

            document.getElementById("index").value = index;

            

            document.getElementById('Role_name').value = role[index].getName();
            
            if(role[index].getMinNum()!=-1)
                document.getElementById('maxRoleNum').value = role[index].getMinNum();
            else
                document.getElementById('maxRoleNum').value=""

            if(role[index].getMaxNum()!=-1)
                document.getElementById('minRoleNum').value = role[index].getMaxNum();
            else
                document.getElementById('minRoleNum').value="";

            document.getElementById('roleConstraint_yes').checked = role[index].getConstraint();

            if(role[index].getConstraint())
                document.getElementById('roleConstraint_no').checked = false;
            else
                document.getElementById('roleConstraint_no').checked = true;


            $.fn.colorbox({width:"50%", inline:true, href:"#roleDefinition"});
        }
        catch(e){
            alert(e);
            return;
        }
    }
    function roleOperate(option){
        try{
            var array = new Array();
            var wrongInput=0;
            var exists = null,tmpRole=null;
            var prevName = "";
            var name = $('#Role_name').val();
            if(name==""){
                alert("Please define a name");
                wrongInput = -1;
                return;
            }
            //for(var i=0;i<learnerArray.getRoles().length;i++){
            exists = learnerArray.getRoles().containsName(name);
           
                //if(exists!=null)
                    //break;
            //}
            if(exists==null)
                exists = staffArray.getRoles().containsName(name);
            
            if(document.getElementById("ArrayList").value=="learnerArray"){
                array = learnerArray;
                prevName = $("#prevLearnerName").val();
            }
            else{
                array = staffArray;
                prevName = $("#prevStaffName").val();

            }

            var roleCat = document.getElementById("roleCategory").value;
            if(document.getElementById("function").value =="add_newRow"){
                if(exists!=null){
                    alert("This name already used for other role.\nPlease give different name")
                    wrongInput = -1;
                    return;
                }
                add_newRow(roleCat,array,option);
            }
            else{
                var index = document.getElementById('index').value;
                if(index<0)
                    throw('please try again something went wrong');
                var role = array.getRoles();
                
                if(prevName!=""){
                    for(var i=0;i<array.length;i++){
                        tmpRole = array.containsName(prevName);
                    if(tmpRole!=null)
                        break;
                    }
                    if (!confirm("You are going to change role status")==true)
                        return;
                    if(prevName!=name){
                        if(exists!=null){
                            alert("This name already used for other role.\nPlease give different name");
                            wrongInput = -1;
                            return;
                        }
                        var replacingName="";
                        if(name.length>11){
                            replacingName = name.substring(0,11);
                            replacingName +=".."
                        }
                        else
                            replacingName = name;
                        //$("#"+role[index].getIdentifier()).html(name);
                        $("#"+role[index].getIdentifier()).html(replacingName);
                        $("#"+role[index].getIdentifier()).attr("title",name);

                        $("#"+role[index].getId()+"_activityTxt").html(name);
                        $("#"+role[index].getId()+"_phaseTxt").html(name);
                        $("#"+role[index].getId()+"_completionTxt").html(name);
                        $("#"+role[index].getId()+"_activityStructureTxt").html(name);
                        changeRoleNamesInGroups(prevName,name);

                    }
                }
                            
                
               
                
                role[index].setName(name);
                if($('#maxRoleNum').val()!="")
                    role[index].setMinNum(parseInt($('#maxRoleNum').val()));
                else
                    role[index].setMinNum(-1);
                if($('#minRoleNum').val()!="")
                    role[index].setMaxNum(parseInt($('#minRoleNum').val()));
                else
                    role[index].setMaxNum(-1);

                role[index].setConstraint(document.getElementById('roleConstraint_yes').checked);
                $("."+roleCat+"RoleOp"+index).colorbox.close();
          }

        }
        catch(e){
            alert(e);
            
        }
        finally{
            if(wrongInput!=-1 && option==0)
                $.fn.colorbox.close();
        }
    }
    function addRolesToActivityForm(character,role){
         try{                                
             var optn = null;
             var checkbox = null;
             var txtNode = null;
             //document.getElementById('selectLearnerRole').innerHTML = "<span style='font:normal 11px Verdana, Arial, Helvetica, sans-serif;color:#0464BB;'>Roles associated with learner responsibilities</span>";
             //document.getElementById('selectStaffRole').innerHTML = "<span style='font:normal 11px Verdana, Arial, Helvetica, sans-serif;color:#0464BB;'>Roles associated with staff responsibilities</span>";
             
              var Roles,Roles2,Roles3,Roles4;
              
              if(character=='learner'){
                Roles = document.getElementById('selectLearnerRole');
                Roles4 = document.getElementById('selectStructureLearnerRole');
                Roles2 = document.getElementById('PhaseSelectLearnerRole');
                Roles3 = document.getElementById('activityCompletionSelectLearnerRole');
              }
              else{
                Roles = document.getElementById('selectStaffRole');
                Roles4 = document.getElementById('selectStructureStaffRole');
                Roles2 = document.getElementById('PhaseSelectStaffRole');
                Roles3 = document.getElementById('activityCompletionSelectStaffRole');
              }
              for(var i=0;i<4;i++){
                  optn = document.createElement("li");
                  checkbox = document.createElement("input");
                  checkbox.style.cssFloat = "left";
                  checkbox.type = "checkbox";                                    
                  checkbox.name = role.getId();
                  optn.appendChild(checkbox);                                    
                  
                  txtNode = $("<p></p>").appendTo(optn);//txtNode = document.createElement("p");
                  txtNode.css({"float":"left","padding":"0px 2px 2px 2px"});//txtNode.style.cssFloat = "left";
                  //txtNode.style.padding = "2px";
                  //txtNode.style.paddingTop = "0px";                  
                  txtNode.html(role.getName());//txtNode.innerHTML = role.getName();
                  
                  txtNode.click(function() {              
                    if($(this).parent().children().first().attr('checked'))
                        $(this).parent().children().first().removeAttr('checked');
                    else
                        $(this).parent().children().first().attr('checked','checked');
                  });
                  
                  
                  if(i==0){
                    Roles.appendChild(optn);
                    checkbox.id = role.getId()+"_activity";
                    txtNode.attr("id", role.getId()+"_activityTxt");//txtNode.id = role.getId()+"_activityTxt";
                    optn.id = role.getId()+"_activityRole";
                    /*checkbox.addEventListener("click",function(e){
                        try{
                            var id = e.target.getAttribute("id");
                            var index = (id).indexOf("_");
                            var role_id = (id).substring(0, index);
                            var role = learnerArray.getRoles().containsId(role_id);
                            if(role==null)
                                role = staffArray.getRoles().containsId(role_id);
                            if(role==null)
                                throw "error while defining roles to execute activity";
                            if(e.target.checked)
                                role.setDeleteFlag(false);
                            else
                                role.setDeleteFlag(true);
                        }
                        catch(e){
                            alert(e)
                        }
                    },false);*/
                  }
                  else if(i==1){
                    Roles2.appendChild(optn);
                    checkbox.id = role.getId()+"_phase";
                    txtNode.attr("id", role.getId()+"_phaseTxt");//txtNode.id = role.getId()+"_phaseTxt";
                    optn.id = role.getId()+"_phaseRole";
                  }
                  else if(i==2){
                    Roles3.appendChild(optn);
                    checkbox.id = role.getId()+"_completion";
                    txtNode.attr("id", role.getId()+"_completionTxt");//txtNode.id = role.getId()+"_completionTxt";
                    optn.id = role.getId()+"_completionRole";
                  }
                  else if(i==3){
                    Roles4.appendChild(optn);
                    checkbox.id = role.getId()+"_activityStructure";
                    txtNode.attr("id", role.getId()+"_activityStructureTxt");//txtNode.id = role.getId()+"_activityStructureTxt";
                    optn.id = role.getId()+"_activityStructureRole";
                    /*checkbox.addEventListener("click",function(e){
                        try{
                            var id = e.target.getAttribute("id");
                            var index = (id).indexOf("_");
                            var role_id = (id).substring(0, index);
                            var role = learnerArray.getRoles().containsId(role_id);
                            if(role==null)
                                role = staffArray.getRoles().containsId(role_id);
                            if(role==null)
                                throw "error while defining roles to execute activity";
                            if(e.target.checked)
                                role.setDeleteFlag(false);
                            else
                                role.setDeleteFlag(true);
                        }
                        catch(e){
                            alert(e)
                        }
                    },false);*/
                  }
                  
                  
              }
              
       }
       catch(e){
        alert(e);
        }
    }
    function deleteRole(role){
        try{
            console.log(roleBeingUse)
            if (!confirm("You are going to delete role")==true)
                return false;

            if(checkToDelete(role)==false){//if(role.getDeleteFlag()!=0){
                alert("Role is being used. If you want to delete you have to disable role where ever it is used");
                return;
            }
            var index=null;
            var inArray = learnerArray.getRoles();
            index = learnerArray.getRoles().indexOfStruct(role.getId());
            //if(roleToDelete!=null)
                //index = learnerArray.getRoles().indexOfStruct(role.getId())
            if(index==null){                
                index = staffArray.getRoles().indexOfStruct(role.getId());
                inArray = staffArray.getRoles();
            }
            if(index==null)
                throw "error while deleting role\n.Role id cannot be found";
            inArray.splice(index,1);
            var del = document.getElementById(role.getIdentifier()+"RoleTree");
            del.parentNode.removeChild(del);

            del = document.getElementById(role.getId()+"_activityRole");
            del.parentNode.removeChild(del);
            del = document.getElementById(role.getId()+"_phaseRole");
            del.parentNode.removeChild(del);
            del = document.getElementById(role.getId()+"_completionRole");
            del.parentNode.removeChild(del);
            del = document.getElementById(role.getId()+"_activityStructureRole");
            del.parentNode.removeChild(del);
            
        }
        catch(e){
            alert("error while deleting role "+ e)
        }

    }

function showRoles(selection){
        try{

            if((staffArray.getRoles().length+learnerArray.getRoles().length)==(selection.childNodes.length)-2)
                return;

            selection.innerHTML = "";
            
            var option = null;/*document.createElement("option");
            option.value = "learner";
            option.innerHTML = "Learner";
            selection.appendChild(option);

            option = document.createElement("option");
            option.value = "staff";
            option.innerHTML = "Staff";
            selection.appendChild(option);
            */
            for(var i=0;i<staffArray.getRoles().length;i++){
                option = document.createElement("option");
                option.value = staffArray.getRoles()[i].getName();
                option.innerHTML = option.value;
                selection.appendChild(option);
            }
            for(i=0;i<learnerArray.getRoles().length;i++){
                option = document.createElement("option");
                option.value = learnerArray.getRoles()[i].getName();
                option.innerHTML = option.value;
                selection.appendChild(option);
            }
        }
        catch(e){
            alert(e);
        }
    }

    function clearRolesActivityForm(){
        var learner = document.getElementById("selectLearnerRole");
        var staff = document.getElementById("selectStaffRole");
        var tmp;
        for(var i=0;i<learner.childNodes.length;i++){
            if(learner.childNodes[i].nodeName=="LI"){
                tmp = learner.childNodes[i];
                for(var j=0;j<tmp.childNodes.length;j++){                   
                    if(tmp.childNodes[j].nodeName=="INPUT")
                        tmp.childNodes[j].checked = false;
                }
            }
        }

        for(var i=0;i<staff.childNodes.length;i++){
            if(staff.childNodes[i].nodeName=="LI"){
                tmp = staff.childNodes[i];
                for(var j=0;j<tmp.childNodes.length;j++){
                    if(tmp.childNodes[j].nodeName=="INPUT")
                        tmp.childNodes[j].checked = false;
                }
            }
        }

    }
    function clearRolesStructureActivity(){
        var learner = document.getElementById("selectStructureLearnerRole");
        var staff = document.getElementById("selectStructureStaffRole");
        var tmp;
        for(var i=0;i<learner.childNodes.length;i++){
            if(learner.childNodes[i].nodeName=="LI"){
                tmp = learner.childNodes[i];
                for(var j=0;j<tmp.childNodes.length;j++){
                    if(tmp.childNodes[j].nodeName=="INPUT")
                        tmp.childNodes[j].checked = false;
                }
            }
        }

        for(i=0;i<staff.childNodes.length;i++){
            if(staff.childNodes[i].nodeName=="LI"){
                tmp = staff.childNodes[i];
                for(j=0;j<tmp.childNodes.length;j++){
                    if(tmp.childNodes[j].nodeName=="INPUT")
                        tmp.childNodes[j].checked = false;
                }
            }
        }

    }
function checkToDelete(role){
    try{
        var checkbox_activity = document.getElementById(role.getId()+"_activity");
        var checkbox_phase = null;
        var checkbox_completion = null;
        var checkbox_activityStructure = document.getElementById(role.getId()+"_activityStructure");
        var roleId = "";
        var roleIdButton = "";
        //console.log(roleBeingUse)
        for(var i=0;i<roleBeingUse.length;i++){
            roleId = roleBeingUse[i];//
            roleIdButton = roleBeingUse[i].substr(0,roleBeingUse[i].indexOf("-"))
            
            if(roleId.indexOf(role.getId())!=-1){
                console.log("vrika "+$("#"+roleIdButton))
                if($("#"+roleIdButton)!=null)
                    return false;
            }
        }
        /*if(checkbox_activity!=null && checkbox_activityStructure!=null)
            if(checkbox_activity.checked || checkbox_activityStructure.checked)
                return false;

        for(var i=0;i<notificationArray.length;i++){
            checkbox_phase = notificationArray[i].getReceivers();
            for(var j=0;j<checkbox_phase.length;j++){
                if(document.getElementById(checkbox_phase[j])!=null)
                    if(checkbox_phase[j]==role.getId()+"_phase")
                        return false;
            }
        }
        for(i=0;i<activityCompletionNotificationArray.length;i++){
            checkbox_completion = activityCompletionNotificationArray[i].getReceivers();
            for(j=0;j<checkbox_completion.length;j++){
                if(document.getElementById(checkbox_completion[j])!=null)
                    if(checkbox_completion[j]==role.getId()+"_completion")
                        return false;
            }
        }*/

        return true;
    }
    catch(e){
        alert(e)
    }
}

    function roleIsBeingUsed(rolesAssigned,id){
        try{
            var checkBoxRole;
            var index = -1;        
            for(var i=0;i<rolesAssigned.length;i++){
                checkBoxRole = rolesAssigned[i]                
                if(checkBoxRole.checked){                    
                    if(document.getElementById(checkBoxRole.id)!=null)
                        if(roleBeingUse.containsIdInArray(checkBoxRole.id+"-"+id)==-1)
                            roleBeingUse.push(checkBoxRole.id+"-"+id)
                }
                else if(!checkBoxRole.checked){
                    if(document.getElementById(checkBoxRole.id)!=null){
                        index = roleBeingUse.containsIdInArray(checkBoxRole.id+"-"+id);
                        if(index!=-1)
                            roleBeingUse.splice(index,1)
                    }
                }
                if(checkBoxRole.checked==null){
                    if(roleBeingUse.containsIdInArray(rolesAssigned[i]+"-"+id)==-1)
                            roleBeingUse.push(rolesAssigned[i]+"-"+id)
                }
            }
        }
        catch(e){
            console.log(e)
        }
    }
    function assignRole(activityId,rolesAssigned,option){
        try{
            if (!confirm("save assigned roles")==true){
                return;
            }
            
            var current_phase = allPhases.getPhase($('#phaseTab').attr("name"));
            if(option=="activity"){                
                var activities = current_phase.getLearningActivitiesArray().getActivities();
                var current_activity = activities.containsId(activityId);
                current_activity.setRolesAssigned(rolesAssigned);
            }
            else if(option=="structure"){
                var structures = current_phase.getActivitiesStructuresArray();
                var currentStruct = structures.containsId(activityId)                
                currentStruct.setRolesAssigned(rolesAssigned);
            }
            
        }
        catch(e){
            console.log("error while assigning role "+e)
        }
        finally{
            $.fn.colorbox.close();
        }
    }
    