var upload = {
    init: function(){
        
	document.getElementById("uploadFile").onsubmit=function() {
            document.getElementById('uploadFile').target='upload_target'; //'upload_target' is the name of the iframe
        }
        
        document.getElementById("activityLearningObjectivesuploadFile").onsubmit=function() {
            document.getElementById('activityLearningObjectivesuploadFile').target='upload_target'; //'upload_target' is the name of the iframe
            
        }
        document.getElementById("PrerequisitesuploadFile").onsubmit=function() {
            document.getElementById('PrerequisitesuploadFile').target='upload_target'; //'upload_target' is the name of the iframe
        }
        document.getElementById("LearningObjectuploadFile").onsubmit=function() {
            document.getElementById('LearningObjectuploadFile').target='upload_target'; //'upload_target' is the name of the iframe
        }
        document.getElementById("activityCompletionuploadFile").onsubmit=function() {
            document.getElementById('activityCompletionuploadFile').target='upload_target'; //'upload_target' is the name of the iframe
        }
        document.getElementById("PhaseDescriptionuploadFile").onsubmit=function() {
            document.getElementById('PhaseDescriptionuploadFile').target='upload_target'; //'upload_target' is the name of the iframe
        }
        document.getElementById("structureLearningObjectuploadFile").onsubmit=function() {
            document.getElementById('structureLearningObjectuploadFile').target='upload_target'; //'upload_target' is the name of the iframe
        }
        document.getElementById("structureInformationuploadFile").onsubmit=function() {
            document.getElementById('structureInformationuploadFile').target='upload_target'; //'upload_target' is the name of the iframe
        }
    },
    toServer: function(type){
        //$("#uploadedFile").val("C:/imsmanifest.xml")
        try{        
            $("#"+type+"filenameToSave").val(ManifestID);
            $("#type"+type+"filenameToSave").val(type);
            $("#"+type+"LoadGif").css("display","block");
            $("#"+type+"submitFile").click();        
            
        }
        catch(e){
            alert(e)
        }
    }
}

