/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package CampusOperations;
import StudentOperationsEJBs.DB_connection;
import java.util.Vector;
import javax.ejb.Stateless;
import java.sql.*;
/**
 *
 * @author Administrator
 */
@Stateless
public class AvailableServicesBean implements AvailableServices{    
    
    public Vector getAvailableCommunicationServices()throws Exception{
        DB_connection connect = new DB_connection();
        Vector communicationServices = new Vector();
        Statement stmt = null;
        try{
            stmt = connect.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM environments");
            while (rs.next()) {
                Vector tmpEntries = new Vector();
                tmpEntries.add(rs.getString(1));
                tmpEntries.add(rs.getString(2));
                tmpEntries.add(rs.getString(3));
                tmpEntries.add(rs.getString(4));
                tmpEntries.add(rs.getString(5));
                communicationServices.add(tmpEntries);
            }
            return communicationServices;
        }
        catch(Exception e){
            throw e;
        } finally{
            stmt.close();
            connect.close();
            
        }

    }
}
