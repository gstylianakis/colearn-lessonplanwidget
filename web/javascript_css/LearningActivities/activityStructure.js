/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var activityStructuresArray = {
    structureArray: null,
    numOfstructures: 1,

    initStructureArray: function(){
        activityStructuresArray.structureArray = new Array();
        return activityStructuresArray.structureArray;
        
        
    },

    addStructure: function(structure){
        activityStructuresArray.structureArray.push(structure);

        //auto den ipirxe prin
        allPhases.addPhase(structure);
    },

    newActivityStructure: function(name,rolesAssigned,information,option,positionX,positionY,environement,annotationDesc){
        try{
            var wrongInput=0,exists=null,tmpStruct=null;
            if(name==""){
                alert("Please define a name");
                wrongInput = -1;
                return;
            }
            exists = allPhases.getPhases().containsName(name);

            var prevName = $("#prevStructureName").val();//codeAllagi prevName = document.getElementById("activityId").value;
            tmpStruct = allPhases.getPhases().containsName(prevName);
            
            if(tmpStruct!=null){
                if (!confirm("You are going to change activity status")==true)
                    return;
                
                if(prevName!=name){
                    if(exists!=null){
                        alert("This name already used for other structure.\nPlease give different name");
                        wrongInput = -1;
                        return;
                    }
                    tmpStruct.setName(name);
                    /*var newName = document.createTextNode(name);
                    var txt = tmpStruct.getShape().childNodes[1].childNodes[0];
                    tmpStruct.getShape().childNodes[1].replaceChild(newName,txt);*/
                    if(name.length>20){
                        var lines = parseInt(name.length/20);
                        for(var k=0;k<lines;k++){
                            replacingName = name.substring((k*20),(k+1)*20);
                            var textNode = document.createTextNode(replacingName, true);
                            var index = (3+k);
                            var txt = tmpStruct.getShape().childNodes[index];
                            if(txt==null){
                                var svgText = document.createElementNS(svgns, 'text');
                                svgText.setAttribute('x', 10);
                                svgText.setAttribute('y', (k*12)+15);
                                svgText.setAttribute('fill', "#000");
                                svgText.setAttribute('font-size',"12");
                                svgText.setAttribute('text-anchor',"left");
                                svgText.setAttribute('fill-opacity',"1");
                                svgText.appendChild(textNode)
                                tmpStruct.getShape().appendChild(svgText)
                            }
                            else{
                                if(tmpStruct.getShape().childNodes[(3+k)].nodeName!="image" && tmpStruct.getShape().childNodes[(3+k)].nodeName!="delete_image")
                                    tmpStruct.getShape().childNodes[(3+k)].replaceChild(textNode,txt.childNodes[0]);
                            }
                        }
                        if((name.length%20)>0){
                            replacingName = name.substring((lines*20),name.length);
                            textNode = document.createTextNode(replacingName, true);
                            txt = tmpStruct.getShape().childNodes[(3+lines)];
                            if(txt==null){
                                svgText = document.createElementNS(svgns, 'text');
                                svgText.setAttribute('x', 10);
                                svgText.setAttribute('y', (lines*12)+15);
                                svgText.setAttribute('fill', "#000");
                                svgText.setAttribute('font-size',"12");
                                svgText.setAttribute('text-anchor',"left");
                                svgText.setAttribute('fill-opacity',"1");
                                svgText.appendChild(textNode)
                                tmpStruct.getShape().appendChild(svgText)
                            }
                            else
                                if(tmpStruct.getShape().childNodes[(3+lines)].nodeName!="image" && tmpStruct.getShape().childNodes[(3+lines)].nodeName!="delete_image")
                                    tmpStruct.getShape().childNodes[(3+lines)].replaceChild(textNode,txt.childNodes[0]);
                        }
                    }
                    else{
                        textNode = document.createTextNode(name, true);
                        for(var k=0;k<tmpStruct.getShape().childNodes.length;k++){
                            if(k==3){
                                txt = tmpStruct.getShape().childNodes[3].childNodes[0];
                                tmpStruct.getShape().childNodes[3].replaceChild(textNode,txt);
                            }
                            else if(k>3)
                                if(tmpStruct.getShape().childNodes[k].nodeName!="image" && tmpStruct.getShape().childNodes[k].nodeName!="delete_image")
                                    tmpStruct.getShape().childNodes[k].parentNode.removeChild(tmpStruct.getShape().childNodes[k]);
                         }
                    }
                    
                    var replacingName = "";
                    if(name.length>11){
                        replacingName = name.substring(0,11);
                        replacingName +=".."
                    }
                    else
                        replacingName = name;
                    document.getElementById(tmpStruct.getId()+"Anchor").innerHTML = replacingName;
                    document.getElementById(tmpStruct.getId()+"Anchor").title = name;
                    //$("#"+tmpStruct.getId()+"inActivitiesTree").html(replacingName);
                }
                //tmpStruct.setRolesAssigned(rolesAssigned);
                tmpStruct.setInformation(information);
                tmpStruct.setEnvironement(environement);
                tmpStruct.setNoOfActivitesTobeExecuted(parseInt($('#NoOfStructureActivitesTobeExecuted').val()));
                return;
            }
            if(exists!=null){
                alert("This name already used for other activity - structure.\nPlease give different name");
                wrongInput=-1;
                return;
            }
    
            var activities = getCurrentActivityList();
     
     //find current phase in order to get the correct activities structure..
            var curr_phase = allPhases.getPhase(document.getElementById('phaseTab').name);
            activityStructuresArray.structureArray = curr_phase.getActivitiesStructuresArray();

            var size = parseInt(activities.getActivities().length) + activityStructuresArray.numOfstructures;
            var structure = new activityStructure(name,size,rolesAssigned,information,option,environement);
            activityStructuresArray.numOfstructures++ ;
            structure.createShape(positionX,positionY,annotationDesc);
            var image = structure.getShape().getElementsByTagName("image")[0];
            image.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
            image.addEventListener('mousedown',function (evt) {
                activityStructuresArray.BrowseActivityStructure(structure);
                evt.stopPropagation();
            },false);

            var delete_image = structure.getShape().getElementsByTagName("image")[2];
            delete_image.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
            delete_image.addEventListener('mousedown',function (evt) {evt.stopPropagation();dele.deleteAllStructure(structure);select()},false);

             var thumbtack = structure.getShape().getElementsByTagName("image")[1];
             thumbtack.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
             thumbtack.addEventListener('mousedown',function (evt) {evt.stopPropagation();annotation.showAnnotation(this)},false);

             var assignRoles_img = structure.getShape().getElementsByTagName("image")[3];
             assignRoles_img.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
             assignRoles_img.addEventListener('mousedown',function (evt) {
                 evt.stopPropagation();
                 var exists = null,show=true;
                 for(var i=0;i<allPhases.getPhases().length;i++){
                    if((allPhases.getPhases()[i]).getType()=="activity-structure")
                        exists = allPhases.getPhases()[i].getActivitiesStructuresArray().containsName(structure.getName());
                        if(exists!=null){
                            show = false;
                            break;
                        }
                    }
                    if(show==false){
                        alert("The participants of the specific activity are assigned in the root structure")
                        return;
                   }

                 clearRolesStructureActivity();
                 var checkBoxRole;
                 for(var i=0;i<structure.getRolesAssigned().length;i++){
                    checkBoxRole = structure.getRolesAssigned()[i];
                    if(checkBoxRole.checked){
                        if(document.getElementById(checkBoxRole.id)!=null)
                            document.getElementById(checkBoxRole.id).checked = true;
                        }
                        else
                            if(document.getElementById(checkBoxRole.id)!=null)
                                document.getElementById(checkBoxRole.id).checked = false;
                 }
                 $("#assignStructureRoleButton").attr("name",structure.getId());
                 $.fn.colorbox({width:"550px",height:"370px", inline:true, href:"#AssignStructureRoles"})
             },false);

            activityStructuresArray.addStructure(structure);
            
            activityStructuresArray.addStructureToPhaseHTMLView(structure);
            //activityStructuresArray.addStructureToActivitiesHTMLView(structure);
            //addLearningActivityToPhaseTree(structure,curr_phase.getName()+"_PhaseTree");
            
        }
        catch(e){
            alert(e)
        }
        finally{
            if(wrongInput!=-1 && option==0)
                $("#structureActivityForm").colorbox.close();
            
            select();
        }
    },

    createShape: function(activityStructure,positionX,positionY,annotationDesc){
        try{
                var size = activityStructure.getSize();
                var name = activityStructure.getName();
                var id = activityStructure.getId();
                
                var svgDoc = document.getElementById('svgPanel').contentDocument;
                var root = svgDoc.getElementById("vectorGraphicsDoc");

                var structureRect = document.createElementNS(svgns, 'g');
                //structureRect.setAttribute('style',"cursor:move;");
                structureRect.setAttribute('id',id);//codeAllagi structureRect.setAttribute('id',size);
                structureRect.setAttribute('ObjType',"activity-structure");
                structureRect.setAttribute("transform", "translate(" + positionX + " " + positionY +")");
                structureRect.setAttribute('dragX',positionX)//(size*5));
                structureRect.setAttribute('dragY',positionY);//(size*5));//(10*size)
                structureRect.vTranslate = [positionX,positionY];//[(5*size),(5*size)];

                if(positionX==0 && positionY==0){
                    structureRect.setAttribute("transform", "translate(" + (size*5) + " " + (size*5) +")");
                    structureRect.setAttribute('dragX',(size*5));
                    structureRect.setAttribute('dragY',(size*5));//(10*size)
                    structureRect.vTranslate = [(5*size),(5*size)];
                }
                var structureRectFrame = document.createElementNS(svgns, 'rect');
                structureRectFrame.setAttribute('x', 0);
                structureRectFrame.setAttribute('y', 0);
                structureRectFrame.setAttribute('rx',5);
                structureRectFrame.setAttribute('ry',5);
                structureRectFrame.setAttribute('fill','#ffffff');
                structureRectFrame.setAttribute('stroke', "#000");
                structureRectFrame.setAttribute("fill-opacity", '1');
                structureRectFrame.setAttribute("stroke-width", '2');
                structureRectFrame.setAttribute('width', 120);
                structureRectFrame.setAttribute('height', 50);
                
                var subprocessIcon = document.createElementNS(svgns, 'rect');
                subprocessIcon.setAttribute('x', 52);
                subprocessIcon.setAttribute('y', 30);
                subprocessIcon.setAttribute('fill','#ffffff');
                subprocessIcon.setAttribute('stroke', "#000");
                subprocessIcon.setAttribute("fill-opacity", '1');
                subprocessIcon.setAttribute("stroke-width", '2');
                subprocessIcon.setAttribute('width', 16);
                subprocessIcon.setAttribute('height', 16);
                
                var title = document.createElementNS(svgns, 'title');
                title.textContent="click to 'enter' activity structure";
                subprocessIcon.appendChild(title);



                var cross = document.createElementNS(svgns, 'path');
                var p1 = "M60,33 V43 M55,38 H65";//"M22 20 V40 M10 30 H34";
                cross.setAttribute("d",p1);
                cross.setAttribute("stroke-width","2");
                cross.setAttribute("stroke","#000");
                cross.setAttribute("title","click to proccess activity structure");

                structureRect.appendChild(structureRectFrame);
                //structureRect.appendChild(svgText);
                structureRect.appendChild(subprocessIcon);
                structureRect.appendChild(cross);
                
                var textNode = null;
                var svgText = null;
                var replacingName = "";
                if(name.length>20){
                    var lines = parseInt(name.length/20);

                    for(var i=0;i<lines;i++){
                        replacingName = name.substring((i*20),(i+1)*20);
                        svgText = document.createElementNS(svgns, 'text');
                        svgText.setAttribute('x', 10);
                        svgText.setAttribute('y', (i*12)+15);
                        svgText.setAttribute('fill', "#000");
                        svgText.setAttribute('font-size',"12");
                        svgText.setAttribute('text-anchor',"left");
                        svgText.setAttribute('fill-opacity',"1");
                        textNode = document.createTextNode(replacingName, true);
                        svgText.appendChild(textNode);
                        structureRect.appendChild(svgText);
                    }
                    if((name.length%20)>0){
                        replacingName = name.substring((lines*20),name.length);
                        svgText = document.createElementNS(svgns, 'text');
                        svgText.setAttribute('x', 10);
                        svgText.setAttribute('y', (lines*12)+15);
                        svgText.setAttribute('fill', "#000");
                        svgText.setAttribute('font-size',"12");
                        svgText.setAttribute('text-anchor',"left");
                        svgText.setAttribute('fill-opacity',"1");
                        textNode = document.createTextNode(replacingName, true);
                        svgText.appendChild(textNode);
                        structureRect.appendChild(svgText);
                    }
                }
                else{
                    svgText = document.createElementNS(svgns, 'text');
                    svgText.setAttribute('x', 10);
                    svgText.setAttribute('y', 12);
                    svgText.setAttribute('fill', "#000");
                    svgText.setAttribute('font-size',"12");
                    svgText.setAttribute('text-anchor',"left");
                    svgText.setAttribute('fill-opacity',"1");
                    textNode = document.createTextNode(name, true);
                    svgText.appendChild(textNode);
                    structureRect.appendChild(svgText);
                }
                //svgText.appendChild(textNode1);
                subprocessIcon.addEventListener("mouseover",function(){                   
                   if(!to_delete)
                        this.style.cursor='pointer';
                   else
                        this.style.cursor=this.parentNode.style.cursor;
                }, false)
                cross.addEventListener("mouseover",function(){
                   if(!to_delete)
                        this.style.cursor='pointer';
                    else
                        this.style.cursor=this.parentNode.style.cursor;
                }, false)
                subprocessIcon.addEventListener("mousedown",function(){
                    if(!to_delete)
                        activityStructuresArray.BrowseInnerOfActivityStructure(activityStructure)
                }, false)
                cross.addEventListener("mousedown",function(){
                    if(!to_delete)
                        activityStructuresArray.BrowseInnerOfActivityStructure(activityStructure)
                }, false)
                
                
                var image = document.createElementNS(svgns, 'image');
                image.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/resources.png");
                image.setAttribute("width","16");
                image.setAttribute("height","16");
                image.setAttribute("x",2);
                image.setAttribute("y",50);
                image.setAttribute("display","none");
                title = document.createElementNS(svgns, 'title');
                title.textContent = "edit resources";
                image.appendChild(title);

                 var thumbtackImg = document.createElementNS(svgns, 'image');
                thumbtackImg.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/thumbtack.png");
                thumbtackImg.setAttribute("width","16");
                thumbtackImg.setAttribute("height","16");
                thumbtackImg.setAttribute("x",28);
                thumbtackImg.setAttribute("y",-16);
                thumbtackImg.setAttribute("display","none");
                title = document.createElementNS(svgns, 'title');
                title.textContent = "show/hide annotation";
                thumbtackImg.appendChild(title);

             var delete_image = document.createElementNS(svgns, 'image');
             delete_image.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/delete.gif");
             delete_image.setAttribute("width","16");
             delete_image.setAttribute("height","16");
             delete_image.setAttribute("x",103);
             delete_image.setAttribute("y",-15);
             delete_image.setAttribute("display","none");
             title = document.createElementNS(svgns, 'title');
             title.textContent = "delete structure";
             delete_image.appendChild(title);

             var assignUser_image = document.createElementNS(svgns, 'image');
             assignUser_image.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/add_user.png");
             assignUser_image.setAttribute("width","16");
             assignUser_image.setAttribute("height","16");
             assignUser_image.setAttribute("x",2);
             assignUser_image.setAttribute("y",-16);
             assignUser_image.setAttribute("display","none");
             title = document.createElementNS(svgns, 'title');
             title.textContent = "assign role to structure";
             assignUser_image.appendChild(title);

            structureRect.appendChild(image);
            structureRect.appendChild(thumbtackImg);
            structureRect.appendChild(delete_image);
            structureRect.appendChild(assignUser_image);

            var annotationElem = annotation.createSVGAnnotationElement();
            structureRect.appendChild(annotationElem);
           
            structureRect.addEventListener('mouseout',function(evt){
                    this.childNodes[0].style.stroke="#000";
                    image.setAttribute("display","none");
                    delete_image.setAttribute("display","none");
                    thumbtackImg.setAttribute("display","none");
                    assignUser_image.setAttribute("display","none");
                    HideTooltip(evt)
                } ,false);




                structureRect.addEventListener('mousedown', Grab,false);
                structureRect.addEventListener('mouseup', Drop,false);
                structureRect.addEventListener('mousemove', Drag,false);
                structureRect.addEventListener('mouseover', onMouseOver,false);
                
                root.appendChild(structureRect);

                 if(annotationDesc!=""){
                    annotation.saveAnnotation(annotationElem, annotationDesc,-1)
                }
                return structureRect;
        }
        catch(e){
            alert(e)
        }
    },
    checkCollision: function(objectMoving,currentStructures){
        try{
            var left1, left2;
            var right1, right2;
            var top1, top2;
            var bottom1, bottom2;
            left1 = parseFloat(objectMoving.getAttribute("dragX"));
            right1 = left1 + parseFloat(objectMoving.childNodes[0].getAttribute("width"));
            top1 = parseFloat(objectMoving.getAttribute("dragY"));
            bottom1 = top1 + parseFloat(objectMoving.childNodes[0].getAttribute("height"));
            
            for(var i=0;i<currentStructures.length;i++){
                left2 = parseFloat(currentStructures[i].getShape().getAttribute("dragX"));
                right2 = left2 + parseFloat(currentStructures[i].getShape().childNodes[0].getAttribute("width"));
                top2 = parseFloat(currentStructures[i].getShape().getAttribute("dragY"));
                bottom2 = top2 + parseFloat(currentStructures[i].getShape().childNodes[0].getAttribute("height"));

                //console.log("to bottom1 "+bottom1+" top2 "+top2+" to bottom2 "+bottom2+" top1 "+top1+" to right1 "+right1+"to left1 "+left1+"to left2"+left2+"right2 "+right2);
                if (bottom1 < top2) continue;
                if (top1 > bottom2) continue;
                if (right1 < left2) continue;
                if (left1 > right2) continue;
                
                return currentStructures[i];

            }
            return null;

        }
        catch(e){
            alert(e)
        }
    },
    addObject:null,
    /*updateView: function(structure,objToadd){
        structure.getShape().childNodes[0].setAttribute('stroke', "#000");
        structure.getShape().childNodes[0].setAttribute("fill-opacity", '1');
        //objToadd.setAttributeNS(null, 'pointer-events', 'none');
        objToadd.childNodes[1].setAttribute('fill', "#000");
        var len = structure.getShape().childNodes.length;
        objToadd.childNodes[1].setAttribute("y",15+parseInt(structure.getShape().childNodes[len-1].getAttribute("y")));
        //remove from activities tree in phase and added in strucure
        //var PhaseName = document.getElementById("phaseTab").innerHTML+"_PhaseTree";
        var PhaseName = document.getElementById("phaseTab").name+"_PhaseTree";

        //add to structure and remove from xima activities in phase
        document.getElementById(structure.getName() + structure.getId()+"_ActivitiesIncluded").appendChild( document.getElementById(objToadd.childNodes[1].childNodes[0].nodeValue + "inActivitiesTree"));        
        //document.getElementById(PhaseName+"_ActivitiesTree").removeChild(document.getElementById(objToadd.childNodes[1].childNodes[0].nodeValue + "inActivitiesTree"));

        structure.getShape().appendChild(objToadd.childNodes[1]);
       
    },*/
    addStructureToPhaseHTMLView: function(activityStructure){
        try{
            
            var curr_phase = allPhases.getPhases().containsName(document.getElementById("phaseTab").name)
            var PhaseName = "";
            if(curr_phase!=null)
                PhaseName = curr_phase.getId()+"_PhaseTree";

            var newRow = $("<li></li>");
            var clickable = $("<div class='hitarea'></div>").appendTo(newRow)
            newRow.attr("id",activityStructure.getId()+"inActivitiesTree");            
            //newRow.click(function (e) {);

            var span = $("<span></span>");
            span.css("color","red")
            //span.attr("title","")
            span.click(function(){
                 $("#Phases_Rows").find("span").each(function(){
                     $(this).css("color","#0464BB")
                 })
                 $(this).parent().parent().parent().find("span:first").css("color","red");
                 $(this).css("color","red")                 
                 activityStructuresArray.BrowseInnerOfActivityStructure(activityStructure)
             })
            span.attr("id",activityStructure.getId()+"Anchor");
            span.attr('class','folder');

            span.mouseover(function(){this.style.cursor='pointer'})
            var spanName="";
            if(activityStructure.getName().length>11){
                spanName = activityStructure.getName().substring(0,11);
                spanName +=".."
            }
            else
                spanName = activityStructure.getName();

            span.text(spanName)
            span.attr("title",activityStructure.getName());
            newRow.append(span);
            
            $("#Phases_Rows").treeview({
                add: newRow
            });
            curr_phase = allPhases.getPhase($('#phaseTab').attr("name"));
        
            if(curr_phase.getType()!="activity-structure")
                newRow.appendTo($("#"+PhaseName+"_ActivitiesTree"));
            else
                newRow.appendTo($("#"+curr_phase.getId()+"inActivitiesTree"+"_ActivitiesIncluded"));

            //gia ta included activties xreiazetai auto
            var activities = $("<ul></ul>").appendTo(newRow);
            activities.attr("id",newRow.attr("id")+"_ActivitiesIncluded");
            //clickable.attr("title","click to get to specific phase")
            clickable.click(function(){
                //newRow.find(">ul").toggle()
                newRow
                .swapClass( "collapsable", "expandable" )
                .swapClass( "lastCollapsable", "lastExpandable" )
                 $(this)
                        .replaceClass( "collapsable-hitarea", "expandable-hitarea" )
                        .replaceClass( "lastcollapsable-hitarea", "lastexpandable-hitarea" )
                 newRow.find( ">ul" ).toggle();
             })




            /*var newRow = document.createElement('li');
            newRow.style.clear = "both";
            newRow.style.height = "auto";
            newRow.style.marginLeft = "3px";
            newRow.id = activityStructure.getId()+"inActivitiesTree";//codeAllagi newRow.id = name+id;

            var anchorDiv = document.createElement('div');
            anchorDiv.className = "informationPanelInnerAnchors";                                    
            anchorDiv.style.cssFloat = "left";
            anchorDiv.style.height = "20px";
            
            newRow.appendChild(anchorDiv);
            
            var anchor = document.createElement('a');
            anchor.style.border = "0px";            
            anchor.style.marginRight = "5px";
            anchor.style.textDecoration = "none";
            anchor.style.width = "auto";
            anchor.style.height = "16px";
            anchor.style.cssFloat="left";
            anchor.id = activityStructure.getId()+"Anchor";//codeAllagi activityStructure.getName()+activityStructure.getId()+"Anchor";
           
            if(activityStructure.getName().length>11){
                anchor.innerHTML = activityStructure.getName().substring(0,11);
                anchor.innerHTML +=".."                
            }
            else
                anchor.innerHTML = activityStructure.getName();            
            anchor.title = activityStructure.getName();
            anchor.style.overflow = "hidden";
            
            var expandStruct=document.createElement("IMG");
            expandStruct.setAttribute("src","javascript_css/project_team_form/images/expand_1.png");
            expandStruct.id = newRow.id+"_img";
            expandStruct.style.width = "9px";
            expandStruct.style.cssFloat="left";
            expandStruct.style.height = "11px";
            expandStruct.style.marginTop = "2px";
            expandStruct.align = "middle";
            expandStruct.onclick = function(){ShowHide(newRow.id+"_ActivitiesIncluded",'toggle');changeIcon(this,newRow.id+"_ActivitiesIncluded",'toggle');};
            anchorDiv.appendChild(expandStruct);
            anchorDiv.appendChild(anchor);
            
            var imgDiv = document.createElement('div');
            imgDiv.style.cssFloat = "right";
            imgDiv.style.marginRight = "1px";
            imgDiv.style.width = "35px";
            imgDiv.style.height = "16px";
            newRow.appendChild(imgDiv);
      
            var editImg=document.createElement("IMG");
            editImg.onclick = function () {activityStructuresArray.BrowseActivityStructure(activityStructure)};
            editImg.setAttribute("src","javascript_css/project_team_form/images/edit.gif");
            editImg.style.border="0px";
            editImg.style.cssFloat="left";
            editImg.align = "top";
            editImg.onmouseover = function(){this.style.cursor='pointer'};
            imgDiv.appendChild(editImg);
                       
            var deleteImg = document.createElement("IMG");
            deleteImg.setAttribute("src","javascript_css/project_team_form/images/delete.png");
            deleteImg.style.border="0px";
            deleteImg.style.cssFloat="left";
            deleteImg.style.marginLeft = "3px";
            deleteImg.onmouseover = function(){this.style.cursor='pointer'};
            deleteImg.align = "top";
            deleteImg.onclick = function(){dele.deleteAllStructure(activityStructure)};
            imgDiv.appendChild(deleteImg);
            
            var activities = document.createElement("ul");
            activities.style.clear = "both";            
            activities.id = newRow.id+"_ActivitiesIncluded";
            activities.style.marginLeft = "10px";

            newRow.appendChild(activities);
            var curr_phase = allPhases.getPhase($('#phaseTab').attr("name"));
            if(curr_phase.getType()!="activity-structure")
                document.getElementById(PhaseName+"_ActivitiesTree").appendChild(newRow);
            else
                document.getElementById(curr_phase.getId()+"inActivitiesTree"+"_ActivitiesIncluded").appendChild(newRow);*/
            
     }
     catch(e){
         alert(e);
     }

},
addStructureToActivitiesHTMLView: function(activityStructure){
        try{
            var activities_Rows = document.getElementById('activities_Rows');
        
            var newRow = document.createElement('li');
            newRow.id = activityStructure.getId()+"Activity-structure";//codeAllagi activityStructure.getName()+activityStructure.getId()+"Activity-structure";            
            var anchorDiv = document.createElement('div');
            anchorDiv.className = "informationPanelInnerAnchors";
            //anchorDiv.style.cssFloat="left";
            newRow.appendChild(anchorDiv);

            var expandStruct=document.createElement("IMG");
            expandStruct.setAttribute("src","javascript_css/project_team_form/images/expand_1.png");
            expandStruct.id = newRow.id+"_img";
            expandStruct.style.width = "9px";
            expandStruct.style.marginLeft = "2px";
            expandStruct.style.paddingTop = "5px";
            expandStruct.style.cssFloat="left";            
            expandStruct.style.marginTop = "2px";            
            //expandStruct.align = "middle";
            //expandStruct.style.backgroundColor = "#ccc";

            expandStruct.onclick = function(){ShowHide(newRow.id+"_Included",'toggle');changeIcon(this,newRow.id+"_Included",'toggle');};
            anchorDiv.appendChild(expandStruct);

            var anchor = document.createElement('a');
            anchor.style.border = "0px";
            anchor.href = "#";
            anchor.style.cssFloat="left";
            //anchor.style.marginTop = "5px";
            anchor.style.textDecoration = "none";           
            anchor.style.width = "auto";
            anchor.style.overflow = "hidden";
            anchor.onclick = function () {activityStructuresArray.BrowseActivityStructure(activityStructure)};
            anchor.style.height = "16px";
            
            anchor.id = activityStructure.getId()+"Anchor";//codeAllagi activityStructure.getName()+activityStructure.getId()+"Anchor";            

            if(activityStructure.getName().length>11){
                anchor.innerHTML = activityStructure.getName().substring(0,11);
                anchor.innerHTML +=".."
            }
            else
                anchor.innerHTML = activityStructure.getName();
            
            anchor.title = activityStructure.getName();
            var imgDiv = document.createElement('div');
            //imgDiv.style.marginTop = "5px";
            imgDiv.style.cssFloat = "right";
            imgDiv.style.marginRight = "1px";
            imgDiv.style.width = "35px";
            imgDiv.style.height = "16px";
            newRow.appendChild(imgDiv);
            
            var editImg=document.createElement("IMG");
            editImg.onclick = function () {activityStructuresArray.BrowseActivityStructure(activityStructure)};
            editImg.setAttribute("src","javascript_css/project_team_form/images/edit.gif");
            editImg.style.border="0px";
            editImg.style.cssFloat="left";
            editImg.align = "top";
            editImg.onmouseover = function(){this.style.cursor='pointer'};
            imgDiv.appendChild(editImg);
                       
            var deleteImg = document.createElement("IMG");
            deleteImg.setAttribute("src","javascript_css/project_team_form/images/delete.png");
            deleteImg.style.border="0px";
            deleteImg.style.cssFloat="left";
            deleteImg.style.marginLeft = "3px";
            deleteImg.onmouseover = function(){this.style.cursor='pointer'};
            deleteImg.align = "top";
            deleteImg.onclick = function(){dele.deleteAllStructure(activityStructure)};
            imgDiv.appendChild(deleteImg);


            anchorDiv.appendChild(anchor);
            
            var activities = document.createElement("ul");
            activities.id = newRow.id+"_Included";            
            activities.style.marginLeft = "7px";
            newRow.appendChild(activities);

            activities_Rows.appendChild(newRow);

            var curr_phase = allPhases.getPhase($('#phaseTab').attr("name"));
            if(curr_phase.getType()=="activity-structure")
                document.getElementById(curr_phase.getId()+"Activity-structure"+"_Included").appendChild(document.getElementById(newRow.id));
     }
     catch(e){
         alert(e);
     }

},
BrowseActivityStructure: function(activityStructure){
    try{
        structureInformationItems = [];
        structureLearningObjectItems = [];
        structureserviceArray = [];
        var tmp_checkboxId = "";
        $("#structureServicesContent").find("input:checkbox").each(function(){
                $(this).attr('checked',false);
                tmp_checkboxId = ($(this).attr('id').substring($(this).attr('id').indexOf('service_')+8,$(this).attr('id').length));
                showSetUp(false,tmp_checkboxId,'structure');

             })
        //clearRolesStructureActivity();
        
        document.getElementById("createNewstructureInformation").checked=true;
        document.getElementById("structureInformationTitle").value = "";
        document.getElementById("createNewstructureInformationContent").value = "Enter your description here";
        document.getElementById("URLstructureInformationContent").value = "http://";
        document.getElementById("ExistingFilestructureInformationContent").value = "";
        //document.getElementById("ExistingResourcestructureInformationContent").value = "";
        clearItemViewer("structureInformation");
        showCompletionPrereq('createNewstructureInformation','structureInformation');
        clearItemViewer("structureLearningObject")
        document.getElementById("createNewstructureLearningObject").checked=true;
        document.getElementById("structureLearningObjectTitle").value = "";
        document.getElementById("createNewstructureLearningObjectContent").value = "Enter your description here";
        document.getElementById("URLstructureLearningObjectContent").value = "http://";
        document.getElementById("ExistingFilestructureLearningObjectContent").value = "";
        //document.getElementById("ExistingResourcestructureLearningObjectContent").value = "";
        showCompletionPrereq('createNewstructureLearningObject','structureLearningObject');
        $("#structureName").val(activityStructure.getName());
        $("#prevStructureName").val(activityStructure.getName());
        $("#NoOfStructureActivitesTobeExecuted").val(activityStructure.getNoOfActivitesTobeExecuted());
        showFeedback(activityStructure,"structureInformation","");
        showFeedback(activityStructure,"structureLearningObject","")
        /*var checkBoxRole;
        for(var i=0;i<activityStructure.getRolesAssigned().length;i++){
            checkBoxRole = activityStructure.getRolesAssigned()[i];
            if(checkBoxRole.checked){
                if(document.getElementById(checkBoxRole.id)!=null)
                    document.getElementById(checkBoxRole.id).checked = true;
            }
            else
                if(document.getElementById(checkBoxRole.id)!=null)
                    document.getElementById(checkBoxRole.id).checked = false;
        }
        */
        $("#numOfIncludedActivities").val(activityStructure.getNumOfIncludedActivities());
        if(activityStructure.getEnvironement()!=null)
            if(activityStructure.getEnvironement().length!=0){

                    var tmpServicesArray = activityStructure.getEnvironement().getServices();

                    for(i=0;i<tmpServicesArray.length;i++){
                      console.log(tmpServicesArray[i].getName())
                        $("#structureservice_"+tmpServicesArray[i].getName()).attr('checked',true);
                        loadLTIParam(tmpServicesArray[i].getName(),tmpServicesArray[i].getUrl(),tmpServicesArray[i],'structure',tmpServicesArray[i].getKey(),tmpServicesArray[i].getSecret())
                        showSetUp(true,tmpServicesArray[i].getName(),'structure');
                    }
                }
            $("#structureservice_setUp").css("display",'none')
        $.fn.colorbox(
                    {width:"75%",height:"75%", inline:true, href:"#structureActivityForm",
                     onLoad:function(){
                        try{
                            $("#activityStructureTabs").tabs();
                            $(".toolsMaterialsTabs").tabs();

                             showCompletionPrereq('createNewstructureLearningObject','structureLearningObject');

                            var exists = null,show=true;
                            for(var i=0;i<allPhases.getPhases().length;i++){
                                if((allPhases.getPhases()[i]).getType()=="activity-structure")
                                    exists = allPhases.getPhases()[i].getActivitiesStructuresArray().containsName(activityStructure.getName());
                                if(exists!=null){
                                    show = false;
                                    break;
                                }
                            }
                            if(show==false){
                                //$("#activityStructureTabs").tabs("disable",1);
                                //$("#activityStructureTabs").tabs("disable",2);
                            }
                            //activityStructuresArray.BrowseActivityStructure(activityStructure);
                        }
                        catch(e){
                            alert(e)
                        }
                     }

                    });
    }
    catch(e){
        alert("error while trying to present activity structure definition "+e);
    }
},
createTabForStructure: function(activityStructure){
        var tabAnchor = document.createElement("a");
        var span = document.createElement("span");
        var name  = activityStructure.getName();
        if(name.length>6){
             span.innerHTML = name.substring(0,6);
             span.innerHTML += "..";
         }
         else
             span.innerHTML = name;
         
        tabAnchor.appendChild(span);
        tabAnchor.title = name;
        
        var tabLi = document.createElement("li");
        tabLi.name = name;
        tabLi.id = "activityStructureView";
        tabLi.appendChild(tabAnchor);
        if($("#phaseTab").find("ul").children().length!=0)
           tabLi.className="selected";
        $("#phaseTab").find("li").each(function(){
            $(this).removeAttr("class");
        });

        $("#phaseTab").find("ul").append(tabLi);
        addImagesInStructureTab(activityStructure,tabLi)
        if($("#phaseTab").find("li").length>=5){
            var width = $("#phaseTab").find("ul").width();
            $("#phaseTab").find("ul").width(width+60);

        }

       if($("#phaseTab").find("li").length==5){
           $("#rightSide").css("display","block");
           $("#leftSide").css("display","block");
           var div = $('#phaseTab');
           var offset = 0;
           $('#leftSide').click(function(){
               offset += 15;
               div.scrollLeft(offset);
            });
           $('#rightSide').click(function(){
               if(offset>0)
                   offset -= 15;
               div.scrollLeft(offset);
       });
      }
},
BrowseInnerOfActivityStructure: function(activityStructure){
    try{        
        clearPhaseView(activityStructure,allPhases);
        removeFromTab();        
        $('#phaseTab').attr("name", activityStructure.getName());
        var tabs = $("#phaseTab").find("ul").children();
        if(tabs.length>1)
            $("#phaseTab").find("li").each(function(){
                 $(this).removeAttr("class");
            });
            var index = tabs.length;
            var Offset = parseInt($("#phaseTab").scrollLeft())//parseInt(($("#phaseTab").find(">ul").offset().left)/89)
            var quadrat = Offset/89;
            var showsSpacemin = quadrat;
            var showsSpacemax = quadrat+3
            if(index < showsSpacemin){
                $("#phaseTab").scrollLeft(Offset+((showsSpacemin-index)*(-89)));
                Offset = parseInt($("#phaseTab").scrollLeft())
                quadrat = Offset/89;
                showsSpacemin = quadrat
                showsSpacemax = quadrat+3
            }
            else if(index > showsSpacemax){
                var move = Offset + ((index-showsSpacemax)*89)//etsi kanw to index max
                $("#phaseTab").scrollLeft(move);//to max gia ginei to index arra tha to kounisw kata index -4 kai tha ginei min =index-4 kai max =index

                Offset = parseInt($("#phaseTab").scrollLeft())
                quadrat = Offset/89;
                showsSpacemin = quadrat
                showsSpacemax = quadrat+3
            }
        $("#"+activityStructure.getId()+"Anchor").css("color","red");

        activityStructuresArray.createTabForStructure(activityStructure);
       
    }
    catch(e){
        alert("error while trying to present activity-structure inner struct")
    }
}
};

var activityStructure = function(name,id,rolesAssigned,information,option,environement){
    
    this.LearningActivitiesArray = initializeActivities();
    this.ActivitiesStructuresArray = new Array();
    this.joinGateways = gateway.initJoinGatewayList();
    this.inclusiveOrGateways = gateway.initInclusiveOrList();
    this.identifier = uniqid();
    this.size = id;
    this.endPoint = [];
    this.propertyExpression = null;
    this.htmlExpressionView = null;
    this.setPropertyExpression = function(propExpression){
        this.propertyExpression = propExpression;
    }
    if(option==0)
        this.id = this.identifier;
    else
        this.id = option;


    this.getPropertyExpression = function(){
        return this.propertyExpression;
    }
    
    this.environement = environement;
    this.getIdentifier = function(){
        return this.identifier;
    }
    roleIsBeingUsed(rolesAssigned,this.id)
    this.rolesAssigned = new Array();
    for(i=0;i<rolesAssigned.length;i++){           
            this.rolesAssigned.push(rolesAssigned[i].cloneNode(true));
    }
    
    this.getLearningActivitiesArray = function(){
        return this.LearningActivitiesArray;
    }
    this.setLearningActivitiesArray = function(learningActivitiesArray){
        this.LearningActivitiesArray = learningActivitiesArray;
    }
    this.getActivitiesStructuresArray = function(){
        return this.ActivitiesStructuresArray;
    }
    this.getJoinGateways= function(){
        return this.joinGateways;
    }
    this.getInclusiveOrGateways = function(){
        return this.inclusiveOrGateways;
    }
    this.setJoinGateways= function(gatewayList){
            this.joinGateways = gatewayList;
    }
    this.setInclusiveOrGateways = function(gatewayList){
            this.inclusiveOrGateways = gatewayList;
    }
    this.setStartPoint = function(obj){
        this.start = obj;
    }
    this.getStartPoint = function(){
        return this.start;
    }
     this.setEndPoint = function(obj){
        this.endPoint = obj;
    }
    this.addEndPoint = function(obj){
        this.endPoint.push(obj);
    }
    this.getEndPoint = function(){
        return this.endPoint;
    }
    //ws edw......kai vgale apo ta sxolia to apokatw
    //this.activitiesIncluded = new Array();
    
    this.activityStructureInformation = information;
    this.name = name;
    //codeAllagi this.id = id;
    
    
    this.NoOfActivitesTobeExecuted = 0;
    this.shape = null;
    this.flowChilds = [];
    this.getId = function(){
        return this.id;
    }
    this.getSize = function(){
        return this.size;
    }
    this.createShape = function(positionX,positionY,annotationDesc){
        this.shape = activityStructuresArray.createShape(this,positionX,positionY,annotationDesc);
    }
    this.getShape = function(){
        return this.shape;
    }
    this.getType = function(){
        return "activity-structure";
    }
    this.getName = function(){
        return this.name;
    }
    this.setName = function(name){
        this.name = name;
    }
    this.setInformation = function(information){
        this.activityStructureInformation = information;
    }
    this.getInformation = function(){
        return this.activityStructureInformation;
    }
    this.setEnvironement = function(environement){
        this.environement = environement;
    }
    this.getEnvironement = function(){
        return this.environement;
    }

    this.setNoOfActivitesTobeExecuted = function(number){
        this.NoOfActivitesTobeExecuted = number;
    }
    this.getNoOfActivitesTobeExecuted = function(){
        return this.NoOfActivitesTobeExecuted;
    }
    this.getRolesAssigned = function(){
            return this.rolesAssigned;
        }
    
    this.setRolesAssigned = function(rolesAssigned){        
        this.rolesAssigned = [];
        roleIsBeingUsed(rolesAssigned,this.id)
        for(var i=0;i<rolesAssigned.length;i++){            
            this.rolesAssigned.push(rolesAssigned[i].cloneNode(true));

        }
    }
    this.addflowChilds = function(activity){
            this.flowChilds.push(activity);
    }
    this.setflowChilds = function(list){
            this.flowChilds = list;
    }
    this.getflowChilds = function(){
            return this.flowChilds;
    }
    this.getNumOfIncludedActivities = function(){
        return parseInt(this.getLearningActivitiesArray().getActivities().length)+parseInt(this.getActivitiesStructuresArray().length)
    }

    this.gethtmlExpressionView = function(){
            return this.htmlExpressionView;
        }
    this.sethtmlExpressionView = function(div){
            this.htmlExpressionView = div.clone(true);
            this.htmlExpressionView.find("#conditionType").val($("#conditionType").val())
console.log(this.htmlExpressionView)
        }
}
var dele={
    deleteAllStructure: function(structure){
        try{
        
            if (!confirm("You are going to delete activity structure")==true)
                return;
        var name = structure.getName();
        var parent_phase = getPhaseOfStructure(structure.getId());

        if(parent_phase==null)
            throw "error while deleting activity structure";

        var activities = structure.getLearningActivitiesArray().getActivities();
        var num = activities.length;
        var tmpactivities = activities.slice();
        
        for(var i=0;i<num;i++){
            deleteActivity(tmpactivities[i],1);
        }
        tmpactivities=null;
        var structures = structure.getActivitiesStructuresArray();
        var tmpstructures = structures.slice();
        var structuresSize=structures.length;
        for(i=0;i<structuresSize;i++){
                num += tmpstructures[i].getLearningActivitiesArray().getActivities().length;
                dele.deleteStructure(tmpstructures[i]);
        }
        tmpstructures = null;

        //delete all gateways
        deleteAllGateways(structure);

        dele.deleteStructure(structure);
        //remove start point endpoint
         if(structure.getStartPoint()!=null)
            deleteStartPoint(structure.getStartPoint().getAttribute("id"),structure,0);

        if(structure.getEndPoint().length!=0){
            var tmpEndPointslength = structure.getEndPoint().length;
            for(i=0;i<tmpEndPointslength;i++)
                deleteEndPoint(structure.getEndPoint()[i].getAttribute("id"),structure,0);
            
        }
        /*if(structure.getEndPoint()!=null){
            
            deleteEndPoint(structure.getEndPoint().getAttribute("id"),structure,0);
            //remove from phase
            structure.setEndPoint(null);
        }*/
        if(document.getElementById("phaseTab").name==name){
            removeFromTab();
            if(parent_phase.getType()=='activity-structure')
                activityStructuresArray.createTabForStructure(parent_phase)
            else
                $("#phaseTab").find("#"+parent_phase.getId()+'Tab').attr('class','selected');
            clearPhaseView(parent_phase,allPhases);
            document.getElementById("phaseTab").name = parent_phase.getName();
        }
         select();
        //clearPhaseView(parent_phase,allPhases)
        //$("#activitiesNum").val(parseInt($("#activitiesNum").val())-num);
        }
        catch(e){
            alert("error while deleting activity structure "+e);
        }
    },
    deleteStructure: function(structure){
        try{
     
            var svgDoc = document.getElementById('svgPanel').contentDocument;
            var textPath = null;
            var root = svgDoc.getElementById("vectorGraphicsDoc");
 
            //remove from connection list
            var parent_phase = getPhaseOfStructure(structure.getId());
            if(parent_phase==null)
                throw "error while deleting activity structure";
            var connections = parent_phase.getLearningActivitiesArray().getConnectionsArray();
            var tmpConnections = connections.slice();
            for(var i=0;i<tmpConnections.length;i++){
                var con = tmpConnections[i];
                if(con.getAttribute("from")==structure.getId()){
                    //empty his flow list                
                    structure.setflowChilds([]);
                    //des an einai se gateway kai vgale to
                    removeFromGateway(con.getAttribute("to"),structure.getId(),parent_phase)
                    removeConnection(con,connections);

                    if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta eksafanizei
                        textPath = root.getElementById("textRefered_"+con.getAttribute("id"));
                        if(textPath!=null)
                            textPath.parentNode.removeChild(textPath);
                    }
                }
                if(con.getAttribute("to")==structure.getId()){
                     //bres ton patera toy kai vgale ayto apo to array poy exei ta paidia toy patera
                    removeFromParent(con.getAttribute("from"),structure.getId(),parent_phase);
                    removeConnection(con,connections);

                    if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta eksafanizei
                        textPath = root.getElementById("textRefered_"+con.getAttribute("id"));
                        if(textPath!=null)
                            textPath.parentNode.removeChild(textPath);
                    }
                }
            }

            //delete from svg panel
            deleteActivityFromPanel(structure.getId());
            //removeAllInnerSVGs
             //remove from parent
             var index = parent_phase.getActivitiesStructuresArray().indexOfStruct(structure.getId());
             if(index==null)
                throw "error while deleting activity structure";
            parent_phase.getActivitiesStructuresArray().splice(index,1);
            //remove from phases
            index = allPhases.getPhases().indexOfStruct(structure.getId());
            if(index==null)
                throw "error while deleting activity structure";
            allPhases.getPhases().splice(index,1);

            //remove from phase view
           var del = document.getElementById(structure.getId()+"inActivitiesTree");
           del.parentNode.removeChild(del);
           //subtract role
            var roleId = ""
            for(var j=0;j<roleBeingUse.length;j++){
                roleId = roleBeingUse[j].substr(roleBeingUse[j].indexOf("-")+1)
                if(roleId==structure.getId()){
                    roleBeingUse.splice(j,1);
                }
            }
           /*var checkBoxRole;
           for(i=0;i<structure.getRolesAssigned().length;i++){            
                checkBoxRole = structure.getRolesAssigned()[i];
                document.getElementById(checkBoxRole.id).checked = false;
           }*/
        }
        catch(e){
            alert(e)
        }
    },
    clearView: function(structure){
        try{
            

            var phase = allPhases.getPhase(structure.getName())
            
            var connectionsList = phase.getLearningActivitiesArray().getConnectionsArray();

            var joinGateways = phase.getJoinGateways();
            var inclusiveOr = phase.getInclusiveOrGateways();
            if(phase.getStartPoint()!=null)
                phase.getStartPoint().style.display = "none";
            
            for(var j=0;j<connectionsList.length;j++){
                connectionsList[j].style.display = "none";
               
            }
            for(j=0;j<joinGateways.length;j++)
                joinGateways[j].getShape().style.display = "none";
            for(j=0;j<inclusiveOr.length;j++)
                inclusiveOr[j].getShape().style.display = "none";
            
        }
        catch(e){
            alert(e)
        }
    }

    /*deleteActivityFromActivityStructureSVG: function(structureId,activityName){

        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById(structureId);

        for(var i=0;i<root.childNodes.length;i++){            
            if(root.childNodes[i].nodeType==1)
                if(root.childNodes[i].childNodes[0]!=null)
                    if(root.childNodes[i].childNodes[0].textContent==activityName){
                        var tag = root.childNodes[i].childNodes[0];
                        tag.parentNode.removeChild(tag);
                    }

        }


    },
    deleteFromHtmlStructureList: function(activityName){
        var activity = document.getElementById(activityName+"inActivitiesTree");
        if(activity!=null)
            activity.parentNode.removeChild(activity);
    },
    deleteActivityFromActivityStructureList: function(structure,index){
        //structure.getIncludedActivities().splice(index,1)
        (structure.getLearningActivitiesArray()).getActivities().splice(index,1)
    },
    deleteActivity: function(activityId){
        try{            
            var structures = getCurrentStructures();
            var index = -1;
            for(var i=0;i<structures.length;i++){
                index = (structures[i].getLearningActivitiesArray()).getActivities().indexInArray(activityId);
                alert(index)
                if(index!=-1){
                    deleteFromStructure.deleteActivityFromActivityStructureList(structures[i],index);
                    deleteFromStructure.deleteFromHtmlStructureList(activityId);

                    deleteActivityFromPanel(activityId);
                    //deleteFromStructure.deleteActivityFromActivityStructureSVG(structures[i].getId(),activityId)
                    break;
                }
            }
        }
        catch(e){
            alert("error while deleting activity from activity structure "+e)
        }
    }*/

}
Array.prototype.indexOfStruct = function (id){
    for (var i = 0; i < this.length; i++){
        if(this[i].getId() == id){
            return i;
        }
    }
    return null;
};

function addImagesInStructureTab(activityStructure,tabLi){
        var imgDiv = $("<div></div>");
        imgDiv.css({"float":"left"})
        imgDiv.appendTo(tabLi);
        var editDiv = $("<div></div>");
        imgDiv.append(editDiv)
        var editImg = $("<img></img>");
        editImg.css({"clear":"both"})
        editImg.click(function(){
            activityStructuresArray.BrowseActivityStructure(activityStructure)
        })
        editImg.mouseover(function(){this.style.cursor='pointer'});
        editImg.attr("title" ,"click to edit "+activityStructure.getName()+" properties");
        editImg.attr("src","javascript_css/images/Edit_icon.png");
        imgDiv.append(editImg)

        var deleteImg = $("<img></img>");
        deleteImg.attr("src","javascript_css/images/icon_delete.gif");
        deleteImg.css({"clear":"both"})
        deleteImg.mouseover(function(){this.style.cursor='pointer'});
        deleteImg.click(function(){dele.deleteAllStructure(activityStructure)});
        deleteImg.attr("title","delete "+activityStructure.getName());
        editDiv.append(deleteImg)


    }