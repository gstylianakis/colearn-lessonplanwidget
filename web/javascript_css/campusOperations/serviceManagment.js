/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    
    function service(){
        this.name = "";
        this.url = "";
        this.key = "";
        this.secret = "";
        this.context_id = "";
        this.context_title = "";
        this.context_label = "";
        this.resource_link_title="";
        this.resource_link_id="";
        this.roleMapping = new Array();
        var roles = ['administrator','content_developer','mentor','instructor'];
        for(var i=0;i<roles.length;i++)
            this.roleMapping.push([roles[i],'none'])

        this.setName = function(name){
            this.name = name;
        }
        this.getName = function(){
            return this.name;
        }
        this.setKey = function(key){
            this.key = key;
        }
        this.getKey = function(){
            return this.key;
        }
        this.setSecret = function(secret){
            this.secret = secret;
        }
        this.getSecret = function(){
            return this.secret;
        }
        this.setUrl = function(url){            
            this.url = url;
        }
        this.getUrl = function(){
            return this.url;
        }
        this.setContextId = function(context_id){

            this.context_id = context_id;
        }
        this.getContextId = function(){
            return this.context_id;
        }
        this.setContextTitle = function(context_title){
            
            this.context_title = context_title;
        }
        this.getContextTitle = function(){
            return this.context_title;
        }
        this.setContextLabel = function(context_label){
            this.context_label = context_label;
        }
        this.getContextLabel = function(){
            return this.context_label;
        }
        this.setRoleMapping = function(roleMaping){                       
            for(var i=0;i<roleMaping.length;i++)
                this.roleMapping[i] = roleMaping[i];
            
        }
        this.getRoleMapping = function(){
            return this.roleMapping;
        }
        this.setResourceTitle = function(title){
            this.resource_link_title = title;
        }
        this.getResourceTitle = function(){
            return this.resource_link_title;
        }
         this.setResourceId = function(id){
            this.resource_link_id = id;
        }
        this.getResourceId = function(){
            return this.resource_link_id;
        }
        //this.checked = false;
        /*this.setChecked = function(option){
            this.checked = option
        }
        this.getChecked = function(){
            return this.checked;
        }*/
    }

    function saveLTIParameters(context_title,context_id,context_label,resourceTitle,resourceId,roleMap,service_launchUrl,checkboxId,option,key,secret){
        try{
        
            var tmp_service = new service();
            tmp_service.setName(checkboxId);
            tmp_service.setUrl(service_launchUrl);
            tmp_service.setKey(key);
            tmp_service.setSecret(secret);
            tmp_service.setContextTitle(context_title);
            tmp_service.setContextId(context_id);
            tmp_service.setContextLabel(context_label);
            tmp_service.setRoleMapping(roleMap);
            tmp_service.setResourceId(resourceId);
            tmp_service.setResourceTitle(resourceTitle);

            var exist = null
            
            if(option=='activity'){
                
                exist = serviceArray.indexOfName(checkboxId);
                if(exist!=-1)
                    serviceArray.splice(exist,1);
                
                serviceArray.push(tmp_service);
         
            }
            else{
                exist = structureserviceArray.indexOfName("structure"+checkboxId);
                if(exist!=-1)
                    structureserviceArray.splice(exist,1);
                
                structureserviceArray.push(tmp_service);
                
            }

                    
            
        }
        catch(e){
            alert("error while setting LTI parameters "+e)
        }
    }
    function loadLTIParam(id,service_launchUrl,service,option,key,secret){
       try{
           
           $("#serviceName").html(id+' Configuration');
           
            if(option=='activity'){
                $("#service_setUp").css('display','block');
                
                if(service==null){                    
                    var exist = serviceArray.containsName(id);                    
                    if(exist!=null){
                        var map = exist.getRoleMapping();
                        initLTIParam(id,service_launchUrl, exist.getRoleMapping(),exist.getContextId(),exist.getContextTitle(),exist.getContextLabel(),exist.getResourceTitle(),exist.getResourceId(),option,key,secret);
                        for(var i=0;i<map.length;i++){
                            
                            document.getElementById(map[i][0]).value = map[i][1];

                        }
                    }
                    else
                        initLTIParam(id,service_launchUrl,null,'','','','','',option,key,secret);
                }
                else{

                    map = service.getRoleMapping();
                    
                    initLTIParam(id,service_launchUrl, service.getRoleMapping(),service.getContextId(),service.getContextTitle(),service.getContextLabel(),service.getResourceTitle(),service.getResourceId(),option,key,secret);
                    for(i=0;i<map.length;i++){                      
                        document.getElementById(map[i][0]).value = map[i][1];
                    }
                    serviceArray.push(service);
                }
            }
            else{
                $("#structureservice_setUp").css('display','block');
                if(service==null){
                    exist = structureserviceArray.containsName(id);
                    if(exist!=null){
                        map = exist.getRoleMapping();
                        initLTIParam(id,service_launchUrl, exist.getRoleMapping(),exist.getContextId(),exist.getContextTitle(),exist.getContextLabel(),exist.getResourceTitle(),exist.getResourceId(),option,key,secret);
                        for(i=0;i<map.length;i++){
                            document.getElementById(map[i][0]).value = map[i][1];
                        }
                    }
                    else
                        initLTIParam(id,service_launchUrl,null,'','','','','',option,key,secret);
                }
                else{
                    map = service.getRoleMapping();
                    initLTIParam(id,service_launchUrl, service.getRoleMapping(),service.getContextId(),service.getContextTitle(),service.getContextLabel(),service.getResourceTitle(),service.getResourceId(),option,key,secret);
                    for(i=0;i<map.length;i++){
                        document.getElementById(map[i][0]).value = map[i][1];
                    }
                    structureserviceArray.push(service);
                }
            }
       }
       catch(e){
           console.log(e)
       }
        
        //alert(serviceArray[id])
    }
    function initLTIParam(checkboxId,service_launchUrl,RoleMap,context_id,context_title,context_label,resource_title,resource_id,div_option,key,secret){
        try{

            var createMap = false;
            if(RoleMap==null){
                createMap = true;
                RoleMap = [];
            }
            var contentDiv = null;
            if(div_option=="activity")
                contentDiv = $("#service_setUp");
            else
                contentDiv = $("#structureservice_setUp");
            
            contentDiv.html("");
            
            var selection,option;
            var role_name="";
            var roles = ['administrator','content_developer','mentor','instructor'];
            var p;
            contentDiv.append('<legend> Set '+checkboxId+' Configuration</legend>');

            var input = document.createElement("input");
            input.type = 'button';
            input.style.top = '0px';
            input.style.cssFloat = "right"
            input.id = "setServiceConfiguration"
            input.addEventListener("click", function(){
                saveLTIParameters($('#context_title').val(),$('#context_id').val(),$('#context_label').val(),$('#resource_title').val(),$('#resource_id').val(),RoleMap,service_launchUrl,checkboxId,div_option,key,secret);
                
                inlineMsg("setServiceConfiguration","service configuration saved",2,input.parentNode.parentNode.parentNode.id,1)

            },false)
            //input.size ='5em';
            input.value = 'save service config';
            contentDiv.append(input)
            contentDiv.append('<p><label class="service_label">context_id</label><br/> <input class="service_input" value=\''+context_id+'\' type="text" id="context_id" /></p>');
            contentDiv.append('<p><label class="service_label">context_title</label><br/> <input class="service_input" value=\''+context_title+'\' type="text" id="context_title" /></p>');
            contentDiv.append('<p><label class="service_label">context_label</label><br/> <input class="service_input" type="text" value=\''+context_label+'\' id="context_label" /><br /></p>');
            contentDiv.append('<p><label class="service_label">resource_link_title</label><br/> <input class="service_input" value=\''+resource_title+'\' type="text" id="resource_title" /></p>');
            contentDiv.append('<p><label class="service_label">resource_link_id</label><br/> <input class="service_input" type="text" value=\''+resource_id+'\' id="resource_id" /><br /></p>');

            for(var k=0;k<roles.length;k++){                
                selection = document.createElement("select"); //$('<select style=\'margin-left:5px;\'> </select>');
                selection.style.marginLeft = '5px';
                selection.name = roles[k];
                selection.id = roles[k];
                if(createMap)
                    RoleMap.push([roles[k],'none'])
                for(var i=0;i<learnerArray.getRoles().length;i++){
                    role_name = learnerArray.getRoles()[i].getName();
                    //role_id = learnerArray.getRoles()[i].getId();
                    option = document.createElement("option");
                    option.value = role_name;
                    option.innerHTML = role_name;
                    selection.appendChild(option);
                }
                for(i=0;i<staffArray.getRoles().length;i++){
                    role_name = staffArray.getRoles()[i].getName();
                    //role_id = staffArray.getRoles()[i].getId();
                    option = document.createElement("option");
                    option.value = role_name;
                    option.innerHTML = role_name;
                    selection.appendChild(option)
                }
                option = document.createElement("option");
                option.value ='none';
                option.selected = 'true';
                option.innerHTML = "none";
                selection.appendChild(option);

                selection.appendChild(option);
                selection.onchange = function(){                    
                    for(var i=0;i<RoleMap.length;i++){
                        if(RoleMap[i][0]==this.name)
                            RoleMap[i][1] = this.value;
                    }
                }
                
                p = $('<p id=\'roleMap_'+k+'\'>person of role </p>');
                p.append(selection);
                p.append('<label> as '+roles[k]+'</label>');

                contentDiv.append(p);
            }


            
        }
        catch(e){
            alert("error while loading LTI parameters"+e)
        }
    }
    function showSetUp(checked,id,option){
        if(checked==true){
            $('#'+id).css('display','block')            
        }
        else{
            $('#'+id).css('display','none')
            if(option=="activity")
                $("#service_setUp").css('display','none');
            else
                $("#structureservice_setUp").css('display','none');
        }
    }
    Array.prototype.indexOfName = function (name){
        for (var i = 0; i < this.length; i++){
            if(this[i].getName() == name){
                return i;
            }
        }
        return -1;
    };