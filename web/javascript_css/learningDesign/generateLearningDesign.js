/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//otan dimiourgw activities kai activities structures k.l.p ta grafw sto xml diavazontas ta array apo activities tou kathe act-phase
//ara i seira pou tipwnontai sto ld exei na kanei me tin seira pou mpikan sto array...
//twra gia ta conditions me endiaferei graphos opote kanw Breadth First Search apo to root
var learningDesign = {
    rootNodes:null,
    generateLD: function(learnerArray,staffArray,phases,option){
        try{

            var xmldoc = createXMLDocument("manifest","http://www.imsglobal.org/xsd/imscp_v1p1");//XML.newDocument("manifest","http://www.imsglobal.org/xsd/imscp_v1p1");


            var manifest = xmldoc.getElementsByTagName("manifest")[0];
            
            manifest.setAttribute("xmlns:imsld","http://www.imsglobal.org/xsd/imsld_v1p0");            
            manifest.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
            manifest.setAttribute("xsi:schemaLocation","http://www.imsglobal.org/xsd/imscp_v1p1 http://www.imsglobal.org/xsd/imscp_v1p1.xsd http://www.imsglobal.org/xsd/imsld_v1p0 http://www.imsglobal.org/xsd/IMS_LD_Level_C.xsd");
            manifest.setAttribute("xmlns:lm","http://www.imsglobal.org/xsd/imsmd_v1p2");
            manifest.setAttribute("identifier",ManifestID);

            var lomElement = createLomMetadata(xmldoc);
            manifest.appendChild(lomElement);
            
            var organizations = xmldoc.createElementNS("http://www.imsglobal.org/xsd/imscp_v1p1","organizations");
           
            var x = xmldoc.createElement("imsld:learning-design");
            x.setAttribute("identifier","ld-"+uniqid());
            x.setAttribute("level","C");
            x.setAttribute("uri","http://www.music.tuc.gr/"+uniqid());
            organizations.appendChild(x);
            manifest.appendChild(organizations);
            
            var ldTitle = xmldoc.createElement("imsld:title");
            ldTitle.textContent = document.getElementById("lesson_plan_title").value;
            var components = xmldoc.createElement("imsld:components");
            var prerequisites = xmldoc.createElement("imsld:prerequisites");
            var prerItem = xmldoc.createElement("imsld:item");

            prerItem.setAttribute("parameters",$("#CoursePrerequisites").val());
            prerequisites.appendChild(prerItem);
            x.appendChild(ldTitle);
            if(document.getElementById("General_Goals").value!=""){
                var learning_objectives = xmldoc.createElement("imsld:learning-objectives");
                var learning_objectives_item = xmldoc.createElement("imsld:item");
                learning_objectives_item.setAttribute("parameters",document.getElementById("General_Goals").value);
                
                learning_objectives.appendChild(learning_objectives_item);
                x.appendChild(learning_objectives);
            }
            
            x.appendChild(prerequisites);
            var rolesNode = xmldoc.createElement("imsld:roles");


            var activitiesNode = xmldoc.createElement("imsld:activities");
            var environements = xmldoc.createElement("imsld:environments");
            var environment = null;
            var xmlType = "";
            var xmlTitle = "";
            var activityType = "";
            var activityTitle = "";
           
            for(var i=0;i<learnerArray.getRoles().length;i++){
                xmlType = xmldoc.createElement("imsld:learner");
                xmlTitle = xmldoc.createElement("imsld:title");
                xmlTitle.textContent = learnerArray.getRoles()[i].getName();
                xmlType.setAttribute("identifier","role-"+learnerArray.getRoles()[i].getIdentifier());//kapoio id monadiko tha mpei edw
                if(learnerArray.getRoles()[i].getConstraint()){
                    xmlType.setAttribute("match-persons","exclusively-in-roles");
                }
                else{
                    xmlType.setAttribute("create-new","not-exclusively");
                }
                if(learnerArray.getRoles()[i].getMinNum()!=-1){
                    xmlType.setAttribute("min-persons",learnerArray.getRoles()[i].getMinNum());
                }
                if(learnerArray.getRoles()[i].getMaxNum()!=-1){
                    xmlType.setAttribute("max-persons",learnerArray.getRoles()[i].getMaxNum());
                }

                xmlType.appendChild(xmlTitle);
                rolesNode.appendChild(xmlType);
            }
            for(i=0;i<staffArray.getRoles().length;i++){
                xmlType = xmldoc.createElement("imsld:staff");
                xmlTitle = xmldoc.createElement("imsld:title");
                xmlTitle.textContent = staffArray.getRoles()[i].getName();
                xmlType.setAttribute("identifier","role-"+staffArray.getRoles()[i].getIdentifier());//kapoio id monadiko tha mpei edw
                if(staffArray.getRoles()[i].getConstraint()){
                    xmlType.setAttribute("match-persons","exclusively-in-roles");
                }
                else{
                    xmlType.setAttribute("create-new","not-exclusively");
                }
                if(staffArray.getRoles()[i].getMinNum()!=-1){
                    xmlType.setAttribute("min-persons",staffArray.getRoles()[i].getMinNum());
                }
                if(staffArray.getRoles()[i].getMaxNum()!=-1){
                    xmlType.setAttribute("max-persons",staffArray.getRoles()[i].getMaxNum());
                }

                xmlType.appendChild(xmlTitle);
                rolesNode.appendChild(xmlType);
            }
            var item,information=null,informationElement = null,itemTitle=null;
            var method = xmldoc.createElement("imsld:method");
            var play = xmldoc.createElement("imsld:play");
            var activityStructureinnerActivities = null,activityStructureinnerActivitiesStructures = null,structureEnvironment = null;
            var structureLearningObjects = null,structureServices=null;
            play.setAttribute("identifier","play-"+uniqid());
            play.setAttribute("isvisible","true");
            var title = xmldoc.createElement("imsld:title");
            title.textContent = document.getElementById("lesson_plan_title").value;
            play.appendChild(title);
            method.appendChild(play);

            learningDesign.rootNodes = graphToLD.getRootNodeids();

            var conditionElement = null,startPoint=null;
            conditionElement = createConditions(xmldoc,method);
            var annotation = null,groupEleme = null,metadata=null,lom=null;
            var generalDescriptionString="",general="",generalDescription="";
            for(i=0;i<phases.getPhases().length;i++){
                var phaseType = phases.getPhases()[i].getType();

                if(phaseType!="activity-structure"){
                    var act = createAct(xmldoc,phases.getPhases()[i]);
                    play.appendChild(act);
                    
                }
                var activities = phases.getPhases()[i].getLearningActivitiesArray();
                //addActivitiesToLD(activities,xmldoc,activitiesNode,1,act);
                var activitiesStructure = phases.getPhases()[i].getActivitiesStructuresArray();
                var environementRef=null;
                for(var j=0;j<activitiesStructure.length;j++){                    
                    annotation = null;
                    activityType = xmldoc.createElement("imsld:activity-structure");
                    if(option=="graphicalRepresentation"){
                        activityType.setAttribute("positionX",activitiesStructure[j].getShape().getAttribute("dragX"));
                        activityType.setAttribute("positionY",activitiesStructure[j].getShape().getAttribute("dragY"));

                        if(learningDesign.rootNodes.containsActivityId(activitiesStructure[j].getId())){
                            PhaseId = act.getAttribute("identifier").substr(4)
                            startPoint = graphToLD.startPoint.containsId(PhaseId)
                            if(startPoint!=null){
                                activityType.setAttribute("startPositionX",startPoint.getPositionX());
                                activityType.setAttribute("startPositionY",startPoint.getPositionY());
                                activityType.setAttribute("startObjectId",PhaseId+"start");
                            }
                        }
                    }
                    activityTitle = xmldoc.createElement("imsld:title");
                    activityTitle.textContent = activitiesStructure[j].getName();
                    activityType.setAttribute("identifier","as-"+activitiesStructure[j].getIdentifier());//activityType.setAttribute("identifier","as-"+activitiesStructure[j].getIdentifier()+"-"+activitiesStructure[j].getName());
                    activityType.setAttribute("sort","visibility-order");
                    activityType.setAttribute("structure-type","selection");
                    if(activitiesStructure[j].getNoOfActivitesTobeExecuted()!=0)
                        activityType.setAttribute("number-to-select",activitiesStructure[j].getNoOfActivitesTobeExecuted());

                    activityType.appendChild(activityTitle);
                    information = activitiesStructure[j].getInformation();
                    if(information.length>0){
                        informationElement = xmldoc.createElement("imsld:information");
                        activityType.appendChild(informationElement);
                    }

                    for(var l=0;l<information.length;l++){
                        item = xmldoc.createElement("imsld:item");
                        item.setAttribute("identifier","item-"+information[l].getIdentifier());
                        item.setAttribute("identifierref","resource-"+information[l].getIdentifier());
                        itemTitle = xmldoc.createElement("imsld:title");
                        itemTitle.textContent = information[l].getTitle();
                        item.appendChild(itemTitle);
                        informationElement.appendChild(item);

                    }
                    structureEnvironment = activitiesStructure[j].getEnvironement();
                    structureLearningObjects = activitiesStructure[j].getEnvironement().getLearningObjects();
                    structureServices = activitiesStructure[j].getEnvironement().getServices();
                    if(structureLearningObjects.length!=0 || structureServices.length!=0){
                        environementRef = xmldoc.createElement("imsld:environment-ref");
                        environementRef.setAttribute("ref","env-"+structureEnvironment.getIdentifier())
                        activityType.appendChild(environementRef);
                        environment = xmldoc.createElement("imsld:environment");
                        environment.setAttribute("identifier","env-"+structureEnvironment.getIdentifier())
                        environements.appendChild(environment);
                        if(structureLearningObjects.length!=0)
                            createLearningObject(xmldoc, structureLearningObjects, environment);
                        if(structureServices.length!=0){
                            createService(xmldoc, structureServices, environment)
                        }
                    }
                    activityStructureinnerActivities = activitiesStructure[j].getLearningActivitiesArray().getActivities();
                    activityStructureinnerActivitiesStructures = activitiesStructure[j].getActivitiesStructuresArray();
                    for(var k=0;k<activityStructureinnerActivities.length;k++){
                       item = xmldoc.createElement("imsld:learning-activity-ref");
                       item.setAttribute("ref","la-"+activityStructureinnerActivities[k].getIdentifier());//item.setAttribute("ref","la-"+activityStructureinnerActivities[k].getIdentifier()+"-"+activityStructureinnerActivities[k].getName());
                       activityType.appendChild(item);                       
                    }
                    for(k=0;k<activityStructureinnerActivitiesStructures.length;k++){
                       item = xmldoc.createElement("imsld:activity-structure-ref");
                       item.setAttribute("ref","as-"+activityStructureinnerActivitiesStructures[k].getIdentifier());//item.setAttribute("ref","as-"+activityStructureinnerActivitiesStructures[k].getIdentifier()+"-"+activityStructureinnerActivitiesStructures[k].getName());
                       activityType.appendChild(item);
                    }
                    groupEleme = activitiesStructure[j].getShape().getElementsByTagName("g");
                   
                    for(k=0;k<groupEleme.length;k++){
                        if(groupEleme[k].getAttribute("name")!=null)
                            if(groupEleme[k].getAttribute("name")=="annotation"){
                                annotation = groupEleme[k];
                                break;
                            }
                    }
                    
                    if(annotation!=null){
                        
                        if(annotation.getAttribute("textContent")!=""){                           
                            metadata = xmldoc.createElement("imsld:metadata");
                            lom = xmldoc.createElement("lm:lom");
                            general = xmldoc.createElement("lm:general");
                            generalDescription = xmldoc.createElement("lm:description");
                            generalDescriptionString = xmldoc.createElement("lm:langstring");
                            generalDescriptionString.textContent = annotation.getAttribute("textContent");

                            
                            generalDescription.appendChild(generalDescriptionString)
                            general.appendChild(generalDescription);
                            lom.appendChild(general);                            
                            metadata.appendChild(lom);                           
                            activityType.appendChild(metadata);
                        }
                    }


                    if(phaseType!="activity-structure")
                        createRoleParts(activitiesStructure[j],act,xmldoc);
                    activitiesNode.appendChild(activityType);
                }
                addActivitiesToLD(activities,xmldoc,activitiesNode,phaseType,act,option,environements,rolesNode,phases.getPhases()[i].getId());

                if(phaseType!="activity-structure"){
                    completeAct(act,phases.getPhases()[i],xmldoc,option,activitiesNode);
                    var completionActFeedback = Act_completionFeedback(phases.getPhases()[i],xmldoc);
                    if(completionActFeedback!=null)
                        act.appendChild(completionActFeedback);
                    
                    var actNotification = Act_sendNotification(xmldoc,phases.getPhases()[i],completionActFeedback);
                    if(completionActFeedback==null && actNotification!=null)
                        act.appendChild(actNotification);
                }

            }
            
            
            components.appendChild(rolesNode);
            createPropertyNode(xmldoc);
            
            if(xmldoc.getElementsByTagName("imsld:properties").length!=0)
                components.appendChild(xmldoc.getElementsByTagName("imsld:properties")[0]);
            components.appendChild(activitiesNode);
            if(environements.childNodes.length>0)
                components.appendChild(environements);
            x.appendChild(components);
            x.appendChild(method);



            joinGatewayPointToLD(phases.getPhases(),xmldoc,option);
            InclusiveORGatewayPointToLD(phases.getPhases(),xmldoc,option);
            
            if(conditionElement!=null)
                if(conditionElement.childNodes.length==0)
                    conditionElement.parentNode.removeChild(conditionElement);
                
            
            //delete tmpProperties
            properties.removeUnused();
            return xmldoc;

        }
        catch(e){
            throw(e)
        }
    }
};



function createXMLDocument(rootTagName, namespaceURL) {
    if (!rootTagName) rootTagName = "";
    if (!namespaceURL) namespaceURL = "";
    if (document.implementation && document.implementation.createDocument) {            
            // This is the W3C standard way to do it
            return document.implementation.createDocument(namespaceURL, rootTagName, null);
    }
    else { // This is the IE way to do it
            // Create an empty document as an ActiveX object
            // If there is no root element, this is all we have to do
        var doc = new ActiveXObject("MSXML2.DOMDocument");

           // If there is a root tag, initialize the document
        if (rootTagName) {
                // Look for a namespace prefix
            var prefix = "";
            var tagname = rootTagName;
            var p = rootTagName.indexOf(':');
            if (p != -1) {
                prefix = rootTagName.substring(0, p);
                tagname = rootTagName.substring(p+1);
            }
            // If we have a namespace, we must have a namespace prefix
                    // If we don't have a namespace, we discard any prefix
            if (namespaceURL) {
                if (!prefix) prefix = "a0"; // What Firefox uses
            }
            else prefix = "";

                    // Create the root element (with optional namespace) as a
                    // string of text
            var text = "<" + (prefix?(prefix+":"):"") + tagname +
                        (namespaceURL
                         ?(" xmlns:" + prefix + '="' + namespaceURL +'"')
                         :"") +
                        "/>";
                    // And parse that text into the empty document
            doc.loadXML(text);
        }
        return doc;
   }
    //}
};

function addActivitiesToLD(activities,xmldoc,activitiesNode,phaseType,act,option,environements,rolesNode,phaseIdentifier){
    try{
        
        var descItem = [];
        var learningObjectivesItem = [];
        var prerequisitesItem = [];
        var activityDescription = null,learningObjectives=null,environement=null,environementRef=null,prerequisites=null,completeActivity=null;
        var completionFeedbackItem = [];
        var activityType = "";
        var activityTitle = "",PhaseId=-1;
        var activityId = "",generalDescriptionString="",general="",generalDescription="";
        var item = null,startPoint=null;
        var annotation = null,groupEleme = null,metadata=null,lom=null;
        var itemTitle = null,completeActivityItem=null,completionFeedback = null,learningObjects = null,services=null,completionFeedbackDesc = null, notifications=null;
        var notification = null, notificationReceivers=null, notificationSubject=null,properties=null,notificationType=null,notificationDefinition=null, notificationContent=null,notificationTitle=null,notificationProperties = null;
        var activitiesArray = activities.getActivities(),completionNotifications=null;
        var change_properties = null,change_property=null,changeProp=null,changePropRef=null,change_value=null;
        var foundRole=-1;
        
        for(var j=0;j<activitiesArray.length;j++){
            completionFeedback = null;
            annotation = null;
            activityType = xmldoc.createElement("imsld:learning-activity");
            if(option=="graphicalRepresentation"){
                activityType.setAttribute("positionX",activitiesArray[j].get_Shape().getAttribute("dragX"));
                activityType.setAttribute("positionY",activitiesArray[j].get_Shape().getAttribute("dragY"));
            }
            activityTitle = xmldoc.createElement("imsld:title");                    
            activityTitle.textContent = activitiesArray[j].getName();
            activityId = "la-"+activitiesArray[j].getIdentifier();//activityId = "la-"+activitiesArray[j].getIdentifier()+"-"+activitiesArray[j].getName();

            activityType.setAttribute("identifier",activityId);            
            if(learningDesign.rootNodes.containsActivityId(activitiesArray[j].getIdentifier())){
                activityType.setAttribute("isvisible","true");
                if(option=="graphicalRepresentation"){
                        if(phaseType!="activity-structure")
                            PhaseId = act.getAttribute("identifier").substr(4)
                        else
                            PhaseId = phaseIdentifier;
                        startPoint = graphToLD.startPoint.containsId(PhaseId)

                        if(startPoint!=null){
                            activityType.setAttribute("startPositionX",startPoint.getPositionX());
                            activityType.setAttribute("startPositionY",startPoint.getPositionY());
                            activityType.setAttribute("startObjectId",PhaseId+"start");
                        }
                    }

            }
            else
                activityType.setAttribute("isvisible","false");
            
             activityType.appendChild(activityTitle);
                    //activityType.appendChild(completeActivityOption);
             activitiesNode.appendChild(activityType);

             learningObjects = activitiesArray[j].getEnvironement().getLearningObjects();
             services = activitiesArray[j].getEnvironement().getServices();
             descItem = activitiesArray[j].getDescriptionItem();
             learningObjectivesItem = activitiesArray[j].getLearningObjectivesItems();
             prerequisitesItem = activitiesArray[j].getPrerequisitesItems();
             completionFeedbackItem = activitiesArray[j].getActivityCompletionFeedbackItem();
             completionNotifications = activitiesArray[j].getActivityNotifications();
             change_properties = activitiesArray[j].getPropertyList();

              if(learningObjectivesItem.length!=0){
                learningObjectives = xmldoc.createElement("imsld:learning-objectives");
                activityType.appendChild(learningObjectives);
             }

             for(k=0;k<learningObjectivesItem.length;k++){
                        item = xmldoc.createElement("imsld:item");
                        item.setAttribute("identifier","item-"+learningObjectivesItem[k].getIdentifier());
                        item.setAttribute("identifierref","resource-"+learningObjectivesItem[k].getIdentifier());
                        itemTitle = xmldoc.createElement("imsld:title");
                        itemTitle.textContent = learningObjectivesItem[k].getTitle();
                        item.appendChild(itemTitle);

                        learningObjectives.appendChild(item);
              }

             if(prerequisitesItem.length!=0){
                        prerequisites = xmldoc.createElement("imsld:prerequisites");
                        activityType.appendChild(prerequisites);
                    }
             for(k=0;k<prerequisitesItem.length;k++){
                        item = xmldoc.createElement("imsld:item");
                        item.setAttribute("identifier","item-"+prerequisitesItem[k].getIdentifier());
                        item.setAttribute("identifierref","resource-"+prerequisitesItem[k].getIdentifier());
                        itemTitle = xmldoc.createElement("imsld:title");
                        itemTitle.textContent = prerequisitesItem[k].getTitle();
                        item.appendChild(itemTitle);
                        prerequisites.appendChild(item);
             }
             if(learningObjects.length!=0 || services.length!=0){
                environementRef = xmldoc.createElement("imsld:environment-ref");
                environementRef.setAttribute("ref","env-"+activitiesArray[j].getEnvironement().getIdentifier())
                activityType.appendChild(environementRef);                                
                environement = xmldoc.createElement("imsld:environment");
                environement.setAttribute("identifier","env-"+activitiesArray[j].getEnvironement().getIdentifier())
                environements.appendChild(environement);
                if(learningObjects.length!=0)
                    createLearningObject(xmldoc, learningObjects, environement);
                if(services.length!=0){
                    createService(xmldoc, services, environement)
                }
             }
             
             if(descItem.length!=0){
                activityDescription = xmldoc.createElement("imsld:activity-description");
                activityType.appendChild(activityDescription);
             }
             for(var k=0;k<descItem.length;k++){
                item = xmldoc.createElement("imsld:item");
                item.setAttribute("identifier","item-"+descItem[k].getIdentifier());
                item.setAttribute("identifierref","resource-"+descItem[k].getIdentifier());
                itemTitle = xmldoc.createElement("imsld:title");
                itemTitle.textContent = descItem[k].getTitle();
                item.appendChild(itemTitle);
                activityDescription.appendChild(item);
             }             
            if(activitiesArray[j].getCompletionParameters()[0]!="none"){
                completeActivity = xmldoc.createElement("imsld:complete-activity");
                if(activitiesArray[j].getCompletionParameters()[0]=="userChoice")
                    completeActivityItem = xmldoc.createElement("imsld:"+activitiesArray[j].getCompletionParameters()[1]);
                else{                    
                    completeActivityItem = xmldoc.createElement("imsld:"+activitiesArray[j].getCompletionParameters()[1][0]);
                    completeActivityItem.textContent = activitiesArray[j].getCompletionParameters()[1][1];                    
                }
                completeActivity.appendChild(completeActivityItem);
                activityType.appendChild(completeActivity);
             }
             if(completionFeedbackItem.length!=0){
                completionFeedback = xmldoc.createElement("imsld:on-completion");
                completionFeedbackDesc = xmldoc.createElement("imsld:feedback-description");
                completionFeedback.appendChild(completionFeedbackDesc);
                activityType.appendChild(completionFeedback);
             }
             for(k=0;k<completionFeedbackItem.length;k++){
                        item = xmldoc.createElement("imsld:item");
                        item.setAttribute("identifier","item-"+completionFeedbackItem[k].getIdentifier());
                        item.setAttribute("identifierref","resource-"+completionFeedbackItem[k].getIdentifier());
                        itemTitle = xmldoc.createElement("imsld:title");
                        itemTitle.textContent = completionFeedbackItem[k].getTitle();
                        item.appendChild(itemTitle);
                        completionFeedbackDesc.appendChild(item);
             }
             if(change_properties.length!=0){
                if(completionFeedback==null){
                    completionFeedback = xmldoc.createElement("imsld:on-completion");
                    activityType.appendChild(completionFeedback);
                }
             }

             for(k=0;k<change_properties.length;k++){
                change_property = change_properties[k];
                changeProp = xmldoc.createElement("imsld:change-property-value");
                changePropRef = xmldoc.createElement("imsld:property-ref");
                changePropRef.setAttribute("ref","globprop-"+change_property.getId());
                changeProp.appendChild(changePropRef);

                change_value = xmldoc.createElement("imsld:property-value");
                change_value.textContent = change_property.getValue();
                changeProp.appendChild(change_value);
                completionFeedback.appendChild(changeProp);
             }

             if(completionNotifications.length!=0){
                if(completionFeedback==null){
                    completionFeedback = xmldoc.createElement("imsld:on-completion");
                    activityType.appendChild(completionFeedback);
                }                
                //notifications = xmldoc.createElement("imsld:notification");
                //completionFeedback.appendChild(notifications);
                if(xmldoc.getElementsByTagName("imsld:properties")[0]==null){
                    properties = xmldoc.createElement("imsld:properties");
                    xmldoc.getElementsByTagName("manifest")[0].appendChild(properties);
                }
            
             }
             for(k=0;k<completionNotifications.length;k++){
                properties = xmldoc.getElementsByTagName("imsld:properties")[0];
                foundRole = -1
                if(properties==null)
                    throw "error while creating notifications";
                
                //edww
                //notificationProperties = xmldoc.createElement("imsld:glob-property");                
                //notificationProperties.setAttribute("identifier","globprop-"+completionNotifications[k].getIdentifier());
                notificationProperties = xmldoc.createElement("imsld:loc-property");                
                notificationProperties.setAttribute("identifier","globprop-"+completionNotifications[k].getIdentifier());
                //allazoun sxolia
                properties.appendChild(notificationProperties);
                
                //edw
                //notificationDefinition = xmldoc.createElement("imsld:global-definition");
                //notificationDefinition.setAttribute("uri","");
                notificationTitle = xmldoc.createElement("imsld:title");                
                //bgainoun ta sxolia                
                notificationTitle.textContent = completionNotifications[k].getSubject();
                //edw 
                //notificationDefinition.appendChild(notificationTitle);
                notificationProperties.appendChild(notificationTitle);                 
                //notificationProperties.appendChild(notificationDefinition);
                //allzoun ta sxolia
                
                notificationType = xmldoc.createElement("imsld:datatype");
                notificationType.setAttribute("datatype", "string");
                //edw
                //notificationDefinition.appendChild(notificationType);
                notificationProperties.appendChild(notificationType );
                //allazoun ta sxolia
                notifications = xmldoc.createElement("imsld:notification");
                

                for(var s=0;s<completionNotifications[k].getReceivers().length;s++){
                    notification = xmldoc.createElement("imsld:email-data");
                    notification.setAttribute("email-property-ref","globprop-"+completionNotifications[k].getIdentifier());
                    var index = (completionNotifications[k].getReceivers()[s]).indexOf("_");
                    var role_id = (completionNotifications[k].getReceivers()[s]).substring(0, index);
                    var role = learnerArray.getRoles().containsId(role_id);
                    if(role==null)
                        role = staffArray.getRoles().containsId(role_id);
                    
                    if(role!=null){
                        notificationReceivers = xmldoc.createElement("imsld:role-ref");
                        notificationReceivers.setAttribute("ref", "role-"+role.getIdentifier());
                        notification.appendChild(notificationReceivers);
                        notifications.appendChild(notification);
                        foundRole=1;
                    }
                    
                }

                notificationSubject = xmldoc.createElement("imsld:subject");
                notificationSubject.textContent = completionNotifications[k].getContent();
                if(foundRole!=-1){
                    notifications.appendChild(notificationSubject);
                    completionFeedback.appendChild(notifications);
                }
             }
             //vazw kai ta annotations
                groupEleme = activitiesArray[j].get_Shape().getElementsByTagName("g");              
                for(var i=0;i<groupEleme.length;i++){
                    if(groupEleme[i].getAttribute("name")!=null)
                        if(groupEleme[i].getAttribute("name")=="annotation"){
                            annotation = groupEleme[i];
                            break;
                        }
                }             
                if(annotation!=null){
                   
                    if(annotation.getAttribute("textContent")!=""){
                        metadata = xmldoc.createElement("imsld:metadata");
                        lom = xmldoc.createElement("lm:lom");
                        general = xmldoc.createElement("lm:general");
                        generalDescription = xmldoc.createElement("lm:description");
                        generalDescriptionString = xmldoc.createElement("lm:langstring");
                        generalDescriptionString.textContent = annotation.getAttribute("textContent");

                        general.appendChild(generalDescription)
                        generalDescription.appendChild(generalDescriptionString)
                        lom.appendChild(general)
                        metadata.appendChild(lom);                        
                        activityType.appendChild(metadata);
                    }
                }
             if(phaseType!="activity-structure")
                createRoleParts(activitiesArray[j],act,xmldoc);

            }
    }
    catch(e){
        throw e
    }
}
function createConditions(xmldoc,method){
    try{
               
        var condElement = xmldoc.createElement("imsld:conditions");
        graphToLD.setXmldoc(xmldoc);
        graphToLD.setConditionElement(condElement);
        graphToLD.initOpenList();
        graphToLD.getRootNodeChilds();
        graphToLD.implementBFS();
        method.appendChild(condElement);

        return condElement;
    }
    catch(e){
        console.log("createCondition "+e);
    }

}
function createAct(xmldoc,phase){
    try{
        var act = xmldoc.createElement("imsld:act");
        act.setAttribute("identifier","act-"+phase.getId());
        var title = xmldoc.createElement("imsld:title");
        title.textContent = phase.getName();
        act.appendChild(title);

        return act;

    }
    catch(e){
        console.log("createAct "+e)
    }
}
function createRoleParts(activity,act,xmldoc){
    try{
        var checkBoxRole = null,role=null,rolepart=null;
        var entered = -1;
        for (var i=0;i<activity.getRolesAssigned().length;i++){
            checkBoxRole = activity.getRolesAssigned()[i];
            rolepart = null;
            if(checkBoxRole!=null){
                if(checkBoxRole.checked){
                    //role = learnerArray.getRoles().containsRole(activity.getRolesAssigned()[i].name);
                    role = learnerArray.getRoles().containsId(activity.getRolesAssigned()[i].name);
                    if(role==null)
                        role = staffArray.getRoles().containsId(activity.getRolesAssigned()[i].name);//role = staffArray.getRoles().containsRole(activity.getRolesAssigned()[i].name);
                    if(role==null){
                        //throw "error while creating role-parts.\nRole not found";
                        //return;
                        continue
                    }
                    rolepart = xmldoc.createElement("imsld:role-part");

                    rolepart.setAttribute("identifier","rolepart-"+uniqid());
                    var title = xmldoc.createElement("imsld:title");
                    title.textContent = activity.getName();
                    rolepart.appendChild(title);

                    var roleRef = xmldoc.createElement("imsld:role-ref");
                    roleRef.setAttribute("ref","role-"+role.getIdentifier());
                    rolepart.appendChild(roleRef);

                    var laRef = null;
                    if(activity.getType()!="activity-structure"){
                        laRef = xmldoc.createElement("imsld:learning-activity-ref");
                        laRef.setAttribute("ref","la-"+activity.getIdentifier());//laRef.setAttribute("ref","la-"+activity.getIdentifier()+"-"+activity.getName());
                    }
                    else{
                        laRef = xmldoc.createElement("imsld:activity-structure-ref");
                        laRef.setAttribute("ref","as-"+activity.getIdentifier());//laRef.setAttribute("ref","as-"+activity.getIdentifier()+"-"+activity.getName());
                    }
                    rolepart.appendChild(laRef);

                    act.appendChild(rolepart);
                    entered=1;
                }
            }
        }
        if(entered==-1){
            throw "save failed..\nPlease check that all activities-activityStructures are assigned to specific role\nActivity "+activity.getName()+" is not assigned to role";
        }
    }
    catch(e){
        console.log("edw me "+e)
        throw e
    }
}
function completeAct(act,phase,xmldoc,option,activitiesNode){
    if(phase.getConditionType()=="NoCondition")
        return;
    var completeAct = xmldoc.createElement("imsld:complete-act");
    
    var completeActivityItem = null;
    var roleName=null,activityName=null,activity=null,role=null,role_partRef=null;
    if(phase.getConditionType()=="TimePeriod"){
        act.appendChild(completeAct);
        completeActivityItem = xmldoc.createElement("imsld:time-limit");
        completeAct.appendChild(completeActivityItem);
        completeActivityItem.textContent = "P0Y0M"+phase.getEndCondition()[0]+"D"+"T"+phase.getEndCondition()[1]+"H"+"0M";
     }
     else if(phase.getConditionType()=="CompleteActivities"){
            if(graphToLD.ActivititiesGoToEndPoint.length>0)
                act.appendChild(completeAct);
            
            var new_property = new properties.property("isusedTofinishphaseAct","string","false");//dimiourgw neo prperty to opoio tha to kanw true otan teleiwnei kapoio apo ta
            new_property.setId(phase.getId())
            if(properties.propertiesList.containsId(new_property.getId())==null)
                properties.propertiesList.push(new_property);//activities pou kataligei sto end point.. To property auto otan ginetai true tha kathorizetai oti teleiwse to act
            var whenProperty = xmldoc.createElement("imsld:when-property-value-is-set");
            var propertyRef = xmldoc.createElement("imsld:property-ref");
            propertyRef.setAttribute("ref","globprop-"+new_property.getId())
            var value = xmldoc.createElement("imsld:property-value");
            var stringvalue = xmldoc.createElement("imsld:langstring")
            stringvalue.textContent = "true"
            value.appendChild(stringvalue)                
            whenProperty.appendChild(propertyRef)
            whenProperty.appendChild(value);

            completeAct.appendChild(whenProperty);
            var endPoint = null,activityNode=null,on_completionNode=null,changeProperty=null;
            var existNotificationElement = null;
            var metadata=null
            for(var i=0;i<graphToLD.ActivititiesGoToEndPoint.length;i++){
                existNotificationElement = null;
                endPoint = graphToLD.ActivititiesGoToEndPoint[i]
                if(phase.getActivitiesStructuresArray().containsId(endPoint.getActivityId())==null && phase.getLearningActivitiesArray().getActivities().containsId(endPoint.getActivityId())==null)
                    continue//thelw ena property gia kathe act kai auto to property tha peirazoun ta activities kai activities structures tou act

                activityNode = getElem.getElementById(activitiesNode,endPoint.getActivityType()+endPoint.getActivityId());

                if(activityNode==null)//des gia gateways pou kataligoun se end
                    continue;
                if(activityNode.getElementsByTagName("imsld:on-completion")[0]==null){
                   metadata = activityNode.getElementsByTagName("imsld:metadata")[0];
                   on_completionNode = xmldoc.createElement("imsld:on-completion");
                   if(metadata!=null){
                    activityNode.insertBefore(on_completionNode, metadata)
                   }
                   else
                    activityNode.appendChild(on_completionNode);

                }

                else
                    on_completionNode = activityNode.getElementsByTagName("imsld:on-completion")[0]

                changeProperty = xmldoc.createElement("imsld:change-property-value");
                propertyRef = xmldoc.createElement("imsld:property-ref");
                propertyRef.setAttribute("ref","globprop-"+new_property.getId())
                changeProperty.appendChild(propertyRef);
                value = xmldoc.createElement("imsld:property-value");
                stringvalue = xmldoc.createElement("imsld:langstring")
                stringvalue.textContent = "true"
                value.appendChild(stringvalue)
                changeProperty.appendChild(value)
                
                existNotificationElement = on_completionNode.getElementsByTagName("imsld:notification")
                if(existNotificationElement!=null)
                    on_completionNode.insertBefore(changeProperty,existNotificationElement[0])
                else
                    on_completionNode.appendChild(changeProperty)
                if(option=="graphicalRepresentation"){
                    changeProperty.setAttribute("positionX",endPoint.getPositionX())
                    changeProperty.setAttribute("positionY",endPoint.getPositionY())
                    changeProperty.setAttribute("identifier",endPoint.getEndPointId())
                }

            }
        //}
     }

    /*else if(phase.getConditionType()=="CompleteActivities"){
        var activityId=-1,role_partRef = null,endPointId=-1;
        role=null;
        if(option=="graphicalRepresentation"){
            if(graphToLD.ActivititiesGoToEndPoint.length>=1){
                completeAct.setAttribute("positionX",graphToLD.ActivititiesGoToEndPoint[0].getPositionX())
                completeAct.setAttribute("positionY",graphToLD.ActivititiesGoToEndPoint[0].getPositionY())
                completeAct.setAttribute("identifier",graphToLD.ActivititiesGoToEndPoint[0].getEndPointId())
            }
        }
        if(graphToLD.ActivititiesGoToEndPoint.length>0){
            for(var i=0;i<graphToLD.ActivititiesGoToEndPoint.length;i++){
                activityId = graphToLD.ActivititiesGoToEndPoint[i].getActivityId();
                activity = phase.getLearningActivitiesArray().getActivities().containsId(activityId);
                if(activity==null)
                    activity = phase.getActivitiesStructuresArray().containsId(activityId);
                if(activity==null)
                    continue;
                for (var k=0;k<activity.getRolesAssigned().length;k++){
                    role = learnerArray.getRoles().containsId(activity.getRolesAssigned()[k].name);
                    if(role==null)
                        role = staffArray.getRoles().containsId(activity.getRolesAssigned()[k].name);
                    if(role==null)
                        continue;

                    role_partRef = getRef.findRolePartRef(act,"role-"+role.getIdentifier(),activity);
                    if(role_partRef==null)
                        continue
                    completeActivityItem = xmldoc.createElement("imsld:when-role-part-completed");
                    completeActivityItem.setAttribute("ref",role_partRef);
                    completeAct.appendChild(completeActivityItem);
                 }

            }
           
        }
        }*/
         /*var orGateway = null,gotoObj=null;
            var condition="",expressionElem="",roleRef="";
            for(i=0;i<phase.getInclusiveOrGateways().length;i++){
                orGateway = phase.getInclusiveOrGateways()[i];
                for(k=0; k<orGateway.getObjGoTo().length;k++)
                    gotoObj = orGateway.getObjGoTo()[k];
                    if(gotoObj.getgoToObjId().getType()=="endPoint"){
                        condition = xmldoc.createElement("imsld:when-condition-true");
                        roleRef = xmldoc.createElement("imsld:role-ref");
                        condition.appendChild(roleRef)
                        expressionElem = xmldoc.createElement("imsld:expression")
                        condition.appendChild(expressionElem);

                    }
            }*/


    
}
    function Act_completionFeedback(act,xmldoc){
        try{
            var completionFeedback=null,completionFeedbackDesc=null;
            var item = null,itemTitle=null;
           
            var completionFeedbackItem = act.getFeedback();
            if(completionFeedbackItem.length!=0){
                completionFeedback = xmldoc.createElement("imsld:on-completion");
                completionFeedbackDesc = xmldoc.createElement("imsld:feedback-description");
                completionFeedback.appendChild(completionFeedbackDesc);                
             }
             for(var k=0;k<completionFeedbackItem.length;k++){
                        item = xmldoc.createElement("imsld:item");
                        item.setAttribute("identifier","item-"+completionFeedbackItem[k].getIdentifier());
                        item.setAttribute("identifierref","resource-"+completionFeedbackItem[k].getIdentifier());
                        itemTitle = xmldoc.createElement("imsld:title");
                        itemTitle.textContent = completionFeedbackItem[k].getTitle();
                        item.appendChild(itemTitle);
                        completionFeedbackDesc.appendChild(item);
             }
             return completionFeedback;
        }
        catch(e){
            console.log("error while creating completion feedback for specific act "+e)
        }
        return null;
    }
    function Act_sendNotification(xmldoc,act,completionFeedback){
        try{
            var notifications = null,properties=null,notificationProperties=null,notificationDefinition=null,notificationTitle=null;
            var notificationType=null,notification=null,notificationReceivers=null,notificationSubject=null;
            var completionNotifications = act.getNotification();
            var change_properties = act.getPropertyList();
            var change_property = null,changeProp=null,changePropRef=null,change_value=null;

            if(completionNotifications.length!=0){
                if(completionFeedback==null){
                    completionFeedback = xmldoc.createElement("imsld:on-completion");
                }
                //notifications = xmldoc.createElement("imsld:notification");
                //completionFeedback.appendChild(notifications);
                if(xmldoc.getElementsByTagName("imsld:properties")[0]==null){
                    properties = xmldoc.createElement("imsld:properties");
                    xmldoc.getElementsByTagName("manifest")[0].appendChild(properties);
                }
             }
             if(change_properties.length!=0){
                if(completionFeedback==null){
                    completionFeedback = xmldoc.createElement("imsld:on-completion");
                }
             }
             for(k=0;k<change_properties.length;k++){
                change_property = change_properties[k];
                changeProp = xmldoc.createElement("imsld:change-property-value");
                changePropRef = xmldoc.createElement("imsld:property-ref");
                changePropRef.setAttribute("ref","globprop-"+change_property.getId());
                changeProp.appendChild(changePropRef);

                change_value = xmldoc.createElement("imsld:property-value");
                change_value.textContent = change_property.getValue();
                changeProp.appendChild(change_value);
                completionFeedback.appendChild(changeProp);
             }
             for(var k=0;k<completionNotifications.length;k++){
                properties = xmldoc.getElementsByTagName("imsld:properties")[0];
                if(properties==null)
                    throw "error while creating notifications";

                //edw
                //notificationProperties = xmldoc.createElement("imsld:glob-property");
                notificationProperties = xmldoc.createElement("imsld:loc-property");
                //allazoun ta sxolia
                notificationProperties.setAttribute("identifier","globprop-"+completionNotifications[k].getIdentifier());
                properties.appendChild(notificationProperties);

                //edw
                //notificationDefinition = xmldoc.createElement("imsld:global-definition");
                //notificationDefinition.setAttribute("uri","");
                notificationTitle = xmldoc.createElement("imsld:title");
                notificationTitle.textContent = completionNotifications[k].getSubject();
                //notificationDefinition.appendChild(notificationTitle);                
                //notificationProperties.appendChild(notificationDefinition);
                notificationProperties.appendChild(notificationTitle);
                //bgainoun ta sxolia
                notificationType = xmldoc.createElement("imsld:datatype");
                notificationType.setAttribute("datatype", "string");
                //edw
                //notificationDefinition.appendChild(notificationType);
                notificationProperties.appendChild(notificationType);
                //allazoun ta sxolia
                
                notifications = xmldoc.createElement("imsld:notification");
                completionFeedback.appendChild(notifications);
                
                for(var s=0;s<completionNotifications[k].getReceivers().length;s++){
                    var index = (completionNotifications[k].getReceivers()[s]).indexOf("_");
                    var role_id = (completionNotifications[k].getReceivers()[s]).substring(0, index);
                    var role = learnerArray.getRoles().containsId(role_id);
                    if(role==null)
                        role = staffArray.getRoles().containsId(role_id);

                    notification = xmldoc.createElement("imsld:email-data");
                    notification.setAttribute("email-property-ref","globprop-"+completionNotifications[k].getIdentifier());
                    if(role!=null){
                        notificationReceivers = xmldoc.createElement("imsld:role-ref");
                        notificationReceivers.setAttribute("ref", "role-"+role.getIdentifier());
                        notification.appendChild(notificationReceivers);
                    }
                    notifications.appendChild(notification);
                }

                notificationSubject = xmldoc.createElement("imsld:subject");
                notificationSubject.textContent = completionNotifications[k].getContent();
                
                notifications.appendChild(notificationSubject);
             }
             return completionFeedback;
        }
        catch(e){
            console.log("error while creating notification element for specific act "+e)
        }
        return null;
    }
    function createLearningObject(xmldoc,learningObjectItems,environment){
        try{
            var item = null,itemTitle=null;
            var learningObject =  xmldoc.createElement("imsld:learning-object");
            learningObject.setAttribute("identifier","lo-"+uniqid());
            learningObject.setAttribute("isvisible","true");
            for(var i=0;i<learningObjectItems.length;i++){
                item = xmldoc.createElement("imsld:item");
                item.setAttribute("identifier","item-"+learningObjectItems[i].getIdentifier());
                item.setAttribute("identifierref","resource-"+learningObjectItems[i].getIdentifier());
                itemTitle = xmldoc.createElement("imsld:title");
                itemTitle.textContent = learningObjectItems[i].getTitle();
                item.appendChild(itemTitle);
                learningObject.appendChild(item);
            }
            environment.appendChild(learningObject);
        }
        catch(e){
            console.log("error while creating environment element "+e)
        }
    }
    function createService(xmldoc,services,environment){
        var tmp_roleMapping = [];
        var service = null;
        var monitor = null;
        var self = null,item=null,title=null;
        var parameters = "";
        var serviceName="";
        for(var r=0;r<services.length;r++){
            service = xmldoc.createElement("imsld:service");
            service.setAttribute("identifier",services[r].getName()+"-"+uniqid());
            service.setAttribute("isvisible","true");
            parameters = "{\"key\":\""+services[r].getKey()+"\",\"secret\":\""+services[r].getSecret()+"\",\"launch_url\":\""+services[r].getUrl()+"\", \"context_id\":\""+services[r].getContextId()+"\", \"context_title\":\""+services[r].getContextTitle()+"\", \"context_label\":\""+services[r].getContextLabel()+"\", \"resource_link_title\":\""+services[r].getResourceTitle()+"\", \"resource_link_id\":\""+services[r].getResourceId()+"\",";
            parameters += "\"role_mapping\":[{";
            tmp_roleMapping = services[r].getRoleMapping();
            for(var r1=0;r1<tmp_roleMapping.length;r1++){
                parameters += "\""+tmp_roleMapping[r1][0]+"\":\""+tmp_roleMapping[r1][1]+"\"";
                if((r1+1)<tmp_roleMapping.length)
                    parameters += ",";
                        //find tmp_roleMapping[r1][1] kai grapse sta metadata oti einai tmp_roleMapping[r1][0] gia to tool
                        //if(tmp_roleMapping[r1][1]!="none"){
                            //(getElem.getElementById(rolesNode,"role-"+tmp_roleMapping[r1][1]))

                        //}

                 }
            parameters += "}]}";
            service.setAttribute("parameters",parameters);
            
            monitor = xmldoc.createElement("imsld:monitor");
            self =  xmldoc.createElement("imsld:self");
            monitor.appendChild(self);
            title =  xmldoc.createElement("imsld:title");
            serviceName = services[r].getName();
            if(serviceName.indexOf("structure")!=-1){
                serviceName = serviceName.substring(9,serviceName.length)
            }
                
            title.textContent = services[r].getName();
            monitor.appendChild(title);
            item = xmldoc.createElement("imsld:item");
            item.setAttribute("identifier","item-"+uniqid());
            monitor.appendChild(item);
            service.appendChild(monitor);
            environment.appendChild(service)
        }
    }
    function createPropertyNode(xmldoc){
        if(properties.propertiesList==null)
            return;
        var prop = null;
        var property = null;
        if(properties.propertiesList.length>=1){
            if(xmldoc.getElementsByTagName("imsld:properties")[0]==null){//den iparxei to element ara to dimiourgw
                prop = xmldoc.createElement("imsld:properties");
                xmldoc.getElementsByTagName("manifest")[0].appendChild(prop);
            }

        }
        prop = xmldoc.getElementsByTagName("imsld:properties")[0];
        if(prop == null)
            return;
        var globProp = null;
        var propDefinition = null;
        var propTitle = null;
        var propType = null;
        var initValue = null;
        
        for(var i=0;i<properties.propertiesList.length;i++){
            property = properties.propertiesList[i];
            //edw
            //globProp = xmldoc.createElement("imsld:glob-property");
            globProp = xmldoc.createElement("imsld:loc-property");
            //allazoun ta sxolia
            
            globProp.setAttribute("identifier","globprop-"+property.getId());
            prop.appendChild(globProp);
            //edw
            //propDefinition = xmldoc.createElement("imsld:global-definition");
            //globProp.appendChild(propDefinition);            
            //propDefinition.setAttribute("uri","");
            //bgainoun ta sxolia
            propTitle = xmldoc.createElement("imsld:title");
            propTitle.textContent = property.getName();
            //edw
            //propDefinition.appendChild(propTitle);
            globProp.appendChild(propTitle);
            //allazoun ta sxolia
            propType = xmldoc.createElement("imsld:datatype");
            
            propType.setAttribute("datatype", property.getType());
            //edw
            //propDefinition.appendChild(propType);
            globProp.appendChild(propType);
            //allazoun ta sxolia
            initValue = xmldoc.createElement("imsld:initial-value");
            initValue.textContent = property.getInitValue();
            //edw
            //propDefinition.appendChild(initValue);
            globProp.appendChild(initValue);
            //allazoun ta sxolia
        }
    }
var graphToLD = {
    openlist: null,
    xmldoc:null,
    conditionElement:null,
    ActivititiesGoToEndPoint:null,
    startPoint:new Array(),
    initOpenList: function(){
       graphToLD.openlist = new Array();
       graphToLD.ActivititiesGoToEndPoint = new Array();
    },
    setXmldoc: function(xmlDOC){
        graphToLD.xmldoc = xmlDOC;
    },
    setConditionElement: function(condElement){
        graphToLD.conditionElement = condElement;
    },
    getRootNodeChilds: function(){
        try{
            var connections = null,activities=null,root=null,structures = null,syncNode=null,orNode=null;
            var cur_phase=null;
            var synchGateways = null,synchGateway=null;//pare kai ta gateway kai vale ta prwta paidia tou stin openlist
            var orGateways = null,orGateway=null;
            //giati an den to kaneis tote apo to gateway kai meta den psaxnei o algorithmos

            for(var j=0;j<allPhases.getPhases().length;j++){
               
                cur_phase = allPhases.getPhases()[j]
                activities = cur_phase.getLearningActivitiesArray();
                structures = cur_phase.getActivitiesStructuresArray();
                connections = activities.getConnectionsArray();
                synchGateways = cur_phase.getJoinGateways();
                orGateways = cur_phase.getInclusiveOrGateways();
                for(var i=0;i<connections.length;i++){
                    var con = connections[i];
                    if(con.getAttribute("from")==cur_phase.getId()+"start"){
                        if(graphToLD.startPoint.containsId(cur_phase.getId())==null)
                            graphToLD.startPoint.push(new startPoint(cur_phase.getId(),cur_phase.getStartPoint().getAttribute("dragX"),cur_phase.getStartPoint().getAttribute("dragY")))
                       
                        root = activities.getActivities().containsId(con.getAttribute("to"));//codeAllagi activities.getActivities().containsName(con.getAttribute("to"));                        
                        if(root==null)
                            root = structures.containsId(con.getAttribute("to"));
                        if(root!=null)
                            graphToLD.openlist.push(root);
                        
                        else{//eksakolouthei na einia to root null opote pigaine na elenkseis gateway
                            syncNode = cur_phase.getJoinGateways().containsName(con.getAttribute("to"));
                            if(syncNode!=null){
                                for(var k=0;k<syncNode.getObjGoTo().length;k++){
                                    graphToLD.openlist.push(syncNode.getObjGoTo()[k])                                    
                                }
                            }
                            /*else{//simainei oti exw mia grammi apo to start pou den paei oute se join gateway
                                //psakse kai se or gateway loipon
                                orNode = orGateways.containsName(con.getAttribute("to"));
                                if(orNode!=null){
                                    for(var k=0;k<orNode.getObjGoTo().length;k++){//to goto einai sintheto kai periexei kai plirofories gia to line mazi me toobjId
                                        graphToLD.openlist.push(orNode.getObjGoTo()[k].getgoToObjId())
                                    }
                                }
                            }*/
                        }
                    }
                }
  
                for(i=0;i<synchGateways.length;i++){//vale ta objects stin open list giati den tha ta psaxei diaforetika
                    synchGateway = synchGateways[i];
                    for(k=0;k<synchGateway.getObjGoTo().length;k++){
                        if(synchGateway.getObjGoTo()[k].getType()=="endPoint")//ane einia end point den to vazeis stin lista)                            
                            continue;

                        if(graphToLD.openlist.containsId(synchGateway.getObjGoTo()[k].getId())==null)
                            graphToLD.openlist.push(synchGateway.getObjGoTo()[k])
                    }
                }
                for(i=0;i<orGateways.length;i++){
                    orGateway = orGateways[i];
                    for(k=0;k<orGateway.getObjGoTo().length;k++){
                        if(orGateway.getObjGoTo()[k].getgoToObjId().getType()=="endPoint")//ane einia end point den to vazeis stin lista
                            continue;
                        if(graphToLD.openlist.containsId(orGateway.getObjGoTo()[k].getgoToObjId())==null)
                            graphToLD.openlist.push(orGateway.getObjGoTo()[k].getgoToObjId())
                    }
                }
            }           
        }
        catch(e){
            console.log("error while traversing tree "+e);
        }
        
    },
    getRootNodeids: function(){
        try{
            var connections = null,activities=null,root=null,structures = null,syncNode=null,orNode=null;
            var rootNodes = new Array();
            var cur_phase = null;
            for(var j=0;j<allPhases.getPhases().length;j++){
                cur_phase = allPhases.getPhases()[j];
                activities = cur_phase.getLearningActivitiesArray();
                structures = cur_phase.getActivitiesStructuresArray();
                connections = activities.getConnectionsArray();

                for(var i=0;i<connections.length;i++){
                    var con = connections[i];
                    if(con.getAttribute("from")==cur_phase.getId()+"start"){
                        root = activities.getActivities().containsId(con.getAttribute("to"));//codeAllagi activities.getActivities().containsName(con.getAttribute("to"));                        
                        if(root==null)
                            root = structures.containsId(con.getAttribute("to"));
                        if(root!=null)
                            rootNodes.push(con.getAttribute("to"))
                        else{//eksakolouthei na einia null opote pigaine na elenkseis gateway                           
                            syncNode = cur_phase.getJoinGateways().containsName(con.getAttribute("to"));                            
                            if(syncNode!=null){
                                for(var k=0;k<syncNode.getObjGoTo().length;k++){
                                    rootNodes.push(syncNode.getObjGoTo()[k].getId())
                                    
                                }
                            }                            
                            if(syncNode==null){//den itan oute sync node pigene na deis an einia orGateway
                                    orNode = cur_phase.getInclusiveOrGateways().containsName(con.getAttribute("to"));                                    
                                if(orNode!=null)
                                    rootNodes.push(orNode.getName())
                            }
                        }
                    }
                }
            }
            return rootNodes;
        }
        catch(e){
            console.log("error while traversing tree "+e);
        }

    },
    completeElement: function(parentActivity){
        //var ifCondition = graphToLD.xmldoc.createElement("imsld:if");
        var complete = graphToLD.xmldoc.createElement("imsld:complete")
        //ifCondition.appendChild(complete);

        var activityRef = graphToLD.xmldoc.createElement("imsld:"+parentActivity.getType()+"-ref");
        if(parentActivity.getType()!="activity-structure")
            activityRef.setAttribute("ref","la-"+parentActivity.getIdentifier());//activityRef.setAttribute("ref","la-"+parentActivity.getIdentifier()+"-"+parentActivity.getName());
        else
            activityRef.setAttribute("ref","as-"+parentActivity.getIdentifier());//activityRef.setAttribute("ref","as-"+parentActivity.getIdentifier()+"-"+parentActivity.getName());

        complete.appendChild(activityRef);
        return complete;

    },
    showElement:function(childActivity){
        //var show = graphToLD.xmldoc.createElement("imsld:show");
        var activityRef = graphToLD.xmldoc.createElement("imsld:"+childActivity.getType()+"-ref");
        if(childActivity.getType()!="activity-structure")
            activityRef.setAttribute("ref","la-"+childActivity.getIdentifier());//activityRef.setAttribute("ref","la-"+childActivity.getIdentifier()+"-"+childActivity.getName());
        else
            activityRef.setAttribute("ref","as-"+childActivity.getIdentifier());//activityRef.setAttribute("ref","as-"+childActivity.getIdentifier()+"-"+childActivity.getName());
        //show.appendChild(activityRef);

        return activityRef;
    },
    hideElement:function(childActivity){
        //var hide = graphToLD.xmldoc.createElement("imsld:hide");
        var activityRef = graphToLD.xmldoc.createElement("imsld:"+childActivity.getType()+"-ref");

        if(childActivity.getType()!="activity-structure")
            activityRef.setAttribute("ref","la-"+childActivity.getIdentifier());//activityRef.setAttribute("ref","la-"+childActivity.getIdentifier()+"-"+childActivity.getName());
        else
            activityRef.setAttribute("ref","as-"+childActivity.getIdentifier());//activityRef.setAttribute("ref","as-"+childActivity.getIdentifier()+"-"+childActivity.getName());

        //hide.appendChild(activityRef);

        return activityRef;
    },
    createCondition: function(conditionsElement,parentActivity,childActivity){
        var ifCondition = graphToLD.xmldoc.createElement("imsld:if");
        var complete = graphToLD.xmldoc.createElement("imsld:complete")
        ifCondition.appendChild(complete);

        var activityRef = graphToLD.xmldoc.createElement("imsld:"+parentActivity.getType()+"-ref");
        activityRef.setAttribute("ref","la-"+parentActivity.getIdentifier());//activityRef.setAttribute("ref","la-"+parentActivity.getIdentifier()+"-"+parentActivity.getName());
        complete.appendChild(activityRef);

        var then = graphToLD.xmldoc.createElement("imsld:then");
        //edw
        var show = graphToLD.xmldoc.createElement("imsld:show");
        then.appendChild(show);
        activityRef = graphToLD.xmldoc.createElement("imsld:"+childActivity.getType()+"-ref");
        activityRef.setAttribute("ref","la-"+childActivity.getIdentifier());//activityRef.setAttribute("ref","la-"+childActivity.getIdentifier()+"-"+childActivity.getName());
        show.appendChild(activityRef);


        var hide = graphToLD.xmldoc.createElement("imsld:hide");
        //Den to krivw otan ginei complete..............
        //then.appendChild(hide);
        activityRef = graphToLD.xmldoc.createElement("imsld:"+parentActivity.getType()+"-ref");
        activityRef.setAttribute("ref","la-"+parentActivity.getIdentifier());//activityRef.setAttribute("ref","la-"+parentActivity.getIdentifier()+"-"+parentActivity.getName());
        hide.appendChild(activityRef);

        //edw
        var elseCond = graphToLD.xmldoc.createElement("imsld:else");
        hide = graphToLD.xmldoc.createElement("imsld:hide");
        elseCond.appendChild(hide);
        activityRef = graphToLD.xmldoc.createElement("imsld:"+childActivity.getType()+"-ref");
        activityRef.setAttribute("ref","la-"+childActivity.getIdentifier());//activityRef.setAttribute("ref","la-"+childActivity.getIdentifier()+"-"+childActivity.getName());
        hide.appendChild(activityRef);

        conditionsElement.appendChild(ifCondition);
        conditionsElement.appendChild(then);
        conditionsElement.appendChild(elseCond);

    },

    implementBFS: function(){
        try{
            //var id;
            
            var activity = null;
            //var len = graphToLD.openlist.childNodes.length;
            var tmpList = graphToLD.openlist;
            var ifCond = null,then=null,show=null,elseCond=null,hide=null;
            var endPoint = null;
            while(graphToLD.openlist.length>0){
                activity = graphToLD.openlist[0];
                //apo edw
                if(activity.getflowChilds().length>0){

                    ifCond = graphToLD.xmldoc.createElement("imsld:if");
                    ifCond.appendChild(graphToLD.completeElement(activity));
                    //graphToLD.conditionElement.appendChild(ifCond);
                    then = graphToLD.xmldoc.createElement("imsld:then");
                    //graphToLD.conditionElement.appendChild(then);
                    show = graphToLD.xmldoc.createElement("imsld:show");
                    then.appendChild(show);
                    elseCond = graphToLD.xmldoc.createElement("imsld:else");
                    //graphToLD.conditionElement.appendChild(elseCond);
                    hide = graphToLD.xmldoc.createElement("imsld:hide");
                    elseCond.appendChild(hide);
                //ws edw
                    for(var j=0;j<activity.getflowChilds().length;j++){
                        //graphToLD.createCondition(graphToLD.conditionElement, activity,activity.getflowChilds()[j]);
                        //to apopanw vgainei apo sxolia kai mpainei to katw ws
                        if(activity.getflowChilds()[j].getType()=="endPoint"){
                            if(activity.getType()=="learning-activity")
                               graphToLD.ActivititiesGoToEndPoint.push(new endPoints(activity.getflowChilds()[j].getId(),activity.getId(),activity.getflowChilds()[j].getPositionX(),activity.getflowChilds()[j].getPositionY(),"la-"))
                            else if(activity.getType()=="activity-structure")
                               graphToLD.ActivititiesGoToEndPoint.push(new endPoints(activity.getflowChilds()[j].getId(),activity.getId(),activity.getflowChilds()[j].getPositionX(),activity.getflowChilds()[j].getPositionY(),"as-"))
                            continue;
                        }
                        
                        show.appendChild(graphToLD.showElement(activity.getflowChilds()[j]));
                        hide.appendChild(graphToLD.hideElement(activity.getflowChilds()[j]));
                        //edw

                        tmpList.push(activity.getflowChilds()[j]);
                    }
                    if(show.childNodes.length>0){
                        graphToLD.conditionElement.appendChild(ifCond);
                        graphToLD.conditionElement.appendChild(then);
                        graphToLD.conditionElement.appendChild(elseCond);
                    }
                    hide = graphToLD.xmldoc.createElement("imsld:hide");
                    //Den to krivw otan ginei complete..............
                    //then.appendChild(hide);
                    hide.appendChild(graphToLD.hideElement(activity));
                }
                graphToLD.openlist.splice(0,1);
            }
           
        }
        catch(e){
            console.log("implement BFS "+e)
        }
    }
}
function joinGatewayPointToLD(phases,xmldoc,option){
    try{
        var joinGateways,entered=0;
        var phase,endPoint=null;
        var ifCond=null,then=null,show=null,elseCond=null,hide=null,And=null;
        var conditions = null;
        var changeProp=null,changePropRef=null,change_value=null;
        if(xmldoc.getElementsByTagName("imsld:conditions")!=null)
            conditions = xmldoc.getElementsByTagName("imsld:conditions")[0];

        if(conditions==null)
            return;

        for(var k=0;k<phases.length;k++){            
            phase = phases[k];
            //if(phase.getType()=="activity-structure")
                //continue;

            for(var i=0;i<phase.getJoinGateways().length;i++){
                joinGateways = phase.getJoinGateways()[i];
                if(joinGateways.getObjFrom().length==0 || joinGateways.getObjGoTo().length==0)//einai ston aera diladi apo tin mia mpanta
                    continue;
                ifCond = xmldoc.createElement("imsld:if");
                conditions.appendChild(ifCond);
                if(joinGateways.getObjFrom().length>1){//den tha kanei load to gateway an einai ena to from object
                    And = xmldoc.createElement("imsld:and");
                    ifCond.appendChild(And);
                    if(option=="graphicalRepresentation"){
                        ifCond.setAttribute("type","joingateway");
                        And.setAttribute("identifier",joinGateways.getName());
                        And.setAttribute("positionX",joinGateways.getShape().getAttribute("dragX"));
                        And.setAttribute("positionY",joinGateways.getShape().getAttribute("dragY"));
                    }
                }
                hide = xmldoc.createElement("imsld:hide");
                for(var l=0;l<joinGateways.getObjFrom().length;l++){                    
                    if(And!=null)
                        And.appendChild(graphToLD.completeElement(joinGateways.getObjFrom()[l]));
                    else
                        ifCond.appendChild(graphToLD.completeElement(joinGateways.getObjFrom()[l]));

                    hide.appendChild(graphToLD.hideElement(joinGateways.getObjFrom()[l]));
                }
                then = xmldoc.createElement("imsld:then");
                conditions.appendChild(then);
                show = xmldoc.createElement("imsld:show");
                //then.appendChild(show);
                //Den to krivw otan ginei complete..............
                //then.appendChild(hide);

                elseCond = xmldoc.createElement("imsld:else");
                //conditions.appendChild(elseCond);
                hide = xmldoc.createElement("imsld:hide");
                elseCond.appendChild(hide);
                for(l=0;l<joinGateways.getObjGoTo().length;l++){
                    
                    if(joinGateways.getObjGoTo()[l].getType()=="endPoint"){
                        endPoint = joinGateways.getObjGoTo()[l];
                        changeProp = xmldoc.createElement("imsld:change-property-value");
                        changePropRef = xmldoc.createElement("imsld:property-ref");
                        changePropRef.setAttribute("ref","globprop-"+phase.getId());
                        changeProp.appendChild(changePropRef);
                        change_value = xmldoc.createElement("imsld:property-value");
                        change_value.textContent = "true";
                        changeProp.appendChild(change_value);
                        then.appendChild(changeProp);
                        if(option=="graphicalRepresentation"){
                            changeProp.setAttribute("positionX",endPoint.getPositionX())
                            changeProp.setAttribute("positionY",endPoint.getPositionY())
                            changeProp.setAttribute("identifier",endPoint.getId())
                        }
                    }
                    else{
                        entered++
                        show.appendChild(graphToLD.hideElement(joinGateways.getObjGoTo()[l]));
                        hide.appendChild(graphToLD.hideElement(joinGateways.getObjGoTo()[l]));
                    }
                }
                if(entered>0){
                    then.appendChild(show);
                    conditions.appendChild(elseCond);
                }
            }
        }
    }
    catch(e){
        alert("error while creating sychronization points "+e);
    }
}
/*function joinGatewayPointToLD(phases,xmldoc){
    try{
        var joinGateways;
        var phase;
        var ifCond, complete,lActivityRef,then,show,elseCond,hide,outerAnd,innerAnd;

        var conditions = xmldoc.getElementsByTagName("imsld:conditions");
        if(conditions==null)
            return;

        var counter=0;
        for(var k=0;k<phases.length;k++){
            phase = phases[k];

            for(var i=0;i<phase.getJoinGateways().length;i++){
        
                joinGateways = phase.getJoinGateways()[i];
                outerAnd = null;
                innerAnd = null;
                for(var j=0;j<joinGateways.getObjGoTo().length;j++){
                    counter=0;
                    ifCond = xmldoc.createElement("imsld:if");
                    outerAnd = xmldoc.createElement("imsld:and");
                    for(var l=0;l<joinGateways.getObjFrom().length;l++){
                        if(counter==2){
                            counter=1;
                            innerAnd = outerAnd.cloneNode(true);
                            outerAnd = xmldoc.createElement("imsld:and");
                            outerAnd.appendChild(innerAnd);
                        }
                        counter++;
                        complete = xmldoc.createElement("imsld:complete");
                        lActivityRef = xmldoc.createElement("imsld:learning-activity-ref");
                        lActivityRef.textContent = joinGateways.getObjFrom()[l].getName();
                        ifCond.appendChild(complete);
                        complete.appendChild(lActivityRef);

                        outerAnd.appendChild(complete);

                    }
                    if(innerAnd==null && counter==1){//simainei oti exw ena from element diladi kanw forking
                        graphToLD.createCondition(conditions[0],joinGateways.getObjFrom()[0],joinGateways.getObjGoTo()[j]);
                        //graphToLD.on_completeActivityHide(conditions[0], joinGateways.getObjFrom()[0]);
                    }
                    else{                        
                        ifCond.appendChild(outerAnd);                    
                        then = xmldoc.createElement("imsld:then");
                        show = xmldoc.createElement("imsld:show");
                        show.textContent = joinGateways.getObjGoTo()[j].getName();
                        then.appendChild(show);
                        elseCond = xmldoc.createElement("imsld:else");
                        hide = xmldoc.createElement("imsld:hide");
                        hide.textContent = joinGateways.getObjGoTo()[j].getName();
                        elseCond.appendChild(hide);
                        conditions[0].appendChild(ifCond);
                        conditions[0].appendChild(then);
                        conditions[0].appendChild(elseCond);
                    }
                    
                }            

            }
        }
    }
    catch(e){
        alert(e)
    }
}*/

function InclusiveORGatewayPointToLD(phases,xmldoc,option){
    try{
        var orGateways;
        var phase;
        var ifCond, complete,lActivityRef,then,show,elseCond,hide,outerAnd;
        var goToOb = null,objFrom=null,objRefShow = null,objRefHide = null,startPoint=null;
        var conditions = xmldoc.getElementsByTagName("imsld:conditions");
        //if(conditions==null)
            //return;
        var changeProp=null,changePropRef=null,change_value=null;
        for(var k=0;k<phases.length;k++){
            phase = phases[k];
            if(phase.getType()=="activity-structure")
                continue;
            outerAnd = null;
            for(var i=0;i<phase.getInclusiveOrGateways().length;i++){
                orGateways = phase.getInclusiveOrGateways()[i];
                
                for(var j=0;j<orGateways.getObjGoTo().length;j++){
                    goToOb = orGateways.getObjGoTo()[j].getgoToObjId();
                    if(goToOb.getPropertyExpression()==null){
                        alert("You have not set a condition to define execution path.\nThe decision point will be ignored.")
                    }
                    if(goToOb.getType()=="endPoint"){
                        if(goToOb.getPropertyExpression()!=null){

                            ifCond = xmldoc.createElement("imsld:if");                            
                            ifCond.appendChild(goToOb.getPropertyExpression().firstChild.cloneNode(true));
                            then = xmldoc.createElement("imsld:then");
                            changeProp = xmldoc.createElement("imsld:change-property-value");
                            changePropRef = xmldoc.createElement("imsld:property-ref");
                            changePropRef.setAttribute("ref","globprop-"+phase.getId());
                            changeProp.appendChild(changePropRef);

                            change_value = xmldoc.createElement("imsld:property-value");
                            change_value.textContent = "true";
                            changeProp.appendChild(change_value);
                            then.appendChild(changeProp);
                            conditions[0].appendChild(ifCond);
                            conditions[0].appendChild(then);

                            if(option=="graphicalRepresentation"){
                                changeProp.setAttribute("positionX",goToOb.getPositionX())
                                changeProp.setAttribute("positionY",goToOb.getPositionY())
                                changeProp.setAttribute("identifier",goToOb.getId())
                                ifCond.setAttribute("type","inclusiveOrgateway");
                                ifCond.setAttribute("id",orGateways.getName());
                                ifCond.setAttribute("positionX",orGateways.getShape().getAttribute("dragX"));
                                ifCond.setAttribute("positionY",orGateways.getShape().getAttribute("dragY"));
                                /*console.log(learningDesign.rootNodes)
                                if(learningDesign.rootNodes.containsActivityId(orGateways.getName())){
                                    //if(phase.getType()!="activity-structure")
                                    PhaseId = phase.getId()
                                    startPoint = graphToLD.startPoint.containsId(PhaseId)
                                    if(startPoint!=null){
                                        ifCond.setAttribute("startPositionX",startPoint.getPositionX());
                                        ifCond.setAttribute("startPositionY",startPoint.getPositionY());
                                        ifCond.setAttribute("startObjectId",PhaseId+"start");
                                    }
                                }*/

                            }
                            
                        }
                        continue;//pane sto epomeno goto, den thelw na ginoun ta apokatw
                    }
                    if(orGateways.getObjFrom().length==0){
                        if(goToOb.getPropertyExpression()!=null){
                            ifCond = xmldoc.createElement("imsld:if");
                            ifCond.appendChild(goToOb.getPropertyExpression().firstChild.cloneNode(true));

                            then = xmldoc.createElement("imsld:then");
                            show = xmldoc.createElement("imsld:show");
                            objRefShow = xmldoc.createElement("imsld:"+goToOb.getType()+"-ref");
                            objRefHide = xmldoc.createElement("imsld:"+goToOb.getType()+"-ref");

                            if(goToOb.getType()=="learning-activity"){
                                objRefShow.setAttribute("ref","la-"+goToOb.getId())
                                objRefHide.setAttribute("ref","la-"+goToOb.getId())
                            }
                            else if(goToOb.getType()=="activity-structure"){
                                objRefShow.setAttribute("ref","as-"+goToOb.getId())
                                objRefHide.setAttribute("ref","as-"+goToOb.getId())
                            }

                            if(objRefShow!=null){
                                show.appendChild(objRefShow);
                                then.appendChild(show);
                                elseCond = xmldoc.createElement("imsld:else");
                                hide = xmldoc.createElement("imsld:hide");
                                hide.appendChild(objRefHide);
                                elseCond.appendChild(hide);
                                if(option=="graphicalRepresentation"){
                                    ifCond.setAttribute("type","inclusiveOrgateway");
                                    ifCond.setAttribute("identifier",orGateways.getName());
                                    ifCond.setAttribute("positionX",orGateways.getShape().getAttribute("dragX"));
                                    ifCond.setAttribute("positionY",orGateways.getShape().getAttribute("dragY"));
                                }
                                conditions[0].appendChild(ifCond);
                                conditions[0].appendChild(then);
                                conditions[0].appendChild(elseCond);
                            }

                        }

                             console.log("ta root nodes einai "+learningDesign.rootNodes)
                                if(learningDesign.rootNodes.containsActivityId(orGateways.getName())){
                                    //if(phase.getType()!="activity-structure")
                                    PhaseId = phase.getId()
                                    startPoint = graphToLD.startPoint.containsId(PhaseId)
                                    if(startPoint!=null){
                                        ifCond.setAttribute("startPositionX",startPoint.getPositionX());
                                        ifCond.setAttribute("startPositionY",startPoint.getPositionY());
                                        ifCond.setAttribute("startObjectId",PhaseId+"start");
                                    }
                                }
                        //}

                    }
                    for(var l=0;l<orGateways.getObjFrom().length;l++){
                        objFrom = orGateways.getObjFrom()[l];
                        ifCond = xmldoc.createElement("imsld:if");
                        outerAnd = xmldoc.createElement("imsld:and");

                        complete = xmldoc.createElement("imsld:complete");
                        lActivityRef = xmldoc.createElement("imsld:"+objFrom.getType()+"-ref");
                        if(objFrom.getType()=="learning-activity")
                            lActivityRef.setAttribute("ref","la-"+objFrom.getId());
                        else if(objFrom.getType()=="activity-structure")
                            lActivityRef.setAttribute("ref","as-"+objFrom.getId());
                        
                        ifCond.appendChild(complete);
                        complete.appendChild(lActivityRef);
                       
                        //var expressionXML = parseXML(orGateways.getObjGoTo()[j].getgoToObjId().getPropertyExpression());
                        
                        if(goToOb.getPropertyExpression()!=null){
                            outerAnd.appendChild(goToOb.getPropertyExpression().firstChild.cloneNode(true));
                            outerAnd.appendChild(complete);
                            ifCond.appendChild(outerAnd);
                        }
                        else
                            ifCond.appendChild(complete);
                        
                        then = xmldoc.createElement("imsld:then");
                        show = xmldoc.createElement("imsld:show");
                        
                        
                        objRefShow = xmldoc.createElement("imsld:"+goToOb.getType()+"-ref");
                        objRefHide = xmldoc.createElement("imsld:"+goToOb.getType()+"-ref")
                        if(goToOb.getType()=="learning-activity"){
                            objRefShow.setAttribute("ref","la-"+goToOb.getId())                            
                            objRefHide.setAttribute("ref","la-"+goToOb.getId())
                        }
                        else if(goToOb.getType()=="activity-structure"){                            
                            objRefShow.setAttribute("ref","as-"+goToOb.getId())                            
                            objRefHide.setAttribute("ref","as-"+goToOb.getId())
                        }
                        
                        if(objRefShow!=null && objRefHide!=null){
                            show.appendChild(objRefShow);
                            then.appendChild(show);
                            elseCond = xmldoc.createElement("imsld:else");
                            hide = xmldoc.createElement("imsld:hide");
                            hide.appendChild(objRefHide);
                            elseCond.appendChild(hide);
                            if(option=="graphicalRepresentation"){
                                ifCond.setAttribute("type","inclusiveOrgateway");
                                ifCond.setAttribute("identifier",orGateways.getName());
                                ifCond.setAttribute("positionX",orGateways.getShape().getAttribute("dragX"));
                                ifCond.setAttribute("positionY",orGateways.getShape().getAttribute("dragY"));
                            }
                            conditions[0].appendChild(ifCond);
                            conditions[0].appendChild(then);
                            conditions[0].appendChild(elseCond);
                        }
                    }                    
                }

            }
        }
    }
    catch(e){
        console.log("inclusiveOrgateway "+e)
    }
}

function createLomMetadata(xmldoc){
    try{
        var metadata = xmldoc.createElementNS("http://www.imsglobal.org/xsd/imscp_v1p1","metadata");
        var lomMetadata = xmldoc.createElement("lm:lom");
        //lomMetadata.setAttribute("xmlns","http://www.imsglobal.org/xsd/imsmd_v1p2");
        
        var general = xmldoc.createElement("lm:general");
        var generalTitle = xmldoc.createElement("lm:title");
        var generalTitleString = xmldoc.createElement("lm:langstring");

        generalTitleString.textContent = $("#lesson_plan_title").val();
        generalTitle.appendChild(generalTitleString);
        general.appendChild(generalTitle);

        var description = xmldoc.createElement("lm:description");
        var descriptionString = xmldoc.createElement("lm:langstring");
        
        descriptionString.textContent = $("#lesson_subject").val();
        description.appendChild(descriptionString);
        general.appendChild(description);

        var lifecycle = xmldoc.createElement("lm:lifecycle");
        var contribute = xmldoc.createElement("lm:contribute");
        var contributeRole = xmldoc.createElement("lm:role");
        var contributeSource = xmldoc.createElement("lm:source");
        contributeSource.appendChild(xmldoc.createElement("lm:langstring"));
        var contributeValue = xmldoc.createElement("lm:value");
        var contributeValueString = xmldoc.createElement("lm:langstring");
        contributeValue.appendChild(contributeValueString);
        
        contributeValueString.textContent = $("#course_writer").val();
        contributeRole.appendChild(contributeSource);
        contributeRole.appendChild(contributeValue);
        contribute.appendChild(contributeRole);
        var contributeDate = xmldoc.createElement("lm:date");
        var dateTime = xmldoc.createElement("lm:datetime");
        dateTime.textContent = $("#datepicker").val();
        contributeDate.appendChild(dateTime);
        contribute.appendChild(contributeDate);
        lifecycle.appendChild(contribute);

        var educational = xmldoc.createElement("lm:educational");
        var educationalDescription = xmldoc.createElement("lm:description");
        var educationalDescriptionString = xmldoc.createElement("lm:langstring");
        
        educationalDescriptionString.textContent = $("#General_Goals").val();
        educationalDescription.appendChild(educationalDescriptionString);
        educational.appendChild(educationalDescription);

        lomMetadata.appendChild(general);
        if($("#course_writer").val()!="")
            lomMetadata.appendChild(lifecycle);
        if($("#General_Goals").val()!="")
            lomMetadata.appendChild(educational);
        metadata.appendChild(lomMetadata);
        

        return metadata;
    }catch(e){
        alert("error while creating lom metadata"+e);
    }
}

/*function traverse(tree) {
        alert(tree.documentElement.childNodes[0]);
        
        alert(tree.documentElement.childNodes[0].firstChild);
    }*/
var getRef = {
    findRolePartRef:function(act,roleRef,activity){
        try{
            var role_parts = act.getElementsByTagName("imsld:role-part");
            var role_part=null,activityRef="",type="learning-activity-ref";
            if(activity.getType()=="activity-structure"){
                activityRef = "as-"+activity.getIdentifier();//activityRef = "as-"+activity.getIdentifier()+"-"+activity.getName();
                type = "activity-structure-ref";
            }
            else
                activityRef = "la-"+activity.getIdentifier();//activityRef = "la-"+activity.getIdentifier()+"-"+activity.getName();

            for(var i=0;i<role_parts.length;i++){
                role_part = role_parts[i];
                if(role_part.getElementsByTagName("imsld:role-ref")[0]!=null && role_part.getElementsByTagName("imsld:"+type)[0]!=null)
                if(role_part.getElementsByTagName("imsld:role-ref")[0].getAttribute("ref")==roleRef && role_part.getElementsByTagName("imsld:"+type)[0].getAttribute("ref")==activityRef)
                    return role_part.getAttribute("identifier");
            }
            return null;
        }
        catch(e){
            alert("error while searching activity ref "+e)
        }
    }
}
var getElem = {
    getElementById: function(element,id){
        
        for(var i=0;i<element.childNodes.length;i++){
            if(element.childNodes[i].getAttribute("identifier")==id)
                return element.childNodes[i];
        }
        return null;
    }
}

Array.prototype.containsActivityId = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] == obj) {
            return true;
        }
    }
    return false;
}

 function endPoints(id,activityId,positionX,positionY,activityType){
        this.activityEndId = activityId;
        this.endPointId = id;
        this.activityType = activityType;
        this.positionX = positionX;
        this.positionY = positionY;
        this.getPositionX = function(){
            return this.positionX
        }
        this.getPositionY = function(){
            return this.positionY
        }
        this.getActivityId = function(){
            return this.activityEndId;
        }
        this.getEndPointId = function(){
            return this.endPointId;
        }
        this.getActivityType = function(){
            return this.activityType;
        }
    }
    function startPoint(id,positionX,positionY){
        this.id = id;
        this.positionX = positionX;
        this.positionY = positionY;
        this.getPositionX = function(){
            return this.positionX
        }
        this.getPositionY = function(){
            return this.positionY
        }
        this.getId = function(){
            return this.id;
        }

    }