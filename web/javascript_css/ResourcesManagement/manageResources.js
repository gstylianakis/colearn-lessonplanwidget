/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var Resources = {
    resourcesXmlTmp: null,
    resourcesRoot: null,
    init: function(option,xmldoc){
        if(option!=0){
            Resources.resourcesXmlTmp = createXMLDocument("resources");
            Resources.resourcesRoot = Resources.resourcesXmlTmp.getElementsByTagName("resources")[0];
        }
        else{
            Resources.resourcesXmlTmp = xmldoc;
            Resources.resourcesRoot = xmldoc.createElementNS("http://www.imsglobal.org/xsd/imscp_v1p1","resources");
        }
        return Resources.resourcesRoot;
    },
    createResource: function(item,option,typeOfResource){
       
        var res = Resources.resourcesXmlTmp.createElementNS("http://www.imsglobal.org/xsd/imscp_v1p1","resource");
        
        if(option!=0){            
            var cont = Resources.resourcesXmlTmp.createElement("content");
            if(typeOfResource!="URL"){

                cont.setAttribute("identifier","resource-"+item.getIdentifier());


                if(typeOfResource=="ExistingFile")
                    cont.textContent = item.getTypeOfContent().substr(12)+"-"+item.getContent();
                
                else{
                    cont.setAttribute("type","text");
                    cont.textContent = item.getContent();
                }
                                   
                Resources.resourcesRoot.appendChild(res);
                res.appendChild(cont);
            }
        }
        else{
            Resources.resourcesRoot.appendChild(res);
            res.setAttribute("identifier","resource-"+item.getIdentifier());
            
            if(typeOfResource=="URL"){
                res.setAttribute("type","webcontent");
                res.setAttribute("href",item.getContent());
            }
            else{
                var file = Resources.resourcesXmlTmp.createElementNS("http://www.imsglobal.org/xsd/imscp_v1p1","file");
                res.setAttribute("type","file");
                res.appendChild(file);
                if(typeOfResource=="ExistingFile"){
                    res.setAttribute("href",item.getTypeOfContent().substr(12)+"-"+item.getContent());
                    file.setAttribute("href",item.getTypeOfContent().substr(12)+"-"+item.getContent());
                }
                else{
                    res.setAttribute("type","text");
                    res.setAttribute("href","resource-"+item.getIdentifier()+".html");
                    file.setAttribute("href","resource-"+item.getIdentifier()+".html");
                }
            }
        }
    },
    //edw kanw generate resources mono gia ta activities...Prepei na ginei kai gia ta phases kai gia ta activities structure
    productResourcesFromObjects: function(phases,option){
        try{
            var descItem,learningObjectivesItem,prerequisitesItem,completionFeedbackItem=null;
            var learningObjectItem = null;
            var information = null;
            var act_completionFeedbackItem=null;

            for(var i=0;i<phases.getPhases().length;i++){
                var activities = phases.getPhases()[i].getLearningActivitiesArray().getActivities();
                var activityStructures = phases.getPhases()[i].getActivitiesStructuresArray();
                if(phases.getPhases()[i].getType()!="activity-structure"){
                    act_completionFeedbackItem = phases.getPhases()[i].getFeedback();
                    for(var k=0;k<act_completionFeedbackItem.length;k++){
                        if(act_completionFeedbackItem[k].getTypeOfContent()=="URLPhaseDescription")
                            Resources.createResource(act_completionFeedbackItem[k],option,"URL");
                        else if(act_completionFeedbackItem[k].getTypeOfContent().indexOf("ExistingFile")!=-1)
                            Resources.createResource(act_completionFeedbackItem[k],option,"ExistingFile");
                        else
                            Resources.createResource(act_completionFeedbackItem[k],option,"");

                    }
                }
                for(var j=0;j<activities.length;j++){
                    descItem = activities[j].getDescriptionItem();
                    learningObjectivesItem = activities[j].getLearningObjectivesItems();
                    prerequisitesItem = activities[j].getPrerequisitesItems();
                    completionFeedbackItem = activities[j].getActivityCompletionFeedbackItem();                    
                    learningObjectItem = activities[j].getEnvironement().getLearningObjects();

                    for(k=0;k<descItem.length;k++){                        
                        if(descItem[k].getTypeOfContent()=="URLDescription")
                            Resources.createResource(descItem[k],option,"URL");
                        else if(descItem[k].getTypeOfContent().indexOf("ExistingFile")!=-1)
                            Resources.createResource(descItem[k],option,"ExistingFile");
                        else//if(descItem[k].getTypeOfContent()=="createNewDescription")
                            Resources.createResource(descItem[k],option,"");

                    }
                    for(k=0;k<learningObjectivesItem.length;k++){                        
                        if(learningObjectivesItem[k].getTypeOfContent()=="URLactivityLearningObjectives")
                            Resources.createResource(learningObjectivesItem[k],option,"URL");
                        else if(learningObjectivesItem[k].getTypeOfContent().indexOf("ExistingFile")!=-1)
                            Resources.createResource(learningObjectivesItem[k],option,"ExistingFile");
                        else//(learningObjectivesItem[k].getTypeOfContent()=="createNewactivityLearningObjectives")
                            Resources.createResource(learningObjectivesItem[k],option,"");

                    }
                    for(k=0;k<prerequisitesItem.length;k++){                        
                        if(prerequisitesItem[k].getTypeOfContent()=="URLPrerequisites")
                            Resources.createResource(prerequisitesItem[k],option,"URL");
                        
                        else if(prerequisitesItem[k].getTypeOfContent().indexOf("ExistingFile")!=-1)
                            Resources.createResource(prerequisitesItem[k],option,"ExistingFile");
                        else//if(prerequisitesItem[k].getTypeOfContent()=="createNewPrerequisites")
                            Resources.createResource(prerequisitesItem[k],option,"");
                    }
                    for(k=0;k<completionFeedbackItem.length;k++){                        
                        if(completionFeedbackItem[k].getTypeOfContent()=="URLactivityCompletion")
                            Resources.createResource(completionFeedbackItem[k],option,"URL");

                        else if(completionFeedbackItem[k].getTypeOfContent().indexOf("ExistingFile")!=-1)
                            Resources.createResource(completionFeedbackItem[k],option,"ExistingFile");

                        else//if(completionFeedbackItem[k].getTypeOfContent()=="createNewactivityCompletion")
                            Resources.createResource(completionFeedbackItem[k],option,"");
                    }
                    for(k=0;k<learningObjectItem.length;k++){
                        if(learningObjectItem[k].getTypeOfContent()=="URLLearningObject")
                            Resources.createResource(learningObjectItem[k],option,"URL");

                        else if(learningObjectItem[k].getTypeOfContent().indexOf("ExistingFile")!=-1)
                            Resources.createResource(learningObjectItem[k],option,"ExistingFile");

                        else//if(completionFeedbackItem[k].getTypeOfContent()=="createNewactivityCompletion")
                            Resources.createResource(learningObjectItem[k],option,"");
                    }
                }
                for(j=0;j<activityStructures.length;j++){
                    learningObjectItem = activityStructures[j].getEnvironement().getLearningObjects();
                    information = activityStructures[j].getInformation();
                    for(k=0;k<information.length;k++){                        
                        if(information[k].getTypeOfContent()=="URLstructureInformation")
                            Resources.createResource(information[k],option,"URL");

                        else if(information[k].getTypeOfContent().indexOf("ExistingFile")!=-1)
                            Resources.createResource(information[k],option,"ExistingFile");

                        else
                            Resources.createResource(information[k],option,"");

                    }
                    for(k=0;k<learningObjectItem.length;k++){
                        if(learningObjectItem[k].getTypeOfContent()=="URLstructureLearningObject")
                            Resources.createResource(learningObjectItem[k],option,"URL");

                        else if(learningObjectItem[k].getTypeOfContent().indexOf("ExistingFile")!=-1)
                            Resources.createResource(learningObjectItem[k],option,"ExistingFile");

                        else//if(completionFeedbackItem[k].getTypeOfContent()=="createNewactivityCompletion")
                            Resources.createResource(learningObjectItem[k],option,"");
                    }
                }
            }
        }
        catch(e){
            console.log("manageResources "+e)
        }
    }
}