/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package IntegrationServices;

/**
 *
 * @author Administrator
 */
public abstract class Adapter {
    private String type;
    public String getType(){
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
    
    public abstract Adapter getAdapter();
    public abstract void triggerDispatcher()throws Exception;
}
