/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


 function deleteCondition(view){
        try{
           
            if(view=="conditionView"){
                document.getElementById("ConditionForPhaseCompletion").disabled = false;
                document.getElementById("addCondition").disabled = false;                                
                document.getElementById("conditionView").innerHTML = "";
                return;
            }
            
            var countDivs = 0;
            var select = null;
            var parent = view.parentNode;
            //alert(view.parentNode.childNodes.length);
            for(var i=0;i<parent.childNodes.length;i++){
                if(select==null && parent.childNodes[i].nodeName=="SELECT" && parent.childNodes[i].disabled)
                    select = parent.childNodes[i];                                    
                if(parent.childNodes[i].nodeName=="DIV")
                    countDivs++;
            }//exei ena paidi pou pame na diagrapsoume kai to selection
            if(countDivs==1 && select!=null)
                select.disabled = false;                                          
            
            parent.removeChild(view);
        }
        catch(e){
            console.log("delete condition "+e);
        }
    }
    function addIsCondition(id,labelValue,conditionView){

        var selection = document.createElement("select");
        selection.id = id;

        var option = document.createElement("option");
        option.value = "IsMemberOfRole";
        option.innerHTML = "is";
        selection.appendChild(option);

        option = document.createElement("option");
        option.value = "IsNotMemberOfRole";
        option.innerHTML = "is Not";
        selection.appendChild(option);
        conditionView.appendChild(selection);

        var label = document.createElement("label");
        label.style.marginLeft = "5px";
        label.innerHTML = labelValue;
        conditionView.appendChild(label);
    }
    function addOptions(selection,optionValue,optionInner,div,opt){
        var option = document.createElement("option");        
        option.value = optionValue;
        option.innerHTML = optionInner;
        //selection.style.zIndex = "10";
        
        if(opt!=2){
            
            option.onclick = function(){document.getElementById("usersInRoleSelection").disabled = true;var child = addDiv(div);showAppropriateExpressionForCondition(this.value,child)};
            
        }
        selection.appendChild(option);

    }
    function showAppropriateExpressionForCondition(option,view){
        try{

            var conditionView ;
            var activities;
            var label;
           
            if(view=="conditionView"){
                conditionView = document.getElementById(view);
                //conditionView.style.zIndex = 1;
                document.getElementById("ConditionForPhaseCompletion").disabled = true;
                document.getElementById("addCondition").disabled = true;
                var p = document.createElement("p");
                p.innerHTML = "Evaluate the following Expression for the Condition"
                conditionView.appendChild(p);
            }
            else{
                conditionView = view;               
            }
            
            var deleteImg = document.createElement("img");
            deleteImg.setAttribute("src","javascript_css/project_team_form/images/delete_button.png");
            deleteImg.style.border="0px";
            deleteImg.align = "top";
            deleteImg.style.position = "relative";
            deleteImg.style.zIndex=1;
            deleteImg.style.cssFloat = "right";
            deleteImg.onclick = function(){deleteCondition(view)};
            //deleteImg.setAttribute("onclick","deleteCondition('"+view+"')");
            conditionView.appendChild(deleteImg);

            var selection;
           
            if(option=="memberOfRole"){
                addIsCondition("memberOfRoleIsCondition","A member of Role",conditionView);

                selection = document.createElement("select");
                selection.id = "memberOfRoleSelection";
                selection.style.marginLeft = "5px";
                selection.style.zIndex = 1;
                selection.onclick = function(){showRoles(this)};
                conditionView.appendChild(selection);
            }
            else if(option=="usersInRole"){
                addIsCondition("usersInRoleIsCondition","Users in Role",conditionView);
                selection = document.createElement("select");
                selection.id = "usersInRoleSelection";
                selection.style.marginLeft = "5px";
                
                addOptions(selection,"selectExpression","Select from the following",conditionView,1);
                addOptions(selection,"propertyHasNoValue","Property has no value",conditionView,1);
                addOptions(selection,"timeAnActivityStarted","Time an Activity started",conditionView,1);
                addOptions(selection,"completed","Completed",conditionView,1);
                addOptions(selection,"all","All of the following are true",conditionView,1);
                addOptions(selection,"any","Any of the following are true",conditionView,1);
                addOptions(selection,"none","None of the following are true",conditionView,1);
                /*addOptions(selection,"Is Equal","isEqual");
                addOptions(selection,"Is not Equal","isNotEqual");
                */
                
                conditionView.appendChild(selection);


            }
            else if(option=="propertyHasNoValue"){
                
                addIsCondition("propertyHasNoValueCondition","Property has no value",conditionView);
                showProperties("propertyHasNoValueCondition");
                
            }
            else if(option=="timeAnActivityStarted"){
                 addIsCondition("timeAnActivityStartedView","Time an Activity started",conditionView);
                 selection = document.createElement("select");
                 selection.id = "timeAnActivityStartedSelection";
                 selection.style.marginLeft = "5px";
                 activities = showAllActivities();
                 
                 for(var i=0;i<activities.length;i++)
                   addOptions(selection,activities[i].getName(),activities[i].getName(),conditionView,2);
                 if(activities.length==0){
                     selection.disabled = true;
                     addOptions(selection,"noActivities","no Activities to select",conditionView,2);
                 }
                 conditionView.appendChild(selection);
            }
            else if(option=="completed"){
                 addIsCondition("completedView","Completed",conditionView);
                 selection = document.createElement("select");
                 selection.id = "completedSelection";
                 selection.style.marginLeft = "5px";
                 activities = showAllActivities();

                 for(var i=0;i<activities.length;i++)
                   addOptions(selection,activities[i].getName(),activities[i].getName(),conditionView,2);
                 if(activities.length==0){
                     selection.disabled = true;
                     addOptions(selection,"noActivities","no Activities to select",conditionView,2);
                 }
                 conditionView.appendChild(selection);
                
            }
            else if(option=="all"){
                 label = document.createElement("label");
                 label.innerHTML = "All of the following are true";
                 
                 var addSelectionDiv = document.createElement("div");
                 addSelectionDiv.style.position = "relative";
                 addSelectionDiv.style.marginLeft = "10px";
                 addSelectionDiv.style.height = "auto";
                 
                 var add = document.createElement("a");
                 add.href = "#";
                 add.title="Needs a minimum of two elements";
                 
                 add.onclick = function(event){mopen(addSelectionDiv,conditionView);event.stopPropagation()};
                 
                 var img = document.createElement("img");
                 img.setAttribute("src","javascript_css/project_team_form/images/buttons2.png");                 
                 img.className="add_button";

                 add.onclick = function(event){mopen(addSelectionDiv,conditionView);event.stopPropagation()};
                 add.appendChild(img);
                 
                 addSelectionDiv.appendChild(label);
                 addSelectionDiv.appendChild(add);
                 
                 conditionView.appendChild(addSelectionDiv);
                 

                
            }
             else if(option=="any"){

                 label = document.createElement("label");
                 label.innerHTML = "Any of the following are true";

                 addSelectionDiv = document.createElement("div");
                 addSelectionDiv.style.position = "relative";
                 addSelectionDiv.style.marginLeft = "10px";
                 addSelectionDiv.style.height = "auto";
                 //addSelectionDiv.style.zIndex = 100;
                 add = document.createElement("a");
                 add.href = "#";
                 add.title="Needs a minimum of two elements";
                 add.className = "add_button";
                 add.onclick = function(event){mopen(addSelectionDiv,conditionView);event.stopPropagation()};
                 img = document.createElement("img");
                 img.setAttribute("src","javascript_css/project_team_form/images/buttons2.png");
                 img.className="add_button";

                 add.appendChild(img);
                 addSelectionDiv.appendChild(label);
                 addSelectionDiv.appendChild(add);

                 conditionView.appendChild(addSelectionDiv);

            }
             else if(option=="none"){

                 label = document.createElement("label");
                 label.innerHTML = "None of the following are true";

                 addSelectionDiv = document.createElement("div");
                 addSelectionDiv.style.position = "relative";
                 addSelectionDiv.style.marginLeft = "10px";
                 addSelectionDiv.style.height = "auto";
                 //addSelectionDiv.style.zIndex = 100;
                 add = document.createElement("a");
                 add.href = "#";
                 add.title="Needs a minimum of two elements";
                 add.className = "add_button";
                 add.onclick = function(event){mopen(addSelectionDiv,conditionView);event.stopPropagation()};
                 img = document.createElement("img");
                 img.setAttribute("src","javascript_css/project_team_form/images/buttons2.png");
                 img.className="add_button";
                 add.appendChild(img);

                 addSelectionDiv.appendChild(label);
                 addSelectionDiv.appendChild(add);

                 conditionView.appendChild(addSelectionDiv);
                 
            }
             
            
            

        }
        catch(e){
            alert(e);
        }
    }

    function addDiv(conditionView){
        try{
            var div = document.createElement("div");
            div.style.marginLeft = "10px";
            div.style.cssFloat = "none";
            div.style.position = "relative";
            
            div.style.zIndex=1;
            conditionView.appendChild(div);
            return div;
        }
        catch(e){
            console.log("addDiv "+e);
        }
    }
var clicks = 0;
function mopen(parentDiv,conditionView)
{
    try{
        
        if(document.getElementById("AllDropDownSelection")!=null)
            return;
	var div = document.createElement("div");
        div.id = "AllDropDownSelection";
        
        div.className = "SelectionDropDown";
        
        var anchor;
        anchor = document.createElement("a");
        anchor.href="#";
        anchor.innerHTML = "A member of a Role";
        anchor.value = "memberOfRole";
        anchor.onclick = function(){var child=addDiv(conditionView);showAppropriateExpressionForCondition(this.value,child)};
        div.appendChild(anchor);

        anchor = document.createElement("a");
        anchor.href="#";
        anchor.innerHTML = "Users in Role";
        anchor.value = "usersInRole";
        anchor.onclick = function(){var child=addDiv(conditionView);showAppropriateExpressionForCondition(this.value,child)};
        div.appendChild(anchor);

        anchor = document.createElement("a");
        anchor.href="#";
        anchor.innerHTML = "Property has no value";
        anchor.value = "propertyHasNoValue";
        anchor.onclick = function(){var child=addDiv(conditionView);showAppropriateExpressionForCondition(this.value,child)};
        div.appendChild(anchor);

        anchor = document.createElement("a");
        anchor.href="#";
        anchor.innerHTML = "Time an Activity started";
        anchor.value = "timeAnActivityStarted";
        anchor.onclick = function(){var child=addDiv(conditionView);showAppropriateExpressionForCondition(this.value,child)};
        div.appendChild(anchor);

        anchor = document.createElement("a");
        anchor.href="#";
        anchor.innerHTML = "Completed";
        anchor.value = "completed";
        anchor.onclick = function(){var child=addDiv(conditionView);showAppropriateExpressionForCondition(this.value,child)};
        div.appendChild(anchor);

        anchor = document.createElement("a");
        anchor.href="#";
        anchor.innerHTML = "All of the following are true";
        anchor.value = "all";
        anchor.onclick = function(){var child=addDiv(conditionView);showAppropriateExpressionForCondition(this.value,child)};
        div.appendChild(anchor);

        anchor = document.createElement("a");
        anchor.href="#";
        anchor.innerHTML = "Any of the following are true";
        anchor.value = "any";
        anchor.onclick = function(){var child=addDiv(conditionView);showAppropriateExpressionForCondition(this.value,child)};
        div.appendChild(anchor);

        anchor = document.createElement("a");
        anchor.href="#";
        anchor.innerHTML = "None of the following are true";
        anchor.value = "none";
        anchor.onclick = function(){var child=addDiv(conditionView);showAppropriateExpressionForCondition(this.value,child)};
        div.appendChild(anchor);

        document.onclick = function(event){                        
            if(event.target.id!=div.id){
                mclose(document.getElementById("AllDropDownSelection"));
                
            }

        }
        parentDiv.appendChild(div);
        
        //
        
    }
    catch(e){
        alert(e);
    }

}
// close showed layer
function mclose(ddmenuitem)
{
    
	if(ddmenuitem){
            var parent = ddmenuitem.parentNode;
            parent.removeChild(ddmenuitem);
        }
}


