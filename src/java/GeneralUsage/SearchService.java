package GeneralUsage;

import StudentOperationsEJBs.DB_connection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 *
 * @author manolis
 */
public class SearchService {

    private static String convertStreamToString(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            {
                Reader reader = null;
                try {
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    int n;
                    while ((n = reader.read(buffer)) != -1) {
                        writer.write(buffer, 0, n);
                    }
                } catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                } finally {
                    is.close();
                    try {
                        reader.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    public String getResults(String qTerms, int page) {
        String results = "";
        try {

            String currURL = "http://147.27.41.18:8081/esearch/esearch.jsp?";

            if (qTerms != null && qTerms.length() > 0) {
                currURL += "&term=" + new URLCoding().encodeURL(qTerms);
            } else {
                return "<p>Found " + 0 + " results.  (" + 0 + " seconds) </p>";
            }
            if (page > 0) {
                currURL += "&page=" + page;
            }
            URL url = new URL(currURL);
            HttpURLConnection http = null;
            long s = 0;
            http = null;
            s = System.currentTimeMillis();
            http = (HttpURLConnection) url.openConnection();
            http.setRequestProperty(currURL, currURL);
            http.setRequestProperty("Accept-Charset", "UTF-8");
            float f = System.currentTimeMillis() - s;

            results = convertStreamToString(http.getInputStream());

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            return results;
        }
    }

    public ArrayList getProjects(String whereClause)throws Exception{
        DB_connection connect = new DB_connection();
        String result = "";
        Statement stmt = null;
        ArrayList projects = new ArrayList();
        try{
            stmt = connect.getConnection().createStatement();
            String query = "SELECT name,lessonSubject,manifestTitle,author FROM lessonplan_widget.manifestfiles " + whereClause;
          
            ResultSet rs = stmt.executeQuery(query);
            int size  =0;
            Hashtable rows = new Hashtable();
            String desc = "";
            while (rs.next()) {
                size++;
                if(rs.getString(2).length()>20)
                    desc = rs.getString(2).substring(0,20)+"..";
                else
                    desc = rs.getString(2);
                desc = desc.replaceAll("'","\\\\'");
                                
                result = "<tr><td width=\"150px\" style=\"cursor:pointer\" data-desc=\""+rs.getString(1)+"\">"+rs.getString(3)+"</td><td width=\"150px\"style=\"padding-left:20px\">"+desc+"</td><td style=\"padding-left:80px\">"+rs.getString(4)+"</td></tr>";
                rows.put(rs.getString(1), result);
            }

            projects.add(String.valueOf(size));
            projects.add(rows);
            return projects;
        }        
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connect.close();
        }
    }
    public HashMap vectorSpaceModel(String queryKeywors,ArrayList queryTerms)throws Exception{//term count model...
        DB_connection connect = new DB_connection();
        Statement stmt = null;
        Statement stmt2 = null;
        HashMap results = new HashMap();
        try{            
            stmt = connect.getConnection().createStatement();
            stmt2 = connect.getConnection().createStatement();
            String query = "SELECT lessonSubject,name FROM lessonplan_widget.manifestfiles ";

            ResultSet rs2 = stmt2.executeQuery(query);
            StringTokenizer st = null;

            while (rs2.next()) {
                st = new StringTokenizer(rs2.getString(1));
                while (st.hasMoreTokens()) {
                    queryTerms.add(st.nextToken());
                }
            }

            int count=0,count2=0,numerator=0,denominator=0,denominator2=0;
            String document="";
            Pattern p ;
            Matcher m,m2 ;
            String term = "";
            String manifestId = "";
            double similarity = 0;
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                document = rs.getString(1);
                manifestId = rs.getString(2);
                numerator = 0;
                denominator = 0;
                denominator2 = 0;
                for(int i=0;i<queryTerms.size();i++){
                    term = (String)queryTerms.get(i);
                    p = Pattern.compile(term);
                    m = p.matcher(document);
                    m2 = p.matcher(queryKeywors);
                    count = 0;
                    count2 = 0;
                    while (m.find()){
                        count +=1;
                    }
                    while (m2.find()){
                        count2 +=1;
                    }
                    numerator += count*count2;
                    denominator += (count*count);
                    denominator2 += (count2*count2);
                }
     
                if(denominator==0 || denominator2==0)
                    similarity = 0;
                else
                    similarity = (numerator/(Math.sqrt(denominator)*Math.sqrt(denominator2)));
         
                results.put(manifestId,similarity);

            }
            return results;
        }
        catch(Exception e){
            throw e;
        } finally{
            stmt.close();
            stmt2.close();
            connect.close();            
        }
    }

    public static LinkedHashMap sortHashMapByValues(HashMap passedMap, boolean ascending) {
        List mapKeys = new ArrayList(passedMap.keySet());
        List mapValues = new ArrayList(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);
        if (!ascending)
            Collections.reverse(mapValues);

        LinkedHashMap someMap = new LinkedHashMap();
        Iterator valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Object val = valueIt.next();
            Iterator keyIt = mapKeys.iterator();
            while (keyIt.hasNext()) {
                Object key = keyIt.next();
                if (passedMap.get(key).toString().equals(val.toString())) {
                    passedMap.remove(key);
                    mapKeys.remove(key);
                    someMap.put(key, val);
                    break;
                }
            }
        }
        return someMap;
    }
}
