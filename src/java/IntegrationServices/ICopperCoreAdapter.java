/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package IntegrationServices;


import org.coppercore.dto.PublicationResult;
import org.coppercore.dto.RunDto;
/**
 *
 * @author Administrator
 */
public interface ICopperCoreAdapter {
    public PublicationResult publishUOL(String uolLocation,String schemasLocation,String uriOffset,String webroot) throws Exception;
    public void addUserToRole(String userId, int roleId)throws Exception;
    public void addUserToRun(String userId, int runId)throws Exception;
    public void changeRun(RunDto runDto)throws Exception;
    public int createRole(int runId, String roleId)throws Exception;//Creates a new role instance for the specified role.
    public int createRun(RunDto runDto)throws Exception;//Creates a new run using the parameters specified in runDto.
    public void createUser(java.lang.String userId)throws Exception;// Adds a new user to CopperCore.
    public String getActiveLDRole(String userId, int runId)throws Exception;// Returns the Learning Design identifier of the active role of the specified user in the specified run.
    /*public int getActiveRole(String userId, int runId);// Returns the id of the active role of the specified user in the specified run.
    public String getContentUri(int uolId);// Returns the content uri of the specified unit of learning.
    public String getProperty(int uolId, String propId, String ownerId, int runId);// Returns the value of the specified property.
    public String getRolesTree(int runId);//Returns the xml tree for the specified run which contains all roles and role instances for that run.
    public RunDto getRun(int runId);//Returns the data of the run.
    public UolDto getUol(int uolId);//  Returns the data of the uol.
    public String getUser(String userId);// Returns the specified user.
    public String getUserRoles(String userId, int runId);//Retrieves an xml representation of the hierarchy with all the roles where the user is assigned to within the context of the specified run.
    public String getVersion();//Returns the version of the deployed CopperCore engine.
    public int importUol(byte[] manifest, String contentUri);//Imports the ims-ld manifest into the ims-ld engine.
    public PublicationResult importUol(byte[] manifest, String contentUri, org.coppercore.common.MessageList logger);
    public Collection listRuns(int uolId);
    public Collection listRunsForUser(String userId);
    public Collection listRunsForUser(String userId, int uolId);
    public Collection listUols();
    public Collection listUolsForUser(String userId);
    public Collection listUsers();
    public Collection listUsersInRun(int runId);
    public void	removeRun(int runId);
    public void removeUol(int uolId);
    public void	removeUser(String userId);
    public void removeUserFromRole(String userId, int roleId);
    public void	removeUserFromRun(String userId, int runId);
    public void setActiveRole(String userId, int runId, int roleId);          
    public void setContentUri(int uolId, String contentUri);
    public void	setProperty(int uolId, String propId, String ownerId, int runId, String value);
    public boolean validate(byte[] manifest, org.coppercore.common.MessageList logger);
    public boolean validateRoles(int runId);
         */
}
