/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package learningDesignExport;

/**
 *
 * @author Administrator
 */
public class Role {
    private String type;
    private String name;
    private int minNum;
    private int maxNum;
    private boolean personExclusiveToOneRole;

    public Role(String type,String name, int minNum, int maxNum, boolean constraint){
        this.type = type;
        this.name = name;
        this.minNum = minNum;
        this.maxNum = maxNum;
        this.personExclusiveToOneRole = constraint;
    }

    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(int maxNum) {
        this.maxNum = maxNum;
    }

    public int getMinNum() {
        return minNum;
    }

    public void setMinNum(int minNum) {
        this.minNum = minNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPersonExclusiveToOneRole() {
        return personExclusiveToOneRole;
    }

    public void setPersonExclusiveToOneRole(boolean personExclusiveToOneRole) {
        this.personExclusiveToOneRole = personExclusiveToOneRole;
    }

    

}
