<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<%@page  contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.File"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Properties"%>
<%
    Properties configFile = new Properties();
    configFile.load( new FileInputStream("C:/Users/stylianos/Documents/NetBeansProjects/LessonPlanWidgetApp/build/web/WEB-INF/classes/GeneralUsage/config.properties"));//periexei tin pliroforia gia to directory pu tha swzontai ta unit of learnings zip arxeia
    
    String dirName = configFile.getProperty("uploadFileDir");

    String filename = request.getParameter("filename");
    String manifestId = request.getParameter("manifestId");
    String filepath=dirName+"/"+manifestId+"/";
//psaxnei to unzip arxeio..........
//an svinontai telika ta unzip arxeia apo ton server tha prepei na psaxeni to zip arxeio....

    BufferedInputStream buf=null;
    ServletOutputStream myOut=null;
    try{
        myOut = response.getOutputStream( );
        File myfile = new File(filepath+filename);
        //set response headers
        response.setContentType("text/plain");
        response.addHeader("Content-Disposition","attachment; filename="+filename );

        response.setContentLength( (int) myfile.length( ) );

        FileInputStream input = new FileInputStream(myfile);
        buf = new BufferedInputStream(input);
        int readBytes = 0;
        //read from the file; write to the ServletOutputStream
        while((readBytes = buf.read( )) != -1)
            myOut.write(readBytes);
    } catch (IOException ioe){
        throw new ServletException(ioe.getMessage( ));
     } finally {
     //close the input/output streams
         if (myOut != null)
             myOut.close( );
          if (buf != null)
          buf.close( );
     }
%>