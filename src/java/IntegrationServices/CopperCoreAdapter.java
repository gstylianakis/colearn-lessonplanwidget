/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package IntegrationServices;

import org.coppercore.delegate.LDCourseManagerDelegate;
import org.coppercore.dto.PublicationResult;
import org.coppercore.dto.RunDto;


/**
 *
 * @author Administrator
 */
public class CopperCoreAdapter extends LDAdapter implements ICopperCoreAdapter{

    private static LDCourseManagerDelegate managerDelegate;
    

    public PublicationResult publishUOL(String UOLLocation,String schemaLocation,String contextUriOffset,String webRoot) throws Exception{
        try{
         return managerDelegate.publishUol(UOLLocation,schemaLocation, contextUriOffset,webRoot);
        }
        catch(Exception e){
            throw e;
        }
    }
    public CopperCoreAdapter() {
        try{
            managerDelegate = new LDCourseManagerDelegate();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public Adapter getAdapter() {
        return this;
    }

    @Override
    public void triggerDispatcher() throws Exception {
        ;
    }

    public void addUserToRole(String userId, int roleId)throws Exception{
        try{
            managerDelegate.addUserToRole(userId, roleId);
        }
        catch(Exception e){
            throw new Exception("error when trying to add user to specific role\n"+e.toString());
        }
    }

    public void addUserToRun(String userId, int runId) throws Exception {
        try{
            managerDelegate.addUserToRole(userId, runId);
        }
        catch(Exception e){
            throw new Exception("error when inserting user in Learning Design Run\n"+e.toString());
        }
    }

    public void changeRun(RunDto runDto)throws Exception {
        try{
            managerDelegate.changeRun(runDto);
        }
        catch(Exception e){
            throw new Exception("error when trying to change run\n"+e.toString());
        }
    }

    public int createRole(int runId, String roleId)throws Exception {
        try{
            return managerDelegate.createRole(runId, roleId);
        }
        catch(Exception e){
            throw new Exception("error when creating new role\n"+e.toString());
        }
    }

    public int createRun(RunDto runDto) throws Exception {
        try{
            return managerDelegate.createRun(runDto);
        }
        catch(Exception e){
            throw new Exception("error when trying to create run of Learning Design\n"+e.toString());
        }
    }

    public void createUser(String userId) throws Exception {
        try{
            managerDelegate.createUser(userId);
        }
        catch(Exception e){
            throw new Exception("error when trying to create new user\n"+e.toString());
        }
    }

    public String getActiveLDRole(String userId, int runId) throws Exception {
        try{
            return managerDelegate.getActiveLDRole(userId, runId);
        }
        catch(Exception e){
            throw new Exception("can't get active role for that specific user in that run\n"+e.toString());
        }
    }





}
