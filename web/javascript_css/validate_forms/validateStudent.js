// form validation function //


var xmlhttp;
var studentDiv;
var parentDiv;
var studentIdNum;
function checkExists(studentInputDiv,parent,StudentNum){
    try{
        document.getElementById('exists').value = 'false';
        var name = studentInputDiv.value;
        studentDiv = studentInputDiv;
        parentDiv = parent;
        studentIdNum = StudentNum;

        xmlhttp=GetXmlHttpObject();
        document.getElementById('loading_'+StudentNum).style.display = 'block';
        if (xmlhttp==null){
            alert ("Your browser does not support Ajax HTTP");
            return;
        }

        var url="javascript_css/campusOperations/checkStudent.jsp";
        url=url+"?name="+name;

        xmlhttp.onreadystatechange=check;
        xmlhttp.open("GET",url,true);
        xmlhttp.send(null);
    }
    catch(e){
        alert(e);
    }

}

function check()
{

  if (xmlhttp.readyState==4)
  {
    document.getElementById('loading_'+studentIdNum).style.display = 'none';    
    if(xmlhttp.responseText.indexOf("false")!=-1)
        inlineMsg(studentDiv,'student not exists. Browse to check',2,parentDiv,2);        
    else
        document.getElementById('exists').value = 'true';
        
  }
}

function GetXmlHttpObject()
{
    if (window.XMLHttpRequest)
    {
       return new XMLHttpRequest();
    }
    if (window.ActiveXObject)
    {
      return new ActiveXObject("Microsoft.XMLHTTP");
    }
 return null;
}