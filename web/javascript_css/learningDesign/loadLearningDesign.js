/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    $(document).ready(function() {
         
         $('#projectResults').paginateTable({rowsPerPage: 4});
         
        $("#openManifestForm").dialog({
            autoOpen: false,
            height: 600,
            width: 550,
            modal: true,
            zIndex: 10000,
            resizable:false,
            buttons: {
                
                Cancel: function() {
                    $( this ).dialog( "close" );
                }/*,
                Search_Project: function(){                    
                    loadLD.getResultData($("#ProjectId").val(),$("#WriterName").val(),$("#ProjectTitle").val(),$("#LessonSubject").val())                    
                }*/
            }
        })
        $("#openManifestForm").css({"background":"#fff"})
    });

var loadLD = {
    xmlhttpObj: null,
    endPoints:[],
    startPoints:[],
    connectionPairs:[],
    syncronizedPairs:[],
    textResources:null,
    openManifest: function(){
        
        $("#openManifestForm").dialog("open");
    },
    loadLearningDesign: function(ldName){
        try{
            
            loadLD.xmlhttpObj = GetXmlHttpObject();

            //ldName = 'manifest-1303206221606'//'manifest-1302528703060'//'manifest-1302210690480';
           
            //$.fn.colorbox({width:"50%",  html:"<p>loading learning design<p><p>ss<img src='loadinfo.gif'/></p>"});
            if (loadLD.xmlhttpObj==null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }       
            var url="javascript_css/learningDesign/loadLD.jsp";
            url=url+"?LDName="+ldName;
            //contentID = "StudentContent";
            //loadingGifID = "#browsingloader";
            loadLD.xmlhttpObj.onreadystatechange= function(){
                if (loadLD.xmlhttpObj.readyState===4){
                    try{
                        loadStructureServices();
                        loadServices();          
                        var response = eval("("+loadLD.xmlhttpObj.responseText+")");
                      
                        if(response.manifest==="" || response.graphicDesign==="")
                           throw "either manifest or graphical representation not found";
                        else{
                          //var doc = loadLD.StringtoXML(response.manifest);
                          var graphicDesc = loadLD.StringtoXML(response.graphicDesign);          

                          if(response.author === response.username){
                              ManifestID = ldName;
                              author = response.author;
                          }

                          loadLD.textResources = eval("("+response.texts+")");

                          loadLD.generateRoles(graphicDesc);
                          //loadLD.generateEnvironments(graphicDesc);

                          loadLD.generatePhases(graphicDesc);                          
                          loadLD.generateStructures(graphicDesc);                          
                          loadLD.generateActivities(graphicDesc)
                          loadLD.generateEndPoint();                          
                          loadLD.generateStartPoint();
                          loadLD.generateConnectionPairs(graphicDesc);
                          
                          loadLD.generateConnections();
                          loadLD.generateMetadata(graphicDesc);
                          properties.loadProperties(graphicDesc);
                          clearAllViews(allPhases,1);
                          if(allPhases.getPhases()[0]!==null)
                            $('#phaseTab').attr("name",allPhases.getPhases()[0].getName());
                          
                       }
                       select();
                       $.unblockUI()
                   }
                   catch(e){
                       console.log("error while trying to load manifest\n"+e)
                   }
                   

                }
            };
            loadLD.xmlhttpObj.open("GET",url,true);
            loadLD.xmlhttpObj.send(null);
          
        }
        catch(e){
            alert("error while trying to connect to server "+e)
        }
    },
    loadODSTeam: function(ODSGroupID){
        try{            
            var teamAjaxCall = GetXmlHttpObject();
            if (teamAjaxCall == null){
                alert ("Your browser does not support Ajax HTTP");
                return;
            }       
            var url="javascript_css/learningDesign/loadODSTeam.jsp";
            url=url+"?ODSGroupID="+ODSGroupID;
            teamAjaxCall.onreadystatechange= function(){
                if (teamAjaxCall.readyState==4){
                    try{
                        //var response = eval("("+teamAjaxCall.responseText+")");                      
                        var res = JSON.parse(teamAjaxCall.responseText);
                        if(res.teamName == "")
                           console.log("No Team Found\n"+e)
                        else{
                            clearTeamsForm();
                            $("#team_name").val(res.teamName);
                            
                            for(var i=0;i<res.students.length;i++){
                                add_newStudentRowInForm(1,res.students[i].userFirstName,'','ODS');
                                //$("#student_"+(i+1)).val(res.students[i].userFirstName);
                                
                            }                                 
                           add_newTeam(document.forms["TeamForm"],"ODS");                                                      
                           $("#project_team_list li").eq(0).attr("class","last");
                           
                        }
                       
                    }
                    catch(e){
                       console.log("error while trying to loading ODS Team\n"+e)
                    }                   
                }
            };
            teamAjaxCall.open("GET",url,true);
            teamAjaxCall.send(null);          
        }
        catch(e){
            alert("error while trying to connect to server "+e)
        }        
    },
    StringtoXML: function(text){
                try{
                    if (window.ActiveXObject){
                      var doc=new ActiveXObject('Microsoft.XMLDOM');
                      doc.async='false';
                      doc.loadXML(text);
                    } else {
                      var parser=new DOMParser();
                      var doc = parser.parseFromString(text,'text/xml');
                      /*var manifest = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","learning-design")[0]
                      
                      for(var i=0;i<manifest.childNodes.length;i++){
                          if (manifest.childNodes[i].nodeType == 3)
                              manifest.removeChild(manifest.childNodes[i])

                      }
                      alert(manifest.childNodes.length)*/
                    }
                }
                catch(e){
                    alert("error while loading manifest "+e);
                }
                return doc;
            },

    getRoleByIdentifier: function(doc,id){
        try{
            var roles = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","roles")[0];
            var role=null;         
            for(var i=0;i<roles.childNodes.length;i++){
                role = roles.childNodes[i];
                if(role.nodeType==3)
                    continue;
                
                if(role.getAttribute("identifier")==id)
                    return role;
            }
            return null;
        }
        catch(e){
            console.log("error while reading activities from manifest "+e)
        }
    },
    getRoleParticipationId: function(doc,id,option){
        try{
            var phases = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","act");
            var phaseEl;
            var roleParts = null,learningActivity=null,rolePartsArray=new Array();
            var role=null,roleRef="",roleTitle="",roleId=-1;
            for(var i=0;i<phases.length;i++){
                phaseEl = phases[i];
                if(phaseEl.nodeType==3)
                    continue;
                
                roleParts = phaseEl.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0", "role-part");
               
                for(var j=0;j<roleParts.length;j++){
                    learningActivity = roleParts[j].getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0", option)[0];
                    
                    if(learningActivity!=null){
                        if(learningActivity.getAttribute("ref")==id){
                            roleRef = roleParts[j].getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0", "role-ref")[0]
                            if(roleRef==null)
                                throw "";
                            role = loadLD.getRoleByIdentifier(doc,roleRef.getAttribute("ref"));
                            //roleTitle = role.childNodes[0].textContent;
                            if(role==null || roleTitle==null)
                                throw " role not found";

                            roleId = role.getAttribute("identifier").substr(role.getAttribute("identifier").indexOf("role-")+5)
                            rolePartsArray.push(roleId);
                        }
                    }
                }
            }
            return rolePartsArray;
        }
        catch(e){
            console.log("error while reading activities from manifest "+e)
        }
    },
    getBelongedPhaseforActivity: function(doc,id,option){
        try{        
            var phases = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","act");
            var phaseEl,phaseTitle=null;
            var roleParts = null,learningActivity=null,rolePartsArray=new Array();

            for(var i=0;i<phases.length;i++){
                phaseEl = phases[i];
                if(phaseEl.nodeType==3)
                    continue;

                roleParts = phaseEl.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0", "role-part");
                for(var j=0;j<roleParts.length;j++){
                    learningActivity = roleParts[j].getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0", option)[0];                                                        
                    if(learningActivity!=null){
                        if(learningActivity.getAttribute("ref")==id){                            
                            phaseTitle = phaseEl.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0", "title")[0];
                            if(phaseTitle!=null)
                                return phaseTitle.textContent;
                        }
                    }
                }
            }
             
            return null;
        }
        catch(e){
            console.log("error while reading activities from manifest "+e)
        }
    },

    getActivityId: function(ref){
        try{
            var indexOfFirstIndent = ref.indexOf("-");
            var indexOfSecondIndent = ref.length;//ref.indexOf("-",indexOfFirstIndent+1);
           
            return ref.substring(indexOfFirstIndent+1,indexOfSecondIndent);
        }
        catch(e){
            console.log("error while reading activities from manifest "+e)
        }
    },


    generateRoles: function(doc){
        try{
            //tha kalesw tin roleOperate(), alla prepei na enimerwsw kai tis times pou elenxontai mesa se aythn
            var roles = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","roles")[0];
            learnerArray = initializeRoleArrays();
            staffArray = initializeRoleArrays();

            var roleName, minRoleNum, maxRoleNum,role,role_title="",role_id="",index_role_id=-1;

            for(var i=0;i<roles.childNodes.length;i++){
                if(roles.childNodes[i].nodeType==3)
                    continue;
                
                role = roles.childNodes[i];                
                roleName="";
                minRoleNum="";
                maxRoleNum="";
             
                if(role.tagName.indexOf('learner',0)!=-1){                    
                    $("#ArrayList").val("learnerArray");
                    $("#roleCategory").val("learner");                                      
                }
                else if(role.tagName.indexOf('staff',0)!=-1){
                    $("#ArrayList").val("staffArray");
                    $("#roleCategory").val("staff");
                }
                role_title = role.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","title")[0];
                if(role_title!=null)//simfwna me to sxima tou ims LD an iparxei titlos, autos tha einai to prwto element
                    roleName = role_title.textContent;
             
                if(role.getAttribute("max-persons")!=null)
                    maxRoleNum = parseInt(role.getAttribute("max-persons"));
                if(role.getAttribute("min-persons")!=null)
                    minRoleNum = parseInt(role.getAttribute("min-persons"));
                $('#roleConstraint_yes').attr('checked', false);
                if(role.getAttribute("match-persons")!=null)
                    if(role.getAttribute("match-persons")=="exclusively-in-roles")
                        $('#roleConstraint_yes').attr('checked', true);

                $("#function").val("add_newRow");
                $('#Role_name').val(roleName);
                $('#minRoleNum').val(minRoleNum);
                $('#maxRoleNum').val(maxRoleNum);

                role_id = role.getAttribute("identifier")
                index_role_id = role_id.indexOf("role-");
                role_id = role_id.substring(index_role_id+5,role_id.length)
                
                roleOperate(role_id);
            }

        }
        catch(e){
            console.log("error on generating roles "+e)
        }
    },
    getServices: function(env,option){
        if(env!=null){
            var environment = null,checkboxId=null,parameters=null,RoleMap=[];
            var title="",label="",launchUrl="",key="",secret="",resourceTitle="",resourceId="",contextId="";
            for (var i=0;i<env.childNodes.length;i++){
                environment = env.childNodes[i];
                if(environment.nodeName.indexOf("service")!=-1){
                    RoleMap=[];
                    parameters = eval("(" + environment.getAttribute("parameters") + ')');

                    if(parameters!=null){
                        contextId = parameters.context_id;
                        title = parameters.context_title;
                        label = parameters.context_label;
                        launchUrl = parameters.launch_url;
                        key = parameters.key;
                        secret =  parameters.secret;
                        resourceTitle = parameters.resource_link_title;
                        resourceId = parameters.resource_link_id;

                        RoleMap.push(['administrator',parameters.role_mapping[0].administrator])
                        RoleMap.push(['content_developer',parameters.role_mapping[0].content_developer])                       
                        RoleMap.push(['mentor',parameters.role_mapping[0].mentor])
                        RoleMap.push(['instructor',parameters.role_mapping[0].instructor])

                    }
                    if(environment.childNodes[0]!=null)
                        if(environment.childNodes[0].childNodes[1]!=null){
                            checkboxId = environment.childNodes[0].childNodes[1].textContent;
                            
                        }
                
                   saveLTIParameters(title,contextId,label,resourceTitle,resourceId,RoleMap,launchUrl,checkboxId,option,key,secret);
                }
            }



        }
    },
    getEnvironment: function(doc,environment){
        try{
            if(environment==null)
                return null;
            var envId = environment.getAttribute("ref");

            var environements = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","environments");
            var service = null;
            var env = null;
            if(environements!=null)
                if(environements[0]!=null){
                    for (var i=0;i<environements[0].childNodes.length;i++){
                        env = environements[0].childNodes[i];        
                        if(env.getAttribute("identifier")==envId){                            
                            return env;
                        }

                    }
                }
            return null;
        }
        catch(e){
            console.log("error while generation environment "+e)
        }
    },
    generatePhases: function(doc){
        try{
            var phases = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","act");
            var phaseEl,phaseName,completeCondition=null,phaseNameElement = null;
            var activitiesToEnd =[], roleParts=null,propertyToFindEnds=null;

            var properties=null;
            var tmpPair = [];
            var rolePart = null,activityId=-1,endPointId=-1,joinId=-1;
            allPhases = initializePhasesArray();

            for(var i=0;i<phases.length;i++){
                phaseName = "";
                phaseEl = phases[i];
                if(phaseEl.nodeType==3)
                    continue;
                phaseNameElement = phaseEl.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","title")[0];
                if(phaseNameElement!=null)//simfwna me to sxima tou ims LD an iparxei titlos, autos tha einai to prwto element
                    phaseName = phaseNameElement.textContent;
                completeCondition = phaseEl.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","complete-act")[0];

                if(completeCondition==null){
                    $("#NoCondition").attr("checked",true);
                    showCompletionPrereq("NoCondition","");
                }
                else{
                    if(completeCondition.childNodes.length!=0){
                        if(completeCondition.childNodes[0].nodeName.indexOf("time-limit")!=-1){
                            $("#TimePeriod").attr("checked",true);
                            var time = completeCondition.childNodes[0].textContent;
                            var monthIndex = time.indexOf("M");
                            var daysIndex = time.indexOf("D");
                            var timeIndex = time.indexOf("T");
                            var hoursIndex = time.indexOf("H");

                            $("#phaseHours").val(time.substring((timeIndex+1),hoursIndex))
                            $("#PhaseDays").val(time.substring(monthIndex+1, daysIndex))

                        }
                        else if(completeCondition.childNodes[0].nodeName.indexOf("when-property-value-is-set")!=-1){
                           $("#CompleteActivities").attr("checked",true);
                           if(completeCondition.childNodes[0].childNodes[0]!=null)
                                propertyToFindEnds = completeCondition.childNodes[0].childNodes[0].getAttribute("ref");
                                properties = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","change-property-value")
                                if(properties==null)
                                    continue
                                for(var k=0;k<properties.length;k++){
                                    if(properties[k].childNodes[0]!=null){
                                        if(properties[k].childNodes[0].getAttribute("ref")==propertyToFindEnds){
                                            endPointId=properties[k].getAttribute("identifier");

                                            if(properties[k].parentNode.parentNode.nodeName.indexOf("activity")!=-1)
                                                activityId = properties[k].parentNode.parentNode.getAttribute("identifier")

                                            else if(properties[k].parentNode.parentNode.nodeName.indexOf("conditions")!=-1){
                                                activityId = properties[k].parentNode.parentNode.childNodes[0].childNodes[0].getAttribute("identifier")
                                                if(activityId==null){
                                                    if(properties[k].parentNode.nodeName.indexOf("then")!=-1){
                                                        if(properties[k].parentNode.previousSibling.getAttribute("type")!=null)
                                                            if(properties[k].parentNode.previousSibling.getAttribute("type")=="joingateway"){
                                                                activityId = properties[k].parentNode.previousSibling.childNodes[0].getAttribute("identifier");
                                                            }
                                                    }
                                                }

                                            }

                                            loadLD.endPoints.push(new endObj(endPointId,properties[k].getAttribute("positionX"),properties[k].getAttribute("positionY"),phaseName));

                                            activitiesToEnd.push(activityId.substr(activityId.indexOf("-")+1))
                                        }
                                        tmpPair = [];
                                        tmpPair.push(activitiesToEnd);
                                        tmpPair.push(endPointId);
                                        tmpPair.push(phaseName);
                                        loadLD.connectionPairs.push(tmpPair)
                                        activitiesToEnd = [];
                                    }
                                }
                        }
                    }
                }
                
                   /*roleParts = completeCondition.childNodes;//exw ftasei sto completeAct

                   endPointId = completeCondition.getAttribute("identifier")
                   loadLD.endPoints.push(new endObj(endPointId,completeCondition.getAttribute("positionX"),completeCondition.getAttribute("positionY"),phaseName));
                   
                   for(var k=0;k<roleParts.length;k++){
                       rolePart = loadLD.findRolePartByIdentifier(doc,roleParts[k].getAttribute("ref"))
                       if(rolePart==null)
                           continue
                       activityId = rolePart.childNodes[2].getAttribute("ref");
                   
                       activitiesToEnd.push(activityId.substr(activityId.indexOf("-")+1))
                   }
                   tmpPair = [];
                   tmpPair.push(activitiesToEnd);
                   tmpPair.push(endPointId);
                   tmpPair.push(phaseName);
                
                   loadLD.connectionPairs.push(tmpPair)
                }*/
                loadLD.generateItem(doc, phaseEl.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","feedback-description")[0], 'PhaseDescription','');

                loadLD.getNotifications(doc,phaseEl.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","notification"),"Phase");
                createPhase(phaseName,allPhases,notificationArray,phaseFeedBackItem,1);//prepei na mpoun ta notifications kai ta feedback
                phaseFeedBackItem = [];
                notificationArray = [];
            }

        }
        catch(e){
            console.log("error while generating Phases (represented by Act LD elements) "+e);
        }
    },
    generateActivities: function(doc){
        try{
            
            var activities = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","learning-activity");
            var activity=null,rolesParticipating = null,activityId=null,tmpStart;
            var activityIdentifier = "",phaseName;
            var activityTitle = "",role=null,includedInStructure=null,environment=null,environmentNode=null;
            var notifiactions = null,completion=null,time="",months="",days="";
            var annotationElement=null,annotationString=null,activityObj=null;
            var annotationStringContent="", tmpPair = [];
            for(var i=0;i<activities.length;i++){
                annotationStringContent = ""
                activity = activities[i];
                //alert(activity.nodeType)
                if(activity.nodeType==3)//an einai text
                    continue;
                //alert(activity.nodeType);

                activityIdentifier = activity.getAttribute("identifier");
                
                activityId = loadLD.getActivityId(activityIdentifier);

                rolesParticipating = loadLD.getRoleParticipationId(doc,activityIdentifier,"learning-activity-ref");             

                if(rolesParticipating.length==0){
                    includedInStructure = loadLD.StructureContains(doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","activity-structure"),activityIdentifier,"learning-activity-ref");
                    
                    if(includedInStructure!=null){
                        phaseName = includedInStructure.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","title")[0].textContent;
                        /*if(activity.getAttribute("isvisible")!=null)
                            if(activity.getAttribute("isvisible")=="true"){//gia start point
                                loadLD.ObjTostartPoint.push(includedInStructure.getId());
                                if(loadLD.startPoint==null){
                                    if(activity.getAttribute("startPositionX")!=null)
                                        loadLD.startPoint = new startObj(activity.getAttribute("startPositionX"),activity.getAttribute("startPositionY"),phaseName)
                                }

                            }*/
                    }
                }
                else
                    phaseName = loadLD.getBelongedPhaseforActivity(doc,activityIdentifier,"learning-activity-ref");
                    //gia start point
                if(activity.getAttribute("startPositionX")!=null)
                    if(activity.getAttribute("startObjectId")!=null){
                        tmpStart = loadLD.startPoints.containsId(activity.getAttribute("startObjectId"))
                        if(tmpStart==null)
                            loadLD.startPoints.push(new startObj(activity.getAttribute("startPositionX"),activity.getAttribute("startPositionY"),activity.getAttribute("startObjectId"),activityId,phaseName))
                        else
                            tmpStart.setObjGoto(activityId)

                    }
                
                for(var k=0;k<rolesParticipating.length;k++)
                    $("#"+rolesParticipating[k]+"_activity").attr("checked",true);


                if(phaseName==null)
                    throw "error "+rolesParticipating+" name "+phaseName;
                activityTitle = activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","title")[0].textContent;
                
                $('#phaseTab').attr("name",phaseName);
                

                //var position = loadLD.ElementsPosition(graphicDesc,activityTitle,"activity");
                var position = {positionX:activity.getAttribute("positionX"),positionY:activity.getAttribute("positionY")};
                if(position==null)
                    throw "error while positioning elements";
                
                loadLD.generateItem(doc, activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","learning-objectives")[0], 'activityLearningObjectives','');
                loadLD.generateItem(doc, activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","prerequisites")[0], 'Prerequisites','activity');
                loadLD.generateItem(doc, activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","activity-description")[0], 'Description','activity');
                loadLD.generateItem(doc, activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","feedback-description")[0], 'activityCompletion','');

                loadLD.getNotifications(doc,activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","notification"),"activity");
                environmentNode = loadLD.getEnvironment(doc, activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","environment-ref")[0])         
                if(environmentNode!=null){
                    if(environmentNode.childNodes[0]!=null)
                        if(environmentNode.childNodes[0].nodeName.indexOf("learning-object")!=-1)//an iparxei tha einai to prwto element
                            loadLD.generateItem(doc, environmentNode.childNodes[0], 'LearningObject','');
                    loadLD.getServices(environmentNode,"activity");
                  //console.log(serviceArray)
                }

                completion = activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","complete-activity")
              
                    
                 if(completion[0]!=null){
                        if(completion[0].childNodes[0].nodeName.indexOf("time-limit")!=-1){
                            $("#activityCompletionTime").attr("checked",true)
                            time = completion[0].childNodes[0].textContent
                        //years = time.indexOf("Y")
                        //months = time.indexOf("M")
                        //time.substr(years+1, month)
                            months = time.indexOf("M")
                            days = time.indexOf("D")
                            var timeIndex = time.indexOf("T");
                            var hoursIndex = time.indexOf("H");
                            $("#activityHours").val(time.substring((timeIndex+1),hoursIndex))
                            $("#activityDays").val(time.substring((months+1),days))
                        }
                        else if(completion[0].childNodes[0].nodeName.indexOf("user-choice")!=-1)
                            $("#userChoice").attr("checked",true)
                }
                else
                    $("#none").attr("checked",true)

                environment = createEnvironment(LearningObjectItems,serviceArray,'',-1)
                annotationElement = activity.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","metadata");
             
                if(annotationElement[0]!=null){
                    annotationString = annotationElement[0].getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","langstring");
                    if(annotationString[0]!=null)
                        annotationStringContent = annotationString[0].textContent
                }
             
                addNewActivity(activityTitle,getCurrentActivityList(),defineRolesSelected("_activity"),descriptionItems,learningObjectivesItems,prerequisitesItems,activityCompletionFeedback,activityCompletionNotificationArray,environment,activityId,parseInt(position.positionX),parseInt(position.positionY),annotationStringContent);
                
                descriptionItems=[];
                learningObjectivesItems=[];
                prerequisitesItems=[];
                activityCompletionFeedback=[];
                LearningObjectItems = [];
                serviceArray = [];                
                activityCompletionNotificationArray = [];
                

                for(k=0;k<rolesParticipating.length;k++)
                    $("#"+rolesParticipating[k]+"_activity").attr("checked",false);

            }           
        }
        catch(e){
            try{
                console.log("error while generating activities "+activityIdentifier+" "+e)
            }catch(er){
                console.log(" "+er);
            }
            //console.log("\nrole-part element is undefined ");
        }
    },

    generateConnectionPairs: function(doc){
        try{           
           
            var conditions = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","conditions")[0];
            if(conditions==null)
                return;
            
            var condition=null, activitiesInCond=null,activityRef = null,tmpPair=null,startId=-1;
            var phaseName="",activityName="",syncElementId="",orgatewayId="",synchElement = null;
            var tmpActivities = [],fromSynch=[],tmpsynchPairs=[];
            var synchronize = false, syncronizeFromElements = null,tmpStart=null;
            var option="",orgateway=false;
            for(var i=0;i<conditions.childNodes.length;i++){
                condition = conditions.childNodes[i];
                if(condition.nodeName.indexOf("if")!=-1){//vrika if condition
                if(condition.getAttribute("type")=="joingateway")
                    if(condition.childNodes[0].nodeName=="imsld:and"){                        
                        for(var k=0;k<condition.childNodes[0].childNodes.length;k++){
                            activityRef = condition.childNodes[0].childNodes[k].childNodes[0];
                            activityName = loadLD.getActivityId(activityRef.getAttribute("ref"));
                            
                            if(activityName==null)                                
                                throw "error on connecting activities ";
                            
                            fromSynch.push(activityName);
                            if(k==0){                                
                                if(activityRef.nodeName=="imsld:learning-activity-ref"){
                                    phaseName = loadLD.getBelongedPhaseforActivity(doc,activityRef.getAttribute("ref"),"learning-activity-ref");
                                    option = "learning-activity-ref"
                                }
                                else if(activityRef.nodeName=="imsld:activity-structure-ref"){
                                    phaseName = loadLD.getBelongedPhaseforActivity(doc,activityRef.getAttribute("ref"),"activity-structure-ref");
                                    option = "activity-structure-ref"
                                }
                                if(phaseName==null)
                                    phaseName = (loadLD.StructureContains(doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","activity-structure"),activityRef.getAttribute("ref"),option)).childNodes[0].textContent//getElementsByTagName("imsld:title")[0].textContent;
                            }
                        }
                        $('#phaseTab').attr("name",phaseName);
                        syncElementId = condition.childNodes[0].getAttribute("identifier");
                        createSyncElement(parseInt(condition.childNodes[0].getAttribute("positionX")),parseInt(condition.childNodes[0].getAttribute("positionY")),syncElementId);
                        synchronize = true;                        
                        
                        tmpPair = [];
                        tmpPair.push(fromSynch);
                        tmpPair.push(syncElementId);
                        tmpPair.push(phaseName);
                        
                        loadLD.connectionPairs.push(tmpPair);
                        fromSynch=[];                        
                        continue;
                    }

                    activitiesInCond = condition.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","complete");
                   
                    if(activitiesInCond==null)
                        throw "no activities found"


                    for(var j=0;j<activitiesInCond.length;j++){                        
                        activityRef = activitiesInCond[j].childNodes[0]//to prwto element sto complete tha einai to activity ref
                        if(activityRef.nodeName=="imsld:learning-activity-ref"){
                            phaseName = loadLD.getBelongedPhaseforActivity(doc,activityRef.getAttribute("ref"),"learning-activity-ref");
                            option="learning-activity-ref"
                        }
                        else if(activityRef.nodeName=="imsld:activity-structure-ref"){
                            phaseName = loadLD.getBelongedPhaseforActivity(doc,activityRef.getAttribute("ref"),"activity-structure-ref");
                            option = "activity-structure-ref"
                        }

                        if(phaseName==null)
                            phaseName = (loadLD.StructureContains(doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","activity-structure"),activityRef.getAttribute("ref"),option)).childNodes[0].textContent//getElementsByTagName("imsld:title")[0].textContent;

                        if(phaseName==null)
                            throw "error on matching activities to phase ";
                        $('#phaseTab').attr("name",phaseName);
                        activityName = loadLD.getActivityId(activityRef.getAttribute("ref"));
                        if(activityName==null)
                            throw "error on connecting activities ";
                        
                        
                        tmpActivities.push(activityName);

                    }
                    
                    if(condition.getAttribute("type")=="inclusiveOrgateway"){
                         createInclusiveOrGateway(condition.getAttribute("positionX"),condition.getAttribute("positionY"),condition.getAttribute("identifier"))
                         orgateway=true;
                         orgatewayId = condition.getAttribute("identifier");
                         
                            if(activitiesInCond.length==0){
                                activityRef = condition.nextSibling.childNodes[0].childNodes[0];
                                if(activityRef.nodeName=="imsld:learning-activity-ref"){
                                    phaseName = loadLD.getBelongedPhaseforActivity(doc,activityRef.getAttribute("ref"),"learning-activity-ref");
                                    option="learning-activity-ref"
                                }
                                else if(activityRef.nodeName=="imsld:activity-structure-ref"){
                                    phaseName = loadLD.getBelongedPhaseforActivity(doc,activityRef.getAttribute("ref"),"activity-structure-ref");
                                    option = "activity-structure-ref"
                                }

                                if(phaseName==null)
                                    phaseName = (loadLD.StructureContains(doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","activity-structure"),activityRef.getAttribute("ref"),option)).childNodes[0].textContent//getElementsByTagName("imsld:title")[0].textContent;

                                if(phaseName==null)
                                    throw "error on matching activities to phase ";
                                $('#phaseTab').attr("name",phaseName);
                            }
                            else{
                                tmpPair = [];
                                tmpPair.push([activityName]);
                                tmpPair.push(orgatewayId);
                                tmpPair.push(phaseName);
                                loadLD.connectionPairs.push(tmpPair);
                            }
                            if(condition.getAttribute("startPositionX")!=null)
                                if(condition.getAttribute("startObjectId")!=null){
                                    tmpStart = loadLD.startPoints.containsId(condition.getAttribute("startObjectId"))
                                    if(tmpStart==null)
                                        tmpStart = new startObj(condition.getAttribute("startPositionX"),condition.getAttribute("startPositionY"),condition.getAttribute("startObjectId"),loadLD.getActivityId(condition.getAttribute("identifier")),phaseName)

                                    startId = addStartPoint(tmpStart.getPositionX(),tmpStart.getPositionY());
                                    if(startId==-1)
                                        continue;
                                    tmpPair = [];
                                    tmpPair.push([startId]);
                                    tmpPair.push(orgatewayId);
                                    tmpPair.push(phaseName);

                                    loadLD.connectionPairs.push(tmpPair)
                                    console.log(loadLD.connectionPairs)
                                }
                       
                   }
                }
                else if(condition.nodeName.indexOf("then")!=-1){//vrika to then condition
                    activitiesInCond = condition.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","show")[0];//iparxei ena show simfwna me to ims learning design mesa sto then element
                 
                    if(synchronize==true){
                        synchronize=false;
                        tmpActivities=[syncElementId];
                    }
                    if(orgateway==true){
                        orgateway=false;
                        tmpActivities=[orgatewayId];
                    }
                    if(activitiesInCond!=null)
                        for(j=0;j<activitiesInCond.childNodes.length;j++){
                            activityRef = activitiesInCond.childNodes[j];
                            activityName = loadLD.getActivityId(activityRef.getAttribute("ref"));

                            if(activityName==null)
                                throw "error on connecting activities ";
                            tmpPair = [];
                            tmpPair.push(tmpActivities);
                            tmpPair.push(activityName);
                            tmpPair.push(phaseName);
                            loadLD.connectionPairs.push(tmpPair);
                        }
                    tmpActivities = [];
                    syncElementId = "";
                    orgatewayId = "";
                }


            }
        }
        catch(e){
            alert("could not create valid graphical representation "+e)
        }
    },
    generateConnections: function(){
        try{
            var from = null,to=null;
            var fireOnThis = null, evObj=null;
            var svgDoc = document.getElementById("svgPanel").contentDocument;
            var phaseName = null;
            for(var i=0;i<loadLD.connectionPairs.length;i++){
                from = loadLD.connectionPairs[i][0];
                to = loadLD.connectionPairs[i][1];
                phaseName = loadLD.connectionPairs[i][2];
         
                if(from==null||to==null||phaseName==null)
                    throw "error on connecting activities ";
                for(var j=0;j<from.length;j++){
                    //xreiazetai na kserw se poio phase eimai giati den douleuoun ta connections alliws
                    $('#phaseTab').attr("name",phaseName);

                    to_connect = true;

                    fireOnThis = svgDoc.getElementById(from[j]);
                    
                    evObj = document.createEvent('MouseEvents');
                    evObj.initMouseEvent( 'mousedown', true, true, window, 1, 12, 345, fireOnThis.getAttribute("dragX"), fireOnThis.getAttribute("dragY"), false, false, true, false, 0, null );
                    fireOnThis.dispatchEvent(evObj);

                    moused = true;
                    fireOnThis = svgDoc.getElementById(to);
                    evObj = document.createEvent('MouseEvents');
                    evObj.initMouseEvent( 'mouseover', true, true,window, 1, 12, 345, fireOnThis.getAttribute("dragX"), fireOnThis.getAttribute("dragY"), false, false, true, false, 0, null );                    
                    fireOnThis.dispatchEvent(evObj);

                    fireOnThis = svgDoc.getElementById(to);
                    evObj = document.createEvent('MouseEvents');
                    evObj.initMouseEvent( 'mouseup', true, true, window, 1, 12, 345, fireOnThis.getAttribute("dragX"), fireOnThis.getAttribute("dragY"), false, false, true, false, 0, null );                    
                    fireOnThis.dispatchEvent(evObj);
                    
                    if(fireOnThis.getAttribute('mousemoveControl')!=null)
                        fireOnThis.setAttribute('mousemoveControl',"false");

                }

            }
        }
        catch(e){
            console.log("error while connecting activities "+e)
        }
    },
    generateStructures: function(doc){
        try{
            var structures = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","activity-structure");
            var structure=null,rolesParticipating = null,structureId=null,structureTitleElem=null;
            var structureIdentifier = "",phaseName="",phaseNameElement=null;
            var structureTitle = "",annotationElement=null,annotationString=null,annotationStringContent="";
            var environmentNode = null,environment=null,structureObj=null,tmpStart=null;
            for(var i=0;i<structures.length;i++){
                structure = structures[i];
                annotationStringContent = ""
                if(structure.nodeType==3)//an einai text diladi exei perasei keno
                    continue;

                structureIdentifier = structure.getAttribute("identifier");                
                structureId = loadLD.getActivityId(structureIdentifier);
                
                rolesParticipating = loadLD.getRoleParticipationId(doc,structureIdentifier,"activity-structure-ref");
                phaseName = loadLD.getBelongedPhaseforActivity(doc,structureIdentifier,"activity-structure-ref");

                if(phaseName==null){//einai eswteriko se kapoio structure
                   phaseNameElement = loadLD.StructureContains(doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","activity-structure"),structureIdentifier,"activity-structure-ref");
                   
                   if(phaseNameElement!=null)
                        phaseName = phaseNameElement.childNodes[0].textContent                        
                   

                }
                if(phaseName==null)                   
                    continue;
                
                //gia ta start points
                if(structure.getAttribute("startPositionX")!=null)
                    if(structure.getAttribute("startObjectId")!=null){
                        tmpStart = loadLD.startPoints.containsId(structure.getAttribute("startObjectId"))
                        if(tmpStart==null)
                            loadLD.startPoints.push(new startObj(structure.getAttribute("startPositionX"),structure.getAttribute("startPositionY"),structure.getAttribute("startObjectId"),structureId,phaseName))
                        else
                            tmpStart.setObjGoto(structureId)

                }
                structureTitleElem = structure.childNodes[0]

                if(structureTitleElem!=null){
                    
                    structureTitle = structureTitleElem.textContent;
                }

                $('#phaseTab').attr("name",phaseName);
                if(rolesParticipating!=null)
                    for(var k=0;k<rolesParticipating.length;k++){                        
                        $("#"+rolesParticipating[k]+"_activityStructure").attr("checked",true);
                    }

                //var position = loadLD.ElementsPosition(graphicDesc,structureTitle,"activity-structure");
                var position = {positionX:structure.getAttribute("positionX"),positionY:structure.getAttribute("positionY")};
                if(position==null)
                    throw "error while positioning elements";

                environmentNode = loadLD.getEnvironment(doc,structure.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","environment-ref")[0])
                if(environmentNode!=null){
                    if(environmentNode.childNodes[0]!=null)
                        if(environmentNode.childNodes[0].nodeName.indexOf("learning-object")!=-1)//an iparxei tha einai to prwto element
                            loadLD.generateItem(doc, environmentNode.childNodes[0], 'structureLearningObject','');
                    loadLD.getServices(environmentNode,"structure");
                  //console.log(serviceArray)
                }

                loadLD.generateItem(doc, structure.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","information")[0], 'structureInformation','');
                environment = createEnvironment(structureLearningObjectItems,structureserviceArray,'',-1)

                annotationElement = structure.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","metadata");
                if(annotationElement[0]!=null){
                    annotationString = annotationElement[0].getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","langstring");
                    if(annotationString[0]!=null)
                       annotationStringContent = annotationString[0].textContent
                }
                console.log("annotationString "+annotationStringContent )
                activityStructuresArray.newActivityStructure(structureTitle,defineRolesSelected('_activityStructure'),structureInformationItems,structureId,parseInt(position.positionX),parseInt(position.positionY),environment,annotationStringContent)

                structureLearningObjectItems=[];
                structureserviceArray=[];
                structureInformationItems=[]

               

                if(rolesParticipating!=null)
                    for(k=0;k<rolesParticipating.length;k++){
                        $("#"+rolesParticipating[k]+"_activityStructure").attr("checked",false);
                    }

            }
        }
        catch(e){
            console.log("error while generating activity structures "+e);
        }
    },
    generateItem: function(doc,element,type,category){
        try{
            var contentIdentifier = "";
            if(element==null)
                return;
            var items = element.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0", "item")
            var item=null,title=null,identifierRef=null,resource=null,content="";
            if(items==null)
                return;
            for (var i=0;i<items.length;i++){
                item = items[i];
                if(item!=null){
                    title = item.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","title")[0];
                    identifierRef = item.getAttribute("identifierref");
                    resource = loadLD.getResourceElement(doc,identifierRef);
                    if(resource!=null){                        
                        if(resource.childNodes.length!=0){
                            if(resource.getAttribute("type")=="text"){
                                $("#createNew"+type).attr("checked",true);
                                contentIdentifier = resource.getAttribute("identifier")
                                contentIdentifier = contentIdentifier.replace("-","");
                                for(var name in loadLD.textResources){
                                     if(name == contentIdentifier){
                                         $("#createNew"+type+"Content").val(loadLD.textResources[name])
                                         break;
                                     }

                                }
                                console.log(contentIdentifier)                               
                            }
                            else{

                                $("#ExistingFile"+type).attr("checked",true);
                                
                                $('#ExistingFile'+type+'Button').attr('name','loadingManifest')
                                content = resource.getAttribute("href").substr(resource.getAttribute("href").indexOf('-')+1)
                                $("#ExistingFile"+type+"Content").val(content);
                                
                            }
                        }
                        else{
                            $("#URL"+type).attr("checked",true);
                            $("#URL"+type+"Content").val(resource.getAttribute("href"));
                        }
                    }
                    $("#"+category+type+"Title").val(title.textContent);

                    defineItemsProperties(type,category);
                }
            }
        }
        catch(e){
            console.log("error while generating feedback/description/objective item \n"+e);
        }
    },
    getResourceElement: function(doc,identifier){
        try{
            var resources = doc.getElementsByTagName("resource");
            for(var i=0;i<resources.length;i++){             
                if(resources[i].getAttribute("identifier")==identifier)
                    return resources[i];
            }

            return null;
        }
        catch(e){
            console.log("error while searching resources "+e)
        }
    },
    getNotifications: function(doc,notifications,option){
        try{
        var notificationNode = null,subjectElementId="",subject=null,title="",subjectContent="";
        var notifiedRoles = [];
        var roleRef = null,notificationObj=null;
        var activityNotificationNum = 1,phaseNotificationNum = 1;
        var role_idIndex=-1,role_id=-1;
       
            if(notifications==null)
                return;
            for(var i=0;i<notifications.length;i++){
                notifiedRoles = [];
                title=null;
                notificationNode = notifications[i];

                for(var j=0;j<notificationNode.childNodes.length;j++){//psaxnw ta email data elements
                    if(notificationNode.childNodes[j].nodeName.indexOf("email-data")!=-1){
                        roleRef = notificationNode.childNodes[j].childNodes[0];//pairnw ta role-ref elements mesa apo to email data

                        if(title==null){
                            subjectElementId = notificationNode.childNodes[j].getAttribute("email-property-ref")
                            if(subjectElementId!=null){
                                subject = loadLD.getElementByIdentifier(doc,"properties",subjectElementId)

                                if(subject!=null)//einai to glob-property
                                    if(subject.childNodes[0]!=null)//global-definition
                                        /*if(subject.childNodes[0].childNodes[0]!=null)
                                            title = subject.childNodes[0].childNodes[0];*/
                                            title = subject.childNodes[0];
                            }
                        }

                        if(roleRef!=null){
                            //1301999199958_completion
                            role_idIndex = roleRef.getAttribute("ref").indexOf("role-");
                            if(option=="activity")
                                notifiedRoles.push(roleRef.getAttribute("ref").substring(role_idIndex+5)+"_completion");
                            else
                                notifiedRoles.push(roleRef.getAttribute("ref").substring(role_idIndex+5)+"_phase");
                        }
                    }
                    else if(notificationNode.childNodes[j].nodeName.indexOf("subject")!=-1)
                        subjectContent = notificationNode.childNodes[j].textContent;
                }
                if(title==null)//simainei oti exei kseminei kapoio notification keno...
                    continue;

                if(option=="activity"){
                    notificationObj = new notification(title.textContent,subjectContent,notifiedRoles,option+"notificationItem"+activityNotificationNum);
                    activityCompletionNotificationArray.push(notificationObj);
                    activityNotificationNum++;
                
               }
               else{
                   notificationObj = new notification(title.textContent,subjectContent,notifiedRoles,option+"notificationItem"+phaseNotificationNum);
                   notificationArray.push(notificationObj);
                   phaseNotificationNum++;
               }
               addItemToNotificationViewer(title,subjectContent,notifiedRoles,option,notificationObj.getIdentifier());
            }
        }
        catch(e){
            console.log("error while reading notifications "+e);
        }
    },
    /*connectSynchPoints: function(doc,connections){
        try{
            var from = null,to=null;
            var fireOnThis = null, evObj=null,activityRef=null;
            var svgDoc = document.getElementById("svgPanel").contentDocument;
            var phaseName = null;
            for(var i=0;i<connections.length;i++){
                from = connections[i][0];
                to = connections[i][1];
                activityRef = from[0];//to prwto element sto complete tha einai to activity ref
                if(activityRef.nodeName=="imsld:learning-activity-ref")
                    phaseName = loadLD.getBelongedPhaseforActivity(doc,activityRef.getAttribute("ref"),"learning-activity-ref");
                else if(activityRef.nodeName=="imsld:activity-structure-ref")
                    phaseName = loadLD.getBelongedPhaseforActivity(doc,activityRef.getAttribute("ref"),"activity-structure-ref");
                if(phaseName==null)
                    phaseName = (loadLD.StructureContains(doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","activity-structure"),activityRef.getAttribute("ref"))).getElementsByTagName("imsld:title")[0].textContent;
                if(phaseName==null)
                    throw "error on matching activities to phase ";
                $('#phaseTab').attr("name",phaseName);

                if(from==null||to==null||phaseName==null)
                    throw "error on connecting activities ";
                for(var j=0;j<from.length;j++){
                    //xreiazetai na kserw se poio phase eimai giati den douleuoun ta connections alliws
                    $('#phaseTab').attr("name",phaseName);

                    to_connect = true;
                    fireOnThis = svgDoc.getElementById(from[j]);
                    evObj = document.createEvent('MouseEvents');
                    evObj.initMouseEvent( 'mousedown', true, true, window, 1, 12, 345, fireOnThis.getAttribute("dragX"), fireOnThis.getAttribute("dragY"), false, false, true, false, 0, null );
                    fireOnThis.dispatchEvent(evObj);

                    moused = true;
                    fireOnThis = to;
                    evObj = document.createEvent('MouseEvents');
                    evObj.initMouseEvent( 'mouseover', true, true,window, 1, 12, 345, fireOnThis.getAttribute("dragX"), fireOnThis.getAttribute("dragY"), false, false, true, false, 0, null );
                    fireOnThis.dispatchEvent(evObj);

                    fireOnThis = to;
                    evObj = document.createEvent('MouseEvents');
                    evObj.initMouseEvent( 'mouseup', true, true, window, 1, 12, 345, fireOnThis.getAttribute("dragX"), fireOnThis.getAttribute("dragY"), false, false, true, false, 0, null );
                    fireOnThis.dispatchEvent(evObj);

                }

            }

        }
        catch(e){
            alert("error while generating synchronization points\n"+e);
        }
    },*/
    StructureContains: function(structures,activityIdentifier,option){
        try{
            var structure=null,activities=null;                        
            for(var i=0;i<structures.length;i++){
                structure = structures[i];                
                activities = structure.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0",option);
                for(var j=0;j<activities.length;j++)
                    if(activities[j].getAttribute("ref")==activityIdentifier)
                        return structure;
            }
            return null;
        }
        catch(e){
            console.log("error while generating activity structure included elements "+e);
        }
    },
    ElementsPosition: function(graphicDescription,activityName,option){
        try{
            var activities = graphicDescription.getElementsByTagName(option);
            for(var i=0;i<activities.length;i++){
                if(activities[i].getAttribute("name")==activityName){
                    return({"positionX":activities[i].getAttribute("positionX"),"positionY":activities[i].getAttribute("positionY")});
                }
            }
            return null;
        }
        catch(e){
            throw "error while positioning graphics "+e;
        }
    },
    getElementByIdentifier: function(doc,elementNodeName,id){
        var elements = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0",elementNodeName);
        if(elements!=null){
            for(var i=0;i<elements[0].childNodes.length;i++){
                if(elements[0].childNodes[i].getAttribute("identifier")==id)
                    return elements[0].childNodes[i];
            }
        }
        return null;
    },
    findRolePartByIdentifier: function(doc,id){
        var roleParts = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","role-part");
        for(var i=0;i<roleParts.length;i++)
            if(roleParts[i].getAttribute("identifier")==id)
                return roleParts[i]

        return null;
    },
    generateEndPoint: function(){
        var endPointObj = null;
        for(var i=0;i<loadLD.endPoints.length;i++){
            endPointObj = loadLD.endPoints[i]
      
            addEndPoint(endPointObj.getId(),endPointObj.getPhaseName(),endPointObj.getPositionX(),endPointObj.getPositionY())
        }
    },
    generateStartPoint: function(){
        var tmpPair = [];
        var start = null;
        var startId = -1;
        
        for(var i=0;i<loadLD.startPoints.length;i++){
            start = loadLD.startPoints[i];
            $('#phaseTab').attr("name",start.getPhaseName());

            startId = addStartPoint(start.getPositionX(),start.getPositionY());          
            if(startId==-1)
                continue;
            for(var k=0;k<start.getObjGoto().length;k++){
                tmpPair = [];
                tmpPair.push([startId]);                
                tmpPair.push(start.getObjGoto()[k]);
                tmpPair.push(start.getPhaseName());
                loadLD.connectionPairs.push(tmpPair)

            }
        }
        
    },
    getResultData: function(projectId,author,manifestTitle,lessonSubject){
        Display_Load("Project");
        $.ajax({
            type: "POST",
            url: "javascript_css/learningDesign/searchLD.jsp?author="+author+"&manifestTitle="+manifestTitle+"&projectId="+projectId+"&lessonSubject="+lessonSubject,
                          // Send the login info to this page
                statusCode: {
                    404: function() {
                        Hide_Load("Project");
                        alert('page not found');
                    }
                },
                success: function(msg){
                   
                    Hide_Load("Project");
                    
                    var result = eval('(' + msg + ')');
                 //console.log(result.table)
                    $("#resultsNum").html("Results "+result.size);
                    $("#projectResults").children('tbody:first').html(result.table);
                    $('#projectResults').paginateTable({rowsPerPage:4});
                    var projId = "";
                    $("#projectResults td:nth-child(1)").click(function(){
                        projId = $(this).attr("data-desc");
                        //$openProjectModalConfirmationWindow.html("open project "+projId).dialog("open");
                        
                        if (!confirm("open project "+projId)===true)
                            return;
                        
                        saveToFile.saveDesign(learnerArray, staffArray, allPhases,'openProject')

                        user = base64Encode(user)
                        if(ODSGroupID===-1)
                            window.location = "wizard.jsp?user="+user+"&projectId="+projId;
                        else 
                            window.location = "wizard.jsp?user="+user+"&projectId="+projId+"&ODSGroupId="+ODSGroupID;
                        
                           
                    });                      
                    
                    $("#pagerId").css("display","block");
                   
                 }
        });
       
    },
    generateMetadata: function(xmldoc){
        try{
            var metadata = xmldoc.getElementsByTagName("metadata")[0];
            var title = metadata.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","title")[0];
            $("#lesson_plan_title").val(title.childNodes[0].textContent);
            var description = metadata.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","description")[0];
            $("#lesson_subject").val(description.childNodes[0].textContent);
            var lifeCycle = metadata.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","lifecycle")[0];
            var contribute = lifeCycle.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","contribute")[0];
            var role = contribute.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","role")[0];
           
            $("#course_writer").val(role.childNodes[1].childNodes[0].textContent);

            var lom_date = contribute.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","date")[0];
            var dateTime = lom_date.childNodes[0].textContent;
            var day = dateTime.substring(0,dateTime.indexOf("/"));
            var month = dateTime.substring(dateTime.indexOf("/")+1,dateTime.indexOf("/",dateTime.indexOf("/")+1));
            var year = dateTime.substring(dateTime.indexOf("/",dateTime.indexOf("/")+1)+1,dateTime.length);
            $("#datepicker").val(day+"/"+month+"/"+year);

            var educational = metadata.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","educational")[0];
            var edu_description = educational.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsmd_v1p2","description")[0];
            $("#General_Goals").val(edu_description.childNodes[0].textContent);

            $("#CoursePrerequisites").val(xmldoc.getElementsByTagName("prerequisites")[0].childNodes[0].getAttribute("parameters"));
        }
        catch(e){
            console.log("error while reading lom metadata "+e)
        }

    }
    /*getEndPoints: function(orExpression,currentPhaseName){
        var endSVG=null,tmpEndObj=null;
        
        var tmpEndedArray=[];
        var endId = "",positionX = 0,positionY=0,activityRef="";
        var tmpConnetcionPair = [];
        if(orExpression!=null){
            for(var j=0;j<orExpression.childNodes.length;j++){
                endId = orExpression.childNodes[j].getAttribute("identifier");
                tmpEndObj = tmpEndedArray.containsId(endId)
                positionX = orExpression.childNodes[j].getAttribute("positionX");
                positionY = orExpression.childNodes[j].getAttribute("positionY");
                if(tmpEndObj==null){
                    endSVG = new endObj(endId)
                          //dimiourgise to svg
                    addEndPoint(currentPhaseName,positionX,positionY);
                          //ipodomi wste na kanw to connect activity with end point
                    activityRef = orExpression.childNodes[j].childNodes[0].getAttribute("ref");
                    endSVG.addFromActivitiesList(loadLD.getActivityId(activityRef));
                    tmpEndedArray.push(endSVG);
                }
                else{//exw dimiourgisei idi to end point kai twra tha valw kai alla activities pou kataligoun se auto to end point
                    activityRef = orExpression.childNodes[j].childNodes[0].getAttribute("ref");
                    tmpEndObj.addFromActivitiesList(loadLD.getActivityId(activityRef));
                }
            }
         }
           // }
        //}
        for(var k=0;k<tmpEndedArray.length;k++){//vale sto connection_list ta end points kai ta activities pou kataligouyn se ayta
            tmpConnetcionPair = [];
            tmpConnetcionPair.push(tmpEndedArray[k].getFromActivitiesList());
            tmpConnetcionPair.push(tmpEndedArray[k].getId());//exei to id tou end element
            tmpConnetcionPair.push(currentPhase.getName());
            loadLD.connectionPairs.push(tmpConnetcionPair.slice());
        }
    }*/
}

    function endObj(id,positionX,positionY,phaseName){
        this.id = id;
        this.positionX = positionX;
        this.positionY = positionY;
        this.phaseName = phaseName;
        this.getId = function(){
            return this.id;
        }
        this.getPositionX = function(){
            return this.positionX;
        }
        this.getPositionY = function(){
            return this.positionY;
        }
        this.getPhaseName = function(){
            return this.phaseName;
        }
    }
    function startObj(positionX,positionY,id,activityId,phaseName){
        
        this.positionX = positionX;
        this.positionY = positionY;
        this.id = id;
        this.objGoTo = [];
         this.phaseName = phaseName;
        this.objGoTo.push(activityId)
        this.getPositionX = function(){
            return this.positionX;
        }
        this.getPositionY = function(){
            return this.positionY;
        }
        this.getId = function(){
            return this.id;
        }
        this.setObjGoto = function(element){
            this.objGoTo.push(element)
        }
        this.getObjGoto = function(){
            return this.objGoTo
        }
        this.getPhaseName = function(){
            return this.phaseName;
        }
    }
     
    function base64Encode(str) {
            var c, d, e, end = 0;
            var u, v, w, x;
            var ptr = -1;
            var input = str.split("");
            var output = "";
            while(end == 0) {
                c = (typeof input[++ptr] != "undefined") ? input[ptr].charCodeAt(0) :
                    ((end = 1) ? 0 : 0);
                d = (typeof input[++ptr] != "undefined") ? input[ptr].charCodeAt(0) :
                    ((end += 1) ? 0 : 0);
                e = (typeof input[++ptr] != "undefined") ? input[ptr].charCodeAt(0) :
                    ((end += 1) ? 0 : 0);
                u = enc64List[c >> 2];
                v = enc64List[(0x00000003 & c) << 4 | d >> 4];
                w = enc64List[(0x0000000F & d) << 2 | e >> 6];
                x = enc64List[e & 0x0000003F];

                // handle padding to even out unevenly divisible string lengths
                if (end >= 1) {x = "=";}
                if (end == 2) {w = "=";}

                if (end < 3) {output += u + v + w + x;}
            }
            // format for 76-character line lengths per RFC
            var formattedOutput = "";
            var lineLength = 76;
            while (output.length > lineLength) {
              formattedOutput += output.substring(0, lineLength) + "\n";
              output = output.substring(lineLength);
            }
            formattedOutput += output;
            return formattedOutput;
        }

        function newProject(){
            try{
                if (!confirm("open new project")===true)
                    return;
                saveToFile.saveDesign(learnerArray, staffArray, allPhases,'openProject')

                user = base64Encode(user)
                if(ODSGroupID===-1)
                    window.location = "wizard.jsp?user="+user+"&projectId=new";
                else
                    window.location = "wizard.jsp?user="+user+"&projectId=new&ODSGroupId="+ODSGroupID;

            }
            catch(e){
                console.log("error while creating new project "+e)
            }
        }