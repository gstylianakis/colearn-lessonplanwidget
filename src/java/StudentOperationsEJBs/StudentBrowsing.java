/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package StudentOperationsEJBs;

/**
 *
 * @author Administrator
 */
import java.util.ArrayList;
import java.util.Vector;
import javax.ejb.Remote;

@Remote
public interface StudentBrowsing {
    public Vector student_searching()throws Exception;
    public Vector student_searching(String criteria)throws Exception;
    public boolean student_exist(String name)throws Exception;
    public boolean usernamePsw_exist(String username, String psw)throws Exception;
    public String enroll_student(String name, String lastName, String username, String psw, String email, String gender)throws Exception;
    public String getStudentParticipationGroups(String username)throws Exception;
}
