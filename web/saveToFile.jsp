<%-- 
    Document   : saveToFile
    Created on : 29 Νοε 2010, 2:50:25 μμ
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="GeneralUsage.Design"%>
<%@page import="GeneralUsage.XMLParsing"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.zip.ZipOutputStream"%>
<%@page import="org.w3c.dom.Document"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="save" scope="page" class="GeneralUsage.Design"/>
<%    
    try{
        //session.setAttribute("saveDir",save.getSaveFileDir());
        request.setCharacterEncoding("UTF-8");    
        String manifest_request = request.getParameter("manifest");     
        Document resources = save.StringToXML(request.getParameter("resourcesXML"));
        Document manifestDom = save.StringToXML(manifest_request);
        String option = request.getParameter("operation_option");

        String manifestId = request.getParameter("manifestId");
        String manifest = request.getParameter("manifest");
        String lesson_subject = request.getParameter("manifest_subject");
        String title = request.getParameter("manifest_title");     
        manifest = manifest.replaceAll("'","\\\\'");
        String graphics = request.getParameter("graphics");
        graphics = graphics.replaceAll("'","\\\\'");

        XMLParsing xml = new XMLParsing();
//System.out.println("to manifest einia "+manifest);
        String LD_metadata = xml.XmlNodeToString(xml.getLomMetadataOfLD(manifestDom));
        LD_metadata = LD_metadata.replaceAll("'","\\\\'");
        title = title.replaceAll("'","\\\\'");
        lesson_subject = lesson_subject.replaceAll("'","\\\\'");
        String username = (String)session.getAttribute("username");
        username = username.replaceAll("'","\\\\'");
        
        save.saveManifestToDB(manifest,manifestId,LD_metadata,graphics,username,title,lesson_subject);
        save.writeToFile(manifest_request, manifestId);

        save.createTextResources(resources,manifestId);

        save.saveResources(resources,manifestId);

        //save.saveGraphicDesign(graphics,manifestId);

        out.println("manifest and resources saved succesfully.\nManifest id is "+manifestId);
        
        //String[] source = {"c:/imsmanifest.xml"};
        ArrayList zipContent = save.getManifestExternalResources(manifestId);
        zipContent.add("imsmanifest.xml");
        save.zipFile(zipContent, manifestId+".zip",manifestId);
//tha prepei na ginontai delete ta unzip arxeia pou tha exoun apothikeutei sto disko.....
//arxika ola ta arxeia ginontai upload sto disko kai sto telos pou ginetai save to project, ta arxeia auta zipparontai
//authn thn stigmh ta unzip arxeia, den diagrafontai...
//vevaia an diagrafontai tha prepei na allaksei o kwdikas sto downloadFileFromServer.jsp, wste na
//psaxntai gia download ta arxeia apo to zip file kai oxi apo to xima file, to opoio psaxnete authn thn stigmh....
        if(option.compareTo("publish")==0){
            out.println("uploading manifest..");
            String teams = request.getParameter("teams");
            out.println("<script language='javascript'> self.location='http://147.27.41.121:8080/runtimeEngine/index.jsp?saveFileDir="+save.getSaveFileDir()+"&uolFile="+manifestId+"&teams="+teams+"'</script>");
            //
            
           
        }
    }
    catch(Exception e){
        out.println(e);
        
    }
%>