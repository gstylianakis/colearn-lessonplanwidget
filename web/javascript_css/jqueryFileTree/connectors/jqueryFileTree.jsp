<%@ page import="java.io.File,java.io.FilenameFilter,java.util.Arrays,java.util.Vector"%>
<jsp:useBean id="ontology" scope="page" class="OntologyOperationsJavaBeans.Ontology"/>
<jsp:useBean id="url_decoder" scope="page" class="GeneralUsage.URL_parsing"/>

<script type="text/javascript" language="JavaScript">

    LL_infoID = new Array();
    LL_infoID[1] = "mouse_definition";


//
// End of customization sections.
///////////////////////////////////////////////////////////////

LL_infoID[0] = "unused";
bNS4 = bNS6 = bIE = bOPERA = false;
if     (navigator.userAgent.indexOf("Opera") != -1) { bOPERA = true; }
else if(navigator.userAgent.indexOf("Gecko") != -1) { bNS6 = true;   }
else if(document.layers)                            { bNS4 = true;   }
else if(document.all)                               { bIE = true;    }

LLx = LLxx = LLy = LLyy = 0;
var LL_mousex;
var LL_mousey;
var STO = null;
var SET = false;
var cID = '';
if(bNS4 || bNS6 || bOPERA) { document.captureEvents(Event.MOUSEMOVE); }
document.onmousemove = LL_getmouseposition;


// Functions used by all browsers

function Null() { return; }

function LL_getmouseposition(e)
{
    if(bIE || bOPERA) {
	LL_mousex = event.clientX;
	LL_mousey = event.clientY;
	}
    else if(bNS6 || bNS4) {
	LL_mousex = e.pageX;
	LL_mousey = e.pageY;
	}
} // LL_getmouseposition()

// Functions to relay browser type to custom functions

    function LL_showinfo(m_section) {
    
    LL_hideallinfo();

    if(cID != m_section) { SET = false; }
        cID = m_section;
        if(bIE)    { LL_bIE_showit(m_section); }
        else if(bNS6)   { LL_bNS6_showit(m_section); }

    } // LL_showinfo()

    function LL_hideallinfo(m_section) {
        clearTimeout(STO);
        if(bIE)    { LL_bIE_hideallinfo(); }
        else if(bNS6)   { LL_bNS6_hideallinfo(); }

    } // LL_hideallinfo()




// IE functions

    function LL_bIE_hidesection(m_section) {
    if(LL_mouseinrectangle()) { return; }
    eval(LL_infoID[m_section] + '.style.visibility="hidden"');
    } // LL_bIE_hidesection()

    function LL_bIE_hideallinfo() {
    for(i = 1; i < LL_infoID.length; i++) {
            eval(LL_infoID[i] + '.style.visibility="hidden"');
            }
    } // LL_bIE_hideallinfo()

    function LL_bIE_showit(m_section) {
            
            LL_bIE_hideallinfo();
            var x = LL_mousex + 1;
            if(x < 0) { x = 0; }
            var y = LL_mousey + 20;
            if(y < 0) { y = 0; }
            if(SET == false) {
                    eval(LL_infoID[m_section] + '.style.left="' + x + '"');
                    eval(LL_infoID[m_section] + '.style.top="' + y + '"');
                    }
            eval(LL_infoID[m_section] + '.style.visibility="visible"');
            LLx = eval(LL_infoID[m_section] + '.style.pixelLeft');
            LLxx = eval(LL_infoID[m_section] + '.scrollWidth') + LLx;
            LLy = eval(LL_infoID[m_section] + '.style.pixelTop');
            LLyy = eval(LL_infoID[m_section] + '.scrollHeight') + LLy;
            SET = true;
            clearTimeout(STO);
            STO = setTimeout('SET = false',2000);
    } // LL_bIE_showit()



    // Netscape 6 functions

    function LL_bNS6_hidesection(m_section) {
    if(LL_mouseinrectangle()) { return; }
    document.getElementById(LL_infoID[m_section]).style.visibility="hidden";
    } // LL_bNS6_hidesection()

    function LL_bNS6_hideallinfo() {
    for(i = 1; i < LL_infoID.length; i++) {
            document.getElementById(LL_infoID[i]).style.visibility="hidden";
            }
    } // LL_bNS6_hideallinfo()

    function LL_bNS6_showit(m_section) {
            
            LL_bNS6_hideallinfo();
            var x = LL_mousex + 1;
            if(x < 0) { x = 0; }
            var y = LL_mousey + 20;
            if(y < 0) { y = 0; }
            if(SET == false) {
                    document.getElementById(LL_infoID[m_section]).style.left = x + 'px';
                    document.getElementById(LL_infoID[m_section]).style.top = y + 'px';
                    }
            document.getElementById(LL_infoID[m_section]).style.visibility="visible";
            var padding = 0;
            if(parseInt(document.getElementById(LL_infoID[m_section]).style.padding) > 0) { padding = parseInt(document.getElementById(LL_infoID[m_section]).style.padding) * 2; }
            LLx = parseInt(document.getElementById(LL_infoID[m_section]).style.left);
            LLxx = parseInt(document.getElementById(LL_infoID[m_section]).style.width) + LLx + padding;
            LLy = parseInt(document.getElementById(LL_infoID[m_section]).style.top);
            LLyy = parseInt(document.getElementById(LL_infoID[m_section]).style.height) + LLy + padding;
            SET = true;
            clearTimeout(STO);
            STO = setTimeout('SET = false',2000);
    } // LL_bNS6_showit()


//-->
</script>

<%
/**
  * jQuery File Tree JSP Connector
  * Version 1.0
  * Copyright 2008 Joshua Gould
  * 21 April 2008
*/

    try{
        String dir = request.getParameter("dir");                             
        if (dir == null) {
            return;
        }
        dir = url_decoder.utf_decode(dir);
        if(ontology.getOntologyURI()!=null)
            ontology.clearOntologyEntry(ontology.getOntologyURI());

        ontology.setOntologyURI(dir);
        ontology.loadOntology();
        
        Vector ontology_classes = ontology.getClasses();//get classes
        //Vector individual = ontology.getIndividuals(dir.substring(0,dir.length()-1));//get individuals
        out.print("<ul class=\"jqueryFileTree\" style=\"display: none;\">");

        if(!ontology.isIndividual(dir.substring(0,dir.length()-1)))
            for(int i=0;i<ontology_classes.size();i++){
                String class_Resource_name = (String)ontology_classes.get(i).toString();
                String class_name = class_Resource_name.substring(class_Resource_name.indexOf("#")+1,class_Resource_name.length());
                
           
           if(ontology.getIndividuals(class_Resource_name).size()!=0)//check if this class contains individuals
                out.print("<li onMouseover=\"LL_showinfo(1)\" class=\"directory collapsed\"><a href=\"#\" rel=\"" +class_Resource_name+ "/\">"
                                            + class_name + "</a></li>");
           
           else
                out.print("<li onMouseover=\"LL_showinfo(1)\" class=\"file\"><a>"
                                            + class_name + "</a></li>");
                
        }
        else{
            Vector individual = ontology.getIndividuals(dir.substring(0,dir.length()-1));
            String individual_Resource_name = "";
            String individual_name = "";
            for(int i=0;i<individual.size();i++){
                individual_Resource_name = (String)individual.get(i);
                individual_name = individual_Resource_name.substring(individual_Resource_name.indexOf("#")+1,individual_Resource_name.length());
                out.println("<li class=\"ext_\"><a href=\"#\">"
                                            + individual_name + "</a></li>");
            }
       }

    }
    catch(Exception e){
        out.println("error while loading ontology "+e.toString());

        ///prepei na kanw kana redirect????
    }
%>