/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package StudentOperationsEJBs;

/**
 *
 * @author Administrator
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DB_connection{
    private Connection conn = null;
    
    public DB_connection()throws Exception{
        this.connect();
    }
    private boolean connect() throws Exception{
        try{
            InitialContext ctx =  new InitialContext();
            DataSource ds = (DataSource)ctx.lookup("jdbc/lessonplan_widget");
            this.conn = ds.getConnection();
            
        } catch(Exception e){
            Logger.getLogger(DB_connection.class.getName()).log(Level.SEVERE, null, e);
            throw e;
        }
        return true;
    }
    public void close() throws SQLException{
        conn.close();
        conn = null;
    }
    
    public Connection getConnection(){
        return this.conn;
    }
    
}
