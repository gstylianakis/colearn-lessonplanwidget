/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GeneralUsage;

import java.io.StringWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
/**
 *
 * @author Administrator
 */
public class XMLParsing {
    public Node getLomMetadataOfLD(Document xmlRecord)throws Exception{
        try{
            NodeList metadata = xmlRecord.getElementsByTagName("metadata");
            return ((Node)metadata.item(0));

        }
        catch(Exception e){
            throw e;
        }
    }

    public String XmlNodeToString(Node node)throws Exception{
        try{

            StringWriter stw = new StringWriter();
            Transformer serializer = TransformerFactory.newInstance().newTransformer();
            serializer.transform(new DOMSource(node), new StreamResult(stw));

            return stw.toString();
        }
        catch(Exception e){
            throw e;
        }
    }
}
