/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function initCompleteActivitiesCondition(){
    return (new completeActivitiesCondition());
}
function completeActivitiesCondition(){
    try{
        this.RolesActivitiesPairs = new Array();

        this.addRoleActivityPair = function(roleId,activity){
            var tmp = new Array();
            tmp.push(roleId);
            tmp.push(activity);//codeAllagi tmp.push(activityName)
            this.RolesActivitiesPairs.push(tmp);
        }
        
        this.getRoleActivityPairIndex = function(roleId,activityName){
            var index = -1;
            var tmp;
            for(var i=0; i<this.RolesActivitiesPairs.length;i++ ){
                     tmp = this.RolesActivitiesPairs[i];
                     if(tmp[0] == roleId && tmp[1].getName() == activityName)//codeAllagi if(tmp[0] == roleId && tmp[1] == activityName)
                         return i;
                   }
            return index;
                   //alert(teams[0].getStudent());
        }
        this.removeRoleActivityPair = function(roleId,activityName){
            var index = this.getRoleActivityPairIndex(roleId,activityName);
            this.RolesActivitiesPairs.splice(index,1);
        }

        this.getRolesActivitiesPairs = function(){
            return this.RolesActivitiesPairs;
        }
    }
    catch(e){
        console.log("completeActivitiescondition "+e);
    }
}

function completeExpressionCondition(role,condition,View){
    
    this.role = role;
    this.initCondition = condition;
    this.conditionView = document.createElement("div");

    var tmpNode;
    for(var i=0;i<View.childNodes.length;i++){
      tmpNode = View.childNodes[i].cloneNode(false);
      tmpNode.onclick = View.childNodes[i].onclick;
      for(var j=0;j<View.childNodes[i].childNodes.length;j++){
        var tmp1 = View.childNodes[i].childNodes[j].cloneNode(true);
        tmp1.onclick =  View.childNodes[i].childNodes[j].onclick;
        tmpNode.appendChild(tmp1);
      }
      this.conditionView.appendChild(tmpNode);
    }


    this.getExpressionRole = function(){
        return this.role;
    }
    this.getInitCondition = function(){
        return this.initCondition;
    }
    this.getConditionView = function(){
        return this.conditionView;
    }


}




function new_pair(role,checkbox,activity) {
    
   return function () {pairs(role,checkbox,activity);};

}

function pairs(role,checkbox,activity){
    try{
        if(checkbox.checked){            
            completeActivities.addRoleActivityPair(role,activity);//codeAllagi completeActivities.addRoleActivityPair(role,name);
        }
        else{
            completeActivities.removeRoleActivityPair(role,activity);//completeActivities.removeRoleActivityPair(role,name);
        }
    }
    catch(e){
        alert("error in pairs "+ e);
    }
}