/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ODSPortalIntegration;

import StudentOperationsEJBs.DB_connection;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;

/**
 * REST Web Service
 *
 * @author stylianos
 */
@Path("add_student")
public class Add_students {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of group_students
     */
    public Add_students() {
    }

    /**
     * Retrieves representation of an instance of ODSPortalIntegration.group_students
     * @return an instance of java.lang.String
     */
    /*@GET
    @Produces("application/json")
    public String getScenarios(int groupId) {
        
        JSONObject scenario = new JSONObject();
        //scenario.put("name",);      
        JSONArray scenarios = new JSONArray();
        scenarios.put(scenario);
  

  
        return null;
    }*/
 
    /**
     * PUT method for updating or creating an instance of group_students
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    /**/
 /**
     * PUT method for updating or creating an instance of HelloWorldResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     *//*
    @POST
    @Consumes("application/xml")
    public void putXml(String content) {
        System.out.println("to contet eini a "+content);
    } */
    @POST  
    @Consumes("application/json")
    public void addUser(String user_data) throws Exception{
        DB_connection con = new DB_connection();
        PreparedStatement pstmt = null;        
        try{             
            String user_firstName = "", user_lastName = "", user_email = "",user_sex = "", ODSUsername = "";
            JSONParser parser = new JSONParser();
     
            ContainerFactory containerFactory = new ContainerFactory(){
                public List creatArrayContainer() {
                  return new LinkedList();
                }
                public Map createObjectContainer() {
                  return new LinkedHashMap();
                }                        
            };                
            Map json_user = (Map)parser.parse(user_data, containerFactory);
            Iterator iter_user = json_user.entrySet().iterator();
            while(iter_user.hasNext()){
                Map.Entry entry_user = (Map.Entry)iter_user.next();
                if(entry_user.getValue()!=null){
                    if(entry_user.getKey().equals("user_firstName")){
                        user_firstName = entry_user.getValue().toString();                    
                    } else if(entry_user.getKey().equals("user_lastName")){
                        user_lastName = entry_user.getValue().toString();
                    } else if(entry_user.getKey().equals("user_email")){
                        user_email = entry_user.getValue().toString();
                    } else if(entry_user.getKey().equals("user_name")){
                        ODSUsername = entry_user.getValue().toString();
                    } else{
                        user_sex = entry_user.getValue().toString();
                    }
                }
            }        
            con.getConnection().setAutoCommit(false);            
            pstmt = con.getConnection().prepareStatement("INSERT INTO students (Name, LastName, email, sex, FirstName, ODSUsername) VALUES (?,?,?,?,?,?)");
            pstmt.setString(1,user_email);
            pstmt.setString(2,user_lastName);
            pstmt.setString(3,user_email);
            pstmt.setString(4,user_sex);
            pstmt.setString(5,user_firstName);
            pstmt.setString(6,ODSUsername);
            try{
                pstmt.executeUpdate();                        
                con.getConnection().commit();
            }catch(SQLIntegrityConstraintViolationException e){
                e.printStackTrace();
            }
            
            pstmt = con.getConnection().prepareStatement("INSERT INTO ld_author (name, password, email, realName, lastName) VALUES (?,?,?,?,?)");
            pstmt.setString(1,ODSUsername);
            pstmt.setString(2,ODSUsername);
            pstmt.setString(3,user_email);
            pstmt.setString(4,user_firstName);
            pstmt.setString(5,user_lastName);            
            pstmt.executeUpdate();                        
            try{
                con.getConnection().commit();
            }catch(SQLIntegrityConstraintViolationException e){
                e.printStackTrace();
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }finally {
            pstmt.close();
            con.getConnection().close();                      
        }        
    }
}
