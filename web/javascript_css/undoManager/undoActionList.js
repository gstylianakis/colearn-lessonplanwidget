/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var undoActionList = {
    undoActions : new Array(),

    undoAction : function(){
        
        if(this.undoActions.length==0)
            return;
        var actionPerformed = this.undoActions.pop();
        console.log("actionPerformed is  "+actionPerformed)
        
        if( actionPerformed.actionPerformed=="addNewActivity" ){
            
            deleteActivity(actionPerformed.FunctionParameters.param1,1)
        }
        else if(actionPerformed.actionPerformed=="deleteActivity"){
            var activity = actionPerformed.FunctionParameters.param1;
            var phase = allPhases.getPhase(actionPerformed.FunctionParameters.param2);
            if(phase=="-1")
                return ;
            var activities = phase.getLearningActivitiesArray();
            var annotation,annotationDesc="";
            var annotationgroupEleme = activity.get_Shape().getElementsByTagName("g");
                for(var i=0;i<annotationgroupEleme.length;i++){
                    if(annotationgroupEleme[i].getAttribute("name")!=null)
                        if(annotationgroupEleme[i].getAttribute("name")=="annotation"){
                            annotation = annotationgroupEleme[i];
                            break;
                        }
                }
                if(annotation!=null){
                    annotationDesc = annotation.getAttribute("textContent");
                }
            addNewActivity(activity.getName(),activities,activity.getRolesAssigned(),activity.getDescriptionItem(),activity.getLearningObjectivesItems(),activity.getPrerequisitesItems(),activity.getActivityCompletionFeedbackItem(),activity.getActivityNotifications(),activity.getEnvironement(),activity.getIdentifier(),activity.get_Shape().getAttribute("dragX"),activity.get_Shape().getAttribute("dragY"),annotationDesc)
        }
        console.log("actionList is "+undoActions);
    },
    doActionFollow: function(actionPerformed,FunctionParameters){
        console.log("doActionFollow "+this.undoActions.length);
        this.undoActions.push({'actionPerformed':actionPerformed,'FunctionParameters':FunctionParameters})
        console.log("actionList is "+this.undoActions);
    }
}
