/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var gateway = {
    initJoinGatewayList: function(){
        return new Array();
    },
    initInclusiveOrList: function(){
        return new Array();
    }
};

var join = {
    syncNode: function(name, synch){
        this.name = name;
        this.ObjFrom = new Array();
        this.ObjGoTo = new Array();
        this.shape = synch;   
    this.setName = function(name){
                        this.name = name;
                   }
    this.getName = function(){
        return this.name;
                  }
    this.addObjFrom = function(obj){
                        this.ObjFrom.push(obj);
                   }
    this.addObjGoTo = function(goToObj){
                        this.ObjGoTo.push(goToObj);
                   }

    this.getObjGoTo = function(){
                        return this.ObjGoTo;
                   }
    this.getObjFrom = function(){
                        return this.ObjFrom;
                   }
    this.setObjGoTo = function(list){
        this.ObjGoTo = list
    }
    this.setObjFrom = function(list){
        this.ObjFrom = list
    }
    this.getShape = function(){
                        return this.shape;
                   }
    },
    inclusiveOr: function(name,synch){
        try{
            this.name = name;
            this.ObjFrom = new Array();
            this.ObjGoTo = new Array();//to goto object prepei na exei kai conditions......
            this.shape = synch;


            this.setName = function(name){
                                this.name = name;
                           }
            this.getName = function(){
                return this.name;
                          }
            this.addObjFrom = function(obj){
                                this.ObjFrom.push(obj);
                           }
            this.addObjGoTo = function(goToobjId,lineId){
                                this.ObjGoTo.push(new inclusiveOrGoToObj(goToobjId,lineId));
                           }

            this.getObjGoTo = function(){
                                return this.ObjGoTo;
                           }
            this.getObjFrom = function(){
                                return this.ObjFrom;
                           }
            this.setObjGoTo = function(list){
                this.ObjGoTo = list
            }
            this.setObjFrom = function(list){
                this.ObjFrom = list
            }
            this.getShape = function(){
                                return this.shape;
                           }

        }
        catch(e){
            console.log("inclusiveor "+e);
        }
    }


}

function inclusiveOrGoToObj(goToobjId,lineId,conditions){
    try{
        this.goToobjId = goToobjId;
        this.lineId = lineId;
        this.conditionDiv = null;
        this.getgoToObjId = function(){
            return this.goToobjId;
        }
        this.getlineId = function(){
            return this.lineId;
        }
        this.setConditionDiv = function(div){
            this.conditionDiv = div;
        }
        this.getConditionDiv = function(){
            return this.conditionDiv;
        }
    }
    catch(e){
        console.log("error while creating or-Gateway "+e)
    }
}
function createSyncElement(posX,posY,option){
     try{
        var phase = allPhases.getPhase(document.getElementById('phaseTab').name);
        //var id = phase.getJoinGateways().length+phase.getName()+"-syncPoint";
        var id = null;
        if(option==0)
            id = uniqid();
        else
            id = option;
        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById("vectorGraphicsDoc");
        var synch = document.createElementNS(svgns, 'g');

        synch.setAttribute('id',id);
        synch.setAttribute('ObjType',"syncPoint");
        synch.setAttribute("transform", "translate(" + posX + " " + posY +")");

        var rectangle = document.createElementNS(svgns, 'rect');
        rectangle.setAttribute('x',0);//80
        rectangle.setAttribute('y',0);//-20
        rectangle.setAttribute('fill','#ffffff');
        rectangle.setAttribute('stroke', "#000");
        rectangle.setAttribute("fill-opacity", '1');
        rectangle.setAttribute("stroke-width", '2');
        rectangle.setAttribute('width', 30);
        rectangle.setAttribute('height', 30);
        rectangle.setAttribute("transform", "rotate(-45 15 15)");//-45 100 100

        var path = document.createElementNS(svgns, 'path');
        var p1 = "M15 8 V22 M8 15 H22";//"M22 20 V40 M10 30 H34";
        path.setAttribute("d",p1);
        path.setAttribute("stroke-width","2");
        path.setAttribute("stroke","#000");


        synch.appendChild(rectangle);
        synch.appendChild(path);
        synch.setAttribute('dragX',(posX));//(10*size)
        synch.setAttribute('dragY',(posY));//(10*size)
        
        synch.vTranslate = [posX,posY];
     
        /*synch.addEventListener('mousedown', dragger,false);
        synch.addEventListener('mouseup', onMouseUp,false);
        synch.addEventListener('mousemove', onMouseMove,false);
        synch.addEventListener('mouseover', onMouseOver,false);//auto xalaei tin simperifora kai snexeia mou "feuge" otan to kinw grigora dekais aristera
        */
       synch.addEventListener('mousedown', Grab,false);
       synch.addEventListener('touchstart', Grab,false);
       
       synch.addEventListener('mouseup', Drop,false);
       synch.addEventListener('touchend', Drop,false);
       
       synch.addEventListener('mousemove', Drag,false);
       synch.addEventListener('touchmove', Drag,false);
       
       synch.addEventListener('mouseover', onMouseOver,false);
       synch.addEventListener('touchend', onMouseOver,false);
       
       synch.addEventListener('mouseout',function(){
           this.setAttribute('mousemoveControl',"false");
                    this.childNodes[0].style.stroke="#000";
                } ,false);
       root.appendChild(synch);

//dimiourgise ena orNode antikeimeno kai valto stin phase pou vriskesai
        var syncNode = new join.syncNode(id,synch);
        phase.getJoinGateways().push(syncNode);

        /*var xlinkns = "http://www.w3.org/1999/xlink";
        sync.setAttributeNS(xlinkns, "href", "#synch" );
        */
        select();
        return syncNode;
     }
     catch(e){
         console.log("error while creating and-Gateway "+e);
     }
}

function createInclusiveOrGateway(positionX,positionY,option){
     try{
  
        var phase = allPhases.getPhase(document.getElementById('phaseTab').name);
        //var id = phase.getInclusiveOrGateways().length+phase.getName()+"-inclusiveOrPoint";
        var id = null;
        if(option==0)
            id = uniqid();
        else
            id = option;

        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById("vectorGraphicsDoc");
        //var svgElem = document.createElementNS(svgns, 'svg');
        //svgElem.setAttribute('x',80);
        //svgElem.setAttribute('y',-20);
  
        var synch = document.createElementNS(svgns, 'g');
        synch.setAttribute("transform", "translate(" + positionX + " " + positionY +")");
        synch.setAttribute('id',id);
        synch.setAttribute('ObjType',"inclusiveOrPoint");        
    
        var rectangle = document.createElementNS(svgns, 'rect');
        rectangle.setAttribute('x',0);
        rectangle.setAttribute('y',0);

        rectangle.setAttribute('fill','#ffffff');
        rectangle.setAttribute('stroke', "#000");
        rectangle.setAttribute("fill-opacity", '1');
        rectangle.setAttribute("stroke-width", '2');
        rectangle.setAttribute('width', 30);
        rectangle.setAttribute('height', 30);        

        var circle = document.createElementNS(svgns, 'circle');
        circle.setAttribute("r","10");
        circle.setAttribute("cx","15");
        circle.setAttribute("cy","15");
        circle.setAttribute("stroke-width","1");
        circle.setAttribute("stroke","#000");
        circle.setAttribute('fill','#ffffff');

        synch.appendChild(rectangle);
        synch.appendChild(circle);
        rectangle.setAttribute("transform", "rotate(-45 15 15)");//45 10 10
        synch.setAttribute('dragX',positionX);//(10*size)
        synch.setAttribute('dragY',positionY);//(10*size)
        synch.vTranslate = [positionX,positionY];
        /*synch.addEventListener('mousedown', dragger,false);
        synch.addEventListener('mouseup', onMouseUp,false);
        synch.addEventListener('mousemove', onMouseMove,false);
        synch.addEventListener('mouseover', onMouseOver,false);//auto xalaei tin simperifora kai snexeia mou "feuge" otan to kinw grigora dekais aristera*/
       synch.addEventListener('mousedown', Grab,false);
       synch.addEventListener('touchstart', Grab,false);
       
       synch.addEventListener('mouseup', Drop,false);
       synch.addEventListener('touchend', Drop,false);
       
       synch.addEventListener('mousemove', Drag,false);
       synch.addEventListener('touchmove', Drag,false);
       
       synch.addEventListener('mouseover', onMouseOver,false);
       synch.addEventListener('touchend', onMouseOver,false);
       
       synch.addEventListener('mouseout',function(){
           this.setAttribute('mousemoveControl',"false");
                    this.childNodes[0].style.stroke="#000";
                } ,false);
       root.appendChild(synch);

//dimiourgise ena syncNode antikeimeno kai valto stin phase pou vriskesai        
        var orNode = new join.inclusiveOr(id,synch);
        
        phase.getInclusiveOrGateways().push(orNode);

        /*var xlinkns = "http://www.w3.org/1999/xlink";
        sync.setAttributeNS(xlinkns, "href", "#synch" );


    */
        select();
     }
     catch(e){
         console.log("createInclusiveOrGateway "+e);
     }
}
    function deleteAllGateways(phase){
       try{
           var gateway=null;
           //delete from panel
           for(var i=0;i<phase.getJoinGateways().length;i++){
               gateway = phase.getJoinGateways()[i];
               deleteActivityFromPanel(gateway.getName());
           }
           for(i=0;i<phase.getInclusiveOrGateways().length;i++){
               gateway = phase.getInclusiveOrGateways()[i];
               deleteActivityFromPanel(gateway.getName());
           }
           //delete from memory
            phase.setInclusiveOrGateways(null);
            phase.setJoinGateways(null);
       }
       catch(e){
           alert(e)
       }
    }
    function deleteGateway(svgGateway){
       try{
          
           if (!confirm("You are going to delete gateway ")==true)
                return;

           var svgDoc = document.getElementById('svgPanel').contentDocument;
           var root = svgDoc.getElementById("vectorGraphicsDoc");
           var textPath = null;

           var cur_phase = allPhases.getPhase(document.getElementById('phaseTab').name);
           var gatewayList = [];

           if(svgGateway.getAttribute("ObjType")=="syncPoint")
               gatewayList = cur_phase.getJoinGateways();
           else if(svgGateway.getAttribute("ObjType")=="inclusiveOrPoint")
               gatewayList = cur_phase.getInclusiveOrGateways();
           else
               throw "object not found";

           var id = svgGateway.getAttribute("id");           
           
           var gatewayIndex = -1;
           var gateway = null;
           for(var i=0;i<gatewayList.length;i++){
               if(gatewayList[i].getName()==id){
                   gatewayIndex = i;
                   gateway = gatewayList[i];
                   break;
               }
           }
           //remove children
            if(gateway!=null){
                gateway.setObjFrom([]);
                gateway.setObjGoTo([]);
           }
           //remove from phase gateways list
           if(gatewayIndex!=1)
                gatewayList.splice(gatewayIndex,1);
           //remove from connection list
            var connections = cur_phase.getLearningActivitiesArray().getConnectionsArray();
            var tmpConnections = connections.slice();
            for(i=0;i<tmpConnections.length;i++){
                var con = tmpConnections[i];
                if(con.getAttribute("from")==id){// || con.getAttribute("to")==e.currentTarget.getAttribute("id")){
                    removeConnection(con,connections);
                    if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta eksafanizei
                        textPath = root.getElementById("textRefered_"+con.getAttribute("id"));
                        if(textPath!=null)
                            textPath.parentNode.removeChild(textPath);
                    }
                }
                if(con.getAttribute("to")==id){
                    removeConnection(con,connections);
                    if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta eksafanizei
                        textPath = root.getElementById("textRefered_"+con.getAttribute("id"));
                        if(textPath!=null)
                            textPath.parentNode.removeChild(textPath);
                    }
                }
            }
            
           
        //delete from svg panel
            deleteActivityFromPanel(id);


       }
       catch(e){
           console.log("error while trying to delete gateway "+e);
       }
    }

    function indexOfLineInInclusiveOrList(objGoTo,lineId){
        var id = null;
        for(var i=0;i<objGoTo.length;i++){
            id = objGoTo[i][1];//giati periexetai array apo to type tou object pou kataligei i grammi kai to id tis grammis
            if(id==lineId)
                return i;
        }
        return -1;
    }