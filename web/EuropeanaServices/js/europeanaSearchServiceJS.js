function setPaging(){
    try{
    var res=document.getElementById("totalPages");
    var mpage=document.getElementById("pageNum");
    if(!res || res==null){
        return;
    }
                if(!mpage || res==null){
                    mpage=1;
                }else{
                    mpage=document.getElementById("pageNum");
                }
                var ctr=res.innerHTML;
                if(ctr>0)
                    $("#resultsPaging,#resultsPagingFooter").paginate({
                        count 		: ctr,
                        start 		: mpage.innerHTML,
                        display     : 15,
                        border					: true,
                        border_color			: '#fff',
                        text_color  			: '#fff',
                        background_color    	: 'black',
                        border_hover_color		: '#ccc',
                        text_hover_color  		: '#000',
                        background_hover_color	: '#fff',
                        images: false,
                        mouse: 'press',
                        onChange  : function(page){

                            getData(page,document.getElementById("searchTerm").innerHTML);
                            $('#dialog-form').scrollTop(0);
                            //                            $('#search').focus();
                            //                            $('#results').focus();
                        }
                });
    }
    catch(e){
        alert("error "+e)
    }
            }


            function divclick(d){
                if(!d){return};
                var data={
                    image:$(d).find('table').find('td')[0].innerHTML
                    ,title:$(d).find('table').find('td')[1].innerHTML
                    ,dsc:$(d).find('table').find('td')[2].innerHTML
                    ,link:$(d).find('table').find('td')[3].innerHTML
                }
                var type = $( "#dialog-form" ).data('type');
                var option=""
                if(type=="Description"||type=="Prerequisites")
                    option="activity";
               
                
                $("#URL"+type).attr('checked',"true");
                $("#URL"+type+"Content").val(data.link);
                $("#"+option+type+"Title").val(data.title);
                defineItemsProperties(type,option);
                $("#URL"+type).removeAttr('checked');
                $("#createNew"+type).attr('checked',"true");
                $("#URL"+type+"Content").val("");
                $("#"+option+type+"Title").val("");
                showCompletionPrereq("createNew"+type,type);
                $( "#dialog-form" ).dialog("close");

                //                $(d).copy();
            }
            $(function() {

            try{
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    height: 500,
                    width: 700,
                    modal: false,                    
                    zIndex: 10000,

                    buttons: {
                        Cancel: function() {
                            $( this ).dialog( "close" );

                        }
                    }

                });

                $( "#searchButton" )               
                .click(function() {
                    try{                    
                    $( "#dialog-form" ).data('type','Description')
                    $( "#dialog-form" ).dialog( "open");


                    
                    }
                    catch(e){
                        alert(e)
                    }

                    //                    setPaging();
                });
                $( "#searchButtonactivityLearningObjectives" ).click(function() {
                    try{
                        $( "#dialog-form" ).data('type','activityLearningObjectives')
                        $( "#dialog-form" ).dialog( "open");
                    }
                    catch(e){
                        alert(e)
                    }

                    //                    setPaging();
                });
                $( "#searchButtonPrerequisites" ).click(function() {
                    try{
                        $( "#dialog-form" ).data('type','Prerequisites')
                        $( "#dialog-form" ).dialog( "open");
                    }
                    catch(e){
                        alert(e)
                    }
                });
                $("#searchButtonLearningObject").click(function() {
                    try{
                        $( "#dialog-form" ).data('type','LearningObject')
                        $( "#dialog-form" ).dialog( "open");
                    }
                    catch(e){
                        alert(e)
                    }
                });
                $( "#searchButtonactivityCompletion" ).click(function() {
                    try{
                        $( "#dialog-form" ).data('type','activityCompletion')
                        $( "#dialog-form" ).dialog( "open");
                    }
                    catch(e){
                        alert(e)
                    }

                });

                $("#searchButtonPhaseDescription" ).click(function() {
                    try{
                        $( "#dialog-form" ).data('type','PhaseDescription')
                        $( "#dialog-form" ).dialog( "open");
                    }
                    catch(e){
                        alert(e)
                    }

                });
                $( "#searchButtonstructureLearningObject" ).click(function() {
                    try{
                        $( "#dialog-form" ).data('type','structureLearningObject')
                        $( "#dialog-form" ).dialog( "open");
                    }
                    catch(e){
                        alert(e)
                    }

                });
                $("#searchButtonstructureInformation" ).click(function() {
                    try{
                        $( "#dialog-form" ).data('type','structureInformation')
                        $( "#dialog-form" ).dialog( "open");
                    }
                    catch(e){
                        alert(e)
                    }

                });
                $( "#search" )                
                .click(function(e) {

                    getData(-1, $('#terms').val());
                    //                    setPaging();
                });


            }
            
            catch(e){
                alert(e)
            }
            });


            function Display_Load(option)
            {
                try{
                $("#loading"+option).fadeIn(900,0);
                $("#loading"+option).html("<img src='EuropeanaServices/js/bigLoader.gif'/>");
                }
                catch(e){
                    aelrt(e)
                }
            }
            //Hide Loading Image
            function Hide_Load(option)
            {
                $("#loading"+option).fadeOut('slow');
                if(option=="")
                    setPaging();
            }
            function Hide_Load_Fast(option)
            {
                $("#loading"+option).fadeOut(100);
                if(option=="")
                    setPaging();
            }

            function checkTermV(term){if(term.replace( /\s/g ,"").length<=2){return false;}return true;}
            var errorMsg="<div class='ui-state-error ui-corner-all' style='padding: 0pt 0.7em; margin:5px 0;'><p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span> Unable to fulfill your request due to a malformed query syntax.                            <strong>Please</strong> try another search.                             </p></div>"
            function getData(page,term){
                Display_Load("");
                if(!checkTermV(term)){
                    $("#results").html(errorMsg);
                    Hide_Load_Fast("");
                    return;}

                $("#results").load("EuropeanaServices/search.jsp?page="+encodeURIComponent(page)+"&term="+encodeURIComponent(term),function() {
                    Hide_Load("");
                });



            }

