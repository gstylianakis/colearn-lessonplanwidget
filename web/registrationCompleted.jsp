
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="enroll" scope="page" class="CampusOperationsJavaBeans.CampusOperations"/>
<%

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.
  
    String success = "";
    
    
    try{
        String name = request.getParameter("name");
        String username =  request.getParameter("username");
        String psw = request.getParameter("password");
        String lastName =  request.getParameter("lastName");
        String email =  request.getParameter("email");
        String gender = request.getParameter("gender");

        success = enroll.enroll_Author(name, lastName, username, psw, email, gender);
        
    }
    catch(Exception e){
        out.println("error while registring");
        System.out.println("error on registration "+e);
    }
%>
<html>
    <head>
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <link href="javascript_css/registerForm.css" rel="stylesheet" type="text/css" media="screen" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>registration completed</title>
    </head>
    <body>
        <div class="bodywrapRegister">
            <div class="registerDiv" style="height: 200px;width: 450px">
                <h1><%=success%></h1>
                <div>
                    <button class="button" style="margin-top:100px" onclick="window.location = './index.jsp'; return false">Login>></button>
                    
                </div>
            </div>
            
        </div>

    </body>
</html>
