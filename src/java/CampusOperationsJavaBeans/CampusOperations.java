/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package CampusOperationsJavaBeans;

import CampusOperations.AvailableServices;
import StudentOperationsEJBs.DB_connection;
import StudentOperationsEJBs.StudentBrowsing;
import java.sql.Connection;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.util.Vector;
/**
 *
 * @author Administrator
 */
public class CampusOperations {

    private StudentOperationsEJBs.StudentBrowsing campusBrowsing;
    private AvailableServices services;

    public CampusOperations()throws Exception {
        try{
            Context context = new InitialContext();
            Context context2 = new InitialContext();
            campusBrowsing = (StudentBrowsing) context.lookup(StudentBrowsing.class.getName());
            services = (AvailableServices)context2.lookup(AvailableServices.class.getName());

        }
        catch(NamingException e){
            throw e;
        }
        catch(Exception ex){
            throw ex;
        }
    }

    public Vector student_searching(String query)throws Exception{
        
        try{
            
            return campusBrowsing.student_searching(query);
        }
        catch(Exception e){
            System.out.println("eskase sto student searching ");
            throw e;
        }
    }

    public boolean student_exists(String name)throws Exception{
        try{
            return campusBrowsing.student_exist(name);
        }
        catch(Exception e){
            throw e;
        }
    }

    public Vector available_services()throws Exception{
        try{
            return services.getAvailableCommunicationServices();
        }
        catch(Exception e){
            throw e;
        }
    }

    public String enroll_Author(String name, String lastName, String username, String psw, String email, String gender)throws Exception{
        try{
            return campusBrowsing.enroll_student(name, lastName, username, psw, email, gender);

        }
        catch(Exception e){
            throw e;
        }
    }
    public String getStudentGroups(String username)throws Exception{
        try{
            System.out.println("o xristis einia o "+username);
            return campusBrowsing.getStudentParticipationGroups(username);
        }
        catch(Exception e){
            throw e;
        }
    }

    
}
