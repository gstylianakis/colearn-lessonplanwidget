<%-- 
    Document   : register
    Created on : 27 Μαρ 2012, 1:07:00 μμ
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader ("Expires", 0);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Register Page</title>
        <link href="javascript_css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="javascript_css/jquery-1.4.1.min.js"></script>

        <link href="javascript_css/registerForm.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="javascript_css/jqtransformplugin/jqtransform.css" rel="stylesheet" type="text/css" media="screen" />
        
        <script type="text/javascript">

            function redirect(location){
                if(location=="./index.jsp")
                    window.location = location;
                else{
                    var failed = 0;
                    $("input").each(function(){
                        if($(this).val()==""){
                            $(this).siblings("label").css("visibility", "visible")
                            failed = -1;
                        }
                        else
                            $(this).siblings("label:eq(1)").css("visibility", "hidden")
                           
                   })
                }
                if(failed!=0){
                    $("#password").val("");
                    $("#confirmPassword").val("")
                }

                if($("#password").val()!=$("#confirmPassword").val()){
                    $("#retypePsw").css("visibility","visible")
                    failed = -1;
                }
                else
                    $("#retypePsw").css("visibility","hidden")

                if(failed==0)
                    document.registerForm.submit();
            }
        </script>
    </head>
    <body>
        <form id="registerForm" class="jqtransform" action="./registrationCompleted.jsp" method="post">
            <div class="bodywrapRegister">
                <div class="registerDiv">
                    <h1>Registration</h1>
                    <fieldset class="registrationRow1">
                        <legend>Account Details</legend>                                                
                        <ul>
                            <li>
                                <div>
                                    <label> Username: </label>
                                    <input class="text ui-widget-content ui-corner-all"  type="text" name="username" id="username" style="width:250px;"/>
                                    <label style="color:red;visibility: hidden"> *required </label>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <label> Password: </label>
                                    <input class="text ui-widget-content ui-corner-all"  type="password" name="password" id="password" style="width:250px;"/>
                                    <label style="color:red;visibility: hidden"> *required </label>
                                </div>
                            </li>
                            <li>
                                 <div>
                                    <label> Confirm Password: </label>
                                    <input class="text ui-widget-content ui-corner-all"  type="password" name="terms" id="confirmPassword" style="width:250px;"/>
                                    <label style="color:red;visibility: hidden"> *required </label>
                                 </div>
                            </li>
                            <li>
                                <div style="margin-right: 200px">
                                    <label style="color:red;visibility: hidden;" id="retypePsw"> *retype password </label>
                                </div>
                            </li>

                        </ul>
                    </fieldset>
                     <fieldset class="registrationRow1">
                        <legend>Personal Information</legend>
                        <ul>
                            <li>
                                <div>
                                    <label> First Name: </label>
                                    <input class="text ui-widget-content ui-corner-all"  type="text" name="name" id="name" style="width:250px;"/>
                                    <label style="color:red;visibility: hidden"> *required </label>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <label> Last Name: </label>
                                    <input class="text ui-widget-content ui-corner-all"  type="text" name="lastName" id="lastName" style="width:250px;"/>
                                    <label style="color:red;visibility: hidden"> *required </label>
                                </div>
                            </li>
                            <li>
                                 <div>
                                    <label> Email: </label>
                                    <input class="text ui-widget-content ui-corner-all"  type="text" name="email" id="email" style="width:250px;"/>
                                    <label style="color:red;visibility: hidden"> *required </label>
                                 </div>
                            </li>
                            <li>
                                 <div>
                                     <label> Gender: </label>
                                     <input class="text ui-widget-content ui-corner-all" type="radio" value="male" name="gender" id="male"  checked/>Male
                                     <input class="text ui-widget-content ui-corner-all" type="radio" value="female" name="gender" id="female"/>
                                     <span style="margin-right: 124px">Female</span>
                                     <label style="color:red;visibility: hidden"> *required </label>
                                 </div>
                            </li>
                        </ul>
                    </fieldset>
                    <div style="margin-right: 100px">
                        <span style="margin:0px 0px 20px 20px;color:#B8B8B8 ">*All fields required</span>
                        <button class="button" onclick="redirect('./registrationCompleted.jsp'); return false">Register>></button>
                        <button class="button" onclick="redirect('./index.jsp'); return false">Cancel</button>
                    </div>
                </div>

            </div>
        </form>
    </body>
</html>
