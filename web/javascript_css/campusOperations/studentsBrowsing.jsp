<%@page  contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.json.JSONObject"%>
<html>
<head><title>jsp direct</title></head>
<%@ page language="java" import="java.sql.*,java.util.Vector" %>
<jsp:useBean id="campus" scope="page" class="CampusOperationsJavaBeans.CampusOperations"/>
<body>
<h1>Students</h1>

<%
    try{
        String criteria = request.getParameter("query");
        
        
        System.out.println("to criteria eina "+criteria);

        Vector students = campus.student_searching(criteria);
        String res = "<table class=\"studentsResultTable\" cellpadding=\"0\" cellspacing=\"0\">";
        if(students.size()==0)
            out.print("not available students");
         else{
            String count = "even";
            res += "<tr><th>Name: </th><th>LastName: </th><th>Email: </th></tr>";
            for(int i=0;i<students.size();i++){
                 if((i%2)==0)
                     count="even";
                 else
                     count="odd";
                 Vector student = (Vector)students.get(i);
                 res += "<tr border=\"0\" class="+count+">";
                 for(int k=0;k<student.size();k++){
                     
                     res += "<td class="+count+">"+(String)student.get(k)+"</td>";

                 }
                 res += "</tr>";
             }
         }
         res += "</table>";
         out.print(res);
    }
    catch(Exception e){
        out.print(e);
    }



%>

</body>
</html>