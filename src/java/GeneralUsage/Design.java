/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GeneralUsage;

import StudentOperationsEJBs.DB_connection;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Administrator
 */
public class Design {
    private static Properties configFile;
    private static String uploadedFilesDir;

    public Design(){
        try {
            configFile = new Properties();
            String parentPath = Design.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            parentPath = parentPath.substring(0,parentPath.indexOf("Design"));  
            System.out.println(parentPath);
            configFile.load(new FileInputStream(parentPath+"config.properties"));            
            uploadedFilesDir = configFile.getProperty("uploadFileDir");            

        } catch (Exception ex) {
            System.out.println("edw "+ex);
        }
    }
    public String getSaveFileDir(){
        return uploadedFilesDir;
    }
    public void writeToFile(String ld,String dir)throws Exception{
        BufferedWriter out = null;
        /*
        FileOutputStream fop = null;
        File file = new File(uploadedFilesDir+dir+"/imsmanifest.xml");
        fop = new FileOutputStream(file);
        */
        try{
            boolean exists = (new File(uploadedFilesDir+dir)).exists();
            if (!exists)
              new File(uploadedFilesDir+dir).mkdir();
            ////
            /*if(!file.exists())
                new File(uploadedFilesDir+dir).mkdir();
            */
            /////
            //FileWriter fstream = new FileWriter(uploadedFilesDir+dir+"/imsmanifest.xml");
            //out = new BufferedWriter(fstream);
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(uploadedFilesDir+dir+"/imsmanifest.xml"), "UTF8"));
            out.write(ld);
	    //////
            /*byte[] contentInBytes = ld.getBytes();
	    fop.write(contentInBytes);
	    fop.flush();
	    fop.close();
            */
            //////
    //Close the output stream

        }
        catch(Exception e){
            throw e;
        }
        finally{
            out.close();
            /////
            /*if (fop != null)
                fop.close();
            */
            ////
        }
    }

    public void zipFile(ArrayList source,String target,String manifestId)throws Exception{
        byte[] buf = new byte[1024];
        try{
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(this.uploadedFilesDir+target));
            // Compress the files
            for (int i=0; i<source.size(); i++) {
                FileInputStream in = new FileInputStream(this.uploadedFilesDir+manifestId+"/"+(String)source.get(i));
            // Add ZIP entry to output stream.
            File file = new File((String)source.get(i)); //"Users/you/image.jpg"
            out.putNextEntry(new ZipEntry(file.getName()));
            //out.putNextEntry(new ZipEntry(source[i]));
            // Transfer bytes from the file to the ZIP file
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            // Complete the entry
            out.closeEntry();
            in.close();
        }
        // Complete the ZIP file
        out.close();
        
        }
        catch(Exception e){
            throw e;
        }
    }
    public Document StringToXML(String xmlRecords)throws Exception{
        try{
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();

            is.setCharacterStream(new StringReader(xmlRecords));

            return db.parse(is);
        }
        catch(Exception e){
            throw e;
        }
    }
    public void createTextResources(Document resourcesXml,String dir){
        try{
            NodeList resources = resourcesXml.getElementsByTagName("resource");
            String filename = "",fileContent = "";
            FileWriter fstream = null;
            BufferedWriter out = null;
            boolean exists = (new File(uploadedFilesDir+dir)).exists();
            for (int i = 0; i < resources.getLength(); i++) {
                Element resource = (Element) resources.item(i);
                Element content = (Element)resource.getFirstChild();
                if(content==null)
                    continue;
                if(content.getAttribute("type").compareTo("text")==0){
                    if (!exists)
                        new File(uploadedFilesDir+dir).mkdir();

                    filename = uploadedFilesDir+dir+"/"+content.getAttribute("identifier")+".html";                  
                    fileContent = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body><div>"+content.getFirstChild().toString()+"</div></body>";
                    //fstream = new FileWriter(filename);
                    //out = new BufferedWriter(fstream);
                    out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF8"));
                    out.write(fileContent);
                    out.close();
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    public void saveResources(Document resourcesXMLFile,String manifestId)throws Exception{        
        Statement stmt = null;
        DB_connection connection = null;   
        try{

            NodeList resources = resourcesXMLFile.getElementsByTagName("resource");
            connection = new DB_connection();            
            stmt = connection.getConnection().createStatement();
            //String com = "insert into lessonplan_widget.resourcesfiles (name,type,content) values ";
            String deleteCom = "delete from lessonplan_widget.manifestresourcesmap where manifestId='"+manifestId+"'";
            String com2 = "insert into lessonplan_widget.manifestresourcesmap (manifestId,resourceId,resourceContent) values ";
            boolean exec = false;
            String identifier="";
            String filename = "";
            String fileContent = "";
            for (int i = 0; i < resources.getLength(); i++) {
                fileContent = "";
                Element resource = (Element) resources.item(i);
                Element content = (Element)resource.getFirstChild();
                if(content==null)
                    continue;
                filename = content.getFirstChild().toString();
                identifier = content.getAttribute("identifier");

                if(content.getAttribute("type").compareTo("webcontent")==0)
                    identifier = content.getAttribute("identifier");
                else if(content.getAttribute("type").compareTo("text")==0){
                    filename = identifier+".html";
                    fileContent = content.getFirstChild().toString();
                }
                else
                    identifier = content.getAttribute("href");
                //com += "('"+identifier+"','text','"+content.getFirstChild().toString()+"')";
                exec = true;
                fileContent = fileContent.replaceAll("'","\\\\'");
                
                com2 += "('"+manifestId+"','"+filename+"','"+fileContent+"')";
                if(i<(resources.getLength()-1)){
                    //com+=",";
                    com2+=",";
                }
                //writeToFile(content.getFirstChild().toString(),"C:/"++".html");
            }
         
            stmt.executeUpdate(deleteCom);
            if(exec){
         
                stmt.executeUpdate(com2);
            }            
        }
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connection.close();
        }

    }
    
    public void saveManifestToDB(String manifest,String id,String metadata,String graphicDetails,String author,String title, String lesson_subject)throws Exception{        
        Statement stmt = null;
        DB_connection connection = null;                    
        try{
            connection = new DB_connection();            
            stmt = connection.getConnection().createStatement();
            String com = "insert into lessonplan_widget.manifestfiles (name,type,content,metadata,graphicRepresantation,author,manifestTitle,lessonSubject) values ";
            com += "('"+id+"','xml','"+manifest+"','"+metadata+"','"+graphicDetails+"','"+author+"','"+title+"','"+lesson_subject+"') ON DUPLICATE KEY UPDATE content='"+manifest+"', metadata='"+metadata+"',graphicRepresantation='"+graphicDetails+"',manifestTitle='"+title+"',lessonSubject='"+lesson_subject+"'";
   
            stmt.executeUpdate(com);

        }
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connection.close();
        }
    }
    public void saveZip(ZipOutputStream manifest,String id)throws Exception{
        Statement stmt = null;
        DB_connection connect = new DB_connection();
        try{                        
            stmt = connect.getConnection().createStatement();
            String com = "insert into lessonplan_widget.manifestfiles (name,type,content) values ";
            com += "('"+id+"','xml','"+manifest+"')";
            stmt.executeUpdate(com);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connect.close();
        }
    }
    /*public void saveGraphicDesign(String graphicDetails, String manifetsId)throws Exception{
        Connection con = null;
        try{
            DB_connection connect = new DB_connection();
            con = connect.connect("lessonplan_widget");
            Statement stmt = con.createStatement();
            String com = "insert into lessonplan_widget.graphicld (manifestID,graphicLD) values ";
            com += "('"+manifetsId+"','"+graphicDetails+"')";
       
            stmt.executeUpdate(com);
        }
        catch(Exception e){
            throw e;
        }
        finally{
            con.close();
        }
    }*/
    public String[] loadManifest(String ldname)throws Exception{
        Statement stmt = null;
        DB_connection connection = new DB_connection();
        try{
            String result[] = {"",""};            
            stmt = connection.getConnection().createStatement();
            String query = "SELECT content,author FROM lessonplan_widget.manifestfiles where name='"+ldname+"'";

            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {

                result[0] = (rs.getString(1));          
                result[1] = (rs.getString(2));                              
                return result;
            }            
            return null;
        }
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connection.close();
        }

    }
    public String loadGraphicDetails(String manifestName)throws Exception{
        DB_connection connect = new DB_connection();
        Statement stmt = null;
        try{
            stmt = connect.getConnection().createStatement();
            String query = "SELECT graphicRepresantation FROM lessonplan_widget.manifestfiles where name='"+manifestName+"'";            
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {

                /*byte[] buf ;
                long len;

    // Open the XML Message (BLOB) field in the ResultSet
                Blob graphic = rs.getBlob("graphicRepresantation");
                len = graphic.length();
                buf = graphic.getBytes(1, (int) len);
                
    // Write contents of the BLOB to the output file
                return new String(buf);*/
                return rs.getString(1);
            }
            
            return null;
        }
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connect.close();
        }

    }
    public String getManifestTextResources(String manifestId)throws Exception{
        DB_connection connect = new DB_connection();
        Statement stmt = null;
        try{

            //String resources = "{'textResources':[{";
            String resources = "{";
            String query = "SELECT resourceId,resourceContent FROM lessonplan_widget.manifestresourcesmap where manifestId='"+manifestId+"' and manifestresourcesmap.resourceContent!=''";
            stmt = connect.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String id = "";
            int count=0;
            while (rs.next()) {
                if(count!=0)
                    resources += ",";
                id = rs.getString(1).substring(0,rs.getString(1).indexOf(".html"));
                id = id.replace("-","");
                String tmpContent = rs.getString(2);
                tmpContent = tmpContent.replaceAll("'","\\\\'");
                tmpContent = tmpContent.replaceAll("\n","\\\\n");
                resources += "'"+id+"':'"+tmpContent+"'";
                count++;
            }           
            return resources+"}";
            //return resources+"}]}";
        }
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connect.close();
        }
    }
    public ArrayList getManifestExternalResources(String manifestId)throws Exception{
        DB_connection connect = new DB_connection();
        Statement stmt = null;
        try{
            
            ArrayList resourcesPath = new ArrayList();
            String query = "SELECT resourceId FROM lessonplan_widget.manifestresourcesmap where manifestId='"+manifestId+"'";
            stmt = connect.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                resourcesPath.add(rs.getString(1));
            }
            return resourcesPath;
        }
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connect.close();
        }
    }
    public void deleteManifest(String id) throws Exception{  
        DB_connection connect = new DB_connection();
        Statement stmt = null;
        try{ 
            String query = "Delete From lessonplan_widget.manifestfiles where name='"+id+"'";
            stmt = connect.getConnection().createStatement();
            stmt.executeUpdate(query);
        }catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            connect.close();
        }
    }
    /*public String getManifestFilesResources(String manifestId)throws Exception{
        Connection con = null;
        Statement stmt = null;
        try{

            //String resources = "{'textResources':[{";
            String resources = "{";
            String query = "SELECT resourceId,resourceContent FROM lessonplan_widget.manifestresourcesmap where manifestId='"+manifestId+"' and manifestresourcesmap.resourceContent!=''";
            //DB_connection connect = new DB_connection();
            con = DB_connection.connect("lessonplan_widget");
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String id = "";
            int count=0;
            while (rs.next()) {
                if(count!=0)
                    resources += ",";
                id = rs.getString(1).substring(0,rs.getString(1).indexOf(".html"));
                id = id.replace("-","");
                resources += "'"+id+"':'"+rs.getString(2)+"'";
                count++;
            }
            return resources+"}";
            //return resources+"}]}";

        }
        catch(Exception e){
            throw e;
        }
        finally{
            stmt.close();
            con.close();
        }
    }*/
}
