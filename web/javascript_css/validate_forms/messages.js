// form validation function //
function validate(form, parentDiv,students) {
  try{
      var name = form.team_name.value;
      //var email = form.email.value;
      //var gender = form.gender.value;
      //var message = form.message.value;
      var nameRegex = /^[a-zA-Z]+(([\'\,\.\-,_, ])?[a-zA-Z0-9]*)*$/;
      //var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
      //var messageRegex = new RegExp(/<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[^'">\s]+))?)+\s*|\s*)\/?>/gim);
      if(name == "") {
        inlineMsg('team_name','You must enter team name',2,parentDiv,1);
        return false;
      }
      if(!name.match(nameRegex)) {
        inlineMsg('team_name','You have entered an invalid name',2,parentDiv,1);
        return false;
      }
      
      for(var i=1;i<students+1;i++){
         var studentName = "student_"+i;
         var studentRole = "student_role"+i;
        
         if(form.elements[studentName].value == ""){
            inlineMsg(form.elements[studentName],'You must enter student name',2,parentDiv,2);
            return false;
         }

         /*if(checkExists(form.elements[studentName].value)){
            inlineMsg(form.elements[studentName],'You must enter student name',2,parentDiv,2);
            return false;
         }*/
         if(form.elements[studentRole].value == ""){
            inlineMsg(form.elements[studentRole],'You must enter student role',2,parentDiv,2);
            return false;
         }
         //checkExists(form.elements[studentName].value);
      }
      /*if(email == "") {
        inlineMsg('email','<strong>Error</strong><br />You must enter your email.',2);
        return false;
      }
      if(!email.match(emailRegex)) {
        inlineMsg('email','<strong>Error</strong><br />You have entered an invalid email.',2);
        return false;
      }
      if(gender == "") {
        inlineMsg('gender','<strong>Error</strong><br />You must select your gender.',2);
        return false;
      }
      if(message == "") {
        inlineMsg('message','You must enter a message.');
        return false;
      }
      if(message.match(messageRegex)) {
        inlineMsg('message','You have entered an invalid message.');
        return false;
      }*/
      return true;
  }
  catch(e){
      alert(e);
      return false;
  }
}

// START OF MESSAGE SCRIPT //

var MSGTIMER = 20;
var MSGSPEED = 5;
var MSGOFFSET = 3;
var MSGHIDE = 3;

// build out the divs, set attributes and call the fade function //
function inlineMsg(target,string,autohide,parentDiv,option) {
    try{
      var msg;
      var msgcontent;
      if(!document.getElementById('msg')) {
        msg = document.createElement('div');
        msg.id = 'msg';

        msgcontent = document.createElement('div');
        msgcontent.id = 'msgcontent';

       
        msg.appendChild(msgcontent);
        msg.style.filter = 'alpha(opacity=0)';
        msg.style.opacity = 0;
        msg.alpha = 0;
      } else {
        msg = document.getElementById('msg');
        msgcontent = document.getElementById('msgcontent');
      }
      document.getElementById(parentDiv).appendChild(msg);
   console.log("to parent div einai "+parentDiv);
      msgcontent.innerHTML = string;
      msg.style.display = 'block';      
      var targetdiv;
      if(option==1)
          targetdiv = document.getElementById(target);
      else if(option == 2)
          targetdiv = target;

      
        targetdiv.focus();
     
      var targetwidth = targetdiv.offsetWidth;    
      var topposition = topPosition(targetdiv) - topPosition(document.getElementById(parentDiv)) ;//topPosition(targetdiv) - ((msgheight - targetheight) / 2);
      var leftposition = (leftPosition(targetdiv) - leftPosition(document.getElementById(parentDiv))) + targetwidth + MSGOFFSET;

      msg.style.top = topposition + 'px';
      msg.style.left = leftposition + 'px';
      clearInterval(msg.timer);
      msg.timer = setInterval("fadeMsg(1)", MSGTIMER);
      if(!autohide) {
        autohide = MSGHIDE;
      }
      window.setTimeout("hideMsg()", (autohide * 1000));
    }
    catch(e){
        alert(e);
    }
}

// hide the form alert //
function hideMsg(msg) {
  var msg = document.getElementById('msg');
  if(!msg.timer) {
    msg.timer = setInterval("fadeMsg(0)", MSGTIMER);
  }
}

// face the message box //
function fadeMsg(flag) {
  if(flag == null) {
    flag = 1;
  }
  var msg = document.getElementById('msg');
  var value;
  if(flag == 1) {
    value = msg.alpha + MSGSPEED;
  } else {
    value = msg.alpha - MSGSPEED;
  }
  msg.alpha = value;
  msg.style.opacity = (value / 100);
  msg.style.filter = 'alpha(opacity=' + value + ')';
  if(value >= 99) {
    clearInterval(msg.timer);
    msg.timer = null;
  } else if(value <= 1) {
    msg.style.display = "none";
    clearInterval(msg.timer);
  }
}

// calculate the position of the element in relation to the left of the browser //
function leftPosition(target) {
  var left = 0;
  if(target.offsetParent) {
    while(1) {
      left += target.offsetLeft;
      if(!target.offsetParent) {
        break;
      }
      target = target.offsetParent;
    }
  } else if(target.x) {
    left += target.x;
  }
  return left;
}

// calculate the position of the element in relation to the top of the browser window //
function topPosition(target) {
  var top = 0;
  if(target.offsetParent) {
    while(1) {
      top += target.offsetTop;
      if(!target.offsetParent) {
        break;
      }
      target = target.offsetParent;
    }
  } else if(target.y) {
    top += target.y;
  }
  return top;
}

// preload the arrow //
if(document.images) {
  arrow = new Image(7,80);
  arrow.src = "images/msg_arrow.gif";
}

