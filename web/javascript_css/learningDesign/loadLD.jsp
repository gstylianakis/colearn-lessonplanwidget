<%@page  contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="GeneralUsage.Design"%>
<%@page import="com.sun.jersey.core.impl.provider.entity.XMLJAXBElementProvider.General"%>
<%@page import="org.json.JSONObject"%>

<%
    try{  
        String ldName = request.getParameter("LDName");
        GeneralUsage.Design design = new Design();
        JSONObject json = new JSONObject();

        String[] manifest_author = design.loadManifest(ldName);
//System.out.println("manifest author "+ manifest_author);
        String graphicDesign = design.loadGraphicDetails(ldName);
//System.out.println("graphicDesign "+ graphicDesign);
        if(graphicDesign!=null && manifest_author[0]!=null){
        
            json.put("manifest",  manifest_author[0]);
            json.put("author",  manifest_author[1]);
            json.put("graphicDesign",graphicDesign);
            json.put("username",session.getAttribute("username"));  
      //System.out.println("graphic design "+graphicDesign);
            json.put("texts",design.getManifestTextResources(ldName));
            //json.put("files",design.getManifestFilesResources(ldName));
        }
        else{
        
            json.put("manifest", "");
            json.put("graphicDesign","");
            json.put("author","");
            json.put("username","");
            json.put("texts","");
            //json.put("files","");
        }

        out.print(json);

//String output = json.toString();
        //System.out.println(design.loadManifest(ldName));
        //out.print();
        
    }
    catch(Exception e){
        out.print(e);
    }
%>