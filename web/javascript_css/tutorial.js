var tutorialTab = {

        
        containerWidth:$('.tutorial-panel').outerWidth(),
        containerHeight:$('.tutorial-panel').outerHeight(),
        tabWidth:$('.tutorial-tab').outerWidth(),

        init:function(){
            $('.tutorial-panel').css('height',$('.tutorial-panel').outerHeight() + 'px');
            $(".tutorialExplanation > li:eq(0)").show().siblings().hide();
            $('#tutorial-tab').click(function(event){
                if ($('.tutorial-panel').hasClass('open')) {
                    $('.tutorial-panel').animate({right:'-' + $('.tutorial-panel').outerWidth()}, 300)
                    .removeClass('open');
                    
                } else {
                    $('.tutorial-panel').animate({right:'0'},  300)
                    .addClass('open');
                    
                }
                event.preventDefault();
            });

            /*$(".tutorialSteps > li").click(function(){
                $(".tutorialExplanation > li:eq("+$(this).index()+")").show().siblings().hide();
                $(this).addClass('tutorialStepSelected')
                       .siblings('li')
                       .removeClass('tutorialStepSelected');
            })*/
        }
    };

    
var menuTab = {

        menucontainerWidth:$('.menu-panel').outerWidth(),
        menucontainerHeight:$('.menu-panel').outerHeight(),
        menuWidth:$('.menu-tab').outerWidth(),

        init:function(){
            $('.menu-panel').css({'height':$('.menu-panel').outerHeight()+30 + 'px','top':'90px'});
            //$(".menuExplanation > li:eq(0)").show().siblings().hide();
            $('#menu-tab').click(function(event){
                if ($('.menu-panel').hasClass('open')) {
                    $('.menu-panel').animate({right:'-' + $('.menu-panel').outerWidth()}, 300)
                    .removeClass('open');

                } else {
                    $('.menu-panel').animate({right:'0'},  300)
                    .addClass('open');

                }
                event.preventDefault();
            });

            /*$(".tutorialSteps > li").click(function(){
                $(".tutorialExplanation > li:eq("+$(this).index()+")").show().siblings().hide();
                $(this).addClass('tutorialStepSelected')
                       .siblings('li')
                       .removeClass('tutorialStepSelected');
            })*/
        }
    };
