/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*var to_connect;
var can_connect = false;
var to_delete;
var toBeConnectedNodes = [];
var connectedFlag = 0;
var moused = false;
var mouseX = 0;
var mouseY = 0;
*/
var shortEndPointList = []
function activity(name,rolesAssigned,DescriptionItems,LearningObjectivesItems,PrerequisitesItems,activityCompletionFeedbackItems,activityCompletionNotifications,environement,option,annotationDesc){
        var sh;     
        this.name = name;//mporei na allaksei stin poreia xrisimopoiwntas tin setName
        this.propertyList = [];
        //this.annotation = annotation;
        //codeAllagi this.id = name;//to opoio den to allazoume
        if(option==0)
            this.identifier = uniqid();
        else
            this.identifier = option;

        this.id = this.identifier;
        
        this.RoleObjParticipating = new Array();

        this.DescriptionItem = new Array();
        this.PrerequisitesItems = new Array();
        this.LearningObjectivesItems = new Array();
        this.activityCompletionFeedbackItems = new Array();
        this.activityCompletionNotifications = new Array();
        this.environement = environement;
        this.servicesArray = new Array();
        this.rolesAssigned = new Array();
        this.CompletionParameters = [completeActivityOption.competionType,completeActivityOption.competionValue];
        this.flowChilds = new Array();

        this.propertyExpression = null;
        this.htmlExpressionView = null;

        for(var i=0;i<DescriptionItems.length;i++)
            this.DescriptionItem.push(DescriptionItems[i]);

        /*for(i=0;i<LearningObject.length;i++)
            this.learningObject.push(LearningObject[i]);*/

        for(i=0;i<activityCompletionFeedbackItems.length;i++)
            this.activityCompletionFeedbackItems.push(activityCompletionFeedbackItems[i]);

        for(i=0;i<activityCompletionNotifications.length;i++)
            this.activityCompletionNotifications.push(activityCompletionNotifications[i]);


        for(i=0;i<LearningObjectivesItems.length;i++)
            this.LearningObjectivesItems.push(LearningObjectivesItems[i]);

        for(i=0;i<PrerequisitesItems.length;i++)
            this.PrerequisitesItems.push(PrerequisitesItems[i]);

        /*for(i=0;i<servicesArray.childNodes.length;i++){
            if(servicesArray.childNodes[i].nodeType==1)
                this.servicesArray.push(servicesArray.childNodes[i].cloneNode(true));
        }*/

        for(i=0;i<rolesAssigned.length;i++){
            this.rolesAssigned.push(rolesAssigned[i].cloneNode(true));
        }
        roleIsBeingUsed(rolesAssigned,this.id)


        this.getId = function(){
            return this.id;
        }
        /*this.setId = function(id){
            this.id = id;
        }*/
        this.getType = function(){
            return "learning-activity"
        }
        this.getName = function(){
                            return this.name;
                           }
        this.setName = function(name){
                            this.name = name;
                           }

       

        this.set_Shape = function(xPos,yPos,sizeX,sizeY,annotationDesc){
                            sh = new shape(xPos,yPos,sizeX,sizeY,name,this.id,annotationDesc);

                           }
        this.get_Shape = function(){
                            return sh;
                         }
        
        this.getRolesAssigned = function(){
            return this.rolesAssigned;
        }
        this.setRolesAssigned = function(rolesAssigned){
            roleIsBeingUsed(rolesAssigned,this.id)
            this.rolesAssigned = [];
            for(var i=0;i<rolesAssigned.length;i++){                
                this.rolesAssigned.push(rolesAssigned[i].cloneNode(true));
            }
        }

        this.setDescriptionItem = function(DescriptionItem){
            this.DescriptionItem = [];
             for(var i=0;i<DescriptionItem.length;i++)
                this.DescriptionItem.push(DescriptionItem[i]);

        }
        this.getDescriptionItem = function(){
            return this.DescriptionItem;
        }

        this.addProperty = function(property){
            this.propertyList.push(property);
        }
        this.setPropertyList = function(propertyList){
            this.propertyList = propertyList;
        }
        this.getPropertyList = function(){
            return this.propertyList;
        }
    
        this.setEnvironement = function(environement){
            this.environement = environement;
            
        }
        this.getEnvironement = function(){                        
            return this.environement;
        }
        
        this.setActivityCompletionFeedbackItem = function(activityCompletionFeedbackItem){
            this.activityCompletionFeedbackItems = [];
             for(var i=0;i<activityCompletionFeedbackItem.length;i++)
                this.activityCompletionFeedbackItems.push(activityCompletionFeedbackItem[i]);

        }
        this.getActivityCompletionFeedbackItem = function(){
            return this.activityCompletionFeedbackItems;
        }

        this.setLearningObjectivesItems = function(LearningObjectivesItems){
            this.LearningObjectivesItems = [];
             for(var i=0;i<LearningObjectivesItems.length;i++)
                this.LearningObjectivesItems.push(LearningObjectivesItems[i]);

        }
        this.getLearningObjectivesItems = function(){
            return this.LearningObjectivesItems;
        }

       this.setPrerequisitesItems = function(PrerequisitesItems){
            this.PrerequisitesItems = [];
             for(var i=0;i<PrerequisitesItems.length;i++)
                this.PrerequisitesItems.push(PrerequisitesItems[i]);

        }
        this.getPrerequisitesItems = function(){
            return this.PrerequisitesItems;
        }
        this.setCompletionParameters = function(completeActivityOption){
            this.CompletionParameters = [completeActivityOption.competionType,completeActivityOption.competionValue];
        }
        this.getCompletionParameters = function(){            
            return this.CompletionParameters;
        }
        this.setActivityCompletionNotifications = function(completionNotifications){
            this.activityCompletionNotifications = [];
            for(var i=0;i<completionNotifications.length;i++)
                this.activityCompletionNotifications.push(completionNotifications[i]);
        }
        this.getActivityNotifications = function(){
            return this.activityCompletionNotifications;
        }

        this.addflowChilds = function(activity){
            this.flowChilds.push(activity);
        }
        this.getflowChilds = function(){
            return this.flowChilds;
        }
        this.setflowChilds = function(flowChilds){
            this.flowChilds = flowChilds;
        }
        this.setPropertyExpression = function(propExpression){
            this.propertyExpression = propExpression;
        }
        this.getPropertyExpression = function(){
            return this.propertyExpression;
        }
        this.getIdentifier = function(){
            return this.identifier;
        }

        this.gethtmlExpressionView = function(){
            return this.htmlExpressionView;
        }
        this.sethtmlExpressionView = function(div){
            this.htmlExpressionView = div.clone(true);
            this.htmlExpressionView.find("#conditionType").val($("#conditionType").val())             
             
        }
    }

    function activitiesArray(activityList){
        try{
            this.activitiesList = activityList;
            this.connectionsArray = new Array();

            this.addActivity = function(activity){
                                   this.activitiesList.push(activity);
                               }
            this.getActivities = function(){
                                    return this.activitiesList;
                                 }
            this.deleteActivityFromList = function(ActivityIndex){
                                    this.activitiesList.splice(ActivityIndex,1);
                                  }
            this.getSpecificActivity = function(ActivityIndex){
                                        return this.activitiesList[ActivityIndex];
                                       }
            this.indexOfActivity = function(name){
                
                                    var index = -1;//an den iparxei activity me to sigkekrimeno onoma tote epistrefetai -1                                    
                                    for(var i=0;i<this.activitiesList.length;i++){
                                        if(this.activitiesList[i].getId()==name)
                                            index = i;
                                    }
                                
                                    return index;
                                }
            this.setConnectionArray = function(connectionArray){
                                this.connectionsArray = connectionArray;
                        }
            this.getConnectionsArray = function(){
                                return this.connectionsArray;
                        }
        }
        catch(e){
            console.log("function activitiesArray "+e);
        }
    }


    function initializeActivities(){
    
        to_connect = false;
        to_delete = false;
        return new activitiesArray(new Array());
    }

    function addNewActivity(name,activities,rolesAssigned,DescriptionItems,LearningObjectivesItems,PrerequisitesItems,activityCompletionFeedbackItems,activityCompletionNotifications,environement,option,xPos,yPos,annotationDesc){
 
        var size;
        to_connect = false;
        to_delete = false;
        var wrongInput=0;
        var exists = null,image=null,delete_image=null,thumbtack=null,assignUser=null,sendNotificationImg=null;//= activities.getActivities().containsName(name);
        for(var i=0;i<allPhases.getPhases().length;i++){
            exists = allPhases.getPhases()[i].getLearningActivitiesArray().getActivities().containsName(name);
            if(exists!=null)
                break;
        }
        try{
            if(name==""){
                alert("Please define a name");
                wrongInput = -1;
                return;
            }
            if(DescriptionItems.length<1){
                alert("Please give  a short description to this activity");
                wrongInput = -1;
                return;
            }
            var tmpactivity = null;
            var prevName="";
            
            if(document.getElementById("none").checked){
                completeActivityOption.competionType = "none";
                completeActivityOption.competionValue = "none";
            }
            else if(document.getElementById("userChoice").checked){
                completeActivityOption.competionType = "userChoice";
                completeActivityOption.competionValue = "user-choice";
            }
            else if(document.getElementById("activityCompletionTime").checked){            
                completeActivityOption.competionType = "activityCompletionTime";
                completeActivityOption.competionValue = ["time-limit","P0Y0M"+$("#activityDays").val()+"D"+"T"+$("#activityHours").val()+"H"+"0M",$("#activityDays").val(),$("#activityHours").val()];
           
            }

            if(document.getElementById("activityName").value!=""){//codeAllagi if(document.getElementById("activityId").value!=""){
                prevName = document.getElementById("prevName").value;//codeAllagi prevName = document.getElementById("activityId").value;

                for(i=0;i<allPhases.getPhases().length;i++){
                    tmpactivity = allPhases.getPhases()[i].getLearningActivitiesArray().getActivities().containsName(prevName);
                    if(tmpactivity!=null)
                        break;
                }
                if(tmpactivity!=null){
                    if (!confirm("You are going to change activity status")==true)
                        return;
                    else{                        
                        //image = tmpactivity.get_Shape().getElementsByTagName("image")[0];

                        tmpactivity.setEnvironement(environement);
                        tmpactivity.setDescriptionItem(DescriptionItems);
                        tmpactivity.setLearningObjectivesItems(LearningObjectivesItems);
                        tmpactivity.setPrerequisitesItems(PrerequisitesItems);
                        tmpactivity.setActivityCompletionFeedbackItem(activityCompletionFeedbackItems);
                        tmpactivity.setActivityCompletionNotifications(activityCompletionNotifications);
                        tmpactivity.setCompletionParameters(completeActivityOption);
                     
                        //tmpactivity.setRolesAssigned(rolesAssigned);

                        tmpactivity.setPropertyList([]);
                        var property = null;
                        $("#changeActivityCompletionProperties").find("select").each(function(){
                            if($(this).val()=="")
                                return true;//continue
                            property =  new properties.property($(this).val(),"",$(this).parent().parent().children()[1].childNodes[0].value);
                            var tmpProp = properties.propertiesList.containsName($(this).val())
                            //tmpProp.setUsed()
                            property.setId(tmpProp.getId());
                            tmpactivity.addProperty(property);
                        })
                        if(prevName!=name){
                            //prepei na allaksw kai to onoma pou iparxei panw sto svgobject tou activity
                            if(exists!=null){
                                alert("This name already used for other activity.\nPlease give different name")               
                                wrongInput = -1;
                                return;
                            }
                            tmpactivity.setName(name);                            
                            if(name.length>15){
                                var lines = parseInt(name.length/15);                
                                for(var k=0;k<lines;k++){
                                    replacingName = name.substring((k*15),(k+1)*15);                                                      
                                    var textNode = document.createTextNode(replacingName, true);
                                    var index = (1+k);                                    
                                    var txt = tmpactivity.get_Shape().childNodes[index];
                                    if(txt==null){
                                        var svgText = document.createElementNS(svgns, 'text');
                                        svgText.setAttribute('x', 50);
                                        svgText.setAttribute('y', (k*12)+15);
                                        svgText.setAttribute('fill', "#000");
                                        svgText.setAttribute('font-size',"12");
                                        svgText.setAttribute('text-anchor',"middle");
                                        svgText.setAttribute('fill-opacity',"1");
                                        svgText.appendChild(textNode)
                                        tmpactivity.get_Shape().appendChild(svgText)
                                    }
                                    else
                                        if(tmpactivity.get_Shape().childNodes[(1+k)].nodeName!="image" && tmpactivity.get_Shape().childNodes[(1+k)].nodeName!="delete_image")
                                            tmpactivity.get_Shape().childNodes[(1+k)].replaceChild(textNode,txt.childNodes[0]);
                                }
                                if((name.length%15)>0){
                                    replacingName = name.substring((lines*15),name.length);                                        
                                    textNode = document.createTextNode(replacingName, true);
                                    txt = tmpactivity.get_Shape().childNodes[(1+lines)];
                                    if(txt==null){
                                        svgText = document.createElementNS(svgns, 'text');
                                        svgText.setAttribute('x', 50);
                                        svgText.setAttribute('y', (lines*12)+15);
                                        svgText.setAttribute('fill', "#000");
                                        svgText.setAttribute('font-size',"12");
                                        svgText.setAttribute('text-anchor',"middle");
                                        svgText.setAttribute('fill-opacity',"1");
                                        svgText.appendChild(textNode)
                                        tmpactivity.get_Shape().appendChild(svgText)
                                    }

                                    else
                                        if(tmpactivity.get_Shape().childNodes[(1+lines)].nodeName!="image" && tmpactivity.get_Shape().childNodes[(1+lines)].nodeName!="delete_image")
                                            tmpactivity.get_Shape().childNodes[(1+lines)].replaceChild(textNode,txt.childNodes[0]);
                                }
                            }
                            else{
                                textNode = document.createTextNode(name, true);
                                for(var k=0;k<tmpactivity.get_Shape().childNodes.length;k++){
                                    if(k==1){
                                        txt = tmpactivity.get_Shape().childNodes[1].childNodes[0];
                                        tmpactivity.get_Shape().childNodes[1].replaceChild(textNode,txt);
                                    }
                                    else if(k>1)
                                        if(tmpactivity.get_Shape().childNodes[k].nodeName!="image" && tmpactivity.get_Shape().childNodes[k].nodeName!="delete_image" )                                            
                                            tmpactivity.get_Shape().childNodes[k].parentNode.removeChild(tmpactivity.get_Shape().childNodes[k]);
                                        
                                }
                            }

                            //var newName = document.createTextNode(name);
                            //var txt = tmpactivity.get_Shape().childNodes[1].childNodes[0];
                            //tmpactivity.get_Shape().childNodes[1].replaceChild(newName,txt);
                            
                           
                           var replacingName="";
                           if(name.length>13){
                                replacingName = name.substring(0,13);
                                replacingName +=".."
                            }
                            else
                                replacingName = name;
                           //document.getElementById(tmpactivity.getId()+"Activity").innerHTML = replacingName;
                           //document.getElementById(tmpactivity.getId()+"Activity").title = name;
                           
                           $("#"+tmpactivity.getId()+"inActivitiesTreeAnchor").html(replacingName);
                           $("#"+tmpactivity.getId()+"inActivitiesTreeAnchor").attr("title",name);
                           
                            //$("#"+tmpactivity.getId()+"inActivitiesTree").attr("id",name+"inActivitiesTree");
                            //tmpactivity.setId(name);
                        }
                        //tmpactivity.get_Shape().appendChild(image);
                        return;
                    }
                }
            }
            if(exists!=null){
                alert("This name already used for other activity.\nPlease give different name");
                wrongInput=-1;
                return;
            }            

            var new_activity = new activity(name,rolesAssigned,DescriptionItems,LearningObjectivesItems,PrerequisitesItems,activityCompletionFeedbackItems,activityCompletionNotifications,environement,option);
            size = activities.getActivities().length;
        
            //new_activity.set_Shape((10*size), (10*size), 60,40);
            new_activity.set_Shape(xPos, yPos, 60,40,annotationDesc);
            //pare apo to shape tin eikona kai vale event listener
            image = new_activity.get_Shape().getElementsByTagName("image")[0];
            if (navigator.appName != 'Microsoft Internet Explorer'){               
                image.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
                image.addEventListener('mousedown',function (evt) {
                        BrowseActivity(new_activity);
                        evt.stopPropagation()
                },false);

                delete_image = new_activity.get_Shape().getElementsByTagName("image")[2];
                delete_image.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
                delete_image.addEventListener('mousedown',function (evt) {evt.stopPropagation();deleteActivity(new_activity,0);select()},false);

                thumbtack = new_activity.get_Shape().getElementsByTagName("image")[1];
                thumbtack.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
                thumbtack.addEventListener('mousedown',function (evt) {evt.stopPropagation();annotation.showAnnotation(this)},false);

                assignUser = new_activity.get_Shape().getElementsByTagName("image")[3];
                assignUser.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
                assignUser.addEventListener('mousedown',function (evt) {
                    evt.stopPropagation();
                    clearRolesActivityForm();
                    var checkBoxRole;
                    var exists = null,show=null;
                    for(var i=0;i<allPhases.getPhases().length;i++){
                        exists = allPhases.getPhases()[i].getLearningActivitiesArray().getActivities().containsId(new_activity.getId())
                        if(exists!=null){
                            if(allPhases.getPhases()[i].getType()=="activity-structure")
                                show = false;
                            else
                                show = true;
                            break;
                        }
                    }
                    if(show==false){
                        alert("The participants of the specific activity are assigned in the structure");
                        return;
                    }
                    for(i=0;i<new_activity.getRolesAssigned().length;i++){                        
                        checkBoxRole = new_activity.getRolesAssigned()[i];
                        if(checkBoxRole.checked){
                            if(document.getElementById(checkBoxRole.id)!=null)
                                document.getElementById(checkBoxRole.id).checked = true;
                        }
                        else
                            if(document.getElementById(checkBoxRole.id)!=null)
                                document.getElementById(checkBoxRole.id).checked = false;
                    }
                    $("#assignRoleButton").attr("name",new_activity.getId());
                    $.fn.colorbox({width:"550px",height:"370px", inline:true, href:"#AssignRoles"})
                },false);
            }
            sendNotificationImg = new_activity.get_Shape().getElementsByTagName("image")[4];
            sendNotificationImg.addEventListener('mouseover', function(evt){ShowTooltip(evt);this.style.cursor='pointer'},false);
            sendNotificationImg.addEventListener('mousedown',function (evt) {
                evt.stopPropagation();
                activityCompletionNotificationArray = [];
                activityCompletionNotificationArray = new_activity.getActivityNotifications();
                clearNotificationViewer('activity');
                $("#activityCompletionNotificationSubject").val("");
                $("#activityCompletionNotificationContent").val("");
                showNotifications(new_activity.getActivityNotifications(),"activity");
                clearRecipients("activity");

                $.fn.colorbox({width:"855px",height:"410px", inline:true, href:"#sendNotificationAfterCompletion"})
            },false);


            activities.addActivity(new_activity);
            new_activity.setPropertyList([]);
            property = null;
            $("#changeActivityCompletionProperties").find("select").each(function(){            
                if($(this).val()=="")
                    return true;//continue
                property =  new properties.property($(this).val(),"",$(this).parent().parent().children()[1].childNodes[0].value);
                var tmpProp = properties.propertiesList.containsName($(this).val())
                property.setId(tmpProp.getId()); 
                new_activity.addProperty(property);
             })

            var curr_phase = allPhases.getPhase($('#phaseTab').attr("name"));
           
            if(curr_phase.getType()=="activity-structure")
                addActivitiesToHTMLView(new_activity,curr_phase.getId()); //document.getElementById(curr_phase.getId()+"Activity-structure"+"_Included").appendChild(document.getElementById(new_activity.getId()+"ActivityRow"));
            else
                addLearningActivityToPhaseTree(new_activity,curr_phase.getId()+"_PhaseTree");

            //(name,activities,rolesAssigned,DescriptionItems,LearningObjectivesItems,PrerequisitesItems,activityCompletionFeedbackItems,activityCompletionNotifications,environement,option,xPos,yPos,annotationDesc)
            undoActionList.doActionFollow("addNewActivity",{'param1':new_activity});
        }
        catch(e){
            console("we had an unexpected error. The "+name+" activity will not be created"+e);
            if(document.getElementById('activitiesDiv_'+size)!=null){
                try{
                    var to_delete = document.getElementById('activitiesDiv_'+size);
                    document.getElementById('educationalProcessOperationPanel').removeChild(to_delete);

                }
                catch(e){
                    alert('the error was unresolved. Your activity list may not be updated');
                }
            }
        }
        finally{
            
            if(wrongInput!=-1){
                if(option==0){                    
                    $( "#dialog-form" ).dialog("close");
                    $("#learningActivity").colorbox.close();
                }
            }
            select();
        }
    }

    function addActivitiesToHTMLView(activity,phaseId){
        try{
            var newActivityRow = $('<li></li>');
            newActivityRow.attr("id",activity.getId()+"ActivityRow");
            var span = $("<span class='file'></span>");            
            //span.mouseover(function(){this.style.cursor='pointer'})
            span.attr("id",activity.getId()+"Activity");
            var spanName = "";
            if(activity.getName().length>13){
                spanName = activity.getName().substring(0,13);
                spanName +=".."
            }
            else
                spanName = activity.getName();
            
            span.text(spanName);
            span.attr("title",activity.getName());
            newActivityRow.append(span);
            newActivityRow.appendTo($("#"+phaseId+"inActivitiesTree"+"_ActivitiesIncluded"));
            $("#Phases_Rows").treeview({
                add: newActivityRow
            });
            checkAllServices(false);//gia na kanei ola ta services mi epilegmena
            
           /*
         var newActivityRow = document.createElement('li');         
         newActivityRow.id = activity.getId()+"ActivityRow";                  
         var anchorDiv = document.createElement('div');
         anchorDiv.className = "informationPanelInnerAnchors";                
         newActivityRow.appendChild(anchorDiv);
         
         var imgDiv = document.createElement('div');
         imgDiv.style.cssFloat = "right";
         imgDiv.style.marginRight = "1px";
         imgDiv.style.width = "35px";
         imgDiv.style.height = "16px";
         newActivityRow.appendChild(imgDiv);
        
         var anchor = document.createElement('a');
         anchor.href = "#";         
         anchor.setAttribute("id", activity.getId()+"Activity");
         anchor.onclick = function () {BrowseActivity(activity);};
         anchor.style.border = "0px";
         anchor.id = activity.getId()+ "inActivitiesTreeAnchor";         
         anchor.style.textDecoration = "none";        
         anchor.style.width = "auto";         
         anchor.name = activity.getId();
         anchor.title = activity.getName();
         anchor.style.height = "16px";
         anchor.style.overflow="hidden";        
         if(activity.getName().length>13){
            anchor.innerHTML = activity.getName().substring(0,13);
            anchor.innerHTML +=".."
         }
         else
            anchor.innerHTML = activity.getName();
 
         anchor.name = activity.getId();//codeAllagi activity.getName();
         anchorDiv.appendChild(anchor);
         var editImg=document.createElement("IMG");
         editImg.onclick = function () {BrowseActivity(activity);};
         editImg.setAttribute("src","javascript_css/project_team_form/images/edit.gif");
         editImg.style.border="0px";
         editImg.style.cssFloat="left";
         editImg.align = "top";
         editImg.title = "edit activity";
         editImg.name = activity.getId();
         editImg.onmouseover = function(){this.style.cursor='pointer'};
         imgDiv.appendChild(editImg);
         var deleteImg = document.createElement("IMG");
         deleteImg.setAttribute("src","javascript_css/project_team_form/images/delete.png");
         deleteImg.style.border="0px";
         deleteImg.style.cssFloat="left";
         deleteImg.style.marginLeft = "3px";            
         deleteImg.onmouseover = function(){this.style.cursor='pointer'};
         deleteImg.align = "top";
         deleteImg.onclick = function(){deleteActivity(activity,0);};
         deleteImg.title = "delete activity";
         imgDiv.appendChild(deleteImg);
         checkAllServices(false);//gia na kanei ola ta services mi epilegmena
         var phaseName = document.getElementById("phaseTab").name;
         var currPhase = allPhases.getPhases().containsName(phaseName);
         if(currPhase.getType()=="phase")
            changeIcon(currPhase.getId()+"_PhaseTree"+"_img",currPhase.getId()+"_PhaseTree"+"_ActivitiesTree",'show');
         document.getElementById(curr_phase.getId()+"inActivitiesTree"+"_ActivitiesIncluded").appendChild(newActivityRow)
        */
         //return newActivityRow;
     }     
     catch(e){
         console.log("addActivitiesToHTMLView "+e);
     }
 }
    
    function BrowseActivity(activity){
        try{
                        
            serviceArray = [];
             var tmp_checkboxId = "";
             $("#ServicesContent").find("input:checkbox").each(function(){                
                $(this).attr('checked',false);
                tmp_checkboxId = ($(this).attr('id').substring($(this).attr('id').indexOf('service_')+8,$(this).attr('id').length));
                showSetUp(false,tmp_checkboxId,'activity');

             })
            
             descriptionItems=[];
             prerequisitesItems=[];
             learningObjectivesItems=[];
             LearningObjectItems = [];
             activityCompletionFeedback=[];
             /*
              activityCompletionNotificationArray = [];
              activityCompletionNotificationArray = activity.getActivityNotifications();
              clearNotificationViewer('activity');
            */
             
             clearItemViewer("activityCompletion");
             clearItemViewer("LearningObject")
             
             clearItemViewer("Description");
             clearItemViewer("activityLearningObjectives");
             clearItemViewer("Prerequisites");
             
             
             document.getElementById("activityDescriptionTitle").value = "";document.getElementById("createNewDescription").checked = true;
             document.getElementById("createNewDescriptionContent").value = "Enter your description here";
             document.getElementById("URLDescriptionContent").value = "http://";
             //document.getElementById("ExistingFileDescriptionContent").value = "";
             //document.getElementById("ExistingResourceDescriptionContent").value = "";
             showCompletionPrereq('createNewDescription','Description');

             document.getElementById("activityLearningObjectivesTitle").value = "";document.getElementById("createNewactivityLearningObjectives").checked = true;
             document.getElementById("createNewactivityLearningObjectivesContent").value = "Enter your description here";
             document.getElementById("URLactivityLearningObjectivesContent").value = "http://";
             document.getElementById("ExistingFilePrerequisitesContent").value = "";
             //document.getElementById("ExistingResourcePrerequisitesContent").value = "";
             showCompletionPrereq('createNewactivityLearningObjectives','activityLearningObjectives');

             document.getElementById("activityPrerequisitesTitle").value = "";document.getElementById("createNewPrerequisites").checked = true;
             document.getElementById("createNewPrerequisitesContent").value = "Enter your description here";
             document.getElementById("URLPrerequisitesContent").value = "http://";
             document.getElementById("ExistingFileactivityLearningObjectivesContent").value = "";
             //document.getElementById("ExistingResourceactivityLearningObjectivesContent").value = "";
             showCompletionPrereq('createNewPrerequisites','Prerequisites');

             document.getElementById("createNewactivityCompletion").checked=true;
             document.getElementById("activityCompletionTitle").value = "";
             document.getElementById("createNewactivityCompletionContent").value = "Enter your description here";
             document.getElementById("URLactivityCompletionContent").value = "http://";
             //document.getElementById("ExistingResourceactivityCompletionContent").value="";
             showCompletionPrereq('createNewactivityCompletion','activityCompletion');
             
             
             document.getElementById("createNewLearningObject").checked=true;
             document.getElementById("LearningObjectTitle").value = "";
             document.getElementById("createNewLearningObjectContent").value = "Enter your description here";
             document.getElementById("URLLearningObjectContent").value = "http://";
             document.getElementById("ExistingFileLearningObjectContent").value = "";
             //document.getElementById("ExistingResourceLearningObjectContent").value = "";
             showCompletionPrereq('createNewLearningObject','LearningObject');
             
             

             document.getElementById("activityName").value = activity.getName();
             document.getElementById("prevName").value = activity.getName();//codeAllagi document.getElementById("activityId").value = activity.getName();
             

             document.getElementById("none").checked = false;
             document.getElementById("userChoice").checked = false;
             document.getElementById("activityCompletionTime").checked = false;
             //document.getElementById("activityCompletionPropertySet").checked = false;
    
             if(activity.getCompletionParameters()[0]!=null){
                document.getElementById(activity.getCompletionParameters()[0]).checked = true;
                if(activity.getCompletionParameters()[0]=="none"){
                    document.getElementById('activityCompletionTimeView').style.display = "none";
                    document.getElementById('activityCompletionPropertySetView').style.display = "none";
                }
                else if(activity.getCompletionParameters()[0]=="userChoice"){
                    document.getElementById('activityCompletionTimeView').style.display = "none";
                    document.getElementById('activityCompletionPropertySetView').style.display = "none";
                }
                else if(activity.getCompletionParameters()[0]=="activityCompletionTime"){
                    document.getElementById('activityCompletionTimeView').style.display = 'block';
                    $("#activityDays").val(activity.getCompletionParameters()[1][2]);
                    $("#activityHours").val(activity.getCompletionParameters()[1][3]);
                    document.getElementById('activityCompletionPropertySetView').style.display = "none";
                }
            }
            showFeedback(activity,"Description","activity");
            showFeedback(activity,"activityLearningObjectives","");
            showFeedback(activity,"Prerequisites","activity");
            showFeedback(activity,"activityCompletion","")
            showFeedback(activity,"LearningObject","")
            

            /*document.getElementById("activityCompletionNotificationSubject").value = "";
            document.getElementById("activityCompletionNotificationContent").value = "";
            
            showNotifications(activity.getActivityNotifications(),"activity");
            clearRecipients("activity");
            */
             
            if(activity.getEnvironement().length!=0){
                
                var tmpServicesArray = activity.getEnvironement().getServices();

                for(var i=0;i<tmpServicesArray.length;i++){
                    $("#service_"+tmpServicesArray[i].getName()).attr('checked',true);                    
                   
                    loadLTIParam(tmpServicesArray[i].getName(),tmpServicesArray[i].getUrl(),tmpServicesArray[i],'activity',tmpServicesArray[i].getKey(),tmpServicesArray[i].getSecret())
                    showSetUp(true,tmpServicesArray[i].getName(),'activity');
                }
            }
             $("#service_setUp").css("display",'none')
            
            /*clearRolesActivityForm();
            var checkBoxRole;

            for(i=0;i<activity.getRolesAssigned().length;i++){
                checkBoxRole = activity.getRolesAssigned()[i];                
                
                if(checkBoxRole.checked){
                    if(document.getElementById(checkBoxRole.id)!=null)
                        document.getElementById(checkBoxRole.id).checked = true;
                }
                else
                    if(document.getElementById(checkBoxRole.id)!=null)
                        document.getElementById(checkBoxRole.id).checked = false;
                
            }*/
            clearPropertyRow("changeActivityCompletionProperties");
            var propertyList = activity.getPropertyList()
            for(i=0;i<propertyList.length;i++){
                if(i==0){
                    $("#ActivitySelectProperty").find("option").each(function(){
                        if($(this).text()==propertyList[i].getName())
                            $(this).attr("selected",true);
                        else
                            $(this).removeAttr("selected");
                    })
                    $("#ActivityProperty1").val(propertyList[i].getValue())
                }
            else
                addPropertyChangeRow(propertyList[i].getValue(), propertyList[i].getName(),"changeActivityCompletionProperties")
        }

            $.fn.colorbox(
                    {width:"78%",height:"78%", inline:true, href:"#learningActivityForm",
                     onLoad:function(){
                        try{           
                         
                            $(".ActivityLearningDefinitionTabs").tabs();
                            $("#successUploadactivityLearningObjectives").css("display","none");
                            $("#successUploadDescription").css("display","none");
                            $("#successUploadLearningObject").css("display","none");
                            $("#successUploadPrerequisites").css("display","none");
                            $("#successUploadactivityCompletion").css("display","none");
                            $("#successUploadPhaseDescription").css("display","none");
                            $("#successUploadstructureLearningObject").css("display","none");
                            $("#successUploadstructureInformation").css("display","none");

                            var exists = null,show=null;
                            for(var i=0;i<allPhases.getPhases().length;i++){
                                exists = allPhases.getPhases()[i].getLearningActivitiesArray().getActivities().containsId(activity.getId());//code Allagi allPhases.getPhases()[i].getLearningActivitiesArray().getActivities().containsName($(this).attr("name"));
                               
                                if(exists!=null){
                                    if(allPhases.getPhases()[i].getType()=="activity-structure")
                                        show = false;
                                    else
                                        show = true;
                                    break;
                                }
                            }
                            if(show==false){
                                $(".ActivityLearningDefinitionTabs").tabs("disable",3);
                                //$(".ActivityLearningDefinitionTabs").tabs("disable",4);
                            }

                            $(".completionActivityParamTabs").tabs();
                            $(".toolsMaterialsTabs").tabs();
                        }
                        catch(e){
                            console.log("BrowseActivity  "+e)
                        }
                     }

                    });

        }
        catch(e){
            console.log("BrowseActivity "+e);
        }
    }

  
  
     function dragger(e){
            try{     
                this.isDragging = true;
                can_connect = false;
                this.mouseCoords = {x: e.clientX, y: e.clientY};
                var target = (e.currentTarget) ? e.currentTarget : e.srcElement;
                if(to_connect){                    
                    if(target.getAttribute("id")=="end")
                        return;
                    var svgdoc = document.getElementById('svgPanel').contentDocument;
                    var drawline = svgdoc.getElementById("drawline");
                    drawline.removeAttribute("visibility");                 
                    DrawLine_mouseDown(e);               
                    onMouseClick(e);
                }
                
                return cancelEvent(e);
            }
            catch(er){
                console.log("dargger "+ er);
            }
        }
   var structureBox = null,tmp=null, lock=false;
   function onMouseMove(e) {
           try{              
                var activities = getCurrentActivityList();
                var structures = getCurrentStructures();
                var connections = activities.getConnectionsArray();
                var shortSyncPointsList = allPhases.getPhase(document.getElementById('phaseTab').name).getJoinGateways();
                var shortOrPointsList = allPhases.getPhase(document.getElementById('phaseTab').name).getInclusiveOrGateways();
                e = e || window.event;
                var target = (e.currentTarget) ? e.currentTarget : e.srcElement;
                if(!to_connect && !to_delete){
                    if(this.isDragging == true){
                        var g = target;
                        
                        if(g.vTranslate==undefined)
                            g.vTranslate = [0,0];

                        var pos = g.vTranslate;
                        
                        var xd = (e.clientX - this.mouseCoords.x);
                        var yd = (e.clientY - this.mouseCoords.y);
                        g.vTranslate = [ pos[0] + xd, pos[1] + yd ];
                 
                        g.setAttribute("transform", "translate(" + g.vTranslate[0] + "," + g.vTranslate[1] + ")");
                        g.setAttribute("dragX",g.vTranslate[0]);
                        g.setAttribute("dragY",g.vTranslate[1]);

                        //var tmpConnections = connections.slice();
                        var tmpConnections = activities.getConnectionsArray().slice();
                        var activity = null,syncPoint=null,orPoint=null,structure=null;
                        for(var i=0;i<tmpConnections.length;i++){
                            var con = tmpConnections[i];
                            
                            //kounas to 'from' mias sindesis kai psaxenis gia to 'to' stoixeio wste na enimerwseis tin sindesi
                            if(con.getAttribute("from")==target.getAttribute("id")){// || con.getAttribute("to")==e.currentTarget.getAttribute("id")){
                                var objectToConn = null;                               
                                if(con.getAttribute("toType")=="activity"){
                                    for(var j=0;j<activities.getActivities().length;j++){
                                        activity = activities.getActivities()[j];
                                        if(activity.getId()==con.getAttribute("to")){
                                            objectToConn = activity.get_Shape();
                                            break;
                                        }

                                    }
                                }
                                else if(con.getAttribute("toType")=="syncPoint"){
                                    objectToConn = null;                                    
                                    for(j=0;j<shortSyncPointsList.length;j++){
                                        syncPoint = shortSyncPointsList[j];
                                        if(syncPoint.getShape().getAttribute("id")==con.getAttribute("to")){
                                            objectToConn = syncPoint.getShape();
                                            break;
                                        }
                                    }
                                }
                                else if(con.getAttribute("toType")=="inclusiveOrPoint"){
                                    objectToConn = null;
                                    for(j=0;j<shortOrPointsList.length;j++){
                                        orPoint = shortOrPointsList[j];                                        
                                        if(orPoint.getShape().getAttribute("id")==con.getAttribute("to")){
                                            objectToConn = orPoint.getShape();
                                            break;
                                        }
                                    }
                                }
                                else if(con.getAttribute("toType")=="activity-structure"){
                                    objectToConn = null;                                    
                                    for(j=0;j<structures.length;j++){
                                        structure = structures[j];
                                        if(structure.getId("id")==con.getAttribute("to")){
                                            objectToConn = structure.getShape();
                                            break;
                                        }
                                    }
                                }
                                else{//den einai apo ta parapanw ara psaxneis to end
                                   objectToConn = allPhases.getPhase($("#phaseTab").attr("name")).getEndPoint()//allPhases.getPhase($("#phaseTab").attr("name")).getEndPoint().containsAttributeId(con.getAttribute("to"));
                                }
                                if(objectToConn!=null){
                                   removeConnection(con,connections);
                                   connections.push(connection(target,objectToConn));

                                }
                            }
                            //kounas to 'to' mias sindesis kai psaxenis gia to 'from' stoixeio wste na enimerwseis tin sindesi
                            else if(con.getAttribute("to")==target.getAttribute("id")){
                                var objectFromConn = null;
                                if(con.getAttribute("fromType")=="activity"){
                                    for(j=0;j<activities.getActivities().length;j++){
                                        activity = activities.getActivities()[j];
                                        if(activity.getId()==con.getAttribute("from")){
                                            objectFromConn = activity.get_Shape();
                                            break;
                                        }

                                    }
                                }
                                else if(con.getAttribute("fromType")=="syncPoint"){
                                    syncPoint = null;
                                    objectFromConn = null;
                                    for(j=0;j<shortSyncPointsList.length;j++){
                                        syncPoint = shortSyncPointsList[j];
                                        if(syncPoint.getShape().getAttribute("id")==con.getAttribute("from")){
                                            objectFromConn = syncPoint.getShape();
                                            break;
                                        }
                                    }
                                }
                                else if(con.getAttribute("fromType")=="inclusiveOrPoint"){
                                    orPoint = null;
                                    objectFromConn = null;
                                    for(j=0;j<shortOrPointsList.length;j++){
                                        orPoint = shortOrPointsList[j];
                                        if(orPoint.getShape().getAttribute("id")==con.getAttribute("from")){
                                            objectFromConn = orPoint.getShape();
                                            break;
                                        }
                                    }
                                }
                                else if(con.getAttribute("fromType")=="activity-structure"){
                                    structure = null;
                                    objectFromConn = null;
                                    for(j=0;j<structures.length;j++){
                                        structure = structures[j];
                                        if(structure.getId("id")==con.getAttribute("from")){
                                            objectFromConn = structure.getShape();
                                            break;
                                        }
                                    }
                                }
                                else{                                    
                                    objectFromConn = allPhases.getPhase($("#phaseTab").attr("name")).getStartPoint();
                                }
                                if(objectFromConn!=null){
                                   removeConnection(con,connections);                                   
                                   connections.push(connection(objectFromConn,target));
                                }
                            }
                        }
                    }
                }                
                this.mouseCoords = {x: e.clientX, y: e.clientY};               
              return cancelEvent(e);
           }
           catch(er){
               console.log("on mouse move "+er);
           }

   }
    function onMouseUp(e) {
        try{            
         
            this.isDragging = false;
            moused = false;
            var target = (e.currentTarget) ? e.currentTarget : e.srcElement;
            if(target.getAttribute("ObjType")=="startPoint"){
                can_connect = false;  
                var svgdoc = document.getElementById('svgPanel').contentDocument;
                var drawline = svgdoc.getElementById("drawline");
                drawline.setAttribute("visibility", "hidden");
            }
            if(can_connect){
                can_connect = false;
                svgdoc = document.getElementById('svgPanel').contentDocument;
                drawline = svgdoc.getElementById("drawline");
                drawline.setAttribute("visibility", "hidden");                      
                onMouseClick(e);
     
            }
            else{
                toBeConnectedNodes = [];
                connectedFlag=0;
            }

            /*if(e.currentTarget.getAttribute("ObjType")=="activity")
                if(activityStructuresArray.addObject!=null){
                    var activities = getCurrentActivityList();
                    for(var i=0;i<activities.getActivities().length;i++)
                        if(activities.getActivities()[i].getId()==e.currentTarget.getAttribute("id")){
                            activityStructuresArray.addObject.includeActivity(activities.getActivities()[i]);
                            var tmp = e.currentTarget;
                            deleteActivityFromPanel(e.currentTarget.getAttribute("id"));
                            activities.getActivities().splice(i,1);
                            activityStructuresArray.updateView(activityStructuresArray.addObject,tmp);
                            break;
                        }
                }*/
                    
            return cancelEvent(e);
        }
        catch(er){
            alert("error reffering to object "+er);
        }

    }

    function onMouseOver(e){                
        this.isDragging = false;   
        this.setAttribute('mousemoveControl',"true");
  
        if(to_connect && moused){
            can_connect = true;
        }
        if(to_delete)
            this.childNodes[0].style.stroke="#FF0000";

        if(!to_connect && !to_delete){
            var target = (e.currentTarget) ? e.currentTarget : e.srcElement;
            if(target.getAttribute("ObjType")!="activity" && target.getAttribute("ObjType")!="activity-structure"){
                e.stopPropagation();
                return;
            }

            target.getElementsByTagName("image")[0].setAttribute("display","block")
            target.getElementsByTagName("image")[1].setAttribute("display","block")
            target.getElementsByTagName("image")[2].setAttribute("display","block")
            target.getElementsByTagName("image")[3].setAttribute("display","block")
            if(target.getElementsByTagName("image")[4]!=null)
                target.getElementsByTagName("image")[4].setAttribute("display","block")
            
        }

        return cancelEvent(e);
    }
    function DrawLine_mouseUp(){
        try{           
            toBeConnectedNodes = [];
            connectedFlag=0;         
            moused = false;         
            can_connect = false;
            var svgdoc = document.getElementById('svgPanel').contentDocument;
            var drawline = svgdoc.getElementById("drawline");
            drawline.setAttribute("visibility", "hidden");

        }
        catch(e){
            console.log("DrawLine_mouseDown "+e);
        }
    }
    function DrawLine_mouseDown(evt){
        try{
            
            var target = (evt.currentTarget) ? evt.currentTarget : evt.srcElement;
            if(target.getAttribute("ObjType")==null){//pataw sto keno
                evt.stopPropagation();
                
                DrawLine_mouseUp();
                return;
            }     
            if(moused){            
		moused = false;
            }else{              
                    moused = true;
                    mouseX = 0;
                    mouseY = 0;
                    var svgdoc = document.getElementById('svgPanel').contentDocument;
                    var drawline = svgdoc.getElementById("drawline");

                    var posX = evt.clientX;
                    var posY = evt.clientY
                    
                    if(evt.type == "touchstart"){
                        posX = (evt.touches[0].clientX); //- translation.x)/newScale;
                        posY = (evt.touches[0].clientY); 
                    }
                    
                    drawline.setAttribute("x1", posX);
                    drawline.setAttribute("y1", posY);

                    drawline.setAttribute("x2", posX);
                    drawline.setAttribute("y2", posY);

            }

        }
        catch(e){
            console.log("Drawline mouseDown"+e);
        }
    }
    
    function  DrawLine_mouseMoved(evt)
    {
        try{          
            if(moused){
                    var svgdoc = document.getElementById('svgPanel').contentDocument;
                    var drawline = svgdoc.getElementById("drawline");
                    if(mouseX == 0 || mouseY == 0){
                            mouseX = evt.clientX;
                            mouseY = evt.clientY;
                    }
                    //store current mouse values
                    mouseX = evt.clientX;
                    mouseY = evt.clientY;
                    //draw the line while the user is dragging
                    drawline.setAttribute("x2", mouseX);
                    drawline.setAttribute("y2", mouseY);
            }
        }
        catch(er){
            console.log("DrawLineMouseMoved "+er);
        }
   }


    function onMouseClick(e){
        try{
      
            var target = (e.currentTarget) ? e.currentTarget : e.srcElement;   
console.log("event einai "+e.type+" target einai "+target.getAttribute("id")+" flag "+connectedFlag);              
            var activities = getCurrentActivityList();
            if(activities==null)
                return;
            
            var curr_phase = allPhases.getPhase(document.getElementById('phaseTab').name);
            var structures = getCurrentStructures();
            var connections = activities.getConnectionsArray();
            var shortSyncPointsList = curr_phase.getJoinGateways();
            var path = null;
            var objFrom=null,objTo=null;
            if(to_connect){                
                connectedFlag++;                
                toBeConnectedNodes.push(target);
                
                if(connectedFlag==1 && target.getAttribute("ObjType")=="endPoint"){//an exw ksekinisei apo endPoint tote fige min kaneis tipota
                    toBeConnectedNodes = [];
                    connectedFlag=0;
                    return;
                }
                if(connectedFlag==2){
                    var name = toBeConnectedNodes[0].getAttribute("id");
                    var name1 = toBeConnectedNodes[1].getAttribute("id");
console.log(name+" kai "+name1);                    
                    if(name == name1){//an sindew to idio object, min kaneis tipota
                        toBeConnectedNodes = [];
                        connectedFlag=0;
                        return;
                    }
                    for(var i=0;i<connections.length;i++){//elenxo an exw idi tin sindesi twn 2 activities gia na min dimiourgw polles idies kai axristes sindesis
                        path = connections[i];                       
                        if(path.getAttribute("from")==name && path.getAttribute("to")==name1){  
                            toBeConnectedNodes = [];
                            connectedFlag=0;
                            return;
                        } // kai elenxw na min kanw kiklikes sindesis
                        if(path.getAttribute("to")==name && path.getAttribute("from")==name1){
                            toBeConnectedNodes = [];
                            connectedFlag=0;
                            return;
                        }
                    }
                    //an  pigaine apo start  sto end min kaneis tipota
                    //'h an pigaine apo start se or point
                    if(toBeConnectedNodes[0].getAttribute("ObjType")=="startPoint" && (toBeConnectedNodes[1].getAttribute("ObjType")=="endPoint" /*|| toBeConnectedNodes[1].getAttribute("ObjType")=="inclusiveOrPoint"*/)){
                         toBeConnectedNodes = [];
                         connectedFlag=0;
                         return;
                    }
                    if((toBeConnectedNodes[0].getAttribute("ObjType")=="syncPoint"||toBeConnectedNodes[0].getAttribute("ObjType")=="inclusiveOrPoint") && (toBeConnectedNodes[1].getAttribute("ObjType")=="syncPoint"||toBeConnectedNodes[1].getAttribute("ObjType")=="inclusiveOrPoint")){
                         toBeConnectedNodes = [];
                         connectedFlag=0;
                         return;//to gateway paei se process kai oxi allo gateway
                    }
                    if(toBeConnectedNodes[0].getAttribute("ObjType")=="syncPoint"){
                        objFrom=null;
                        objTo=null;
                        objFrom = shortSyncPointsList.containsName(name);

                        objTo = activities.getActivities().containsId(name1);//codeAllagi activities.getActivities().containsName(name1);
                        if(objTo==null)
                            objTo = structures.containsId(name1);
                        if(objTo==null)//psakse gia end point
                            if(curr_phase.getEndPoint().containsAttributeId(toBeConnectedNodes[1].getAttribute("id"))!=null)
                                objTo = new EndPointObj(toBeConnectedNodes[1].getAttribute("id"),toBeConnectedNodes[1].getAttribute("dragX"),toBeConnectedNodes[1].getAttribute("dragY"));

                        if(objFrom!=null && objTo!=null)
                           objFrom.addObjGoTo(objTo);
                    }

                    if(toBeConnectedNodes[1].getAttribute("ObjType")=="syncPoint"){
                        objFrom=null;
                        objTo=null;
                        objTo = shortSyncPointsList.containsName(name1);                       
                        objFrom = activities.getActivities().containsId(name);//codeAllagi activities.getActivities().containsName(name);
                        if(objFrom==null)
                           objFrom = structures.containsId(name);
                 
                        if(objFrom!=null && objTo!=null)
                           objTo.addObjFrom(objFrom);
                    }
                    //prosthetw sta paidia tou gia na me voithisei stin metatropi apo ton grapho sto xml
                    if((toBeConnectedNodes[0].getAttribute("ObjType")=="activity" || toBeConnectedNodes[0].getAttribute("ObjType")=="activity-structure")&& (toBeConnectedNodes[1].getAttribute("ObjType")=="activity" ||toBeConnectedNodes[1].getAttribute("ObjType")=="activity-structure" || toBeConnectedNodes[1].getAttribute("ObjType")=="endPoint")){

                            var activityFrom = null;
                            if(toBeConnectedNodes[0].getAttribute("ObjType")=="activity")
                                activityFrom = activities.getActivities().containsId(toBeConnectedNodes[0].getAttribute("id"));//codeAllagi activities.getActivities().containsName(toBeConnectedNodes[0].getAttribute("id"));
                            else
                                activityFrom = getCurrentStructures().containsId(toBeConnectedNodes[0].getAttribute("id"));
                            
                            var activityTo = null;
                            if(toBeConnectedNodes[1].getAttribute("ObjType")=="activity")
                                activityTo = activities.getActivities().containsId(toBeConnectedNodes[1].getAttribute("id"));//codeAllagi activities.getActivities().containsName(toBeConnectedNodes[1].getAttribute("id"));
                            else if(toBeConnectedNodes[1].getAttribute("ObjType")=="activity-structure")
                                activityTo = getCurrentStructures().containsId(toBeConnectedNodes[1].getAttribute("id"));
                            else if(toBeConnectedNodes[1].getAttribute("ObjType")=="endPoint")
                                activityTo = new EndPointObj(toBeConnectedNodes[1].getAttribute("id"),toBeConnectedNodes[1].getAttribute("dragX"),toBeConnectedNodes[1].getAttribute("dragY"));
                            
                            


                            activityFrom.addflowChilds(activityTo);
                     }

                    var connect = new connection(toBeConnectedNodes[0],toBeConnectedNodes[1]);                    
                    connections.push(connect);
                    

                    if(toBeConnectedNodes[0].getAttribute("ObjType")=="inclusiveOrPoint"){// || toBeConnectedNodes[0].getAttribute("ObjType")=="inclusiveOrPoint"){
                        var orPointId=toBeConnectedNodes[0].getAttribute("id");
                        var inclusiveOrPoint = curr_phase.getInclusiveOrGateways().containsName(orPointId);

                        if(inclusiveOrPoint!=null){
                            var conId = toBeConnectedNodes[0].getAttribute("id")+"_"+toBeConnectedNodes[1].getAttribute("id");
                            //codeAllagi inclusiveOrPoint.addObjGoTo(activities.getActivities().containsName(name1),conId);
                            var svgDoc = document.getElementById('svgPanel').contentDocument;
                            var root = svgDoc.getElementById("vectorGraphicsDoc");
                            var line = svgDoc.getElementById(conId);//auto den paizei ston IE!!!!!!
                            var text = document.createElementNS(svgns, 'text');

                            var positionText = Math.abs((toBeConnectedNodes[1].getAttribute("dragX")-toBeConnectedNodes[0].getAttribute("dragX"))/2)
                            text.setAttribute("dx",Math.abs(positionText-30))
                            text.setAttribute("dy",-7)
                            var text_id = "textRefered_"+line.getAttribute("id");
                            text.setAttribute("id", text_id);

                            var textPath = document.createElementNS(svgns, 'textPath');
                            var xlinkns = "http://www.w3.org/1999/xlink";
                            textPath.setAttributeNS(xlinkns, "href", "#"+conId);

                            textPath.textContent = "click..";                            
                            text.appendChild(textPath);

                            var title = document.createElementNS(svgns, 'title');
                            title.textContent="click to set condition";
                            textPath.appendChild(title);
                        
                            activityTo = activities.getActivities().containsId(toBeConnectedNodes[1].getAttribute("id"));//codeAllagi activities.getActivities().containsName(toBeConnectedNodes[1].getAttribute("id"));
                            if(text!=null){
                                if(activityTo==null)//mporei na eimai se structure
                                    activityTo = getCurrentStructures().containsId(toBeConnectedNodes[1].getAttribute("id"));
                                if(activityTo==null)//end points
                                    if(curr_phase.getEndPoint().containsAttributeId(toBeConnectedNodes[1].getAttribute("id"))!=null)
                                        activityTo = new EndPointObj(toBeConnectedNodes[1].getAttribute("id"),toBeConnectedNodes[1].getAttribute("dragX"),toBeConnectedNodes[1].getAttribute("dragY"));
                                
                                if(activityTo!=null)
                                    text.onclick = function(){updateCondition(text_id,activityTo)};
                            }
                            if(activityTo!=null)
                                inclusiveOrPoint.addObjGoTo(activityTo,conId);
                            else
                                console.log("error while defining or point GotTO object ");
                            root.appendChild(text);
                        }
                    }
                    if(toBeConnectedNodes[1].getAttribute("ObjType")=="inclusiveOrPoint"){// || toBeConnectedNodes[0].getAttribute("ObjType")=="inclusiveOrPoint"){
                        orPointId=toBeConnectedNodes[1].getAttribute("id");
                        inclusiveOrPoint = curr_phase.getInclusiveOrGateways().containsName(orPointId);

                        if(inclusiveOrPoint!=null){
                            if(toBeConnectedNodes[0].getAttribute("ObjType")=="startPoint"){//an exw erthei apo start point
                                //inclusiveOrPoint.addObjFrom(curr_phase.getStartPoint());
                            }
                            else{
                                var fromObj = activities.getActivities().containsId(name);//codeAllagi activities.getActivities().containsName(name);
                                if(fromObj==null)
                                    fromObj = structures.containsId(name);
                                if(fromObj!=null)
                                    inclusiveOrPoint.addObjFrom(fromObj);
                            }
                        }
                    }
                    connectedFlag = 0;
                    toBeConnectedNodes = [];

                }
            }
            else{
                connectedFlag=0;
                toBeConnectedNodes = [];
            }
            return cancelEvent(e);
        }
        catch(e){
            console.log("onMouseClicked "+e);
        }
    }
    function cancelEvent(e)
        {
            e = e ? e : window.event;
            if(e.stopPropagation)
                e.stopPropagation();
            if(e.preventDefault)
                e.preventDefault();
            e.cancelBubble = true;
            e.cancel = true;
            e.returnValue = false;
            return false;
        }

function shape(xPos, yPos,sizeX,sizeY,name,id,annotationDesc){
    try{
    
            var activities = getCurrentActivityList();
            var size = activities.getActivities().length;
            
            var activityRect = document.createElementNS(svgns, 'g');            
            activityRect.setAttribute('id',id);
            activityRect.setAttribute('ObjType',"activity");
            activityRect.setAttribute('mousemoveControl','false');            
            activityRect.setAttribute('dragX',xPos);
            activityRect.setAttribute('dragY',yPos);
            activityRect.setAttribute("transform", "translate(" + xPos + " " + yPos +")");
            activityRect.vTranslate = [xPos,yPos];

            var rectangle = document.createElementNS(svgns, 'rect');
            rectangle.setAttribute('x', 0);
            rectangle.setAttribute('y', 0);
            rectangle.setAttribute('rx',5);
            rectangle.setAttribute('ry',5);
            rectangle.setAttribute('fill','#ffffff');
            rectangle.setAttribute('stroke', "#000");
            rectangle.setAttribute("fill-opacity", '1');
            rectangle.setAttribute("stroke-width", '2');
            rectangle.setAttribute('width', 100);
            rectangle.setAttribute('height', 50);
 
            activityRect.appendChild(rectangle);
            var textNode = null;
            var svgText = null;
            var replacingName = "";            
            if(name.length>15){
                var lines = parseInt(name.length/15);
                
                for(var i=0;i<lines;i++){
                    replacingName = name.substring((i*15),(i+1)*15);                   
                    svgText = document.createElementNS(svgns, 'text');
                    svgText.setAttribute('x', 50);
                    svgText.setAttribute('y', (i*12)+15);
                    svgText.setAttribute('fill', "#000");
                    svgText.setAttribute('font-size',"12");
                    svgText.setAttribute('text-anchor',"middle");
                    svgText.setAttribute('fill-opacity',"1");
                    textNode = document.createTextNode(replacingName, true);
                    svgText.appendChild(textNode);
                    activityRect.appendChild(svgText);
                }
                if((name.length%15)>0){
                    replacingName = name.substring((lines*15),name.length);
                    svgText = document.createElementNS(svgns, 'text');
                    svgText.setAttribute('x', 50);
                    svgText.setAttribute('y', (lines*12)+15);
                    svgText.setAttribute('fill', "#000");
                    svgText.setAttribute('font-size',"12");
                    svgText.setAttribute('text-anchor',"middle");
                    svgText.setAttribute('fill-opacity',"1");
                    textNode = document.createTextNode(replacingName, true);
                    svgText.appendChild(textNode);
                    activityRect.appendChild(svgText);
                }
            }
            else{
                svgText = document.createElementNS(svgns, 'text');
                svgText.setAttribute('x', 50);
                svgText.setAttribute('y', 15);
                svgText.setAttribute('fill', "#000");
                svgText.setAttribute('font-size',"12");
                svgText.setAttribute('text-anchor',"middle");
                svgText.setAttribute('fill-opacity',"1");
                textNode = document.createTextNode(name, true);
                svgText.appendChild(textNode);
                activityRect.appendChild(svgText);
            }
            var svgDoc = document.getElementById('svgPanel').contentDocument;

            var root = svgDoc.getElementById("vectorGraphicsDoc");//auto den paizei ston IE!!!!!!


            if(xPos==0 && yPos==0){
                activityRect.vTranslate = [(5*size),(5*size)];
                activityRect.setAttribute('dragX',(size*5));//(10*size)
                activityRect.setAttribute('dragY',(size*5));//(10*size)
                activityRect.setAttribute("transform", "translate(" + (size*5) + " " + (size*5) +")");
            }
            var image = document.createElementNS(svgns, 'image');
            image.setAttribute("width","16");
            image.setAttribute("height","16");
            image.setAttribute("x",2);
            image.setAttribute("y",50);
            image.setAttribute("display","none");
            var title = document.createElementNS(svgns, 'title');
            title.textContent = "edit resources";
            image.appendChild(title);

            var delete_image = document.createElementNS(svgns, 'image');
            delete_image.setAttribute("width","16");
            delete_image.setAttribute("height","16");
            delete_image.setAttribute("x",82);
            delete_image.setAttribute("y",-15);
            delete_image.setAttribute("display","none");
            title = document.createElementNS(svgns, 'title');
            title.textContent = "delete activity";
            delete_image.appendChild(title);
            
            var thumbtackImg = document.createElementNS(svgns, 'image');
            thumbtackImg.setAttribute("width","16");
            thumbtackImg.setAttribute("height","16");
            thumbtackImg.setAttribute("x",28);
            thumbtackImg.setAttribute("y",-16);
            thumbtackImg.setAttribute("display","none");
            title = document.createElementNS(svgns, 'title');
            title.textContent = "show/hide annotation";
            thumbtackImg.appendChild(title);

            var assign_userImg = document.createElementNS(svgns, 'image');
            assign_userImg.setAttribute("width","16");
            assign_userImg.setAttribute("height","16");
            assign_userImg.setAttribute("x",2);
            assign_userImg.setAttribute("y",-16);
            assign_userImg.setAttribute("display","none");
            title = document.createElementNS(svgns, 'title');
            title.textContent = "assign role to activity";
            assign_userImg.appendChild(title);

            var notificationImg = document.createElementNS(svgns, 'image');
            notificationImg.setAttribute("width","16");
            notificationImg.setAttribute("height","16");
            notificationImg.setAttribute("x",28);
            notificationImg.setAttribute("y",49);
            notificationImg.setAttribute("display","none");
            title = document.createElementNS(svgns, 'title');
            title.textContent = "send notification";
            notificationImg.appendChild(title);

            activityRect.appendChild(image);
            activityRect.appendChild(thumbtackImg);
            activityRect.appendChild(delete_image);
            activityRect.appendChild(assign_userImg);
            activityRect.appendChild(notificationImg);

            if (navigator.appName == 'Microsoft Internet Explorer'){
                activityRect.addEventListener('mousedown', dragger,false);
                activityRect.addEventListener('mouseup', onMouseUp,false);
                activityRect.addEventListener('mousemove', onMouseMove,false);
                activityRect.addEventListener('mouseover', onMouseOver,false);
            }
            else{
                image.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/resources.png");
                delete_image.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/delete.gif");
                thumbtackImg.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/thumbtack.png");
                assign_userImg.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/add_user.png");
                notificationImg.setAttributeNS("http://www.w3.org/1999/xlink","href","../images/ico_envelope.png");

                activityRect.addEventListener('mousedown', Grab,false);
                activityRect.addEventListener('touchstart', Grab,false);                
                
                activityRect.addEventListener('mouseup', Drop,false);
                activityRect.addEventListener('touchend', Drop,false);
                
                activityRect.addEventListener('mousemove', Drag,false);
                activityRect.addEventListener('touchmove', Drag,false);
                
                activityRect.addEventListener('mouseover', onMouseOver,false);
                activityRect.addEventListener('touchend', onMouseOver,false);                
                
                activityRect.addEventListener('mouseout',function(evt){               
                    image.setAttribute("display","none");
                    delete_image.setAttribute("display","none");
                    thumbtackImg.setAttribute("display","none");
                    assign_userImg.setAttribute("display","none");
                    notificationImg.setAttribute("display","none");
                    this.setAttribute('mousemoveControl',"false");                    
                    this.childNodes[0].style.stroke="#000";
                    HideTooltip(evt)
                } ,false);
            }
            var annotationElem = annotation.createSVGAnnotationElement();
            activityRect.appendChild(annotationElem);            
            root.appendChild(activityRect);
            if(annotationDesc!=""){
                annotation.saveAnnotation(annotationElem, annotationDesc,-1)
            }
            
            return activityRect;

    }
    catch(e){
        console.log(e)
        if(activityRect!=null)
            root.removeChild(activityRect);
    }
}


function select(){
    to_connect = false;
    to_delete = false;
    document.getElementById("selectOpt").style.backgroundColor = "#ffc77e";
    document.getElementById("connectOpt").style.backgroundColor = "transparent";
    document.getElementById("deleteSVGElement").style.backgroundColor = "transparent";
    document.getElementById('svgPanel').contentDocument.getElementById("vectorGraphicsDoc").style.cursor = "move";
}
function connect(connectOpt){
    try{//url('images/pen.cur')
        to_connect = true;
        to_delete = false;
        document.getElementById("selectOpt").style.backgroundColor = "transparent";
        document.getElementById("deleteSVGElement").style.backgroundColor = "transparent";
        connectOpt.style.backgroundColor = "#ffc77e";
        document.getElementById('svgPanel').contentDocument.getElementById("vectorGraphicsDoc").style.cursor = "url('../images/oth203.cur'),auto";


    }
    catch(e){
        console.log("connect "+e);
    }
}
function deleteActivity(activity,option){
    try{
        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById("vectorGraphicsDoc");
        var textPath = null;

        if(option==0)
            if (!confirm("You are going to delete activity ")==true)
                return;
        to_delete = true;
        to_connect = false;
        var del = null;        
        //remove from learning Activites View        
        del = document.getElementById(activity.getId()+"ActivityRow");     
        if(del!=null)
            del.parentNode.removeChild(del);


        //remove from phase view
        var phase = getActivityPhase(activity.getId());
        if(phase==null)
            return;
        
        del = document.getElementById(activity.getId() + "inActivitiesTree");
        if(del!=null)
            del.parentNode.removeChild(del);
        
        //remove from phase activites list
        var activities = phase.getLearningActivitiesArray();
        var activityIndex = activities.indexOfActivity(activity.getId());
        if(activityIndex!=1)
            activities.deleteActivityFromList(activityIndex);
        
        //remove from connectionlist
        var connections = activities.getConnectionsArray();
        var tmpConnections = activities.getConnectionsArray().slice();
        for(var i=0;i<tmpConnections.length;i++){
            var con = tmpConnections[i];
            
            if(con.getAttribute("from")==activity.getId()){// || con.getAttribute("to")==e.currentTarget.getAttribute("id")){
                removeConnection(con,connections);
                //empty his flow childs
                activity.setflowChilds([]);
                removeFromGateway(con.getAttribute("to"),activity.getId(),phase)
                if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta koynaei
                    textPath = root.getElementById("textRefered_"+con.getAttribute("id"));
                    if(textPath!=null)
                        textPath.parentNode.removeChild(textPath);
                }
            }
            if(con.getAttribute("to")==activity.getId()){
                removeConnection(con,connections);
                //bres ton patera toy kai vgale ayto apo to array poy exei ta paidia toy patera
                removeFromParent(con.getAttribute("from"),activity.getId(),phase);
                
                if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta eksafanizei
                    textPath = root.getElementById("textRefered_"+con.getAttribute("id"));
                    if(textPath!=null)
                        textPath.parentNode.removeChild(textPath);
                }
            }
            
            
        }
        //delete from svg panel
        deleteActivityFromPanel(activity.getId());

        //subtract role
        //var roles = activity.getRolesAssigned()
        var roleId = ""        
        for(var j=0;j<roleBeingUse.length;j++){
            roleId = roleBeingUse[j].substr(roleBeingUse[j].indexOf("-")+1)
            if(roleId==activity.getId()){
                roleBeingUse.splice(j,1);
            }
        }
        /*var checkBoxRole;
        var index,role_id;
        for(i=0;i<activity.getRolesAssigned().length;i++){                       
            checkBoxRole = activity.getRolesAssigned()[i];
            document.getElementById(checkBoxRole.id).checked = false;
            index = (checkBoxRole.id).indexOf("_");
            role_id = (checkBoxRole.id).substring(0, index);

            for(var j=0;j<notificationArray.length;j++){
                index = notificationArray[j].getReceivers().containsIdInArray(role_id+"_completion")
                if(index!=-1)
                  notificationArray[j].getReceivers().splice(index,1);
            }
            
        }*/
        //select();
        undoActionList.doActionFollow("deleteActivity", {'param1':activity,'param2':$('#phaseTab').attr("name")})
    }
    catch(e){
        console.log("deleteActivity "+e);
    }

}


function getShapes(activities){
    var act = activities.getActivities();
    for(var i=0;i<act.length;i++){
        alert(act[i].get_Shape());
    }
}

function connection (obj1, obj2) {
  try{
  
        var bb1width = parseFloat(obj1.childNodes[0].getAttribute("width"));
        var bb1height = parseFloat(obj1.childNodes[0].getAttribute("height"));//obj1.getBBox();
        var bb2width = parseFloat(obj2.childNodes[0].getAttribute("width"));
        var bb2height = parseFloat(obj2.childNodes[0].getAttribute("height"));//obj1.getBBox();
        //var bb2 = obj2.getBBox();
       
        var bb1Xpos = parseFloat(obj1.getAttribute("dragX"));
        var bb1Ypos = parseFloat(obj1.getAttribute("dragY"));

        var bb2Xpos = parseFloat(obj2.getAttribute("dragX"));
        var bb2Ypos = parseFloat(obj2.getAttribute("dragY"));

        var svgDoc = document.getElementById('svgPanel').contentDocument;
       // var root = svgDoc.getElementsByTagNameNS(svgns, 'svg')[0];
       var root = svgDoc.getElementById("vectorGraphicsDoc");

        var p = [{x:  bb1Xpos + bb1width / 2, y:  bb1Ypos - 1},
        {x:  bb1Xpos + bb1width / 2, y:  bb1Ypos + bb1height + 1},
        {x:  bb1Xpos - 1, y:  bb1Ypos + bb1height / 2},
        {x:  bb1Xpos + bb1width + 1, y:  bb1Ypos + bb1height / 2},
        {x:  bb2Xpos + bb2width / 2, y:  bb2Ypos - 1},
        {x:  bb2Xpos + bb2width / 2, y: bb2Ypos + bb2height + 1},
        {x: bb2Xpos - 1, y: bb2Ypos + bb2height / 2},
        {x: bb2Xpos + bb2width + 1, y: bb2Ypos + bb2height / 2}];

        var d = {}, dis = [];
        for (var i = 0; i < 4; i++) {
            for (var j = 4; j < 8; j++) {
                var dx = Math.abs(p[i].x - p[j].x);
                    dy = Math.abs(p[i].y - p[j].y);
                if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                    dis.push(dx + dy);
                    d[dis[dis.length - 1]] = [i, j];
                }
            }
        }
    if (dis.length == 0) {
        var res = [0, 4];
    } else {
        res = d[Math.min.apply(Math, dis)];
    }
    var x1 = p[res[0]].x,
        y1 = p[res[0]].y,
        x4 = p[res[1]].x,
        y4 = p[res[1]].y,
        dx = Math.max(Math.abs(x1 - x4) / 2, 10),

        dy = Math.max(Math.abs(y1 - y4) / 2, 10);
        var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3);
        var y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3);
        var x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3);
        var y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);

        var midx = parseInt(x1.toFixed(3)) + parseInt(x4.toFixed(3));
        var midy = parseInt(y1.toFixed(3)) + parseInt(y4.toFixed(3)) ;
        
        //var p1 = "M"+x1.toFixed(3)+" "+y1.toFixed(3)+" C"+x2+" "+y2+" "+x3+" "+y3+" "+x4.toFixed(3)+" "+y4.toFixed(3);
        var line_id = obj1.getAttribute("id")+"_"+obj2.getAttribute("id");
        var p1 = "M"+x1.toFixed(3)+" "+y1.toFixed(3)+" L"+(midx/2)+" "+(midy/2)+" L"+x4.toFixed(3)+" "+y4.toFixed(3);

        var path = document.createElementNS(svgns, 'path');
        path.setAttribute('d',p1);        
        path.setAttribute('id',line_id);
        path.setAttribute("stroke-width","2");
        path.setAttribute("stroke","#000");
        
        //path.setAttribute("marker-end","url(#marker1)");

        path.setAttribute("marker-mid","url(#marker1)");
        path.setAttribute("fill","none");
        path.setAttribute("from",obj1.getAttribute("id"));
        path.setAttribute("fromType",obj1.getAttribute("ObjType"));

        path.setAttribute("to",obj2.getAttribute("id"));
        path.setAttribute("toType",obj2.getAttribute("ObjType"));
        
        var texts = root.getElementsByTagName("text");
        var text = null;
        for(var i=0;i<texts.length;i++){
            if(texts[i].getAttribute("id")=="textRefered_"+line_id){
                text = texts[i]
                break;
            }
        }

        if(text!=null){//diladi iparxei text panw stin grammi opote milame gia or condition
            var positionText = Math.abs((bb1Xpos-bb2Xpos)/2)
            text.setAttribute("dx",Math.abs(positionText-30))
        }
        path.addEventListener("mouseover",function(){           
            if(to_delete){
                this.style.stroke="#FF0000";
            }
        }, false);
        path.addEventListener("mouseout",function(){
            
            //if(this.style.stroke=="#FF0000"){
                this.style.stroke="#000";
            //}
        }, false);

        path.addEventListener("mouseup",function(){

            if(to_delete){
                var texts = null;
                var text = null;
                var connections = getCurrentActivityList().getConnectionsArray();
                if (!confirm("You are going to delete connection")==true)
                    return;

                var this_id = this.getAttribute("id");
                var id = "textRefered_"+ this_id;
                texts = root.getElementsByTagName("text");                
                for(var i=0;i<texts.length;i++){
                    text = texts[i];                    
                    if(text.getAttribute("id")==id){                        
                        text.parentNode.removeChild(text);                        
                        break;
                    }
                }
                //remove toObject elemements from fromObjectElement
                //afou vazw kathe paidi tou object-kombou se lista, prepei na to afairw

                var current_phase = allPhases.getPhase($('#phaseTab').attr("name"));
                var fromObjId = this_id.substring(0,this_id.indexOf("_",0));//giati to path id einai tou tipou fromObjId_toObjectId
                var gotoObjId = this_id.substring(this_id.indexOf("_",0)+1,this_id.length);//giati to path id einai tou tipou fromObjId_toObjectId

                var fromObj = null;
                var gotoObj = null;
                var gateway = false;
                var inclusiveOr = false;
                //tsekare mipws to from object einai to start
                console.log(fromObj)
                
                fromObj = getCurrentActivityList().getActivities().containsId(fromObjId);
                if(fromObj==null)//psakse mipws to fromObj eiani activity-structure
                    fromObj = getCurrentStructures().containsId(fromObjId);
                if(fromObj==null){
                    fromObj = current_phase.getInclusiveOrGateways().containsName(fromObjId)
                    if(fromObj!=null){
                        gateway = true;
                        inclusiveOr = true;
                    }
                }
                if(fromObj==null){
                    fromObj = current_phase.getJoinGateways().containsName(fromObjId);
                    if(fromObj!=null)
                        gateway = true;
                }

               if(fromObj==null)
                    console.log("from object is null")
                else{
                    var itemIndex = -1;
                    if(gateway){
                        if(!inclusiveOr){
                            itemIndex = fromObj.getObjGoTo().indexOfStruct(gotoObjId);
                            if(itemIndex!=null)
                                fromObj.getObjGoTo().splice(itemIndex,1)
                        }
                        else{
                            itemIndex = indexOfLineInInclusiveOrList(fromObj.getObjGoTo(),fromObjId+gotoObjId);
                            if(itemIndex!=null)
                                fromObj.getObjGoTo().splice(itemIndex,1)

                        }

                    }
                    else{
                        itemIndex = fromObj.getflowChilds().indexOfStruct(gotoObjId);                        
                        if(itemIndex!=null)
                            fromObj.getflowChilds().splice(itemIndex,1)

                    }
                }

                gotoObj = current_phase.getInclusiveOrGateways().containsName(gotoObjId)
                if(gotoObj==null)
                    gotoObj = current_phase.getJoinGateways().containsName(gotoObjId);
                if(gotoObj!=null){
                    itemIndex =  gotoObj.getObjFrom().indexOfStruct(fromObjId);                    
                    if(itemIndex!=null)
                        gotoObj.getObjFrom().splice(itemIndex,1)
                }
                
                removeConnection(this, connections)

            }
        }, false);

        root.appendChild(path);
        return path;

  }
  catch(e){
      console.log("connection "+e);
  }
}

function removeConnection(connection,connections){
    try{
        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById("vectorGraphicsDoc");
        var PathConnections = svgDoc.getElementsByTagNameNS(svgns,"path");//auto den paizei ston IE         
      
         for(var i=0;i<PathConnections.length;i++){
             if(PathConnections[i].getAttribute("id")==connection.getAttribute("id")){
                 root.removeChild(PathConnections[i]);                 
                 break;
             }

         }
         for(i=0;i<connections.length;i++){
             if(connection.getAttribute("id")==connections[i].getAttribute("id")){
                 connections.splice(i, 1);
             }
         }

         
    }
    catch(e){
        console.log("rmoveconnection "+e);
    }
}


Array.prototype.contains = function (element){
    for (var i = 0; i < this.length; i++){
        if(this[i].getAttribute("id") == element){            
            return true;
        }
    }
          return false;
};
Array.prototype.containsRole = function (element){
    for (var i = 0; i < this.length; i++){
        if(this[i].getName() == element){
            return this[i];
        }
    }
          return null;
};

Array.prototype.containsName = function (element){
    for (var i = 0; i < this.length; i++){        
        if(this[i].getName() == element){
            return this[i];
        }
    }
          return null;
};
Array.prototype.containsId = function (element){
    for (var i = 0; i < this.length; i++){
        if(this[i].getId() == element){
            return this[i];
        }
    }
          return null;
};
Array.prototype.containsNameInArray = function (element){
    for (var i = 0; i < this.length; i++){
        if(this[i] == element){
            return this[i];
        }
    }
          return null;
};
Array.prototype.containsAttributeId = function (id){   
   for (var i = 0; i < this.length; i++){       
        if(this[i].getAttribute("id") == id){
            return this[i];
        }
    }
          return null;
};
 
 function getCurrentActivityList(){
        try{
            //var phase = allPhases.getPhase(document.getElementById('phaseTab').innerHTML);
            //console.log("to name einai "+$('#phaseTab').attr("name"))
            var phase = allPhases.getPhase($('#phaseTab').attr("name"));
         
            if(phase=="-1")
                return null;

            return phase.getLearningActivitiesArray();
        }
        catch(e){
            alert('error while proccessing the specific phase. Phase does not exist '+e);
        }
    }
function getCurrentStructures(){
        try{
            //var phase = allPhases.getPhase(document.getElementById('phaseTab').innerHTML);
            var phase = allPhases.getPhase(document.getElementById('phaseTab').name);

            if(phase=="-1")
                return null;
            
            return phase.getActivitiesStructuresArray();
        }
        catch(e){
            alert('error while proccessing the specific phase. Phase does not exist');
        }
}
Array.prototype.clone = function(array,type){
    if(array.length==0)
        this.push(["","createNew"+type,""]);


    for(var i=0;i<array.length;i++)
        this.push(array[i].slice(0));        
    
    return this;
}

function deleteActivityFromPanel(svgObj){
    try{
        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById("vectorGraphicsDoc");
     
        for(var i=0;i<root.childNodes.length;i++){            
            if(root.childNodes[i].nodeType==1)
                if(root.childNodes[i].getAttribute("id")==svgObj)
                    root.removeChild(root.childNodes[i]);
                
        }
    }
    catch(e){
        console.log("DeleteActivityFromPanel "+e);
    }
}



function addEndPoint(id,currentPhaseName,positionX,positionY){
    try{

            var currentPhase = allPhases.getPhase(currentPhaseName)

            if(currentPhase==-1)
                console.log("error while creating end point of phase with name "+currentPhaseName)
            if(currentPhase.getType()=="activity-structure"){
               return;
            }

            if(currentPhase.getConditionType()!="CompleteActivities"){
              alert('if \'When Activities have been completed\' as the completion criterion of this act is not selected end point is meaningless and when you re-open learning design this element will not appear')
            }
            //check if exists this id
            if(currentPhase.getEndPoint().containsAttributeId(id)){
                return;
            }
            var svgDoc = document.getElementById('svgPanel').contentDocument;
            var root = svgDoc.getElementById("vectorGraphicsDoc");
            var num = currentPhase.getEndPoint().length;
            var endPoint = document.createElementNS(svgns, 'g');
            if(id==-1)
                endPoint.setAttribute('id',currentPhase.getId()+"end"+num);
            else
                endPoint.setAttribute('id',id);
    
            endPoint.setAttribute('ObjType',"endPoint");

            //currentPhase.setEndPoint(endPoint);
            currentPhase.addEndPoint(endPoint);

            var circle = document.createElementNS(svgns, 'circle');
            circle.setAttribute("r", "12");
            circle.setAttribute("cx", 20);
            circle.setAttribute("cy", 20);
            circle.setAttribute("width", 40);//width 15, ta xreiazomai ta attributes gia to connection
            circle.setAttribute("height",40);//height 15, kai oxi gia tin dimiourgia tou sxhmatos
            circle.setAttribute("fill", "#fff");
            circle.setAttribute("stroke", "#ce0000");
            circle.setAttribute("stroke-width", "2");

            var rect = document.createElementNS(svgns, 'rect');
            rect.setAttribute("width", "10");
            rect.setAttribute("height", "10");
            rect.setAttribute("y", 15);
            rect.setAttribute("x", 15);
            endPoint.appendChild(circle);
            endPoint.appendChild(rect);

            
            
            endPoint.setAttribute('dragX',positionX);
            endPoint.setAttribute('dragY',positionY);
            endPoint.vTranslate = [positionX,positionY];
            endPoint.setAttribute("transform", "translate(" + positionX + " " + positionY +")");
           
            endPoint.addEventListener('mousedown', Grab,false);
            endPoint.addEventListener('touchstart', Grab,false);
            
            endPoint.addEventListener('mouseup', Drop,false);
            endPoint.addEventListener('touchend', Drop,false);
            
            endPoint.addEventListener('mousemove', Drag,false);
            endPoint.addEventListener('touchmove', Drag,false);
            
            endPoint.addEventListener('mouseover', onMouseOver,false);
            endPoint.addEventListener('touchend', onMouseOver,false);
            
            endPoint.addEventListener('mouseout',function(){
                    this.setAttribute('mousemoveControl',"false");  
                    this.childNodes[0].style.stroke="#ce0000";
                } ,false);

            root.appendChild(endPoint);        
            select();
        
    }
    catch(e){
        console.log("addEndPoint "+e)
    }
}

//var start=0;

function addStartPoint(positionX,positionY){
    try{
        //if(start==0){
            //start++;
              
            var svgDoc = document.getElementById('svgPanel').contentDocument;
            var root = svgDoc.getElementById("vectorGraphicsDoc");
            var startPoint = document.createElementNS(svgns, 'g');

            var currentPhase = allPhases.getPhase(document.getElementById('phaseTab').name)
            if(currentPhase.getStartPoint()!=null)
                return -1;
            
            currentPhase.setStartPoint(startPoint);
            
            startPoint.setAttribute('id',currentPhase.getId()+"start");
            startPoint.setAttribute('ObjType',"startPoint");

            var circle = document.createElementNS(svgns, 'circle');
            circle.setAttribute("r", "12");//r,15
            circle.setAttribute("width", "35");//width 15, ta xreiazomai ta attributes gia to connection
            circle.setAttribute("height", "35");//height 15, kai oxi gia tin dimiourgia tou sxhmatos
            circle.setAttribute("cx", "20");
            circle.setAttribute("cy", "20");
            circle.setAttribute("fill", "#fff");
            circle.setAttribute("stroke", "#ce0000");
            circle.setAttribute("stroke-width", "2");
            var poly = document.createElementNS(svgns, 'polygon');

            poly.setAttribute("points", "30,20 14,26 14,13");//32,20 10,30 10,10
            poly.setAttribute("fill", "#cccccc");
            poly.setAttribute("stroke", "#000000");
            poly.setAttribute("stroke-width", "1");
            startPoint.appendChild(circle);
            startPoint.appendChild(poly);
 
   
            
            startPoint.setAttribute('dragX',positionX);//(10*size)
            startPoint.setAttribute('dragY',positionY);//(10*size)
            startPoint.vTranslate = [positionX,positionY];
            startPoint.setAttribute("transform", "translate(" + positionX + " " + positionY +")");
            if (navigator.appName == 'Microsoft Internet Explorer'){
                startPoint.addEventListener('mousedown', dragger,false);
                startPoint.addEventListener('mouseup', onMouseUp,false);
                startPoint.addEventListener('mousemove', onMouseMove,false);
                startPoint.addEventListener('mouseover', onMouseOver,false);
            }
            else{
                startPoint.addEventListener('mousedown', Grab,false);
                startPoint.addEventListener('touchstart', Grab,false);
                
                startPoint.addEventListener('mouseup',Drop ,false);
                startPoint.addEventListener('touchend', Drop,false);
                
                startPoint.addEventListener('mousemove',Drag,false);
                startPoint.addEventListener('touchmove', Drag,false);
                
                startPoint.addEventListener('mouseover', onMouseOver,false);
                startPoint.addEventListener('touchend', onMouseOver,false);
                
                startPoint.addEventListener('mouseout',function(){
                        this.setAttribute('mousemoveControl',"false");  
                        this.childNodes[0].style.stroke="#ce0000";
                    } ,false);
            }

            root.appendChild(startPoint);
            select();

            return currentPhase.getId()+"start";
        //}
    }
    catch(e){
        console.log("addStartPoint "+e)
    }
}
    function deleteStartPoint(id,curr_phase,option){
        if(option!=0)
            if (!confirm("You are going to delete start point")==true)
                return;
        //var curr_phase = allPhases.getPhase(document.getElementById('phaseTab').name);
                     //remove from panel
        deleteActivityFromPanel(id);
             //remove from connections         
         var conections = curr_phase.getLearningActivitiesArray().getConnectionsArray();
         
         var tmpConnections = conections.slice();
         
         for(var i=0;i<tmpConnections.length;i++){
            var con = tmpConnections[i];            
            if(con.getAttribute("from")==curr_phase.getId()+"start"){
                removeConnection(con,conections);
            }
        }
        //remove from phase
        curr_phase.setStartPoint(null);
    }
    function deleteEndPoint(id,curr_phase,option){
        if(option!=0)
            if (!confirm("You are going to delete end point")==true)
                return;
        var textPath = null;
        var svgDoc = document.getElementById('svgPanel').contentDocument;
        var root = svgDoc.getElementById("vectorGraphicsDoc");
        //var curr_phase = allPhases.getPhase(document.getElementById('phaseTab').name);
                     //remove from panel
        deleteActivityFromPanel(id);
             //remove from connections         
         var conections = curr_phase.getLearningActivitiesArray().getConnectionsArray();
         var tmpConnections = conections.slice();
         for(var i=0;i<tmpConnections.length;i++){
            var con = tmpConnections[i];        
            if(con.getAttribute("to") == id){//curr_phase.getId()+"end"){
                //remove from parent
                //bres ton patera toy kai vgale ayto apo to array poy exei ta paidia toy patera
                removeFromParent(con.getAttribute("from"),id,curr_phase);
                removeConnection(con,conections);

                if(navigator.userAgent.indexOf("Chrome")!=-1){//ston mozilla ta koynaei
                    textPath = root.getElementById("textRefered_"+con.getAttribute("id"));
                    if(textPath!=null)
                        textPath.parentNode.removeChild(textPath);
                }
            }
        }
        //remove from phase
        var endPoints = curr_phase.getEndPoint();
        var index = endPoints.indexOfSVGObj(id);
        if(index!=-1)
            endPoints.slice(index,1);
        //curr_phase.setEndPoint(null)

    }
    function removeFromParent(parentId,childId,current_phase){
        try{
           
            var parent = null,gateway=false,itemIndex=null,inclusiveOr=false;
            parent = current_phase.getLearningActivitiesArray().getActivities().containsId(parentId);
            if(parent == null)//psakse mipws to parent eiani activity-structure
                parent = current_phase.getActivitiesStructuresArray().containsId(parentId);
            if(parent==null){
                parent = current_phase.getInclusiveOrGateways().containsName(parentId);                
                if(parent!=null){
                    gateway = true;
                    inclusiveOr = true;
                }
            }
            if(parent==null){
                parent = current_phase.getJoinGateways().containsName(parentId);
                if(parent!=null)
                    gateway = true;
            }
            if(parent==null)
                console.log("parent node is null");
            else{
                if(gateway){
                    if(!inclusiveOr){
                         itemIndex = parent.getObjGoTo().indexOfStruct(childId);
                         if(itemIndex!=null)
                            parent.getObjGoTo().splice(itemIndex,1)
                    }
                    else{
                        itemIndex = indexOfLineInInclusiveOrList(parent.getObjGoTo(),parentId+childId);
                            if(itemIndex!=null)
                                parent.getObjGoTo().splice(itemIndex,1)

                    }

                }
                else{

                    itemIndex = parent.getflowChilds().indexOfStruct(childId);
            
                    if(itemIndex!=null)
                        parent.getflowChilds().splice(itemIndex,1)
                }
            }

        }
        catch(e){
            console.log(e)
        }
    }
    function removeFromGateway(gatewayId,activityId,current_phase){

        var gateway = null,itemIndex=null;
        gateway = current_phase.getInclusiveOrGateways().containsName(gatewayId)
        if(gateway==null)
            gateway = current_phase.getJoinGateways().containsName(gatewayId);
        if(gateway!=null){
            itemIndex =  gateway.getObjFrom().indexOfStruct(activityId);
            if(itemIndex!=null)
                gateway.getObjFrom().splice(itemIndex,1)
        }
    }

    function EndPointObj(id,positionX,positionY){
        this.id = id;
        this.positionX = positionX
        this.positionY = positionY
        this.propertyExpression = null;
        this.htmlExpressionView = null;

        this.setPropertyExpression = function(propExpression){
            this.propertyExpression = propExpression;
        }
        this.getPropertyExpression = function(){
            return this.propertyExpression;
        }
        this.getPositionX = function(){
            return positionX
        }
        this.getPositionY = function(){
            return positionY
        }
        this.getId = function(){
            return this.id
        }
        this.getType = function(){
            return "endPoint";
        }
        this.gethtmlExpressionView = function(){
            return this.htmlExpressionView;
        }
        this.sethtmlExpressionView = function(div){
            this.htmlExpressionView = div.clone(true);
            this.htmlExpressionView.find("#conditionType").val($("#conditionType").val())
           
        }
    }
    //apo dw gia collison
               //;


               /*if(e.currentTarget.getAttribute("ObjType")=="activity"){
                    structureBox = activityStructuresArray.checkCollision(e.currentTarget,structures);

                    if(structureBox!=null && lock==false){
                       lock = true;
                       structureBox.getShape().childNodes[0].setAttribute('stroke', "#fff");
                       structureBox.getShape().childNodes[0].setAttribute("fill-opacity", '0');
                       tmp = structureBox;
                       activityStructuresArray.addObject = structureBox;
                    }
                    else if(structureBox==null){
                        activityStructuresArray.addObject = null;
                        if(tmp!=null){
                            tmp.getShape().childNodes[0].setAttribute('stroke', "#000");
                            tmp.getShape().childNodes[0].setAttribute("fill-opacity", '1');
                            tmp = null;
                            lock = false;
                        }
                    }
               }*/
              //ws edw