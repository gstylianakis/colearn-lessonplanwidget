/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ODSPortalIntegration;

import StudentOperationsEJBs.DB_connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import org.json.simple.JSONValue;

/**
 * REST Web Service
 *
 * @author stylianos
 */
@Path("getLDFromGroupID/{group_id}/{status}")

public class GetLDFromGroupID {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GetLDFromGroupID
     */
    public GetLDFromGroupID() {
    }

    /**
     * Retrieves representation of an instance of ODSPortalIntegration.GetLDFromGroupID
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getJson(@PathParam("group_id") String group_id, @PathParam("status") String status) throws Exception{
        System.out.println("eimai sthn getLDfrom groupID");
        DB_connection con = new DB_connection();
        Statement stmt = null;  
        ResultSet rs = null;   
       
        try{          
            
            List group_ld = new LinkedList();
            
            stmt = con.getConnection().createStatement();
            
            rs = stmt.executeQuery("SELECT f.manifestTitle, f.lessonSubject, f.name FROM manifest_group m inner join "+
                                   "manifestfiles f on m.manifest_name=f.name where m.Group_name= (select Name from team where ODS_NODE_ID="+group_id+") and m.Deployed="+status
                                  );
            while (rs.next()) {
                Map ld = new LinkedHashMap();
                ld.put("title",rs.getString(1));
                ld.put("subject", rs.getString(2));
                ld.put("manifest_id", rs.getString(3));
                group_ld.add(ld);
            }            
            return JSONValue.toJSONString(group_ld);

        }
        catch(Exception e){
            System.out.println("error while getting group designs ");
            e.printStackTrace();
            throw e;
        }
        finally{
            rs.close();
            stmt.close();
            con.close();
        }
    }

    /**
     * PUT method for updating or creating an instance of GetLDFromGroupID
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    /*@PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }*/
}
