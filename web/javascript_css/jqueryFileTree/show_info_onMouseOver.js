/*<!-- Copyright 2002 Bontrager Connection, LLC

LL_infoID = new Array();




LL_infoID[1] = "mouse_definition";
LL_infoID[2] = "house_definition";
LL_infoID[3] = "email_format_prompt";
LL_infoID[4] = "needTwoOperands";

//
// End of customization sections.
///////////////////////////////////////////////////////////////


LL_infoID[0] = "unused";
bNS4 = bNS6 = bIE = bOPERA = false;
if     (navigator.userAgent.indexOf("Opera") != -1) { bOPERA = true; }
else if(navigator.userAgent.indexOf("Gecko") != -1) { bNS6 = true;   }
else if(document.layers)                            { bNS4 = true;   }
else if(document.all)                               { bIE = true;    }

LLx = LLxx = LLy = LLyy = 0;
var LL_mousex;
var LL_mousey;
var STO = null;
var SET = false;
var cID = '';
if(bNS4 || bNS6 || bOPERA) { document.captureEvents(Event.MOUSEMOVE); }
document.onmousemove = LL_getmouseposition;


// Functions used by all browsers

function Null() { return; }

function LL_getmouseposition(e)
{
if(bIE || bOPERA) {
	LL_mousex = event.clientX;
	LL_mousey = event.clientY;
	}
else if(bNS6 || bNS4) {
	LL_mousex = e.pageX;
	LL_mousey = e.pageY;
	}
} // LL_getmouseposition()




// Functions to relay browser type to custom functions

function LL_showinfo(m_section) {


LL_hideallinfo();
if(cID != m_section) { SET = false; }
cID = m_section;
     if(bIE)    { LL_bIE_showit(m_section); }
else if(bNS6)   { LL_bNS6_showit(m_section); }
else if(bOPERA) { LL_bOPERA_showit(m_section); }
else if(bNS4)   { LL_bNS4_showit(m_section); }
} // LL_showinfo()

function LL_hideallinfo(m_section) {
clearTimeout(STO);
     if(bIE)    { LL_bIE_hideallinfo(); }
else if(bNS6)   { LL_bNS6_hideallinfo(); }
else if(bOPERA) { LL_bOPERA_hideallinfo(); }
else if(bNS4)   { LL_bNS4_hideallinfo(); }
} // LL_hideallinfo()




// IE functions

function LL_bIE_hidesection(m_section) {
if(LL_mouseinrectangle()) { return; }
eval(LL_infoID[m_section] + '.style.visibility="hidden"');
} // LL_bIE_hidesection()

function LL_bIE_hideallinfo() {
for(i = 1; i < LL_infoID.length; i++) {
	eval(LL_infoID[i] + '.style.visibility="hidden"');
	}
} // LL_bIE_hideallinfo()

function LL_bIE_showit(m_section) {
	LL_bIE_hideallinfo();
	var x = LL_mousex + 1;
	if(x < 0) { x = 0; }
	var y = LL_mousey + 20;
	if(y < 0) { y = 0; }
	if(SET == false) {
		eval(LL_infoID[m_section] + '.style.left="' + x + '"');
		eval(LL_infoID[m_section] + '.style.top="' + y + '"');
		}
	eval(LL_infoID[m_section] + '.style.visibility="visible"');
	LLx = eval(LL_infoID[m_section] + '.style.pixelLeft');
	LLxx = eval(LL_infoID[m_section] + '.scrollWidth') + LLx;
	LLy = eval(LL_infoID[m_section] + '.style.pixelTop');
	LLyy = eval(LL_infoID[m_section] + '.scrollHeight') + LLy;
	SET = true;
	clearTimeout(STO);
	STO = setTimeout('SET = false',2000);
} // LL_bIE_showit()



// Netscape 6 functions

function LL_bNS6_hidesection(m_section) {
if(LL_mouseinrectangle()) { return; }
document.getElementById(LL_infoID[m_section]).style.visibility="hidden";
} // LL_bNS6_hidesection()

function LL_bNS6_hideallinfo() {
for(i = 1; i < LL_infoID.length; i++) {
	document.getElementById(LL_infoID[i]).style.visibility="hidden";
	}
} // LL_bNS6_hideallinfo()

function LL_bNS6_showit(m_section) {
	LL_bNS6_hideallinfo();
	var x = LL_mousex + 1;
	if(x < 0) { x = 0; }
	var y = LL_mousey + 20;
	if(y < 0) { y = 0; }
	if(SET == false) {
		document.getElementById(LL_infoID[m_section]).style.left = x + 'px';
		document.getElementById(LL_infoID[m_section]).style.top = y + 'px';
		}
	document.getElementById(LL_infoID[m_section]).style.visibility="visible";
	var padding = 0;
	if(parseInt(document.getElementById(LL_infoID[m_section]).style.padding) > 0) { padding = parseInt(document.getElementById(LL_infoID[m_section]).style.padding) * 2; }
	LLx = parseInt(document.getElementById(LL_infoID[m_section]).style.left);
	LLxx = parseInt(document.getElementById(LL_infoID[m_section]).style.width) + LLx + padding;
	LLy = parseInt(document.getElementById(LL_infoID[m_section]).style.top);
	LLyy = parseInt(document.getElementById(LL_infoID[m_section]).style.height) + LLy + padding;
	SET = true;
	clearTimeout(STO);
	STO = setTimeout('SET = false',2000);
} // LL_bNS6_showit()



// Opera 6 functions
function LL_bOPERA_hidesection(m_section) {
if(LL_mouseinrectangle()) { return; }
document.getElementById(LL_infoID[m_section]).style.visibility="hidden";
} // LL_bOPERA_hidesection()

function LL_bOPERA_hideallinfo() {
for(i = 1; i < LL_infoID.length; i++) {
	document.getElementById(LL_infoID[i]).style.visibility="hidden";
	}
} // LL_bOPERA_hideallinfo()

function LL_bOPERA_showit(m_section) {
	LL_bOPERA_hideallinfo();
	var x = LL_mousex + 1;
	if(x < 0) { x = 0; }
	var y = LL_mousey + 20;
	if(y < 0) { y = 0; }
	if(SET == false) {
		document.getElementById(LL_infoID[m_section]).style.left = x + 'px';
		document.getElementById(LL_infoID[m_section]).style.top = y + 'px';
		}
	document.getElementById(LL_infoID[m_section]).style.visibility="visible";
	var padding = 0;
	if(parseInt(document.getElementById(LL_infoID[m_section]).style.padding) > 0) { padding = parseInt(document.getElementById(LL_infoID[m_section]).style.padding) * 2; }
	LLx = parseInt(document.getElementById(LL_infoID[m_section]).style.left);
	LLxx = parseInt(document.getElementById(LL_infoID[m_section]).style.width) + LLx + padding;
	LLy = parseInt(document.getElementById(LL_infoID[m_section]).style.top);
	LLyy = parseInt(document.getElementById(LL_infoID[m_section]).style.height) + LLy + padding;
	SET = true;
	clearTimeout(STO);
	STO = setTimeout('SET = false',2000);
} // LL_bOPERA_showit()



// Netscape 4 functions

function LL_bNS4_hidesection(m_section) {
if(LL_mouseinrectangle()) { return; }
eval('document.' + LL_infoID[m_section] + '.visibility="hide"');
} // LL_bNS4_hidesection()

function LL_bNS4_hideallinfo() {
for(i = 1; i < LL_infoID.length; i++) {
	eval('document.' + LL_infoID[i] + '.visibility="hide"');
	}
} // LL_bNS4_hideallinfo()

function LL_bNS4_showit(m_section) {
	LL_bNS4_hideallinfo();
	var x = LL_mousex + 1;
	if(x < 0) { x = 0; }
	var y = LL_mousey + 20;
	if(y < 0) { y = 0; }
	if(SET == false) {
		eval('document.' + LL_infoID[m_section] + '.left="' + x + '"');
		eval('document.' + LL_infoID[m_section] + '.top="' + y + '"');
		}
	eval('document.' + LL_infoID[m_section] + '.visibility="show"');
	LLx = eval('parseInt(document.' + LL_infoID[m_section] + '.left)');
	LLxx = eval('parseInt(document.' + LL_infoID[m_section] + '.clip.width)') + LLx;
	LLy = eval('parseInt(document.' + LL_infoID[m_section] + '.top)');
	LLyy = eval('parseInt(document.' + LL_infoID[m_section] + '.clip.height)') + LLy;
	SET = true;
	clearTimeout(STO);
	STO = setTimeout('SET = false',2000);
} // LL_bNS4_showit()

//-->
*/

addEvent(window, "load", makeNiceTitles);

var XHTMLNS = "http://www.w3.org/1999/xhtml";
var CURRENT_NICE_TITLE;
var browser = new Browser();

function makeNiceTitles() {
    if (!document.createElement || !document.getElementsByTagName) return;
    // add namespace methods to HTML DOM; this makes the script work in both
    // HTML and XML contexts.
    if(!document.createElementNS)
    {
        document.createElementNS = function(ns,elt) {
            return document.createElement(elt);
        }
    }

    if( !document.links )
    {
        document.links = document.getElementsByTagName("a");
    }
    for (var ti=0;ti<document.links.length;ti++) {
        var lnk = document.links[ti];
        if (lnk.title) {
            lnk.setAttribute("nicetitle",lnk.title);
            lnk.removeAttribute("title");
            addEvent(lnk,"mouseover",showNiceTitle);
            addEvent(lnk,"mouseout",hideNiceTitle);
            //addEvent(lnk,"focus",showNiceTitle);
            addEvent(lnk,"blur",hideNiceTitle);
        }
    }
    var instags = document.getElementsByTagName("ins");
    if (instags) {
    for (var ti=0;ti<instags.length;ti++) {
        var instag = instags[ti];
        if (instag.dateTime) {
            var strDate = instag.dateTime;
            var dtIns = new Date(strDate.substring(0,4),parseInt(strDate.substring(4,6)-1),strDate.substring(6,8),strDate.substring(9,11),strDate.substring(11,13),strDate.substring(13,15));
            instag.setAttribute("nicetitle","Added on "+dtIns.toString());
            addEvent(instag,"mouseover",showNiceTitle);
            addEvent(instag,"mouseout",hideNiceTitle);
            //addEvent(instag,"focus",showNiceTitle);
            addEvent(instag,"blur",hideNiceTitle);
        }
    }
    }
}

function findPosition( oLink ) {
  if( oLink.offsetParent ) {
    for( var posX = 0, posY = 0; oLink.offsetParent; oLink = oLink.offsetParent ) {
      posX += oLink.offsetLeft;
      posY += oLink.offsetTop;
    }
    return [ posX, posY ];
  } else {
    return [ oLink.x, oLink.y ];
  }
}

function showNiceTitle(e) {
    if (CURRENT_NICE_TITLE) hideNiceTitle(CURRENT_NICE_TITLE);
    if (!document.getElementsByTagName) return;
    if (window.event && window.event.srcElement) {
        lnk = window.event.srcElement
    } else if (e && e.target) {
        lnk = e.target
    }
    if (!lnk) return;
    if (lnk.nodeName.toUpperCase() != 'A') {
        // lnk is not actually the link -- ascend parents until we hit a link
        lnk = getParent(lnk,"A");
    }
    if (!lnk) return;
    nicetitle = lnk.getAttribute("nicetitle");

    var d = document.createElementNS(XHTMLNS,"div");
    d.className = "nicetitle";
    tnt = document.createTextNode(nicetitle);
    pat = document.createElementNS(XHTMLNS,"p");
    pat.className = "titletext";
    pat.appendChild(tnt);
    d.appendChild(pat);
    /*if (lnk.href) {
        tnd = document.createTextNode(lnk.href);
        pad = document.createElementNS(XHTMLNS,"p");
        pad.className = "destination";
        pad.appendChild(tnd);
        d.appendChild(pad);
    }*/

    STD_WIDTH = 300;
    if (lnk.href) {
        h = lnk.href.length;
    } else { h = nicetitle.length; }
    if (nicetitle.length) {
      t = nicetitle.length;
    }
    h_pixels = h*6; t_pixels = t*10;

    if (h_pixels > STD_WIDTH) {
        w = h_pixels;
    } else if ((STD_WIDTH>t_pixels) && (t_pixels>h_pixels)) {
        w = t_pixels;
    } else if ((STD_WIDTH>t_pixels) && (h_pixels>t_pixels)) {
        w = h_pixels;
    } else {
        w = STD_WIDTH;
    }

    //d.style.width = w + 'px';

    /*
    mx = lnk.offsetLeft;
    my = lnk.offsetTop;
    */
    mpos = findPosition(lnk);
    mx = mpos[0];
    my = mpos[1];
    //xy = getMousePosition(e);
    //mx = xy[0]; my = xy[1];

    d.style.left = (mx-20) + 'px';
    d.style.top = (my-20) + 'px';
    if (window.innerWidth && ((mx+w) > window.innerWidth)) {
        d.style.left = (window.innerWidth - w - 25) + "px";
    }
    if (document.body.scrollWidth && ((mx+w) > document.body.scrollWidth)) {
        d.style.left = (document.body.scrollWidth - w - 25) + "px";
    }

    document.getElementsByTagName("body")[0].appendChild(d);

    CURRENT_NICE_TITLE = d;
}

function hideNiceTitle(e) {
    if (!document.getElementsByTagName) return;
    if (CURRENT_NICE_TITLE) {
        document.getElementsByTagName("body")[0].removeChild(CURRENT_NICE_TITLE);
        CURRENT_NICE_TITLE = null;
    }
}

// Add an eventListener to browsers that can do it somehow.
// Originally by the amazing Scott Andrew.
function addEvent(obj, evType, fn){
  if (obj.addEventListener){
    obj.addEventListener(evType, fn, false);
    return true;
  } else if (obj.attachEvent){
	var r = obj.attachEvent("on"+evType, fn);
    return r;
  } else {
	return false;
  }
}

function getParent(el, pTagName) {
	if (el == null) return null;
	else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())	// Gecko bug, supposed to be uppercase
		return el;
	else
		return getParent(el.parentNode, pTagName);
}

function getMousePosition(event) {
  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }
  return [x,y];
}

// Determine browser and version.

function Browser() {
// blah, browser detect, but mouse-position stuff doesn't work any other way
  var ua, s, i;

  this.isIE    = false;
  this.isNS    = false;
  this.version = null;

  ua = navigator.userAgent;

  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  // Treat any other "Gecko" browser as NS 6.1.

  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
}
