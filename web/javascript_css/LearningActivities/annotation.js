/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var annotation = {
    createSVGAnnotationElement: function(){
        var annotationElem = document.createElementNS(svgns, 'g');
        var title = document.createElementNS(svgns, 'title');
        title.textContent="click to edit annotation";
        annotationElem.appendChild(title);
        
        annotationElem.setAttribute("name","annotation");
        annotationElem.setAttribute("textContent","");
        annotationElem.setAttribute("display","none");
        var annotationBox = document.createElementNS(svgns, 'rect');
        annotationBox.setAttribute("x","0");
        annotationBox.setAttribute("y","-60");
        annotationBox.setAttribute("width","88");
        annotationBox.setAttribute("height","20");
        annotationBox.setAttribute("rx","2");
        annotationBox.setAttribute("ry","2");
        annotationBox.setAttribute("style", "stroke-dasharray: 5 2; fill: #fff; fill-opacity: 0.4; stroke-opacity: 1; stroke:black");
        annotationElem.appendChild(annotationBox);

        var text = document.createElementNS(svgns, 'text');
        text.setAttribute("x", 0);
        text.setAttribute("y", -45);
        text.setAttribute("font-family", "Arial");
        text.setAttribute("font-size",11);
        

        annotationElem.appendChild(text);

        var dotted_line = document.createElementNS(svgns, 'line');
        dotted_line.setAttribute("x1", 25);
        dotted_line.setAttribute("y1", 0);
        dotted_line.setAttribute("x2", 25);
        dotted_line.setAttribute("y1", -35);
        dotted_line.setAttribute("style", "stroke-dasharray: 9, 5; stroke: black; stroke-width:1;");
        annotationElem.appendChild(dotted_line);
        annotationElem.addEventListener("mouseover",function(e){
            e.stopPropagation()
            this.style.cursor='pointer';
        }, false)
        annotationElem.addEventListener("mousedown",function(e){            
            e.stopPropagation()
            annotation.editAnnotation(this)
        }, false)
        
        return annotationElem;
    },
    saveAnnotation:function(parent,descValue,option){
  
        var annotationBox = parent.getElementsByTagName('rect')[0];
        var annotationText = parent.getElementsByTagName('text')[0];
       
        var tspan = null;
        var lines = parseInt(descValue.length / 20);
        var restText = (descValue.length % 20);
        var text = "",start = 0,end=0;
        
        //empty text
        parent.setAttribute("textContent",descValue);
        if ( annotationText.hasChildNodes() ){
            while ( annotationText.childNodes.length >= 1 ){
                annotationText.removeChild( annotationText.firstChild );
            }
        }
        console.log("lines " +lines+ "annotationText " +annotationText.hasChildNodes())

        for(var i=0;i<lines;i++){
            if(i==0){
                start = i*20;
                end = (i+1)*20
            }
            else{
                start = (i*20)//+1;
                end = ((i+1)*20)//+1
            }

            text = descValue.substring(start, end)
            
            tspan = document.createElementNS(svgns, 'tspan');
            tspan.setAttribute("x", "5");
            if(i!=0)
                tspan.setAttribute("dy","10");
            tspan.textContent = text;           
            tspan.setAttribute("fill", "blue");
            tspan.setAttribute("display", "inline");
        //text.appendChild(tspan1)
            annotationText.appendChild(tspan);
        }
        tspan = document.createElementNS(svgns, 'tspan');
        tspan.setAttribute("x", "5");
            //tspan2.setAttribute("dy", "-45");
        
        if(restText!=0){     
            tspan.textContent = descValue.substr(end)
            tspan.setAttribute("fill", "blue");
            tspan.setAttribute("display", "inline");
            if(annotationText.childNodes.length!=0)
                tspan.setAttribute("dy", "10");
            annotationText.appendChild(tspan);
        }
     
         // if there are tooltip contents to be displayed, adjust the size and position of the box
         if ( '' != descValue )
         {
            //var xPos = TrueCoords.x + (10 * tipScale);
            //var yPos = TrueCoords.y + (10 * tipScale);

            //return rectangle around text as SVGRect object
            var outline;
            if(option==-1)
                outline={'width':96,'height':(lines*11)+10}
            else
                outline = annotationText.getBBox();
            
            var new_height = Number(outline.height) + 10;
            var old_height = annotationBox.getAttribute("height");
            var old_ypos = annotationBox.getAttribute("y");
     
            annotationBox.setAttribute('width', Number(outline.width) + 10);
            annotationBox.setAttribute('height', new_height);
            annotationBox.setAttribute('y', old_ypos-(new_height-old_height));
            annotationText.setAttribute('y', (old_ypos-(new_height-old_height))+15);
            parent.setAttribute("textContent",descValue);

         }

      },
      showAnnotation:function(thumbtack){
     
        var annotationG = thumbtack.parentNode.getElementsByTagName("g");
        var annotation = null;
        for(var i=0;i<annotationG.length;i++){
            if(annotationG[i].getAttribute("name")!=null)
                if(annotationG[i].getAttribute("name")=="annotation"){
                    annotation = annotationG[i];
                    break;
                }
        }
        if(annotation!=null){
            if(annotation.getAttribute("display")=="none")
                annotation.setAttribute("display","block");
                
            
            else
                annotation.setAttribute("display","none");

        }

      },
      editAnnotation: function(parent){
        var text = parent.getElementsByTagName("text")[0];
        var string = ""
        for(var i=0;i<text.childNodes.length;i++)
            string += text.childNodes[i].textContent
        $("#annotationTextArea").val(string)
        
        $("#annotation-form").data('parent', parent).dialog("open");
      }

      

}
$(function() {
    $( "#annotation-form" ).dialog({
        autoOpen: false,
	height: 300,
	width: 350,
	modal: false,
	buttons: {
            Cancel: function() {
                $( this ).dialog( "close" );
            },
            "save annotation": function() {
                annotation.saveAnnotation($(this).data('parent'),$("#annotationTextArea").val(),0)
                $( this ).dialog( "close" );
                
            }
            
        },
	close: function() {
            $("#annotationTextArea").val( "" );
	}
    });

    
});