/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var graphicDetails = {
    /*generateGraphicDetailsDoc:function(phases){
        try{
            var xmldoc = createXMLDocument("AuthoringToolGraphicDetails")
            var activity = null;
            var graphic = xmldoc.getElementsByTagName("AuthoringToolGraphicDetails")[0];
            var activityElements = xmldoc.createElement("activities");
            var gateways = xmldoc.createElement("gateways");
            graphic.appendChild(activityElements);
            graphic.appendChild(gateways);

            var activityElement = null,synchElement=null;

            for(var i=0;i<phases.getPhases().length;i++){
                var activities = phases.getPhases()[i].getLearningActivitiesArray();
                for(var j=0;j<activities.getActivities().length;j++){
                    activity = activities.getActivities()[j];
                    activityElement = xmldoc.createElement("activity");
                    activityElement.setAttribute("name",activity.getName());
                    activityElement.setAttribute("positionX",activity.get_Shape().vTranslate[0]);
                    activityElement.setAttribute("positionY",activity.get_Shape().vTranslate[1]);
                    activityElements.appendChild(activityElement);
                }
                var structures = phases.getPhases()[i].getActivitiesStructuresArray();
                for(j=0;j<structures.length;j++){
                    activityElement = xmldoc.createElement("activity-structure");
                    activityElement.setAttribute("name",structures[j].getName());
                    activityElement.setAttribute("positionX",structures[j].getShape().vTranslate[0]);
                    activityElement.setAttribute("positionY",structures[j].getShape().vTranslate[1]);
                    activityElements.appendChild(activityElement);
                }
                var synchPoints = phases.getPhases()[i].getJoinGateways();
                for(j=0;j<synchPoints.length;j++){
                    synchElement = xmldoc.createElement("synch-point");
                    synchElement.setAttribute("name",synchPoints[j].getName());
                    synchElement.setAttribute("positionX",synchPoints[j].getShape().vTranslate[0]);
                    synchElement.setAttribute("positionY",synchPoints[j].getShape().vTranslate[1]);
                    gateways.appendChild(synchElement);
                }
            }           
            return xmldoc;
        }
        catch(e){
            alert("error while creating graphics schema "+e);
        }
    }*/
    /*
    generateGraphicDetailsDoc:function(phases,ldManifest){
        try{
            var activity=null,activityElement=null;
            var activity_structure=null,synchElement=null,endPointElement;

            for(var i=0;i<phases.getPhases().length;i++){
                var activities = phases.getPhases()[i].getLearningActivitiesArray();
                for(var j=0;j<activities.getActivities().length;j++){
                    activity = activities.getActivities()[j];
                    activityElement = graphicDetails.getElementFromManifest("la-"+activity.getIdentifier()+"-"+activity.getName(),"learning-activity",ldManifest)
                    
                    if(activityElement!=null){
                        activityElement.setAttribute("positionX",activity.get_Shape().vTranslate[0]);                        
                        activityElement.setAttribute("positionY",activity.get_Shape().vTranslate[1]);
                    }                   
                }
                var structures = phases.getPhases()[i].getActivitiesStructuresArray();
                for(j=0;j<structures.length;j++){
                    activity_structure = structures[j];
                    activityElement = graphicDetails.getElementFromManifest("as-"+activity_structure.getIdentifier()+"-"+activity_structure.getName(),"activity-structure",ldManifest)
                    activityElement.setAttribute("positionX",structures[j].getShape().vTranslate[0]);
                    activityElement.setAttribute("positionY",structures[j].getShape().vTranslate[1]);                    
                }
                var synchPoints = phases.getPhases()[i].getJoinGateways();
                for(j=0;j<synchPoints.length;j++){
                                  
                    synchElement.setAttribute("positionX",synchPoints[j].getShape().vTranslate[0]);
                    synchElement.setAttribute("positionY",synchPoints[j].getShape().vTranslate[1]);
                    gateways.appendChild(synchElement);
                }
                
            }
        }
        catch(e){
            alert("error while creating graphicrepresentation schema "+e);
        }
    },
    getElementFromManifest: function(elementRef,elementType,ldManifest){
        try{
            var elements = ldManifest.getElementsByTagName("imsld:learning-activity");
            alert(elements.length)
            for(var i=0;i<elements.length;i++){
                alert(elements[i].getAttribute("identifier")+" "+elementRef)
                if(elements[i].getAttribute("identifier")==elementRef)
                    return elements[i];
            }
            return null;
        }
        catch(e){
            alert("error while searching element "+e)
        }
    }
*/
}
