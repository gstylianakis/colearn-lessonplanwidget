<%@page  contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="GeneralUsage.SearchService"%>
<%@page import="GeneralUsage.Design"%>
<%
    try{
 
        String author = request.getParameter("author");
        String manifestTitle = request.getParameter("manifestTitle");
        String manifestId = request.getParameter("projectId");
        String lessonSubject = request.getParameter("lessonSubject");

        String query="";
        String whereClause="";


        if(author!=null)
            query = " author like '%"+author+"%'";

        if(manifestTitle!=null){
            if(manifestTitle.compareTo("")!=0){
                if(query.compareTo("")==0)
                    query = " manifestTitle like '%"+manifestTitle+"%'";
                else
                    query += " and manifestTitle like '%"+manifestTitle+"%'";
            }
        }
        if(manifestId!=null){
            if(manifestId.compareTo("")!=0){
                if(query.compareTo("")==0)
                    query = " name like '%"+manifestId+"%'";
                else
                    query += " and name like '%"+manifestId+"%'";
            }
        }
        if(query.compareTo("")!=0)
            whereClause = " where "+query;
        SearchService search = new SearchService();
        ArrayList results = search.getProjects(whereClause);
        ArrayList queryTerms = new ArrayList();

        Hashtable rows = (Hashtable)results.get(1);

        String tablerows = "";
        if(lessonSubject.compareTo("")!=0){
            StringTokenizer st = new StringTokenizer(lessonSubject);
            while (st.hasMoreTokens()) {
                queryTerms.add(st.nextToken());
            }
            HashMap similarity = search.vectorSpaceModel(lessonSubject,queryTerms);

            LinkedHashMap manifestSortBySimilarity = SearchService.sortHashMapByValues(similarity,false);
            Iterator itr = manifestSortBySimilarity.keySet().iterator();
            while(itr.hasNext()){                
                String key = (String) itr.next();                
                if((Double)manifestSortBySimilarity.get(key)!=0)
                    tablerows += rows.get(key);               
            }
        }
        else{
            Enumeration rowsvalues = rows.elements();

            while(rowsvalues.hasMoreElements())
                tablerows += rowsvalues.nextElement();
        }

        out.print("{'size':'"+results.get(0)+"','table':'"+tablerows+"'}");
    }
    catch(Exception e){
        System.out.println("error loading manifests "+e);
    }
%>