/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function feedbackItem(title,typeOfContent,content,id){
    
    this.title = title;
    this.typeOfContent = typeOfContent;
    this.content = content;
    this.id = id;
    this.identifier = uniqid();

    this.getId = function(){
        return this.id;
    }
    
    this.setTitle = function(title){
        this.title = title;
    }
    this.getTitle = function(){
        return this.title;
    }

    this.setTypeOfContent = function(typeOfContent){        
        this.typeOfContent = typeOfContent;
    }
    this.getTypeOfContent = function(){
        return this.typeOfContent;
    }

     this.setContent = function(content){
        this.content = content;
    }
    this.getContent = function(){
        return this.content;
    }
    this.getIdentifier = function(){
        return this.identifier;
    }
}

function createFeedbackItem(title,type,value,id){
    try{    
        return new feedbackItem(title,type,value,id);
    }
    catch(e){
        console.log("createFeedbackItme "+e);
    }
}

function showFeedback(phase,type,category){
    try{
        var feedback;
        
        if(type=="PhaseDescription")
            feedback = phase.getFeedback();
        else if(type=="Description"){
            feedback = phase.getDescriptionItem();            
        }
        else if(type=="activityLearningObjectives"){
            feedback = phase.getLearningObjectivesItems();
        }
        else if(type=="Prerequisites"){
            feedback = phase.getPrerequisitesItems();
        }
        else if(type=="activityCompletion"){           
            feedback = phase.getActivityCompletionFeedbackItem();
        }
        else if(type=="structureInformation")
            feedback = phase.getInformation();
        else if(type=="LearningObject"){          
            if(phase.getEnvironement()!=null){
                if(phase.getEnvironement().length!=0)
                    feedback = phase.getEnvironement().getLearningObjects();
                else
                    return;
            }
            else
                return;
        }
        else if(type=="structureLearningObject"){            
            if(phase.getEnvironement()!=null){
                
                if(phase.getEnvironement().length!=0)
                    feedback = phase.getEnvironement().getLearningObjects();
                else
                    return;
            }
            else
                return;
        }

        var tmp = [];        
        for(var i=0;i<feedback.length;i++){
            tmp.push(feedback[i].getTitle());
            tmp.push(feedback[i].getTypeOfContent());
            tmp.push(feedback[i].getContent());
            tmp.push(feedback[i].getId());
            addItemToItemViewer(tmp,type,category);
            tmp = [];
        }



        //
    }
    catch(e){
        console.log("showFeedback "+e);
    }
}

var completeActivityOption = {
    competionType:null,
    completionValue:null

};