/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralUsage;

/**
 *
 * @author manolis
 */

import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class URLCoding {

    public String encodeURL(String param) {
        String encodedurl = null;
        try {
            encodedurl = URLEncoder.encode(param, "UTF-8");
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }

        return encodedurl;
    }

    public String decodeURL(String param) {
        String decodedurl = null;
        try {
            decodedurl = URLDecoder.decode(param, "UTF-8");
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        }
        return decodedurl;
    }

}
