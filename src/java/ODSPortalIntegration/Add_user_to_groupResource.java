/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ODSPortalIntegration;

import StudentOperationsEJBs.DB_connection;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;

/**
 * REST Web Service
 *
 * @author stylianos
 */
@Path("add_user_to_group")
public class Add_user_to_groupResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Add_user_to_groupResource
     */
    public Add_user_to_groupResource() {
    }

    /**
     * Retrieves representation of an instance of ODSPortalIntegration.Add_user_to_groupResource
     * @return an instance of java.lang.String
     */
    

    /**
     * PUT method for updating or creating an instance of Add_user_to_groupResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @POST
    @Consumes("application/json")
    public void putJson(String user_group) throws Exception{
        DB_connection con = new DB_connection();
        PreparedStatement pstmt = null;        
        try{ 
            System.out.println("here");
            String user_id = "", group_id = "";
            JSONParser parser = new JSONParser();
            ContainerFactory containerFactory = new ContainerFactory(){
                public List creatArrayContainer() {
                  return new LinkedList();
                }
                public Map createObjectContainer() {
                  return new LinkedHashMap();
                }                        
            };                
            Map json = (Map)parser.parse(user_group, containerFactory);
            Iterator iter = json.entrySet().iterator();
            while(iter.hasNext()){
              Map.Entry entry = (Map.Entry)iter.next();     
              System.out.println(entry.getValue().toString());
              if(entry.getKey().toString().equals("group_id")){
                  group_id = entry.getValue().toString();
              } else{
                  user_id = entry.getValue().toString();
              }
            }                        
            con.getConnection().setAutoCommit(false);            
            pstmt = con.getConnection().prepareStatement("INSERT INTO studentsinteam (Group_participation, student) VALUES (?,?)");
            pstmt.setString(1, group_id);
            pstmt.setString(2,user_id);            
            pstmt.executeUpdate();                        
            con.getConnection().commit();
            

        }catch(SQLIntegrityConstraintViolationException e){
            System.out.println("user already exists in group ");
            e.printStackTrace();
            //throw new Exception("user already in group ");            
            //do nothing
        }finally {
            pstmt.close();
            con.getConnection().close();                      
        }
    }
}
