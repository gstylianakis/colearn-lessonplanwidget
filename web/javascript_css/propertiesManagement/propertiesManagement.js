/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var properties = {
    propertiesList: new Array(),
    textId:null,
    propertiesNum:0,
    expressionString:"",
    activityTo: null,
    
    clearProperties: function(){
        properties.propertiesNum = 0;
        var index = 1;
        parseTree.treeHead = new Array();
        $("#conditionalStatement").children().each(function(i){
            if(i>index)
            $(this).remove();
        });
        $("#typingExpressionArea").css("display","none");
        $("#expressionValue").text("");
    },
    property: function(name,type,initValue){
        this.id = uniqid();
        this.isused = 0;
        this.setUsed = function(){
            this.isused++;
        }
        this.setunUsed = function(){
            this.isused--;
        }
        this.getId = function(){
            return this.id;
        }
        this.setId = function(id){
            this.id = id;
        }
        this.name = name;
        this.type = type;
        this.value = initValue;
        
        this.setName = function(name){
            this.name = name;
        }
        this.setType = function(type){
            this.type = type;
        }
        this.setValue = function(value){
            this.value = value;
        }
        this.getName = function(){
            return this.name;
        }
        this.getType = function(){
            return this.type;
        }
        this.getValue = function(){
            return this.value;
        }
        this.getInitValue = function(){
            return initValue;
        }
    },
    Operate: function(){
        try{
            var wrongInput=0;
            var prevPropertyName = $("#propertyPrevValue").val();
            var exist = properties.propertiesList.containsName(prevPropertyName);
            var name = document.getElementById('PropertyName').value;
            var propType = document.getElementById('PropertyType').value;
            var propValue = $('.PropertyValue').val();

            var nameExistance = properties.propertiesList.containsName(name)
            if(nameExistance!=null){
                if(prevPropertyName!=name){//giati an einia idia tote mpainei edw alla milame gia to idio property
                    alert("This name already used for other property.\nPlease type a different name");
                    wrongInput=-1;
                    return false;
                }
            }

            if(exist==null){//den iparxei to property kai to dimiourgw
                var properties_Rows = $('#properties_Rows');
                var new_property = new properties.property(name,propType,propValue);
                properties.createHTML(properties_Rows,name,new_property.getId())
                properties.propertiesList.push(new_property);
                addPropertyToWhenComplete("changeProperties",new_property);
                addPropertyToWhenComplete("changeActivityCompletionProperties",new_property);
            }
            else{//iparxei, opote to kanw update
                if (!confirm("You are going to edit property")==true)
                    return false;

                deletePropertyToWhenComplete("changeProperties",exist);
                deletePropertyToWhenComplete("changeActivityCompletionProperties",exist);

                exist.setName(name);
                exist.setType(propType);
                exist.setValue(propValue);

                var propName = "";
                if(name.length>11){
                    propName = name.substring(0,11);
                    propName +=".."
                }
                else
                    propName = name;
                
                $("#"+exist.getId()).text(propName);
                $("#"+exist.getId()).attr("title",propName);

                addPropertyToWhenComplete("changeProperties",exist);
                addPropertyToWhenComplete("changeActivityCompletionProperties",exist);
            }
            
     }
     catch(e){
            alert("error while creating propery"+ e);
     }
     finally{
         if(wrongInput!=-1){
            $("#PropertiesOpt").colorbox.close();
            
         }
     }
    },
    createHTML: function(properties_Rows,name,id){

            var propName = "";
            if(name.length>11){
                propName = name.substring(0,11);
                propName +=".."
            }
            else
                propName = name;


            var newPropertyRow = $("<li style='margin-top:10px'></li>").appendTo(properties_Rows);
            //newPropertyRow.attr("id",$('#PropertyName').val()+"_PropertyRow");
            var imgDiv = $('<div></div>');
            imgDiv.css({"cssFloat":"right","marginRight":"1px","width":"35px","height":"16px","display":"none"});

            var anchor = $('<a></a>').appendTo(newPropertyRow);
            anchor.attr("href","#");
            anchor.attr("id",id);
            anchor.click(function(){properties.browseProperty($(this).attr("id"))});
            anchor.css({"border":"0px","marginTop":"5px","marginRight":"5px","textDecoration":"none","width":"40px","height":"16px","display":"inline"});
            anchor.text(propName);
            anchor.attr("title",name);

            var deleteImg = $("<img></img>").appendTo(imgDiv);
            deleteImg.attr("src","javascript_css/project_team_form/images/delete.png");
            deleteImg.css({"border":"0px","cssFloat":"right","marginLeft":"3px"});
            deleteImg.click(function(){
                if (!confirm("You are going to delete property")==true)
                    return false;

                var indexdeleteProp = properties.propertiesList.indexOfStruct(id);
                deletePropertyToWhenComplete("changeProperties",properties.propertiesList.containsId(id))
                deletePropertyToWhenComplete("changeActivityCompletionProperties",properties.propertiesList.containsId(id))

                if(indexdeleteProp!=null){
                    properties.propertiesList.splice(indexdeleteProp,1);
                    $("#"+id).parent().remove();
                    $("#available_properties").val(parseInt($("#available_properties").val())-1);
                }
            });
            deleteImg.attr("title","delete property");

            var editImg = $("<img></img>").appendTo(imgDiv)
            editImg.click(function () {properties.browseProperty(id)});
            editImg.attr("src","javascript_css/images/Edit_icon.png");
            editImg.css({"border":"0px","cssFloat":"right"});
            editImg.attr("title","edit property");

            newPropertyRow.append(imgDiv);

            newPropertyRow.mouseover(function(){imgDiv.css("display","block");this.style.cursor='pointer';})
            newPropertyRow.mouseout(function(){imgDiv.css("display","none")});

            $("#available_properties").val(parseInt($("#available_properties").val())+1);
    },
    browseProperty: function(id){
        try{

            var property = properties.propertiesList.containsId(id);
            if(property==null)
                return;
            document.getElementById("PropertyName").value = property.getName();
            document.getElementById("PropertyType").value = property.getType();
            properties.checkType(property.getType());
            $(".PropertyValue").val(property.getValue());
            $("#propertyPrevValue").val( property.getName());
            
            $.fn.colorbox({width:"50%", inline:true, href:"#PropertiesDefinition"});
        }
        catch(e){
            alert("error on reading property "+e)
        }
    },
    checkType: function(value){
        try{
            if(value=="Boolean"){
                var tmp = $("#propertyValue").find(".PropertyValue");
                tmp.removeAttr("class");
                tmp.css('display',"none");
                $("#propertyValue").find("select").addClass("PropertyValue");
                $("#propertyValue").find("select").css("display",'block');
            }
            else if(value=="String"){
                tmp = $("#propertyValue").find(".PropertyValue");
                tmp.removeAttr("class");
                tmp.css('display',"none");
                $("#PropValue").addClass("PropertyValue");
                $("#PropValue").css("display",'block');
                $("#PropValue").unbind();
            }
            else if(value=="Integer"){
                tmp = $("#propertyValue").find(".PropertyValue");
                tmp.removeAttr("class");
                tmp.css('display',"none");
                $("#PropValue").addClass("PropertyValue");
                $("#PropValue").css("display",'block');
                $("#PropValue").val("");
                $("#PropValue").keypress(function(evt){return isNumberKey(evt);});
            }
        }
        catch(e){
            alert(e);
        }

    },
    clear: function(id,index){
        try{
            $("#"+id).children().each(
            function(i){
                if(i>index)
                    $(this).remove();
            })

        }
        catch(e){
            alert(e);
        }
    },
    createPropertyTable: function(){
         try{
             $("#conditionType").val("expression");
             var newItem = document.createElement("div");
             newItem.style.clear = "both";
             var tmp,property,content;

             for(var i=0;i<properties.propertiesList.length;i++){
                property = properties.propertiesList[i];
                for(var j=0;j<3;j++){
                    if(j==0)
                        content = property.getName();
                    else if(j==1)
                        content = property.getType();
                    else
                        content = property.getValue();
                
                    tmp = document.createElement("div");
                    tmp.className = "td";
                    tmp.style.height="12px";
                    tmp.textContent = content;
                    newItem.appendChild(tmp);
                }
             }
             newItem.style.height = "12px";
             newItem.style.width = "400px";
             $("#orConditionDefinition").append(newItem);
         }
     catch(e){
         alert("error while trying to load properties "+e)
     }
    },
    Expression: function(TypeId,StatementId){
        try{
            var option=null;          
            
            /*var curr_node = parseTree.searchKey(StatementId);
             if(curr_node!=null)
                curr_node.setOp($("#"+TypeId).val());
*/
            var tmp_Leftnode=null,tmp_Rightnode=null;

            if($("#"+TypeId).val()=="equal"||$("#"+TypeId).val()=="less than"||$("#"+TypeId).val()=="greater than"||$("#"+TypeId).val()=="not equal"){
//auto itan apanw diladi ekie pou eeinai ta sxolia
var curr_node = parseTree.searchKey(StatementId);
if(curr_node!=null)
    curr_node.setOp($("#"+TypeId).val());

                var selectfirst = null,selectSec=null;
                selectfirst = secondaryFunc.createOperand("ConditionFirstOperand"+TypeId,0,0,StatementId,TypeId);
                $("#"+StatementId).append(selectfirst);
                selectSec = secondaryFunc.createOperand("ConditionSecondOperand"+TypeId,1,0,StatementId,TypeId);
                $("#"+StatementId).append(selectSec);

                $("#typingExpressionArea").css("display","block");                
                if(curr_node==null){
                   curr_node = new treeNode($("#"+TypeId).val(),TypeId);
                   parseTree.treeHead.push(curr_node);
                }
                tmp_Leftnode = new treeNode(selectfirst.value,selectfirst.id);
                tmp_Rightnode = new treeNode(selectSec.value,selectSec.id);

                
                curr_node.setRightNode(tmp_Rightnode);
                curr_node.setLeftNode(tmp_Leftnode);
                

            }

            else if($("#"+TypeId).val()=="sum"||$("#"+TypeId).val()=="multiply"||$("#"+TypeId).val()=="divide"||$("#"+TypeId).val()=="subtract"){
                selectfirst = null,selectSec=null;
                var operator=null;


                selectfirst = secondaryFunc.createOperand("ConditionFirstOperand"+TypeId,0,1,StatementId,TypeId);//secondaryFunc.createFirstOperand("ConditionFirstOperand",1);
                $("#"+StatementId).append(selectfirst);
                selectSec = secondaryFunc.createOperand("ConditionSecondOperand"+TypeId,3,1,StatementId,TypeId);
                $("#"+StatementId).append(selectSec);
                operator = document.createElement("select");
                operator.id = "operator"+TypeId;
                operator.style.marginLeft = "5px";
                for(var i=0;i<4;i++){
                    option = document.createElement("option");
                    if(i==0)
                        option.textContent = "equal";
                    else if(i==1)
                        option.textContent = "not equal";
                    else if(i==2)
                        option.textContent = "less than";
                    else if(i==3)
                        option.textContent = "greater than";

                    operator.appendChild(option);
                }

                $("#"+StatementId).append(operator);
                var thirdOperand = secondaryFunc.createOperand("ConditionThirdOperand"+TypeId,1,1,StatementId,TypeId);
                $("#"+StatementId).append(thirdOperand);

                operator.onchange=function(){                                        
                    tmp_node = parseTree.searchKey(StatementId);//edw itan operator.id to key
                  
                    if(tmp_node!=null)
                        tmp_node.setOp(operator.value);
                    //$("#expressionValue").text("");
                    parseTree.traverseTree();
                }

                /*if(curr_node==null){
                   curr_node = new treeNode($("#"+TypeId).val(),TypeId);
                   parseTree.treeHead.push(curr_node);
                }
                
                tmp_Leftnode = new treeNode(selectfirst.value,selectfirst.id);
                

                var tmp_node = new treeNode(operator.value,operator.id);
                tmp_node.setRightNode(new treeNode(thirdOperand.value,thirdOperand.id));
                tmp_node.setLeftNode(new treeNode(selectSec.value,selectSec.id));

                curr_node.setLeftNode(tmp_Leftnode);
                curr_node.setRightNode(tmp_node);
*/
///auto den ipirxe, all to apanw///
            curr_node = parseTree.searchKey(StatementId);
            if(curr_node!=null)
                curr_node.setOp(operator.value);

                if(curr_node==null){
                   curr_node = new treeNode(operator.value,StatementId);
                   parseTree.treeHead.push(curr_node);
                }
                if(curr_node!=null)
                    curr_node.setOp(operator.value);

                tmp_Rightnode = new treeNode(thirdOperand.value,thirdOperand.id);


                var tmp_node = new treeNode($("#"+TypeId).val(),TypeId);
                tmp_node.setLeftNode(new treeNode(selectfirst.value,selectfirst.id));
                tmp_node.setRightNode(new treeNode(selectSec.value,selectSec.id));

                curr_node.setLeftNode(tmp_node);
                curr_node.setRightNode(tmp_Rightnode);
/////////////////////////
                $("#typingExpressionArea").css("display","block");
                
            }
        }
        catch(e){
            alert("error while defining conditions "+e);
        }
    },
    CombineExpression:function(TypeId,statementId){
        try{
           var index=0,tmp_LeftNode=null,tmp_RightNode=null;

           if(TypeId=="conditionType"){
                properties.propertiesNum = 0;
                index = 1;
                parseTree.treeHead = new Array();

            }
            $("#"+statementId).children().each(function(i){
                if(i>index)
                    $(this).remove();
            });

            if($("#"+TypeId).val()=="and"||$("#"+TypeId).val()=="or"){
                properties.propertiesNum+=1;
                var div = $("<div></div>");
                div.css("clear", "both");
                div.css("marginLeft", parseInt($("#"+statementId).css("marginLeft"))+12);
                div.css("marginTop", "10px");
                div.attr("id","conditionalStatement"+properties.propertiesNum+"_1");

                var select = secondaryFunc.createSelect("conditionType"+properties.propertiesNum+"_1","conditionalStatement"+properties.propertiesNum+"_1",1);
                div.append(select);
                $("#"+statementId).append(div);

                var div2 = $("<div></div>");
                div2.css("clear", "both");
                div2.css("marginLeft", parseInt($("#"+statementId).css("marginLeft"))+12);
                div2.css("marginTop", "10px");
                div2.attr("id","conditionalStatement"+properties.propertiesNum+"_2");

                var select2=null;
                select2 = secondaryFunc.createSelect("conditionType"+properties.propertiesNum+"_2","conditionalStatement"+properties.propertiesNum+"_2",2);
                div2.append(select2);
                $("#"+statementId).append(div2);

                var curr_node = parseTree.searchKey(statementId);
                if(curr_node!=null)
                    if($("#"+TypeId).val()=="and"||$("#"+TypeId).val()=="or")
                        curr_node.setOp(" ");
                     else
                         curr_node.setOp($("#"+TypeId).val());

                if(curr_node==null){
                    //curr_node = new treeNode($("#"+TypeId).val(),TypeId);
                    curr_node = new treeNode(" ",-2);
                    parseTree.treeHead.push(curr_node);
                }                                    
                var tmp_node = new treeNode($("#"+TypeId).val(),TypeId);
                tmp_LeftNode = new treeNode(select.value,div.attr("id"));
                tmp_RightNode = new treeNode(select2.value,div2.attr("id"));
                tmp_node.setLeftNode(tmp_LeftNode);
                tmp_node.setRightNode(tmp_RightNode);

                var tmp_node2 = new treeNode(" ",-2);
                tmp_node2.setRightNode(new treeNode(")",-1));
                tmp_node2.setLeftNode(tmp_node);


                
                curr_node.setLeftNode(new treeNode("(",-1));
                curr_node.setRightNode(tmp_node2);

            }
            else
                properties.Expression(TypeId,statementId);                

            parseTree.traverseTree();
        }
        catch(e){
            alert("error while combining expressions "+e);
        }
    },
    loadProperties: function(doc){
        //var propertys = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","glob-property");
        var propertys = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","loc-property");
        
        var title = "",datatype="",initialValue="",properties_Rows,new_property;
        for(var i=0;i<propertys.length;i++){
            var globProp = propertys[i];
            
            if(globProp.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","initial-value")[0]==null)
                continue;
            title = globProp.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","title")[0].textContent
            datatype = globProp.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","datatype")[0].getAttribute("datatype")
            initialValue = globProp.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","initial-value")[0].textContent

            if(title=="isusedTofinishphaseAct")
                continue;
            properties_Rows = $('#properties_Rows');
            //alert(title,"" +datatype)
            new_property = new properties.property(title,datatype,initialValue);
            properties.createHTML(properties_Rows,title,new_property.getId())
            properties.propertiesList.push(new_property);
            addPropertyToWhenComplete("changeProperties",new_property);
            addPropertyToWhenComplete("changeActivityCompletionProperties",new_property);
            
        }

    },
    removeUnused:function(){
      for(var i=0; i<properties.propertiesList.length;i++){
          if(properties.propertiesList[i].getName()=="isusedTofinishphaseAct")
              properties.propertiesList.splice(i,1);
      }
     
    }
    /*loadOrConditionProperties: function(doc){
        var less_than = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","less-than");
        var is = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","is");
        var is_not = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","is-not");
        var greater_than = doc.getElementsByTagNameNS("http://www.imsglobal.org/xsd/imsld_v1p0","greater-than");
    }*/

}

var secondaryFunc = {
    createOperand: function(id,From,Type,StatementId,expId){
        var selectSec = document.createElement("select");
        selectSec.id = id;
        selectSec.style.marginLeft="5px";
        var option = null,tmp_node=null;

        for(var i=0;i<properties.propertiesList.length;i++){
            property = properties.propertiesList[i];
            option = document.createElement("option");
            option.textContent = property.getName();
            selectSec.appendChild(option);
         }
         option = document.createElement("option");
         option.textContent = "Type custom value";
         selectSec.appendChild(option);
         selectSec.onchange= function(){
                    if($(this).val()=="Type custom value"){
                        $("#"+id).remove();
                        var input = $("<input></input>");
                        input.attr("id",id);
                        input.attr("type","text");
                        input.css({marginLeft:"5px",width:"100px"});

                        if(From==0)
                           input.insertBefore($("#ConditionSecondOperand"+expId));
                        else if(From==1)
                            $("#"+StatementId).append(input);
                        else
                            input.insertBefore($("#operator"+expId));

                        input.keyup(function(){
                            tmp_node = parseTree.searchKey(id);
                            if(tmp_node!=null)
                                tmp_node.setOp($(this).val());
                            
                            parseTree.traverseTree();
                            
                        });
                    }
                    else{
                        tmp_node = parseTree.searchKey(id);
                        if(tmp_node!=null)
                            tmp_node.setOp($(this).val());
                       
                        parseTree.traverseTree();
                       }
                };
         return selectSec;
    },
    createSelect:function(id,statementId){
        var tmp_node = null;

        var select = $("<select></select>");
        select.attr("id",id);
        select.change(function(){            
            tmp_node = parseTree.searchKey(id);            
            if(tmp_node!=null)
                tmp_node.setOp($(this).val());        
            properties.CombineExpression(id,statementId);
                });
        select.append($("<option></option>").text("expression"));
        select.append($("<option></option>").text("equal"));
        select.append($("<option></option>").text("not equal"));
        select.append($("<option></option>").text("less than"));
        select.append($("<option></option>").text("greater than"));
        select.append($("<option></option>").text("sum"));
        select.append($("<option></option>").text("subtract"));
        select.append($("<option></option>").text("divide"));
        select.append($("<option></option>").text("multiply"));
        select.append($("<option></option>").text("and"));
        select.append($("<option></option>").text("or"));
        
        return select;
    }
}
var parseTree = {
    treeHead:new Array(),
   
    inorderTree:function(node){
        try{
            if(node==null){                
                return;
            }

            if(node.getLeftNode() != null)
                parseTree.inorderTree(node.getLeftNode());            
                $("#expressionValue").text($("#expressionValue").text()+" "+node.getOp());
            if(node.getRightNode()!=null)
                parseTree.inorderTree(node.getRightNode());

        }
        catch(e){
            console.log(e)
        }
    },
    traverseTree:function(){
        $("#expressionValue").text("");
        parseTree.inorderTree(parseTree.treeHead[0]);

    },
    
    searchKey:function(id){
        try{
            var node = null;
            var openList = new Array();
            if(parseTree.treeHead.length!=0)
                openList.push(parseTree.treeHead[0]);
            while(openList.length>0){
                node = openList[0];                
                if(node.getId()==id)
                    return node;
                if(node.getLeftNode()!=null)
                    openList.push(node.getLeftNode());
                if(node.getRightNode()!=null)
                    openList.push(node.getRightNode());

                openList.splice(0,1);
            }
            return null;
        }
        catch(e){
            
        }
    }
}

function treeNode(op,id){
        this.op = op;
        this.id = id;
        this.leftNode = null;
        this.rightNode = null;

        this.getLeftNode = function(){
            return this.leftNode;
        }
        this.getOp = function(){
            return this.op;
        }
        this.getRightNode = function(){
            return this.rightNode;
        }
        this.setLeftNode = function(leftNode){
            this.leftNode = leftNode;
        }

        this.setRightNode = function(rightNode){
            this.rightNode = rightNode;
        }
        this.setOp = function(op){
            this.op = op;
        }
        this.getId = function(){
            return this.id;
        }
    }

    function addCondition(content){
        try{
            
            var svgDoc = document.getElementById('svgPanel').contentDocument;                        
            var text = svgDoc.getElementById(properties.textId);            
            //text.childNodes[0].textContent = content;
            var old_textPath = text.childNodes[0];

            if(text!=null){
                
                if(navigator.userAgent.indexOf("Chrome")!=-1){
                    var textPath = document.createElementNS(svgns, 'textPath');
                    var xlinkns = "http://www.w3.org/1999/xlink";
                    textPath.setAttributeNS(xlinkns, "href",text.childNodes[0].getAttribute("href"));
                    if(content=="")
                        content = "click to set condition"
                    textPath.textContent = content.substring(0,7)+"..";
                    text.removeChild(text.getElementsByTagName("textPath")[0])
                    var title = document.createElementNS(svgns, 'title');
                    title.textContent=content;
                    textPath.appendChild(title);
                    text.appendChild(textPath)
                }
                else{
                    old_textPath.textContent = content.substring(0,7)+"..";
                    title = document.createElementNS(svgns, 'title');
                    title.textContent=content;
                    old_textPath.appendChild(title);                   
                }
               
            }
            //LDConditionFromOrGateway.xmlLikeNode="<manifest xmlns:imsld='http://www.imsglobal.org/xsd/imsld_v1p0'>";          
            LDConditionFromOrGateway.xmlLikeNode = createXMLDocument('expression');        
            LDConditionFromOrGateway.xmlRoot = LDConditionFromOrGateway.xmlLikeNode.getElementsByTagName("expression")[0];
            LDConditionFromOrGateway.inclusiveOrtoXml(parseTree.treeHead[0],LDConditionFromOrGateway.xmlRoot);

            properties.activityTo.sethtmlExpressionView($("#conditionalStatement").eq(0));
            //LDConditionFromOrGateway.xmlLikeNode +="</manifest>"
         
            properties.activityTo.setPropertyExpression(LDConditionFromOrGateway.xmlRoot);
       
            $.fn.colorbox.close();
        }
        catch(e){
            alert(e)
        }
    }
    function updateCondition(text_id,activityTo){
        try{
            properties.activityTo = activityTo;
            /*if(content!="click to add condition"){
                $("#prevExpressionP").css("display","block");
                $("#prevExpressionValue").val(content);
            }*/


            properties.clear("orConditionDefinition",1);
            properties.createPropertyTable();
            properties.clearProperties();
            properties.textId = text_id;

            var svgDoc = document.getElementById('svgPanel').contentDocument;
            var text = svgDoc.getElementById(text_id);
            var textPath = text.getElementsByTagName("textPath")[0];
            $("#prevExpressionValue").html(textPath.getElementsByTagName("title")[0].textContent);
            
//console.log(properties.activityTo.gethtmlExpressionView())
            if(properties.activityTo.gethtmlExpressionView()!=null){
                $("#conditionalStatement").eq(0).replaceWith(properties.activityTo.gethtmlExpressionView().clone(true))
               
                $("#conditionType").val(properties.activityTo.gethtmlExpressionView().find("#conditionType").val())
//console.log($("#conditionalStatement"))
            }
            
            $.fn.colorbox({width:"70%",height:"70%", inline:true, href:"#orCondView"});
         }
         catch(e){
            alert("error while defining branching condtion "+e);
         }
    }

var LDConditionFromOrGateway = {
    xmlLikeNode: "",
    xmlRoot:"",
    inclusiveOrtoXml: function(node,xmlNode){
        try{
            if(node==null)
                return;

            if(node.getOp()=="and" || node.getOp()=="or" || node.getOp()=="equal" ||node.getOp()=="not equal" ||node.getOp()=="less than" ||node.getOp()=="greater than" || node.getOp()=="sum"||node.getOp()=="multiply"||node.getOp()=="divide"||node.getOp()=="subtract"){
                var str = node.getOp().replace(" ","-");
                if(node.getOp()=="equal")
                    str = str.replace("equal","is");                    
                
                else if(node.getOp()=="not equal")
                    str = str.replace("not-equal","is-not");
                

                var xmlnewNode = LDConditionFromOrGateway.xmlLikeNode.createElement("imsld:"+str);
                xmlNode.appendChild(xmlnewNode);
            }
            else{
                if(node.getOp()!="(" && node.getOp()!=")" && node.getOp()!=" "){
                     var tmpNode = LDConditionFromOrGateway.xmlLikeNode.createElement("imsld:property-value");

                     var tmpProp = properties.propertiesList.containsName(node.getOp())
                     if(tmpProp!=null){
                        var propNode = LDConditionFromOrGateway.xmlLikeNode.createElement("imsld:property-ref");
                        propNode.setAttribute("ref","globprop-"+tmpProp.getId())
                        tmpNode.appendChild(propNode);
                     }
                     else
                         tmpNode.textContent = node.getOp();
                     xmlNode.appendChild(tmpNode);                     
                }
                xmlnewNode = xmlNode;
            }

            if(node.getLeftNode() != null)
                LDConditionFromOrGateway.inclusiveOrtoXml(node.getLeftNode(),xmlnewNode);

            if(node.getRightNode()!=null)
                LDConditionFromOrGateway.inclusiveOrtoXml(node.getRightNode(),xmlnewNode);
            
        }
        catch(e){
            console.log("error while creating conditions "+e)
        }
    }
    /*inclusiveOrtoXml: function(node,closingTag){
        try{
           
            if(node.getOp()=="and" || node.getOp()=="or" || node.getOp()=="equal" ||node.getOp()=="not equal" ||node.getOp()=="less than" ||node.getOp()=="greater than" || node.getOp()=="sum"||node.getOp()=="multiply"||node.getOp()=="divide"||node.getOp()=="subtract"){
                var str = node.getOp().replace(" ","-")
                LDConditionFromOrGateway.xmlLikeNode += "<imsld:"+str+">";
                closingTag = str;
            }            
            else{
                if(node.getOp()!="(" && node.getOp()!=")" && node.getOp()!=" ")                   
                    LDConditionFromOrGateway.xmlLikeNode +="<imsld:property-value>"+node.getOp()+"</imsld:property-value>";
                    
            }
            
            if(node.getLeftNode() != null)
                LDConditionFromOrGateway.inclusiveOrtoXml(node.getLeftNode(),closingTag);
            
            if(node.getRightNode()!=null)
                LDConditionFromOrGateway.inclusiveOrtoXml(node.getRightNode(),closingTag);
            
            if(node.getRightNode()!=null && node.getLeftNode()!=null && node.getOp()!=" ")
                LDConditionFromOrGateway.xmlLikeNode +="</imsld:"+closingTag+">";
        }
        catch(e){
            alert("error while creating conditions "+e)
        }
    }*/
}

function deletePropertyToWhenComplete(option,property){
    if(property==null)
        return;
    
    $("#"+option).find("option").each(function(){
            
            if(this.textContent==property.getName()){
                if(option=="changeProperties")
                    console.log(this)
                this.parentNode.removeChild(this)
            }
            
                

            //}
        });
}
    function addPropertiesToWhenComplete(new_row){
        var property=null;
        new_row.find("select").each(function(index){
            for(var i=0;i<properties.propertiesList.length;i++){
                property = properties.propertiesList[i];               
                $(this).append($("<option value='"+property.getName()+"'></option>").text(property.getName()));
            }
        });
    }
function addPropertyToWhenComplete(option,property){
    try{
        if(property==null)
            return;
        $("#"+option).find("select").each(function(){
         
             $(this).append($("<option value='"+property.getName()+"'></option>").text(property.getName()));
            
        });
        
    }
    catch(e){
        alert(e)
    }
}
