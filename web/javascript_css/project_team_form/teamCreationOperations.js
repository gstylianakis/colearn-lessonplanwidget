/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//object pou krataei tis omades....oi omades swzontai se array

  function Teams(GroupArray){
        try{
            
            this.teams = GroupArray;
            this.addTeam = function(team){
                this.teams.push(team);
            }
            this.getTeams = function(){
                return this.teams;
            }
        }
        catch(e){
            alert(e);
        }
    }

    //object pou einai mia omada..Oi mathites kathe omadas kratountai se array auths ths domis
    function Group(StudentGroup,StudentGroupName){
        this.StudentGroupArray = StudentGroup;
        this.name = StudentGroupName;
        this.id = uniqid();
        this.getId = function(){
            return this.id;
        }
        this.initStudentArray = function(){
            this.StudentGroupArray = [];
        }
        this.addStudent = function(student){
            this.StudentGroupArray.push(student);
        }
        this.getStudent = function(){//id
            return this.StudentGroupArray;
        }
        this.getName = function(){
            return this.name;
        }
        this.setName = function(name){
            this.name = name;
        }
    }

    var groups = new Array();
    function initializeGroups(){
           try{
               groups = new Teams(new Array());
           }
           catch(e){
               alert(e);
           }
           
       }

    function editGroupHtml(team){
       var teamTree = $("#"+team.getId()+"_group");       
       teamTree.find("ul").each(function(){
           $(this).remove();
       })
       var children = $("<ul></ul>").appendTo(teamTree);
       var childrenLi = null,childrenSpan=null,child=null,spanName="";
       for(var i=0;i<team.getStudent().length;i++){
            child = team.getStudent()[i];
            childrenLi = $("<li></li>")
            childrenSpan = $("<span class='file'></span>");
            spanName = "";
            if(child[1].length>13){
                spanName = child[0].substring(0,13);
                spanName +=".."
            }
            else
                spanName = child[0];
            childrenSpan.text(spanName);
            childrenSpan.attr("title",child[0]);
            childrenLi.append(childrenSpan);
            childrenLi.appendTo(children);
            $("#project_team_list").treeview({
                add: childrenLi
            });
      }
   }
        function editTeam(team){
            try{
                var student = new Array();
                var count = 2;
                team.initStudentArray();
                for(var i=0;i<document.getElementById('StudentsNum').value-1;i++){
                    if(count%3==0){
                  //vazw to stud_role
                    student.push(document.TeamForm.elements[count].value)
                    team.addStudent(student);
                    student = new Array();
                    count = count+2;
                  }
                  //vazw to onoma
                  student.push(document.TeamForm.elements[count].value);
                  count++;
               }//to teleutaio den exei allaksei id
               if(document.getElementById('StudentsNum').value>1){
                   var stud_role = 'student_role'+(document.getElementById('StudentsNum').value-1);
                   document.TeamForm.elements[count].setAttribute('id',stud_role);
                   student.push(document.TeamForm.elements[count].value);
                   team.addStudent(student);
               }
           }
           catch(e){
               alert("error while editing team "+e)
           }
       }
//dimiourgia mias omadas...
    function createTeam(teamName){
        try{
            var student = new Array();
            var count = 2;
            var newTeam = new Group(new Array(),teamName);
            for(var i=0;i<document.getElementById('StudentsNum').value-1;i++){
               if(count%3==0){

                  //vazw to stud_role
                   student.push(document.TeamForm.elements[count].value)
                   newTeam.addStudent(student);
                   student = new Array();
                   count = count+2;
                  }
                  //vazw to onoma
                  student.push(document.TeamForm.elements[count].value);
                  count++;


               }//to teleutaio den exei allaksei id
               if(document.getElementById('StudentsNum').value>1){
                   var stud_role = 'student_role'+(document.getElementById('StudentsNum').value-1);
                   document.TeamForm.elements[count].setAttribute('id',stud_role);
                   student.push(document.TeamForm.elements[count].value);

                   newTeam.addStudent(student);
               }
               groups.addTeam(newTeam);

               return newTeam;
        }
        catch(e){
            alert(e);
        }
    }
    function createHtmlView(teamName,new_team){
               var newTeam = $('<li></li>');
               newTeam.attr("id",new_team.getId()+"_group");
               var span = $("<span class='folder'></span>");
               span.css({"width":"135px"});

               span.attr("id",new_team.getId());
                var spanName = "";
                if(teamName.length>10){
                    spanName = teamName.substring(0,10);
                    spanName +=".."
                }
                else
                    spanName = teamName;

                span.text(spanName);
                span.attr("title",teamName);
                newTeam.append(span);
                newTeam.appendTo($("#project_team_list"));
                $("#project_team_list").treeview({
                    add: newTeam
                });
               var imgDiv = $("<div></div>");
               imgDiv.css({"float":"right","marginRight":"1px","width":"35px","height":"16px","display":"none"});
               span.append(imgDiv);

               var editImg=$("<img></img>");
               editImg.attr("src","javascript_css/images/Edit_icon.png");
               editImg.css({"border":"0px","float":"left"});
               editImg.attr("align","top");
               editImg.attr("title","edit team properties");
               editImg.mouseover(function(){this.style.cursor='pointer'});
               editImg.click(function () {ShowTeam(new_team.getId())});
               imgDiv.append(editImg);


               var deleteImg=$("<img></img>");
               deleteImg.attr("src","javascript_css/project_team_form/images/delete.png");
               deleteImg.css({"border":"0px","float":"left","marginLeft":"3px"});
               deleteImg.attr("align","top");
               deleteImg.attr("title","delete team");
               deleteImg.mouseover(function(){this.style.cursor='pointer'});
               deleteImg.click(function(){deleteTeam(new_team.getId())});
               imgDiv.append(deleteImg);
               newTeam.mouseover(function(){imgDiv.css("display","block");this.style.cursor='pointer';})
               newTeam.mouseout(function(){imgDiv.css("display","none");})

               var children = $("<ul></ul>").appendTo(newTeam);
               //children.attr("id",new_team.getId()+"members");
               
               var childrenLi = null,childrenSpan=null,child=null;
               for(var i=0;i<new_team.getStudent().length;i++){
                   child = new_team.getStudent()[i];
                   childrenLi = $("<li></li>")
                   childrenSpan = $("<span class='file'></span>");
                   spanName = "";
                   if(child[1].length>13){
                        spanName = child[0].substring(0,13);
                        spanName +=".."
                    }
                    else
                        spanName = child[0];
                    childrenSpan.text(spanName);
                    childrenSpan.attr("title",child[0]);
                    childrenLi.append(childrenSpan);
                    childrenLi.appendTo(children);
                    $("#project_team_list").treeview({
                        add: childrenLi
                    });
               }
    }
       //apothikeusi omadas stin domi twn omadwn...
       function add_newTeam(form, org){
           try{
               
               var wrongInput=0,existing=null;

               var students = parseInt(document.getElementById('StudentsNum').value)-1;
               var validation = true;
               if(org!="ODS")
                    validation = validate(form,'projet_team_inputForm',students);
               else{
                   document.getElementById('exists').value='true';
                   wrongInput = -1;
               }
               var teamName = $('#team_name').val();               
               existing = groups.getTeams().containsName(teamName);

               /*var newTeam = document.createElement("li");
               newTeam.style.display = 'inline';
               var anchorDiv = document.createElement('div');               
               anchorDiv.className = "informationPanelInnerAnchors";               
               newTeam.appendChild(anchorDiv);

               var anchor = document.createElement('a');
                anchor.style.border = "0px";
                anchor.href = "#";                
                anchor.style.textDecoration = "none";
                anchor.style.height = "16px";
                anchor.style.width = "110px";
                //anchor.style.backgroundColor = "#000"
                anchor.style.overflow="hidden";
                if(teamName.length>13){
                    anchor.innerHTML = teamName.substring(0,13);
                    anchor.innerHTML +=".."
                }
                else
                    anchor.innerHTML = teamName;
                anchor.title = teamName;

                anchorDiv.appendChild(anchor);

               var imgDiv = document.createElement("div");
               imgDiv.style.cssFloat = "right";
               imgDiv.style.marginRight = "1px";
               imgDiv.style.width = "35px";
               imgDiv.style.height = "16px";
               newTeam.appendChild(imgDiv);

               var editImg=document.createElement("IMG");
               
               editImg.setAttribute("src","javascript_css/project_team_form/images/edit.gif");
               editImg.style.border="0px";
               editImg.style.cssFloat="left";
               editImg.align = "top";
               editImg.onmouseover = function(){this.style.cursor='pointer'};
               imgDiv.appendChild(editImg);

               var deleteImg = document.createElement("IMG");
               deleteImg.setAttribute("src","javascript_css/project_team_form/images/delete.png");
               deleteImg.style.border="0px";
               deleteImg.style.cssFloat="left";
               deleteImg.style.marginLeft = "3px";
               deleteImg.onmouseover = function(){this.style.cursor='pointer'};
               deleteImg.align = "top";
               deleteImg.onclick = function(){};
               imgDiv.appendChild(deleteImg);*/
               if(teamName==''){
                    alert("Please define a name");
                    wrongInput = -1;
                    return false;
               }
               if(validation){
                   if(document.getElementById('exists').value=='false'&& students>0){
                       alert("there are students that not exist");
                       wrongInput = -1;
                       return false;
                   }

                   else{
                        var prevName = $("#team_prevName").val();
                        
                        var tmpTeam = groups.getTeams().containsName(prevName);                      
                        if(tmpTeam!=null){
                            if (!confirm("You are going to change group")==true)
                                return false;

                            if(prevName!=teamName){
                                if(existing!=null){
                                    alert("This name already used for other team.\nPlease give different name");
                                    wrongInput=-1;
                                    return false;
                                }
                                var replacingName = "";
                                if(teamName.length>10){
                                    replacingName = teamName.substring(0,10);
                                    replacingName +=".."
                                }
                                else
                                    replacingName = teamName;

                                var tmpImgNode = $("#"+tmpTeam.getId()).find("div").clone(true)
                                $("#"+tmpTeam.getId()).text(replacingName);
                                $("#"+tmpTeam.getId()).append(tmpImgNode);
                                $("#"+tmpTeam.getId()+"_group").mouseover(function(){tmpImgNode.css("display","block");this.style.cursor='pointer';})
                                $("#"+tmpTeam.getId()+"_group").mouseout(function(){tmpImgNode.css("display","none");})

                                $("#"+tmpTeam.getId()).attr("title",teamName);
                                
                                tmpTeam.setName(teamName);

                                
                            }
                            editTeam(tmpTeam);
                            editGroupHtml(tmpTeam);
                            return false;
                        }
                       
                            if(existing!=null){
                                alert("This name already used for other team.\nPlease give different name");
                                wrongInput=-1;
                                return false;
                            }
                             //dimiourgw tin lista me ta groups...
                            var new_team = createTeam(teamName);
                            document.getElementById('available_teamsNum').value = parseInt(document.getElementById('available_teamsNum').value)+1
                            createHtmlView(teamName,new_team)
                            /*document.getElementById('project_team_list').appendChild(newTeam);
                       
                       //dimiourgw tin lista me ta groups...
                       var new_team = createTeam(teamName);
                       editImg.onclick = function () {ShowTeam(new_team.getId())};
                       deleteImg.onclick = function () {deleteTeam(new_team.getId())};
                       anchor.onclick = function () {ShowTeam(new_team.getId())};
                       anchor.id = new_team.getId();//$("#"+teamName).colorbox({width:"50%", inline:true, href:"#browsingTeams"});
                       newTeam.id = new_team.getId()+"_group";*/
                   }
               }
               
           }
           catch(e){
               alert(e);               
           }
           finally{
               if(wrongInput!=-1){
                    $(".project_team_form").colorbox.close();
                    return false;
               }
           }
           
       }
    
    //pernoume tin omada me id... apo tin domi twn omadwn...
       function getTeamIndex(id){
           try{
               
               var teams = groups.getTeams();
               
               for(var i=0; i<teams.length;i++ ){
                 if(teams[i].getId() == id)
                     return i;
               }
               return -1;
               //alert(teams[0].getStudent());
           }
           catch(e){
               alert(e);
           }
       }

       function deleteTeam(id){
           try{

                if (!confirm("You are going to delete the selected group ")==true)
                    return;
               var toDelete  = getTeamIndex(id);
               //alert(toDelete)
       
               if(toDelete!=-1)
                    groups.getTeams().splice(toDelete,1);
       
               var del = document.getElementById(id+"_group");
               del.parentNode.removeChild(del);
               
               document.getElementById('available_teamsNum').value = parseInt(document.getElementById('available_teamsNum').value)-1

           }
           catch(e){
               alert(e)
           }
       }
       function changeRoleNamesInGroups(prevRoleName,newRoleName){
           var teams = groups.getTeams();
           var students=null;
           for(var i=0;i<teams.length;i++){
               students = teams[i].getStudent();
               for(var j=0;j<students.length;j++)
                   if(students[j][1]==prevRoleName)
                       students[j][1] = newRoleName;
           }
       }
       /*function getTeam(name){
           try{
               var teams = groups.getTeams();
               var teamPos = getTeamPosition(name);
               var studentList = new Array();
               if(teamPos!=-1){

                   var group = teams[teamPos];                                      
                   var students = group.getStudent();
                   var teamName = document.createElement("div");
                   teamName.align = 'left';
                   
                   var teamNameHtml = "<label for=\"name\">Team Name:</label>"+
                                  "<input type=\"text\" size=\"13px\" id=\"browsing_teamNameInput\" value=\""+group.name+"\" disabled=\"true\"/>";
                   teamName.innerHTML = teamNameHtml;
                   studentList.push(teamName);

                   for(var i=0;i<students.length;i++){
                        var studentNameDiv = document.createElement("li");
                        var studentRoleDiv = document.createElement("li");

                        //studentNameDiv.setAttribute("class","rowElem");
                        //studentRoleDiv.setAttribute("class","rowElem");
                        
                        var innerNameHTML = "<label for=\"name\">Student Name:</label>"+
                           "<input type=\"text\" size=\"13px\" value=\""+students[i][0]+"\" disabled=\"true\"/>";

                       var innerRoleHTML = "<label for=\"name\">Student Role:</label>"+
                           "<input type=\"text\" size=\"13px\" value=\""+students[i][1]+"\" disabled=\"true\"/>";
                           
                        studentNameDiv.innerHTML = innerNameHTML;
                        studentRoleDiv.innerHTML = innerRoleHTML;

                       studentList.push(studentNameDiv);
                       studentList.push(studentRoleDiv);
                   }
               }
                
               return studentList;
           }
           catch(e){
               alert(e);
           }
       }*/


        function ShowTeam(teamId){
            try{
                 
                var team = groups.getTeams().containsId(teamId);
                if(team==null)
                    throw "error while presenting teams";

                var student_num = team.getStudent().length +1;
                $('StudentsNum').val(student_num);
                clearTeamsForm();
                $('#team_name').val(team.getName());
                $('#team_prevName').val(team.getName());
                //var project_teamForm = document.getElementById('projectTeam').parentNode;

                for(var i=0;i<student_num-1;i++){
                    add_newStudentRowInForm(1,team.getStudent()[i][0],team.getStudent()[i][1],'');//name kai rolos tou student
                }
               
               $.fn.colorbox({width:"50%", inline:true, href:"#projet_team_inputForm"});
                  
             }
              
          
           catch(e){
               alert(e);
           }
           return false;
       }
       function add_newStudentRowInForm(option,studentName,studentRole,org){
           try{      
               var project_teamForm = document.getElementById('projectTeam');
               var num = parseInt(document.getElementById('StudentsNum').value);
               var new_studentEntry = document.createElement('div');
               new_studentEntry.style.cssFloat = "left";
               new_studentEntry.style.clear = "both";
               var divIdName = 'student_'+num+'Div';
               new_studentEntry.setAttribute('id',divIdName);
               new_studentEntry.setAttribute('class', 'rowStud');
              if(org!="ODS"){
                if(option==0)
                    new_studentEntry.innerHTML = "<label>Student Name:&nbsp;&nbsp; </label><input id=\"student_"+num+" type=\"text\" size=\"15px\" onchange=\"if(this.value != '') checkExists(this,'projet_team_inputForm',"+num+");\" name=\"student_"+num+"\"/><div style=\"float:right;height:16px; width:16px; \" ><img id=\"loading_"+num+"\" style=\"display:none\" src=\"javascript_css/validate_forms/images/ajax-loader.gif\"/></div>"+
                                "&nbsp;&nbsp;<label>Role:&nbsp;&nbsp;&nbsp;&nbsp; </label><select size=0 id=\"student_role"+num+"\" name=\"student_role"+num+"\">"+getDefinedRoles('')+"</select> <input align=\"left\" type=\"button\" class=\"delete_button\" onclick=\"remove_student("+num+")\"/>";
                else
                      new_studentEntry.innerHTML = "<label>Student Name:&nbsp;&nbsp; </label><input id=\"student_"+num+" type=\"text\" size=\"15px\" onchange=\"if(this.value != '') checkExists(this,'projet_team_inputForm',"+num+");\" value=\""+studentName+"\" name=\"student_"+num+"\"/><div style=\"float:right;height:16px; width:16px; \" ><img id=\"loading_"+num+"\" style=\"display:none\" src=\"javascript_css/validate_forms/images/ajax-loader.gif\"/></div>"+
                                "&nbsp;&nbsp;<label>Role:&nbsp;&nbsp;&nbsp;&nbsp; </label><select size=0 id=\"student_role"+num+"\" name=\"student_role"+num+"\">"+getDefinedRoles(studentRole)+"</select> <input align=\"left\" type=\"button\" class=\"delete_button\" onclick=\"remove_student("+num+")\"/>";
               }
               else{
                  if(option==0)
                    new_studentEntry.innerHTML = "<label>Student Name:&nbsp;&nbsp; </label><input id=\"student_"+num+" type=\"text\" size=\"15px\"  name=\"student_"+num+"\"/>"+
                                "&nbsp;&nbsp;<label>Role:&nbsp;&nbsp;&nbsp;&nbsp; </label><select size=0 id=\"student_role"+num+"\" name=\"student_role"+num+"\">"+getDefinedRoles('')+"</select> <input align=\"left\" type=\"button\" class=\"delete_button\" onclick=\"remove_student("+num+")\"/>";
                   else
                      new_studentEntry.innerHTML = "<label>Student Name:&nbsp;&nbsp; </label><input id=\"student_"+num+" type=\"text\" size=\"15px\" value=\""+studentName+"\" name=\"student_"+num+"\"/>"+
                                "&nbsp;&nbsp;<label>Role:&nbsp;&nbsp;&nbsp;&nbsp; </label><select size=0 id=\"student_role"+num+"\" name=\"student_role"+num+"\">"+getDefinedRoles(studentRole)+"</select> <input align=\"left\" type=\"button\" class=\"delete_button\" onclick=\"remove_student("+num+")\"/>"; 
                }
                
               project_teamForm.parentNode.appendChild(new_studentEntry);
               document.getElementById('StudentsNum').value = num + 1;

           }
           catch(e){
               console.log("error while trying to add new student to group");
               alert(e)
           }
       }

        function remove_student(student_DivNum){

           try{
               student_DivNum = "student_"+student_DivNum+"Div";
               var project_teamForm = document.getElementById('projectTeam').parentNode;
               var student = document.getElementById(student_DivNum);
               project_teamForm.removeChild(student);
               document.getElementById('StudentsNum').value -= 1;

               var count = 2;
               for(var i=0;i<document.getElementById('StudentsNum').value-1;i++){
                  if(count%3==0){
                      var stud_role = 'student_role'+(i);                      
                      document.TeamForm.elements[count].setAttribute('id',stud_role);                      
                      count = count+2;
                  }
                  var studID = 'student_'+(i+1);
                  document.TeamForm.elements[count].setAttribute('id',studID);
                  count++;

               }//to teleutaio den exei allaksei id
               if(document.getElementById('StudentsNum').value>1){
                   stud_role = 'student_role'+(document.getElementById('StudentsNum').value-1);
                   document.TeamForm.elements[count].setAttribute('id',stud_role);
               }
console.log(project_teamForm)
           }
           catch(e){
               alert(e)
           }

       }
    function clearTeamsForm(){
            try{
                                

                document.getElementById('team_name').value = '';
                //var student_num = document.getElementById('StudentsNum').value;
                var project_teamForm = document.getElementById('projectTeam').parentNode;

               //for(var i=1;i<student_num;i++){
                for(var i=0;i<project_teamForm.childNodes.length;i++){
                    if(document.getElementById("student_"+i+"Div"))
                        project_teamForm.removeChild(document.getElementById("student_"+i+"Div"));
               }
               document.getElementById('StudentsNum').value = 1;

               //kanw clear tous students
               //GroupArray = new Array();

           }
           catch(e){
               alert(e);
           }

       }

       function generateTeamsDoc(){
           try{
                var xmldoc = createXMLDocument("teamGroups");
                var manifest = xmldoc.getElementsByTagName("teamGroups")[0];

                var teams = groups.getTeams();
                if(teams.length==0)
                    throw "Deployment failed..\nPlease create group of students that participate in the learning design";

                for(var i=0;i<teams.length;i++){
                    var team = xmldoc.createElement("group");
                    team.setAttribute("name", teams[i].getName());
                    manifest.appendChild(team);
                    var studs = teams[i].getStudent();
                    for(var j=0;j<studs.length;j++){
                        var stud = xmldoc.createElement("student");
                        stud.setAttribute("name", studs[j][0]);
                        stud.setAttribute("role", studs[j][1]);                        
                        team.appendChild(stud);
                    }

               }
               return ((new XMLSerializer()).serializeToString(xmldoc));
           }
           catch(e){
               
               throw(e);
           }
       }

       function getDefinedRoles(initialValue){
        try{
            //var definedRoles = "<option value=\"learner\">learner</option>";
            var definedRoles = "";
            var role;
            var defineRole = "";
            for(var i=0;i<learnerArray.getRoles().length;i++){
                role = learnerArray.getRoles()[i].getName();
                if(initialValue==role)
                    defineRole = "<option selected value=\""+role+"\">"+role+"</option>"
                else
                    defineRole = "<option value=\""+role+"\">"+role+"</option>";
                
                definedRoles += defineRole;
            }
            /*if(initialValue=="staff")
                definedRoles+="<option selected value=\"staff\">staff</option>";
            else
                definedRoles+="<option value=\"staff\">staff</option>";
            */
            for(i=0;i<staffArray.getRoles().length;i++){
                role = staffArray.getRoles()[i].getName();
                if(initialValue==role)
                    defineRole = "<option selected value=\""+role+"\">"+role+"</option>"
                else
                    defineRole = "<option value=\""+role+"\">"+role+"</option>";

                definedRoles += defineRole;
            }
            
            return definedRoles;
        }
        catch(e){
            alert(e)
        }
    }